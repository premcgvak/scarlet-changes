$j = jQuery;
if (!window.hasOwnProperty('IWD')) {
	IWD = {};
};

IWD.ColorSwatch = {
		
		skipInit: false,
		
		applyUploadImage: function(response){
			var id = response.id;
			
			var image = $j('#'+id + '_image');
			image.attr('src', response.url);
			
			var input = $j('#'+id + '_value');
			input.val(response.file);
		},
		
		initOpenCloseBlock: function(){
			if (IWD.ColorSwatch.skipInit==true){
				return;
			}
			$j(document).on('click','a.open', function(event){
				event.preventDefault();
				$j(this).removeClass('open').addClass('close');
				IWD.ColorSwatch.setDisplayOptionsGlobal($j(this));
			});
			
			$j(document).on('click','a.close', function(event){
				event.preventDefault();
				$j(this).removeClass('close').addClass('open');
				IWD.ColorSwatch.setDisplayOptionsGlobal($j(this));
			});
		},
		
		initTabs: function(){
			$j('.tab-item-link').click(function(event){
				event.preventDefault();
				var parent = $j(this).closest('ul');
				parent.find('.active').removeClass('active');
				$j(this).addClass('active');
				var id = $j(this).attr('id') + '_tab';
				var container = parent.next();
				container.find('.tab-user').hide();
				
				$j('#'+id).show();
			});
		},
		
		setDisplayOptionsGlobal: function(link){
                    console.log('setDisplayOptionsGlobal');
                    var parent = link.closest('.entry-edit');
                    var block = parent.find('.fieldset');
                    if (block.hasClass('close-tab')){
                        block.removeClass('close-tab');
                        //IWD.ColorSwatch.destroyUploader(block);
                    }else{
                        block.addClass('close-tab');
                    }

                    if (!block.hasClass('init-tab')){
                        block.addClass('init-tab');
                        //IWD.ColorSwatch.initUploader(block);
                    }
                    
                    $j(document).on('click','td', function(event){
                        if(!$j(this).parent('tr').hasClass('uplodify-loaded')){
                            $j('div.grid table.data tbody tr').removeClass('uplodify-loaded');
//                            $j('div.grid table.data tbody tr td input.upload-input').each(function(){
//                                $(this).uploadify('destroy');
//                            })
                            $j(this).parent('tr').find("input.upload-input").each(function(){
                                $j(this).show();
                                //Only compatbible with chrome
                                //http://stackoverflow.com/questions/23627617/uploadify-demo-page-not-working-in-firefox-29-0-1-and-13-0-0-214
                                IWD.ColorSwatch.initSwf($j(this));
                            });
                            $j(this).parent('tr').addClass('uplodify-loaded');
                        }
                    });
		},
		
		initUploader: function(block){//Should not be used anymore
			if (colorPickerMode==1){
				block.find(".colorpicker-colorswatch").each(function(){
					
					var color = $j(this).data('color');
					
					$j(this).colpick({
						colorScheme:'dark',
						layout:'rgb',
						color:color,
						onSubmit:function(hsb,hex,rgb,el) {
							$j(el).css('background-color', '#'+hex);
							$j(el).find('input:first').val(hex);
							$j(el).colpickHide();
						}
					})
					
					
				});
				
			}else{
				block.find("input.upload-input").each(function(){
					var id = $j(this).attr('id');
					$j(this).uploadify({	        
				        swf           	: urlFlashObject,
				        fileObjName		: 'colowswatchImages',
				        uploader      	: urlUpload,	 
				        formData		: {'idRow' : id},
				        buttonText		: 'Select File...',
				        width			: '70',
				        height			: '20',
				        debug			: false,
				        fileDataName 	: 'file',
				        fileTypeDesc	: 'Image Files',
				        fileTypeExts	: '*.gif; *.jpg; *.png',
				        auto     		: true,
				        fileSizeLimit	: '100KB',
				        multi			:false,
				        'onUploadSuccess' : function(file, data, response) {
				           
				            json = jQuery.parseJSON(data);
				            console.debug(json);
				            IWD.ColorSwatch.applyUploadImage(json);	            
				        }
				    });
				});
			}
		}, 
                
                initSwf: function(input){
                    var id = $j(input).attr('id');                    
                    $j(input).uploadify({       
                        swf           	: urlFlashObject,
                        fileObjName		: 'colowswatchImages',
                        uploader      	: urlUpload,	 
                        formData		: {'idRow' : id},
                        buttonText		: 'Select File...',
                        width			: '70',
                        height			: '20',
                        debug			: false,
                        fileDataName 	: 'file',
                        fileTypeDesc	: 'Image Files',
                        fileTypeExts	: '*.gif; *.jpg; *.png',
                        auto     		: true,
                        fileSizeLimit	: '100KB',
                        multi			:false,
                        onUploadSuccess : function(file, data, response) {
                            json = jQuery.parseJSON(data);
                            console.debug(json);
                            IWD.ColorSwatch.applyUploadImage(json);
                        }, 
                        onUploadError : function(file, errorCode, errorMsg, errorString) {
                            //NOTE:The upload doesn't works on FF if the upload is over https and the certificate is not valid
                            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
                        }
                    });
                    FileToUploadUploader=document.getElementById(id);
                },
                
                destroyUploader: function(block){
                    block.find("input.upload-input").each(function(){
                        var id = $j(this).attr('id');
                        console.log('destroyUploader : '+id);
                        $j(this).uploadify('destroy');
                    });
		}
};

$j(document).ready(function($){
	if (typeof(urlFlashObject)!="undefined"){
		IWD.ColorSwatch.initOpenCloseBlock();
		IWD.ColorSwatch.initTabs();
	}
});