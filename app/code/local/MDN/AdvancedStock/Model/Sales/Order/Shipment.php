<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @copyright  Copyright (c) 2015 Maison du Logiciel (http://www.maisondulogiciel.com)
 * @author : Olivier ZIMMERMANN
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class MDN_AdvancedStock_Model_Sales_Order_Shipment extends Mage_Sales_Model_Order_Shipment {

    /**
     * Prevent strictly to ship non reserved orders
     */
    protected function _beforeSave() {

        parent::_beforeSave();

        //ERP Strict Mode, it is not possible to ship if the product has not been reserved
        if (Mage::getStoreConfig('advancedstock/general/prevent_non_reserved_shipment')) {

            //Security is active only on shipment creation, not on shipment update in code
            if (!$this->getOrigData('entity_id')) {

                //Browse each shipment item and check if they have been reserved
                foreach ($this->getAllItems() as $item) {
                    $orderItem = $item->getOrderItem();

                    //check if product manages stock
                    $productStock = mage::getModel('cataloginventory/stock_item')->load($item->getproduct_id(), 'product_id');
                    if ((!$productStock->getId()) || (!$productStock->ManageStock()))
                        continue;

                    //prevent to have wrong stock movement from magento native ship button (or other button)
                    //that are is not from ERP order preparation screen (which by nature prevent this)
                    $reservedQty = $orderItem->getreserved_qty();
                    if(!$reservedQty){
                        $reservedQty = 0;
                    }

                    $qtyRequestedToBeShipped = $item->getqty();

                    if ($reservedQty < 1 || $reservedQty < $qtyRequestedToBeShipped) {
                        $errorMessage = mage::helper('AdvancedStock')->__("Cannot ship this order because the product %s has %s reserved quantity and shipment requested quantity is %s", $orderItem->getname(),$reservedQty, $qtyRequestedToBeShipped);

                        //prevent the user
                        Mage::getSingleton('adminhtml/session')->addError($errorMessage);

                        //Block the shipment creation
                        throw new Exception($errorMessage);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Create stock movement when order is shipped
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave() {
        try {
                parent::_afterSave();

                if (Mage::getStoreConfig('healthyerp/erp/enabled') == 1) {

                    //Define if shipment just created
                    $newShipmentId = $this->getentity_id();
                    $creation = ($newShipmentId != $this->getOrigData('entity_id'));
                    if ($creation) {

                        //Create stock movements
                        $order = $this->getOrder();
                        foreach ($this->getAllItems() as $item) {
                            try {

                                //retrieve information
                                $orderItem = $item->getOrderItem();
                                $qty = $this->getRealShippedQtyForItem($item);
                                $orderPreparationWarehouse = $orderItem->getPreparationWarehouse();

                                //if order preparation is empty,
                                if (!$orderPreparationWarehouse->getId()) {
                                    $preparationWarehouseId = mage::helper('AdvancedStock/Router')->getWarehouseForOrderItem($orderItem, $order);
                                    $orderPreparationWarehouse = Mage::getModel('AdvancedStock/Warehouse')->load($preparationWarehouseId);
                                }

                                //check if product manages stock
                                $productStock = mage::getModel('cataloginventory/stock_item')->load($item->getproduct_id(), 'product_id');
                                if ((!$productStock->getId()) || (!$productStock->ManageStock()))
                                    continue;

                                if ($orderPreparationWarehouse) {
                                    $additionalDatas = array('sm_ui' => $item->getId(), 'sm_type' => 'order');
                                    mage::getModel('AdvancedStock/StockMovement')->createStockMovement($item->getproduct_id(),
                                        $orderPreparationWarehouse->getId(),
                                        null,
                                        $qty,
                                        mage::helper('AdvancedStock')->__('Shipment for order #') . $this->getOrder()->getincrement_id(),
                                        $additionalDatas);
                                } else
                                    throw new Exception(mage::helper('AdvancedStock')->__('Cant find warehouse for orderitem #' . $orderItem->getId()));

                                //reset reserved qty
                                $productId = $item->getproduct_id();
                                $oldReservedQty = $orderItem->getreserved_qty();
                                $newReservedQty = $oldReservedQty - $qty;
                                if ($newReservedQty < 0)
                                    $newReservedQty = 0;

                                $orderItem->getErpOrderItem()->setreserved_qty($newReservedQty)->save();

                                //updates
                                mage::helper('AdvancedStock/Product_Reservation')->storeReservedQtyForStock($productStock, $productId);

                                mage::helper('AdvancedStock/Product_Base')->planUpdateStocksWithBackgroundTask($productId, 'from shipment #' . $newShipmentId);

                                mage::helper('BackgroundTask')->AddTask('Update product availability status for product #' . $productId . ' (after shipment)',
                                    'SalesOrderPlanning/ProductAvailabilityStatus',
                                    'RefreshForOneProduct',
                                    $productId,
                                    null,
                                    false
                                );

                                $debug = 'CreateShipment StackTrace for PID=' . $productId . ' WarehouseId=' . $orderPreparationWarehouse->getId() . ' newReservedQty=' . $newReservedQty;
                                try {
                                    throw new Exception('LogException');
                                } catch (Exception $ex) {
                                    $stackTrace = $ex->getTraceAsString();
                                    if (!empty($stackTrace)) {
                                        $debug .= $stackTrace;
                                    }
                                }
                                if (!empty($debug)) {
                                    mage::log($debug, null, 'erp_create_shipment.log');
                                }

                            } catch (Exception $ex) {
                                mage::log($ex->getMessage(), null, 'erp_create_shipment.log');
                            }
                        }

                        //dispatch event
                        $this->setOrigData('entity_id', $newShipmentId);
                        Mage::dispatchEvent('salesorder_shipment_aftercreate', array('shipment' => $this, 'order' => $order));
                    }
                }

            }catch (Exception $ex) {
                Mage::log('Error in MDN_AdvancedStock_Model_Sales_Order_Shipment : ' . $ex->getMessage());
                throw new Exception($ex->getMessage());
            }

        return $this;
    }

    /**
     * Return real shipped qty for an item
     * Welcome in magento.....
     *
     * @param unknown_type $item
     */
    public function getRealShippedQtyForItem($item) {
        //init vars
        $qty = $item->getQty();
        $orderItem = $item->getOrderItem();
        $orderItemParentId = $orderItem->getparent_item_id();

        //define if we have to multiply qty by parent qty
        $mustMultiplyByParentQty = false;
        if ($orderItemParentId > 0) {
            $parentOrderItem = mage::getmodel('sales/order_item')->load($orderItemParentId);
            if ($parentOrderItem->getId()) {
                //if shipped together
                if ((($parentOrderItem->getproduct_type() == 'bundle') && (!$parentOrderItem->isShipSeparately())) || ($parentOrderItem->getproduct_type() == 'configurable')) {
                    $mustMultiplyByParentQty = true;
                    $qty = ($orderItem->getqty_ordered() / $parentOrderItem->getqty_ordered());
                }
            }
        }

        //if multiply by parent qty
        if ($mustMultiplyByParentQty) {
            $parentShipmentItem = null;
            foreach ($item->getShipment()->getAllItems() as $ShipmentItem) {
                if ($ShipmentItem->getorder_item_id() == $orderItemParentId)
                    $parentShipmentItem = $ShipmentItem;
            }
            if ($parentShipmentItem) {
                $qty = $qty * $parentShipmentItem->getQty();
            }
        }

        return $qty;
    }
    public function prepareGridCollection($manifest_id)
    {

        $collection = Mage::getModel('sales/order_shipment')->getCollection();

        $collection->getSelect()->columns(array('shipment_increment_id'=>'increment_id'));

        $collection->getSelect()->joinLeft(
            array('cs'=>Mage::getSingleton('core/resource')->getTableName('ch_canadapost_shipment')),
            'main_table.entity_id = cs.magento_shipment_id',
            array('manifest_id')
        );

        if (!empty($manifest_id)) { //view

            $manifest = Mage::getModel('chcanpost2module/manifest')->load($manifest_id);

            if ($manifest->getStatus() == 'pending') {

                $collection->addFieldToFilter('cs.manifest_id', array(array('eq' => $manifest_id), array('null'=>true)));

            } else {

                $collection->addFieldToFilter('cs.manifest_id', $manifest_id);

            }

            $shipment_collection  = Mage::getModel('chcanpost2module/shipment')->getCollection()->addFieldToFilter('manifest_id', $manifest_id);

            $shipment_collection->getSelect()->joinLeft(
                array('s' => Mage::getSingleton('core/resource')->getTableName('sales/shipment')),
                's.entity_id = main_table.magento_shipment_id',
                array('store_id')
            );


            if ($shipment_collection->count() > 0 && Mage::getStoreConfig('carriers/chcanpost2module/scope') == 0) {

                $collection->getSelect()->where('main_table.store_id='.$shipment_collection->getFirstItem()->getData('store_id'));

            }

        } else { //create

            $collection->addFieldToFilter('cs.manifest_id', array('null'=>true));

        }

        $collection->getSelect()->joinLeft(
            array('o'=>Mage::getSingleton('core/resource')->getTableName('sales_flat_order')),
            'main_table.order_id = o.entity_id',
            array(
                'order_increment_id' => 'o.increment_id',
                'order_created_at' => 'o.created_at',
                'ordered_by' => 'CONCAT(o.customer_firstname, \' \',o.customer_lastname)',
            )
        );

        $collection->getSelect()->distinct(true); //->group('cs.magento_shipment_id');

        $collection->addFieldToFilter('o.shipping_method', array('like' => 'chcanpost2module_%'));

        return $collection;

    }
    public function createCpShipment($group_id, $manifest_id, $magento_shipment_id) {

        $shipping_address = Mage::getModel('sales/order_address')->load($this->getShippingAddressId());

        //I have no clue why this does not work
        //$quote = Mage::getModel('sales/quote')->load($this->getOrder()->getQuoteId());
        $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter('entity_id', $this->getOrder()->getQuoteId())->getFirstItem();

        $params = array('weight' => 0);

        $params['_order'] = $this->getOrder();

        $inner_boxes = array();

        $shipment = Mage::getModel('sales/order_shipment')->load($magento_shipment_id);




        // GET THE COUNTRY \
        // LOOP LOOPY PRODUT TARRIG CODES
        if($this->getOrder()->getShippingAddress()->getCountryId() != 'CA') {
            $missingHardCodes = false;
            foreach($shipment->getAllItems() as $item) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                if(!$product->getHsTariffCode()) {
                    throw new Exception(Mage::helper('chcanpost2module')->__('Canada Post requires all items in a foreign shipment to have valid HS Tariff codes. Please refer to The following article for assistance <a href="http://www.collinsharper.com/magentohelp/kb/faq.php?id=128" target="_blank">Canada Post HS Tariff Codes</a>'));
                }
            }

        }

        $packages = $shipment->getPackages();

        if (empty($packages)) {

            $pack = Mage::helper('chcanpost2module')->getBoxForItems($shipment->getAllItems());

        } else {

            $pack = array();

            foreach (unserialize($packages) as $i=>$box) {

                $pack[$i]['box'] = array(
                    'weight' => round($box['params']['weight'],2),
                    'w' => Mage::helper('chunit')->getConvertedLength($box['params']['width'], ($box['params']['dimension_units'] == 'INCH' ? 'inch' : 'cm'), 'cm'),
                    'h' => Mage::helper('chunit')->getConvertedLength($box['params']['height'], ($box['params']['dimension_units'] == 'INCH' ? 'inch' : 'cm'), 'cm'),
                    'l' => Mage::helper('chunit')->getConvertedLength($box['params']['length'], ($box['params']['dimension_units'] == 'INCH' ? 'inch' : 'cm'), 'cm'),
                    'weight_units' => ($box['params']['weight_units'] == 'POUND' ? 'lb' : 'kg'),
                    'dimension_units' => ($box['params']['dimension_units'] == 'INCH' ? 'inch' : 'cm'),
                );

                $pack[$i]['items'] = $box['items'];

            }

        }

        foreach ($pack as $box) {

            $params['box'] = $box['box'];

            $params['weight'] = 0;

            if (!empty($params['box']['weight'])) {

                $params['weight'] = (!empty($params['box']['weight_units']) ? Mage::helper('chunit')->getConvertedWeight($params['box']['weight'], $params['box']['weight_units'], 'kg') : $params['box']['weight']);

            }

            //else weight already included in box weight
            // 20131216 - this is legacy code that is replaced by the new return from getBoxForItems which includes box weight and item weight.
            // this will as well need to be tested with creating a manual package and running manuifest then
            if (0 && empty($packages)) {

                foreach($shipment->getAllItems() as $item) {

                    if (!empty($box['items']) && empty($box['items'][$item->getOrderItemId()])) {

                        continue;

                    }

                    $weight = Mage::helper('chunit')->getConvertedWeight($item->getWeight(), Mage::getStoreConfig('catalog/measure_units/weight'), 'kg');

                    $params['weight'] += ($weight * $item->getQty()) ;

                }

            }

            $params['service_code'] = str_replace('chcanpost2module_', '', $this->getOrder()->getShippingMethod());

            if ($params['service_code'] == 'failure') {

                $data = array(
                    'country_code' => $this->getOrder()->getShippingAddress()->getCountryId(),
                    'postal-code'  => $this->getOrder()->getShippingAddress()->getPostcode(),
                    'weight'       => $params['weight'],
                    'box'          => $params['box'],
                    'xmlns' => 'http://www.canadapost.ca/ws/ship/rate'
                );

                $data = Mage::getModel('chcanpost2module/quote_param')->getParamsByQuote($this->getOrder()->getId(), $data, 'order');


                $rates = Mage::helper('chcanpost2module/rest_getRates')->getRates($data);

                $service_price = 0;

                if (!empty($rates)) {

                    foreach ($rates as $rate) {

                        if ($params['service_code'] == 'failure' || $rate['price'] < $service_price) {

                            $service_price = $rate['price'];

                            $params['service_code'] = $rate['code'];

                        }

                    }

                }

            }

            $service_info = Mage::helper('chcanpost2module/rest_service')->getInfo($params['service_code'], $this->getOrder()->getShippingAddress()->getCountry());

            if (!empty($service_info->options->option)) {

                $mandatory_options = array();

                $available_options = array();

                foreach ($service_info->options->option as $opt) {

                    if (strtolower((string)$opt->mandatory) == 'true') {

                        $mandatory_options[] = (string)$opt->{'option-code'};

                    }

                    $available_options[] = (string)$opt->{'option-code'};

                }

            }

            $quote_params = Mage::getModel('chcanpost2module/quote_param')->getParamsByQuote($this->getOrder()->getId(), array(), 'order');
            if(isset($quote_params['COD']) && $quote_params['COD'] == 0) {
                unset($quote_params['COD']);
            }

            if(isset($quote_params['cod']) && $quote_params['cod'] == 0) {
                unset($quote_params['cod']);
            }

            if (!empty($quote_params)) {

                $params['options'] = Mage::helper('chcanpost2module/option')->composeForOrder($quote_params, $this, $shipping_address, $mandatory_options, $available_options);

                $params['cp_office_id'] = $quote_params['office_id'];

            }

            $params['current_shipment_id'] = $magento_shipment_id;

            $response = Mage::helper('chcanpost2module/rest_shipment')->create($shipping_address, $this->getOrder(), $group_id, $params);

            $result = false;

            $this->xml = new SimpleXMLElement($response);

            if (!empty($this->xml->{'shipment-id'})
                //&& !empty($this->xml->{'tracking-pin'}) //hmmm - it seems this part is not always present
                && !empty($this->xml->links)
            ) {

                $this->saveShipmentInfo($manifest_id, $magento_shipment_id);

                $result = true;

            } else if (!empty($this->xml->message->description)) {

                Mage::helper('chcanpost2module')->log($this->xml->message->description);

                $this->error = $this->xml->message->description;

            }

        }

        return $result;

    }


    private function saveShipmentInfo($manifest_id, $magento_shipment_id)
    {

        $shipment_id = Mage::getModel('chcanpost2module/shipment')
            ->setOrderId($this->getOrderId())
            ->setShipmentId($this->xml->{'shipment-id'})
            ->setStatus($this->xml->{'shipment-status'})
            ->setTrackingPin($this->xml->{'tracking-pin'})
            ->setManifestId($manifest_id)
            ->setMagentoShipmentId($magento_shipment_id)
            ->save()
            ->getId();

        if (!empty($this->xml->links)) {

            foreach ($this->xml->links->link as $link) {

                Mage::getModel('chcanpost2module/link')
                    ->setCpShipmentId($shipment_id)
                    ->setUrl($link['href'])
                    ->setMediaType($link['media-type'])
                    ->setRel($link['rel'])
                    ->save();

            }

        }

        $track = Mage::getModel('sales/order_shipment_track')->addData(array(
            'carrier_code' => 'chcanpost2module',
            'title' => Mage::helper('chcanpost2module')->__('Shipment for order #').$this->getOrder()->getIncrementId(),
            'number' => $this->xml->{'tracking-pin'},
        ));

        $this->addTrack($track);

    }


    /**
     *
     * @param Collinsharper_Canpost_Model_Shipment $cp_shipment
     * @return type
     */
    public function removeCpShipment($cp_shipment)
    {
        if ($cp_shipment->getId()) {

            $result = Mage::helper('chcanpost2module/rest_shipment')->void($cp_shipment->getId());

            $cp_shipment->delete();

            foreach ($this->getAllTracks() as $track) {

                $track->delete();

            }

        }

        return true;

    }


    public function getError()
    {

        return $this->error;

    }

}