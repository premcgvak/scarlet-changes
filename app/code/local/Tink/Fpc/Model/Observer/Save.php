<?php
/**
 * Lesti_Fpc (http:gordonlesti.com/lestifpc)
 *
 * PHP version 5
 *
 * @link      https://github.com/GordonLesti/Lesti_Fpc
 * @package   Lesti_Fpc
 * @author    Gordon Lesti <info@gordonlesti.com>
 * @copyright Copyright (c) 2013-2014 Gordon Lesti (http://gordonlesti.com)
 * @license   http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/**
 * Class Lesti_Fpc_Model_Observer_Save
 */
class Tink_Fpc_Model_Observer_Save extends Lesti_Fpc_Model_Observer_Save
{
    /**
     * @param $observer
     */
    public function contentmanagerPageSaveAfter($observer)
    {
        if ($this->_getFpc()->isActive()) {
            $page = $observer->getEvent()->getObject();
            if ($page->getId()) {
                $tags = array(sha1('contentmanager_' . $page->getId()),
                    sha1('contentmanager_' . $page->getIdentifier()));
                $this->_getFpc()
                    ->clean($tags, Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG);
            }
        }
    }

    /**
     * @param $observer
     */
    public function modelSaveAfter($observer)
    {
        if ($this->_getFpc()->isActive()) {
            $object = $observer->getEvent()->getObject();
            if ($object instanceof Mage_Cms_Model_Block) {
                $this->_cmsBlockSaveAfter($object);
            } elseif ($object instanceof Mage_Index_Model_Event) {
                $dataObject = $object->getDataObject();
                if ($object->getType() === 'mass_action' &&
                    $object->getEntity() === 'catalog_product' &&
                    $dataObject instanceof Mage_Catalog_Model_Product_Action) {
                    $this->_catalogProductSaveAfterMassAction(
                        $dataObject->getProductIds()
                    );
                }
            } elseif($object instanceof Banana_ContentManager_Model_Content){
                $this->contentmanagerPageSaveAfter($observer);
            }
        }
    }
}
