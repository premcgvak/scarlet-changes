<?php
/**
 * Lesti_Fpc (http:gordonlesti.com/lestifpc)
 *
 * PHP version 5
 *
 * @link      https://github.com/GordonLesti/Lesti_Fpc
 * @package   Lesti_Fpc
 * @author    Gordon Lesti <info@gordonlesti.com>
 * @copyright Copyright (c) 2013-2014 Gordon Lesti (http://gordonlesti.com)
 * @license   http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 */

/**
 * Class Lesti_Fpc_Model_Observer_Tags
 */
class Tink_Fpc_Model_Observer_Tags extends Lesti_Fpc_Model_Observer_Tags
{
    /**
     * @param $observer
     */
    public function fpcObserverCollectCacheTags($observer)
    {
        /** @var Lesti_Fpc_Helper_Data $helper */
        $helper = Mage::helper('fpc');
        $fullActionName = $helper->getFullActionName();
        $cacheTags = array();
        $request = Mage::app()->getRequest();
        switch ($fullActionName) {
            case 'contentmanager_index_view' :
                $cacheTags = $this->getContentManagerViewCacheTags($request);
                break;

        }

        $cacheTagObject = $observer->getEvent()->getCacheTags();
        $additionalCacheTags = $cacheTagObject->getValue();
        $additionalCacheTags = array_merge($additionalCacheTags, $cacheTags);
        $cacheTagObject->setValue($additionalCacheTags);

        parent::fpcObserverCollectCacheTags($observer);
    }

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @return array
     */
    protected function getContentManagerViewCacheTags(
        Mage_Core_Controller_Request_Http $request
    )
    {
        $cacheTags = array();
        $cacheTags[] = sha1('contentmanager');
        $pageId = $request->getParam(
            'page_id',
            $request->getParam('id', false)
        );
        if ($pageId) {
            $cacheTags[] = sha1('contentmanager_' . $pageId);
        }
        return $cacheTags;
    }
}
