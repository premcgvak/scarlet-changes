<?php


class Tink_Fpc_Model_Observer extends Lesti_Fpc_Model_Observer
{
    /**
     * @param $observer
     */
    public function httpResponseSendBefore($observer)
    {
        $response = $observer->getEvent()->getResponse();
        $body_content = $response->getBody();

        // Don't cache content if body is less than 300 car
        if(isset($body_content[300])){
            parent::httpResponseSendBefore($observer);
        }
    }

    /**
     * @param $observer
     */
    public function coreBlockAbstractToHtmlAfter($observer)
    {
        if ($this->_getFpc()->isActive() &&
            !$this->_cached &&
            Mage::helper('fpc')->canCacheRequest()) {
            $fullActionName = Mage::helper('fpc')->getFullActionName();
            $block = $observer->getEvent()->getBlock();
            $blockName = $block->getNameInLayout();
            if($blockName == 'messages'){
                $blockName = $blockName;
            }
            $dynamicBlocks = Mage::helper('fpc/block')->getDynamicBlocks();
            $lazyBlocks = Mage::helper('fpc/block')->getLazyBlocks();
            $dynamicBlocks = array_merge($dynamicBlocks, $lazyBlocks);
            $cacheableActions = Mage::helper('fpc')->getCacheableActions();
            if (in_array($fullActionName, $cacheableActions)) {
                $this->_cacheTags = array_merge(
                    Mage::helper('fpc/block')->getCacheTags($block),
                    $this->_cacheTags
                );
                if (in_array($blockName, $dynamicBlocks)) {

                    $placeholder = Mage::helper('fpc/block')
                        ->getPlaceholderHtml($blockName);
                    $html = $observer->getTransport()->getHtml();
                    $this->_html[] = $html;
                    $this->_placeholder[] = $placeholder;
                    // Add fix to display messages block
                    if ($fullActionName == 'contentmanager_index_view' && $blockName == 'global_messages') {
                        $placeholder .= Mage::helper('fpc/block')->getPlaceholderHtml('messages');
                    }
                    // End of fix
                    $observer->getTransport()->setHtml($placeholder);
                }
            }
        }
    }
}