<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Email Template Mailer Model
 *
 * @method Mage_Core_Model_Email_Queue setEntityId(int $value)
 * @method Mage_Core_Model_Email_Queue setEntityType(string $value)
 * @method Mage_Core_Model_Email_Queue setEventType(string $value)
 * @method Mage_Core_Model_Email_Queue setIsForceCheck(int $value)
 * @method int getIsForceCheck()
 * @method int getEntityId()
 * @method string getEntityType()
 * @method string getEventType()
 * @method string getMessageBodyHash()
 * @method string getMessageBody()
 * @method Mage_Core_Model_Email_Queue setMessageBody(string $value)
 * @method Mage_Core_Model_Email_Queue setMessageParameters(array $value)
 * @method Mage_Core_Model_Email_Queue setProcessedAt(string $value)
 * @method array getMessageParameters()
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */
class Tink_Smtp_Model_Email_Queue extends Mage_Core_Model_Email_Queue
{

    /**
     * Send all messages in a queue
     *
     * @return Mage_Core_Model_Email_Queue
     */
    public function send()
    {
        /** @var $collection Mage_Core_Model_Resource_Email_Queue_Collection */
        $collection = Mage::getModel('core/email_queue')->getCollection()
            ->addOnlyForSendingFilter()
            ->setPageSize(self::MESSAGES_LIMIT_PER_CRON_RUN)
            ->setCurPage(1)
            ->load();


        ini_set('SMTP', Mage::getStoreConfig('system/smtp/host'));
        ini_set('smtp_port', Mage::getStoreConfig('system/smtp/port'));

        $smtp_host = Mage::getStoreConfig('system/smtp/host');
        $smtp_port = Mage::getStoreConfig('system/smtp/port');
        $smtp_username = Mage::getStoreConfig('system/smtp/username');
        $smtp_password = Mage::getStoreConfig('system/smtp/password');
        $config = array(
            'port' => $smtp_port,
            'auth' => 'login',
            'username' => $smtp_username,
            'password' => $smtp_password
        );

        /** @var $message Mage_Core_Model_Email_Queue */
        foreach ($collection as $message) {
            if ($message->getId()) {
                $parameters = new Varien_Object($message->getMessageParameters());
                if ($parameters->getReturnPathEmail() !== null) {
                    $mailTransport = new Zend_Mail_Transport_Sendmail("-f" . $parameters->getReturnPathEmail());
                    Zend_Mail::setDefaultTransport($mailTransport);
                }

                $mailer = new Zend_Mail('utf-8');
                $transport = new Zend_Mail_Transport_Smtp($smtp_host, $config);
                Zend_Mail::setDefaultTransport($transport);

                foreach ($message->getRecipients() as $recipient) {
                    list($email, $name, $type) = $recipient;
                    switch ($type) {
                        case self::EMAIL_TYPE_BCC:
                            $mailer->addBcc($email, '=?utf-8?B?' . base64_encode($name) . '?=');
                            break;
                        case self::EMAIL_TYPE_TO:
                        case self::EMAIL_TYPE_CC:
                        default:
                            $mailer->addTo($email, '=?utf-8?B?' . base64_encode($name) . '?=');
                            break;
                    }
                }

                if ($parameters->getIsPlain()) {
                    $mailer->setBodyText($message->getMessageBody());
                } else {
                    $mailer->setBodyHTML($message->getMessageBody());
                }

                $mailer->setSubject('=?utf-8?B?' . base64_encode($parameters->getSubject()) . '?=');
                $mailer->setFrom($parameters->getFromEmail(), $parameters->getFromName());
                if ($parameters->getReplyTo() !== null) {
                    $mailer->setReplyTo($parameters->getReplyTo());
                }
                if ($parameters->getReturnTo() !== null) {
                    $mailer->setReturnPath($parameters->getReturnTo());
                }

                try {
                    $mailer->send();
                    unset($mailer);
                    $message->setProcessedAt(Varien_Date::formatDate(true));
                    $message->save();
                }
                catch (Exception $e) {
                    unset($mailer);
                    $oldDevMode = Mage::getIsDeveloperMode();
                    Mage::setIsDeveloperMode(true);
                    Mage::logException($e);
                    Mage::setIsDeveloperMode($oldDevMode);

                    return false;
                }
            }
        }

        return $this;
    }

    /**
     * Returm Zend_mail with SMTP Authentication config.
     * @return Zend_Mail
     */
    protected function getMail(){
        $smtp_host = Mage::getStoreConfig('system/smtp/host');
        $smtp_port = Mage::getStoreConfig('system/smtp/port');
        $smtp_username = Mage::getStoreConfig('system/smtp/username');
        $smtp_password = Mage::getStoreConfig('system/smtp/password');
        $config = array(
            'port' => $smtp_port,
            'auth' => 'login',
            'username' => $smtp_username,
            'password' => $smtp_password
        );
        $transport = new Zend_Mail_Transport_Smtp($smtp_host, $config);
        Zend_Mail::setDefaultTransport($transport);
        return new Zend_Mail('utf-8');
    }
}
