<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Paypal
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * NVP API wrappers model
 * @TODO: move some parts to abstract, don't hesitate to throw exceptions on api calls
 */
class Tink_RoundingErrorFix_Model_Api_Nvp extends Mage_Paypal_Model_Api_Nvp
{
    /**
     * NVP doesn't support passing discount total as a separate amount - add it as a line item
     *
     * @param array $request
     * @param int $i
     * @return true|null
     */
    protected function _exportLineItems(array &$request, $i = 0)
    {
        if (!$this->_cart) {
            return;
        }
        $this->_cart->isDiscountAsItem(true);

        // Call our fix method for rounding error
        // return parent::_exportLineItems($request, $i);
        return $this->_bugFixExportLineItems($request, $i);
    }

    protected function _bugFixExportLineItems(array &$request, $i = 0)
    {
        if (!$this->_cart) {
            return;
        }

        // always add cart totals, even if line items are not requested
        if ($this->_lineItemTotalExportMap) {
            foreach ($this->_cart->getTotals() as $key => $total) {
                if (isset($this->_lineItemTotalExportMap[$key])) { // !empty($total)
                    $privateKey = $this->_lineItemTotalExportMap[$key];
                    $request[$privateKey] = $this->_filterAmount($total);
                }
            }
        }

        $this->fixRoundingError($request);

        // add cart line items
        $items = $this->_cart->getItems();
        if (empty($items) || !$this->getIsLineItemsEnabled()) {
            return;
        }
        $result = null;
        foreach ($items as $item) {
            foreach ($this->_lineItemExportItemsFormat as $publicKey => $privateFormat) {
                $result = true;
                $value = $item->getDataUsingMethod($publicKey);
                if (isset($this->_lineItemExportItemsFilters[$publicKey])) {
                    $callback   = $this->_lineItemExportItemsFilters[$publicKey];
                    $value = call_user_func(array($this, $callback), $value);
                }
                if (is_float($value)) {
                    $value = $this->_filterAmount($value);
                }
                $request[sprintf($privateFormat, $i)] = $value;
            }
            $i++;
        }

        return $result;
    }

    protected function fixRoundingError(&$request){
        $requestBaseGrandTotal = round($request['ITEMAMT'] + $request['TAXAMT'] + $request['SHIPPINGAMT'], 2);

        /** @var Mage_Sales_Model_Quote $checkout */
        $checkout = Mage::getSingleton('checkout/session')->getQuote();
        $totals = $checkout->getTotals();

        // get the rounded rounding error
        $roundingError = round($request['AMT'] - $requestBaseGrandTotal, 2); // -0.01 or +0.01

        // fix the rounding error
        if($roundingError) {
            $order = array(); // create an array from order data resembling the structure or the request array
            $roundingDeltas = array(); // save the rounding error
            $baseSubTotal = $checkout->getBaseSubtotal();
            $address = $checkout->getShippingAddress();
            $baseShippingAmout = $address->getShippingAmount();
            $taxAmount = $address->getTaxAmount();
            $discount = (isset($totals['discount'])) ? $totals['discount']->getValue() : 0;
            $order['ITEMAMT'] = round($baseSubTotal + $baseShippingAmout + $discount, 2);
            $roundingDeltas['ITEMAMT'] = ($baseSubTotal + $baseShippingAmout + $discount) - $order['ITEMAMT'];

            $order['TAXAMT'] = round($taxAmount, 2);
            $roundingDeltas['TAXAMT'] = $taxAmount - $order['TAXAMT'];

            $order['SHIPPINGAMT'] = round($baseShippingAmout, 2);
            $roundingDeltas['SHIPPINGAMT'] = $baseShippingAmout - $order['SHIPPINGAMT'];

            if($roundingDeltas['ITEMAMT'] && $order['ITEMAMT'] > 0) {
                $request['ITEMAMT'] += $roundingError;
            } else {
                // hide rounding error in tax
                if($order['TAXAMT'] > 0) {
                    $request['TAXAMT'] += $roundingError;
                } else {
                    // do not correct rounding error in this unexpected situation
                }
            }

        }
    }
}
