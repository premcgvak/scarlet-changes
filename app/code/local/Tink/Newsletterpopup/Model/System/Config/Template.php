<?php

class Tink_Newsletterpopup_Model_System_Config_Template
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'SJ', 'label'=>Mage::helper('adminhtml')->__('SJ - Theme')),
            array('value' => 'default', 'label'=>Mage::helper('adminhtml')->__('Default')),
            array('value' => 'label', 'label'=>Mage::helper('adminhtml')->__('Label')),
        );
    }
}