<?php
class Tink_Newsletterpopup_Block_Newsletterpopup extends ES_Newssubscribers_Block_Newssubscribers
{

    public function getConfirmation()
    {
        return Mage::getStoreConfig('newsletter/general/confirmmessage');
    }

    public function getDisplayDelay(){
        return Mage::getStoreConfig('newsletter/general/displaydelay');
    }
}