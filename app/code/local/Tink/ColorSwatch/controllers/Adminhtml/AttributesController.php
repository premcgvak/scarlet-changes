<?php
require_once(Mage::getModuleDir('controllers', 'IWD_ColorSwatch') . DS . 'Adminhtml' . DS . 'AttributesController.php');

class Tink_ColorSwatch_Adminhtml_AttributesController extends IWD_ColorSwatch_Adminhtml_AttributesController{
	
	/**
        * Check the permission to run it
        *
        * @return boolean
        */
       protected function _isAllowed()
       {
          return Mage::getSingleton('admin/session')->isAllowed('system/iwdall/colorswatch');
       }
	
}