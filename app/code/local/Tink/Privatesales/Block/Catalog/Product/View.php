<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Tink_Privatesales_Block_Catalog_Product_View extends Plumrocket_Privatesales_Block_Catalog_Product_View
{
    protected function _prepareLayout() {

        // Override Privatesales due to Catalog_View heritage which rewrite meta title with category name
        $product = $this->getProduct();
        $headBlock = $this->getLayout()->getBlock('head');
        if ($product && $headBlock) {

            // set meta title = [product name] [manufacturer]
            $title = $product->getMetaTitle();
            if ($title) {
                $headBlock->setTitle($title);
            } else {
                $headBlock->setTitle($product->getName() . ' ' . $product->getAttributeText('manufacturer'));
            }

            // set meta description = [manufacturer] [product name] for sale in Scarlet & Julia’s [nom-rubrique] category. Discover a wide range of beauty products and exclusive offers!
            $description = $product->getMetaDescription();
            if ($description) {
                $headBlock->setDescription( ($description) );
            } else {
                $current_category   = Mage::registry('current_category');
                $headBlock->setDescription($this->__('%1$s %2$s for sale in Scarlet & Julia’s %3$s category. Discover a wide range of beauty products and exclusive offers!', $product->getAttributeText('manufacturer'), $product->getName(), $current_category->getName()));
            }
        }
    }
}