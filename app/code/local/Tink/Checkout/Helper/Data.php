<?php

/**
 * Class Tink_Checkout_Helper_Data
 */
class Tink_Checkout_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Return formated price
     * @param $price
     * @param int $precision
     * @return mixed
     */
    public function formatPrice($price, $precision = 2)
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        return $quote->getStore()->getCurrentCurrency()->format($price, array('precision'=>$precision));
    }

    /**
     * Return the Max Quantity for a product.
     * @param string $_product
     * @return string
     */
    function getMaxQty( $_product = '' ){
        $qty = 0;
       // $product_type = get_class($_product->getTypeInstance(true));
        //if($product_type == 'Mage_Catalog_Model_Product_Type_Simple'){
        $product_type = $_product->getTypeId();
        if($product_type == 'simple'){
            $qty = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty();
        } else {
            foreach ($_product->getTypeInstance(true)->getUsedProducts ( null, $_product) as $simple) {
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($simple)->getQty();
                $qty += $stock;
            }
        }

        return $qty;
    }

}
