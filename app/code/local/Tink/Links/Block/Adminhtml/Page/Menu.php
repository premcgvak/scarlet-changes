<?php
/**
 * Hook Mage_Adminhtml_block_Page_Menu
 * to manage Banana and SweethTooth rewards
 */

class Tink_Links_Block_Adminhtml_Page_Menu extends Mage_Adminhtml_Block_Page_Menu
{

    /**
     * Retrieve Adminhtml Menu array
     *
     * @return array
     */
    public function getMenuArray()
    {
        $menu = $this->_buildMenuArray();

        // Remove html in SweetTooth Rewards menu entry
        if(isset($menu['rewards'])){
            $menu['rewards']['label'] = 'Rewards';
        }

        $this->_addCctMenus($menu);
        return $menu;
    }

    /**
     * Add CT menus to the Admin menu
     */
    private function _addCctMenus(&$menu)
    {
        //Retrieve CT collection
        $collection = Mage::getModel('contentmanager/contenttype')->getCollection();
        if($collection->count() > 0)
        {
            //Add Menus
            $i = 1;
            $hasCctShowed = false;
            foreach($collection as $contentType)
            {
                $aclResource = 'admin/contentmanager/content_'.$contentType->getIdentifier();
                $aclResourceAll = 'admin/contentmanager/content_everything';

                if (!$this->_checkAcl($aclResource) && !$this->_checkAcl($aclResourceAll)) {
                    continue;
                }

                $menu['cms']['children']['contenttype_'.$contentType->getIdentifier()] = array(
                    'label' => $contentType->getTitle(),
                    'url' => Mage::helper("adminhtml")->getUrl("adminhtml/contenttype_content/",array("ct_id"=>$contentType->getId())),
                    'level' => 1,
                    'active' => 0,
                    'last' => ($i == $collection->count())?1:0
                );
                $hasCctShowed = true;
                $i++;
            }

            //Remove 'last' tag from CMS menu
            if($hasCctShowed)
            {
                foreach($menu['cms']['children'] as $key => &$children)
                {
                    if(!preg_match("/contenttype_/", $key))
                    {
                        unset($children['last']);
                    }
                }
            }
        }

    }
    


}
