<?php
require_once(Mage::getModuleDir('controllers', 'S3ibusiness_Giftpromo') . DS . 'Adminhtml' . DS . 'IndexController.php');

class Tink_Giftpromo_Adminhtml_indexController extends S3ibusiness_Giftpromo_Adminhtml_indexController
{
      /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        //return true;
        return Mage::getSingleton('admin/session')->isAllowed('promo/quote/giftpromo');
    }
}