<?php

class Tink_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Return if the product is in stock
     * @param object $product
     * @return boolean
     */
    function isInStock($product){
        if(!isset($product)) {
            return FALSE;
        }

        $stock = $product->getStockItem();

        return isset($stock) && $stock->getIsInStock();
    }

    /**
     * Return quantity if it's under stock threshold, else return FALSE
     * @param object $product
     * @return boolean|int
     */
    function getQtyLimitReached($product){
        $checkoutHelper = Mage::helper('tinkcheckout/data');
        if(isset($product, $checkoutHelper)) {
            $qty = $checkoutHelper->getMaxQty($product);
            $limit = (int) Mage::getStoreConfig('cataloginventory/options/stock_threshold_qty');

            if($qty != 0 && $qty <= $limit) {
                return $qty;
            }
        }

        return FALSE;
    }

    /**
     * Return if product has only 2 remaining items in stock
     * @param object $product
     * @return boolean
     */
    function has2RemainingItems($product){
        if(!isset($product)) {
            return FALSE;
        }

        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);

        return isset($stock) && $stock->getQty() == 2;
    }

    /**
     * Return if the product is in promotion
     * @param object $product
     * @return boolean
     */
    function isPromotion($product){
        if(!isset($product)) {
            return FALSE;
        }

        $promotionCatId = Mage::getStoreConfig('tinkconfig/global/promotion_category_id');
        $categories = $product->getCategoryIds();

        return isset($promotionCatId, $categories) && in_array($promotionCatId, $categories);
    }

    function isNewProduct($product){
        if(!isset($product)) {
            return FALSE;
        }

        $newCatId = Mage::getStoreConfig('tinkconfig/global/new_category_id');
        $categories = $product->getCategoryIds();

        return isset($newCatId, $categories) && in_array($newCatId, $categories);
    }

    function isBestSeller($product){
        if(!isset($product)) {
            return FALSE;
        }

        $newCatId = Mage::getStoreConfig('tinkconfig/global/bestseller_category_id');
        $categories = $product->getCategoryIds();

        return isset($newCatId, $categories) && in_array($newCatId, $categories);
    }

}
