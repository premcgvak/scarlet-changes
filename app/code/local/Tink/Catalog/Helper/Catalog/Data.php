<?php

class Tink_Catalog_Helper_Catalog_Data extends Mage_Catalog_Helper_Data
{
    public function getBreadcrumbPath()
    {
        if (! Mage::helper('privatesales')->moduleEnabled()) {
            return parent::getBreadcrumbPath();
        }

        if (!$this->_categoryPath) {
            $path = array();
            if ($category = Mage::registry('current_category')) {
                $pathInStore = $category->getPathInStore();
                $pathIds = array_reverse(explode(',', $pathInStore));

                //$categories = $category->getParentCategories();
                $cats = Mage::getModel('catalog/category')->getCollection()
                    ->addFieldToFilter('entity_id', array('in' => $pathIds))
                    ->addFieldToFilter('is_active', 1)
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('url_key');


                // Exclude privates sales homepages and group of boutique
                $excludedCategories = array (
                    Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_GROUP_OF_BOUTIQUES,
                    Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_MENU_ITEM
                );
                $cats->addFieldToFilter('privatesale_type', array('nin' => $excludedCategories));

                foreach($cats as $cat) {
                    $categories[$cat->getId()] = $cat;
                }

                // add category path breadcrumb
                foreach ($pathIds as $categoryId) {
                    if (isset($categories[$categoryId]) && $categories[$categoryId]->getName()) {
                        $path['category'.$categoryId] = array(
                            'label' => $categories[$categoryId]->getName(),
                            'link' => $this->_isCategoryLink($categoryId) ? $categories[$categoryId]->getUrl() : ''
                        );
                    }
                }
            }

            if ($this->getProduct()) {
                $path['product'] = array('label'=>$this->getProduct()->getName());
            }

            $this->_categoryPath = $path;
        }
        return $this->_categoryPath;
    }

}