<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Product Url model
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Tink_Catalog_Model_Product_Url extends Mage_Catalog_Model_Product_Url
{

    /**
     * Retrieve Product URL using UrlDataObject
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $params
     * @return string
     */
    public function getUrl(Mage_Catalog_Model_Product $product, $params = array())
    {
        $url = $product->getData('url');
        if (!empty($url)) {
            return $url;
        }

        $requestPath = $product->getData('request_path');
        if (empty($requestPath)) {
            $requestPath = $this->_getRequestPath($product, $this->_getCategoryIdForUrl($product, $params));
            $product->setRequestPath($requestPath);
        }
        
        // Override standard implementation to add store group notion : private or public store
        $storeGroupName = $product->getAttributeText('c_scope');
        if (!empty($storeGroupName)) {
            $storeId = $this->_getStoreIdForStoreGroup($storeGroupName);
        }

        if (!isset($storeId)){
            // Standard Mage implementation
            if (isset($params['_store'])) {
                $storeId = $this->_getStoreId($params['_store']);
            } else {
                $storeId = $product->getStoreId();
            }
        }

        if ($storeId != $this->_getStoreId()) {
            $params['_store_to_url'] = true;
        }

        // reset cached URL instance GET query params
        if (!isset($params['_query'])) {
            $params['_query'] = array();
        }

        $this->getUrlInstance()->setStore($storeId);
        $productUrl = $this->_getProductUrl($product, $requestPath, $params);
        $product->setData('url', $productUrl);
        return $product->getData('url');
    }
    
    /**
     * Returns store_id value for store group and current language - alias storeview name -
     *
     * @param int|null $scope
     * @return int
     */
    protected function _getStoreIdForStoreGroup($storeGroupName){
        $currentStore = Mage::app()->getStore();
        $stores = Mage::app()->getStores();
        foreach ($stores as $store) {
            if($store->getName() == $currentStore->getName() && $store->getGroup()->getName() == $storeGroupName) {
                return $store->getId();
            }
        }
    }
}
