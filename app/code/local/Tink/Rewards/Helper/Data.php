<?php

class Tink_Rewards_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Return number of rewards points for current customer
     * @return int
     */
    function getRewardsPointsForCustomer(){
        $rewardPoints = 0;

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            // First get customer ID.
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $customerId   = $customerData->getId();

            // Then load customer usable reward points
            $usable_points = Mage::getModel('rewards/customer_indexer_points')->getCollection()->addFieldToFilter(
                'customer_id',
                $customerId
            );
            $usable_points_array = array(1 => 0);
            if ($usable_points->count()) {
                $usable_points_array [1] = $usable_points->getFirstItem()->getCustomerPointsUsable();
            }
            // Little check if empty
            if (!empty($usable_points_array[1])) {
                $rewardPoints = $usable_points_array[1];
            }
        }

        return $rewardPoints;
    }

}
