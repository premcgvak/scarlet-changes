<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_SalesRule
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Tink_SalesRule_Model_Rule_Condition_Address extends Mage_SalesRule_Model_Rule_Condition_Address
{
    public function loadAttributeOptions()
    {
        parent::loadAttributeOptions();
        $attributes = $this->getData('attribute_option');
        $attributes['base_subtotal_with_discount'] = Mage::helper('salesrule')->__('Subtotal with discount');
        $this->setAttributeOption($attributes);

        return $this;
    }

    public function getInputType()
    {
        switch ($this->getAttribute()) {

            case 'base_subtotal':case 'base_subtotal_with_discount': case 'weight': case 'total_qty':
            return 'numeric';

            case 'shipping_method': case 'payment_method': case 'country_id': case 'region_id':
            return 'select';
        }
        return 'string';
    }



    /**
     * Validate Address Rule Condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        $address = $object;
        if (!$address instanceof Mage_Sales_Model_Quote_Address) {
            if ($object->getQuote()->isVirtual()) {
                $address = $object->getQuote()->getBillingAddress();
            }
            else {
                $address = $object->getQuote()->getShippingAddress();
            }
        }

        if ('payment_method' == $this->getAttribute() && ! $address->hasPaymentMethod()) {
            $address->setPaymentMethod($object->getQuote()->getPayment()->getMethod());
        }

        // Don't consider discount when calculate free shipping
        $rowsTotals=0;
        foreach ($object->getQuote()->getAllItems() as $item) {
            $rowsTotals+=($item->getRowTotal());
        }
        
        $address->setBaseSubtotalWithDiscount($rowsTotals+$address->getDiscountAmount());

        return Mage_Rule_Model_Condition_Abstract::validate($address);
    }
}
