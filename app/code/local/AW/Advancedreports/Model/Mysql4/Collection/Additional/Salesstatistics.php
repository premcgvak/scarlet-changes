<?php

class AW_Advancedreports_Model_Mysql4_Collection_Additional_Salesstatistics
    extends AW_Advancedreports_Model_Mysql4_Collection_Abstract
{
    /**
     * Reinitialize select
     *
     * @return AW_Advancedreports_Model_Mysql4_Collection_Sales
     */
    public function reInitItemSelect()
    {
        $orderTable = Mage::helper('advancedreports/sql')->getTable('sales_flat_order');
        $this->getSelect()->reset();
        $this->getSelect()->from(
            array('main_table' => $orderTable),
            array(
                 'grouper' => new Zend_Db_Expr('ROUND(1)'),
            )
        );

        $this->getSelect()->group('grouper');
        return $this;
    }

    public function reInitOrderSelect($isAllStores = false)
    {
        $orderTable = Mage::helper('advancedreports/sql')->getTable('sales_flat_order');

        $currencyRate = $this->_getCurrencyRate($isAllStores);

        $this->getSelect()->reset();
        $this->getSelect()->from(
            array('main_table' => $orderTable),
            array(
                 'orders_count'         => "COUNT(main_table.entity_id)",
                 # Just because it's unique
                 'base_subtotal'        => "SUM(main_table.base_subtotal * $currencyRate)",
                 'base_tax_amount'      => "SUM(main_table.base_tax_amount * $currencyRate)",
                 'base_discount_amount' => "SUM(main_table.base_discount_amount * $currencyRate)",
                 'base_grand_total'     => "SUM(main_table.base_grand_total * $currencyRate)",
                 'base_total_invoiced'  => "SUM(main_table.base_total_invoiced * $currencyRate)",
                 'base_total_refunded'  => "SUM(main_table.base_total_refunded * $currencyRate)",
                 'grouper'              => new Zend_Db_Expr('ROUND(1)'),
            )
        );

        $this->getSelect()->group('grouper');

        return $this;
    }

    /**
     * Add items
     *
     * @return AW_Advancedreports_Model_Mysql4_Collection_Additional_Salesstatistics
     */
    public function addItems()
    {
        $itemTable = Mage::helper('advancedreports/sql')->getTable('sales_flat_order_item');
        $this->getSelect()->join(
            array(
                 'item' => $itemTable), "main_table.entity_id = item.order_id AND item.parent_item_id IS NULL",
            array(
                 'items_count'    => 'SUM(item.qty_ordered)',
                 'items_invoiced' => 'SUM(item.qty_invoiced)'
            )
        );
        return $this;
    }

    protected function _getCurrencyRate($isAllStores)
    {
        if ($isAllStores) {
            $currencyRate = "main_table.store_to_base_rate";
        } else {
            $currencyRate = new Zend_Db_Expr("1");
        }
        return $currencyRate;
    }
}
