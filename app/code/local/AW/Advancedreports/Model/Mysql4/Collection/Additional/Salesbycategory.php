<?php
class AW_Advancedreports_Model_Mysql4_Collection_Additional_Salesbycategory
    extends AW_Advancedreports_Model_Mysql4_Collection_Abstract
{
    /**
     * Reinitialize select
     *
     * @return AW_Advancedreports_Model_Mysql4_Collection_Additional_Salesbycategory
     */
    public function reInitSelect()
    {
        $orderTable = Mage::helper('advancedreports/sql')->getTable('sales_flat_order');
        $filterField = Mage::helper('advancedreports')->confOrderDateFilter();

        $this->getSelect()->reset();
        $this->getSelect()->from(array('main_table' => $orderTable),
            array(
                'order_created_at'   => $filterField,
                'order_id'           => 'entity_id',
                'order_increment_id' => 'increment_id',
                'status' => 'status'
            ));

        return $this;
    }

    public function addOrderItemInfo($isAllStores = false)
    {
        $itemTable = Mage::helper('advancedreports/sql')->getTable('sales_flat_order_item');
        $refundTable = Mage::helper('advancedreports/sql')->getTable('sales_flat_creditmemo_item');

        $currencyRate = $this->_getCurrencyRate($isAllStores);

        $this->getSelect()
            ->join(
                array('item' => $itemTable),
                "(item.order_id = main_table.entity_id AND item.parent_item_id IS NULL)",
                array(
                    'items_ordered'    => "SUM(item.qty_ordered)",
                    'sum_tax_invoiced' => "SUM(COALESCE(IF(item.product_type = 'bundle' AND child_item.base_price>0, child_item.base_tax_invoiced, null), item.base_tax_invoiced) * $currencyRate)",
                    'sum_invoiced'     => "SUM((COALESCE(IF(item.product_type = 'bundle' AND child_item.base_price>0, child_item.base_row_invoiced, null), item.base_row_invoiced)
                                            + COALESCE(IF(item.product_type = 'bundle' AND child_item.base_price>0, child_item.base_tax_invoiced, null), item.base_tax_invoiced)
                                            - COALESCE(IF(item.product_type = 'bundle' AND child_item.base_price>0, child_item.base_discount_invoiced, NULL), item.base_discount_invoiced, 0)) * $currencyRate)",
                    'sum_subtotal'     => "SUM(item.base_row_total * $currencyRate)",
                    'sum_total'        => "SUM((item.base_row_total
                                            + item.base_tax_amount
                                            + COALESCE(item.base_hidden_tax_amount, 0)
                                            + COALESCE(item.base_weee_tax_applied_amount, 0)
                                            - COALESCE(IF(item.product_type = 'bundle' AND child_item.base_price>0, child_item.base_discount_amount, NULL), item.base_discount_amount, 0)) * $currencyRate)",
                )
            )
            ->joinLeft(
                array('child_item' => new Zend_Db_Expr(
                "(SELECT parent_item_id, SUM(base_price) AS base_price, SUM(base_tax_invoiced) AS base_tax_invoiced,
                  SUM(base_row_invoiced) AS base_row_invoiced, SUM(base_discount_invoiced) AS base_discount_invoiced,
                  SUM(base_discount_amount) AS base_discount_amount
                  FROM {$itemTable} AS bundle_item GROUP BY parent_item_id)"
                )),
                "(child_item.parent_item_id = item.item_id)",
                array()
            )
            ->joinLeft(
                array('refunded' => $refundTable),
                "(refunded.order_item_id = item.item_id)",
                array('sum_refunded' => "SUM(refunded.base_row_total * $currencyRate)")
            );

        return $this;
    }

    public function addCategoryFilter($categoryNames)
    {
        $categoryNames = explode(',', $categoryNames);
        $catNameAttr = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_category', 'name');
        $catNameTable = $catNameAttr->getBackendTable();
        $categoryTable = Mage::helper('advancedreports/sql')->getTable('catalog_category_product');

        $this->getSelect()
            ->join(
                array('category' => $categoryTable),
                "(category.product_id = item.product_id)",
                array()
            )
            ->join(
                array('category_name' => $catNameTable),
                $this->getConnection()
                    ->quoteInto(
                        "(category_name.entity_id = category.category_id
                        AND category_name.value IN (?)
                        AND category_name.attribute_id = {$catNameAttr->getId()})", $categoryNames
                    ),
                array()
            );

        return $this;
    }

    public function addProfitInfo($dateFrom, $dateTo, $isAllStores = false)
    {
        if ($isAllStores) {
            $currencyRate = "order.store_to_base_rate";
        } else {
            $currencyRate = new Zend_Db_Expr("1");
        }

        $costAttr = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'cost');
        $costTable = $costAttr->getBackendTable();
        $itemTable = Mage::helper('advancedreports/sql')->getTable('sales_flat_order_item');
        $orderTable = Mage::helper('advancedreports/sql')->getTable('sales_flat_order');

        $filterField = Mage::helper('advancedreports')->confOrderDateFilter();
        $orderStatusList = explode(",", Mage::helper('advancedreports')->confProcessOrders());
        $orderStatusList = implode("','", $orderStatusList);

        $skuTypeCondition = '1=1';
        $storeIdsCondition = '1=1';
        $typeList = "'configurable', 'bundle'";

        if ($storeIds = $this->getStoreIds()) {
            $storeIdsCondition = "(order.store_id in ('" . implode("','", $storeIds) . "'))";
        }

        $itemProductIdField = "IFNULL(item2.product_id, item.product_id)";
        $groupBy = 'item.item_id';
        $additionalJoinCondition = 'item.order_id = t.item_order_id AND (t.parent_item_id = item.item_id OR t.parent_item_id IS NULL)';
        $productTypeCondition = "((item.product_type <> 'configurable' AND item2.product_type <> 'bundle') OR (item.product_type <> 'configurable' AND item2.product_type IS NULL))";

        $this->getSelect()
            ->joinLeft(
                new Zend_Db_Expr(
                    "(SELECT item.item_id as item_id, (SUM(IFNULL(COALESCE(IF(bundle_item_cost.base_price>0,bundle_item_cost.base_cost,NULL), cost_store.value, cost_def.value, cost_parent_store.value, cost_parent_def.value) * $currencyRate, 0) * IFNULL(item.qty_ordered,item2.qty_ordered))) AS `total_cost`,
                    (
                        SUM(IFNULL(item2.base_row_total, item.base_row_total) * $currencyRate)
                        - SUM(COALESCE(IF(bundle_item_cost.base_price>0,bundle_item_cost.base_discount_amount,NULL), item2.base_discount_amount, item.base_discount_amount) * $currencyRate)
                        - (SUM(IFNULL(COALESCE(IF(bundle_item_cost.base_price>0,bundle_item_cost.base_cost,NULL), cost_store.value, cost_def.value, cost_parent_store.value, cost_parent_def.value) * $currencyRate, 0) * IFNULL(item.qty_ordered,item2.qty_ordered)))
                    ) AS `total_profit`,
                    (
                        100
                        * (
                            SUM(IFNULL(item2.base_row_total, item.base_row_total) * $currencyRate)
                            - SUM(COALESCE(IF(bundle_item_cost.base_price>0,bundle_item_cost.base_discount_amount,NULL), item2.base_discount_amount, item.base_discount_amount) * $currencyRate)
                            - (SUM(IFNULL(COALESCE(IF(bundle_item_cost.base_price>0,bundle_item_cost.base_cost,NULL), cost_store.value, cost_def.value, cost_parent_store.value, cost_parent_def.value) * $currencyRate, 0) * IFNULL(item.qty_ordered,item2.qty_ordered)))
                        )
                        / (
                            SUM(IFNULL(item2.base_row_total, item.base_row_total) * $currencyRate)
                            - SUM(COALESCE(IF(bundle_item_cost.base_price>0,bundle_item_cost.base_discount_amount,NULL), item2.base_discount_amount, item.base_discount_amount) * $currencyRate)
                        )
                    ) AS `total_margin`,
                    (
                        SUM(IFNULL(item2.base_row_total, item.base_row_total) * $currencyRate)
                        - SUM(COALESCE(IF(bundle_item_cost.base_price>0,bundle_item_cost.base_discount_amount,NULL), item2.base_discount_amount, item.base_discount_amount) * $currencyRate)
                    ) AS `total_revenue_excl_tax`,
                    (
                        SUM(IFNULL(item2.base_row_total, item.base_row_total) * $currencyRate)
                        + SUM(IFNULL(item2.base_tax_amount, item.base_tax_amount) * $currencyRate)
                        + SUM(COALESCE(item2.base_hidden_tax_amount, item.base_hidden_tax_amount, 0) * $currencyRate)
                        + SUM(IFNULL(item2.base_weee_tax_applied_amount, item.base_weee_tax_applied_amount) * $currencyRate)
                        - SUM(COALESCE(IF(bundle_item_cost.base_price>0,bundle_item_cost.base_discount_amount,NULL), item2.base_discount_amount, item.base_discount_amount) * $currencyRate)
                    ) AS `total_revenue`,
                    {$itemProductIdField} AS `item_product_id`,
                    item.order_id AS `item_order_id`,
                    item.parent_item_id AS `parent_item_id`
                    FROM {$itemTable} AS `item`
                    INNER JOIN {$orderTable} AS `order` ON order.entity_id = item.order_id
                    LEFT JOIN {$itemTable} AS `item2` ON order.entity_id = item2.order_id AND item2.item_id = item.parent_item_id AND item2.product_type IN ({$typeList})
                    LEFT JOIN (SELECT parent_item_id, SUM(COALESCE(cost_store.value, cost_def.value, 0)) AS base_cost, SUM(base_price) AS base_price, SUM(bundle_item.base_discount_amount) AS base_discount_amount
                    FROM {$itemTable} AS bundle_item
                        INNER JOIN {$orderTable} AS `order` ON order.entity_id = bundle_item.order_id
                        LEFT JOIN {$costTable} AS `cost_def` ON cost_def.entity_id = bundle_item.product_id AND cost_def.attribute_id = {$costAttr->getId()} AND cost_def.store_id = 0
                        LEFT JOIN {$costTable} AS `cost_store` ON cost_store.entity_id = bundle_item.product_id AND cost_store.attribute_id = {$costAttr->getId()} AND cost_store.store_id = order.store_id
                        GROUP BY parent_item_id) AS `bundle_item_cost` ON item.item_id = bundle_item_cost.parent_item_id
                    LEFT JOIN {$costTable} AS `cost_def` ON cost_def.entity_id = item.product_id AND cost_def.attribute_id = {$costAttr->getId()} AND cost_def.store_id = 0
                    LEFT JOIN {$costTable} AS `cost_store` ON cost_store.entity_id = item.product_id AND cost_store.attribute_id = {$costAttr->getId()} AND cost_store.store_id = order.store_id
                    LEFT JOIN {$costTable} AS `cost_parent_def` ON cost_parent_def.entity_id = item2.product_id AND cost_parent_def.attribute_id = {$costAttr->getId()} AND cost_parent_def.store_id = 0
                    LEFT JOIN {$costTable} AS `cost_parent_store` ON cost_parent_store.entity_id = item2.product_id AND cost_parent_store.attribute_id = {$costAttr->getId()} AND cost_parent_store.store_id = order.store_id
                    WHERE {$productTypeCondition} AND {$skuTypeCondition} AND (order.{$filterField} >= '{$dateFrom}' AND order.{$filterField} <= '{$dateTo}') AND (order.status IN ('{$orderStatusList}'))
                    AND {$storeIdsCondition}
                    GROUP BY {$groupBy})"
                ),
                "item.item_id = IFNULL(t.parent_item_id, t.item_id) AND {$additionalJoinCondition}",
                array()
            );

        $currencyRate = $this->_getCurrencyRate($isAllStores);

        $this->getSelect()
            ->columns(
                array(
                    'total_cost'             => "COALESCE(SUM(t.total_cost * $currencyRate), 0)",
                    'total_profit'           => "COALESCE(SUM(t.total_profit * $currencyRate), 0)",
                    'total_margin'           => "COALESCE((100 * (SUM(t.total_revenue_excl_tax) - SUM(t.total_cost))/ SUM(t.total_revenue_excl_tax)), 0)",
                    'total_revenue_excl_tax' => "COALESCE(SUM(t.total_revenue_excl_tax * $currencyRate), 0)",
                    'total_revenue'          => "COALESCE(SUM(t.total_revenue * $currencyRate), 0)",
                )
            );
        return $this;
    }

    protected function _getCurrencyRate($isAllStores)
    {
        if ($isAllStores) {
            $currencyRate = "main_table.store_to_base_rate";
        } else {
            $currencyRate = new Zend_Db_Expr("'1'");
        }
        return $currencyRate;
    }
}