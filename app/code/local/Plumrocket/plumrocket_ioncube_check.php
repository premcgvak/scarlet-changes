<?php

if (! defined('PLUMROCKET_COMMUNITY_EDITION_INVALID')) {
	define('PLUMROCKET_COMMUNITY_EDITION_INVALID', 'PLUMROCKET_COMMUNITY_EDITION_INVALID');
}

function ioncube_event_handler($err_code, $params)
{
	$version = '';
	$currentFile = '';
	$moduleName = '';
	$moduleCode = '';
	
	if ($params && isset($params['current_file'])) {
		$currentFile = $params['current_file'];
		
		if (preg_match('/app\/code\/local\/Plumrocket\/([A-Za-z0-9\-\_]+)\//', $currentFile, $maths)) {
			if (isset($maths[1])) {
				$moduleCode = 'Plumrocket_' . $maths[1];
				$moduleName = 'Plumrocket ' . $maths[1];
				
				$xmlFilePath = dirname(__FILE__) . '/../../../etc/modules/' . $moduleCode . '.xml';
				if (file_exists($xmlFilePath)) {
					$xml = simplexml_load_file($xmlFilePath);
					
					$version = $xml->modules->$moduleCode->version;
					
					if (isset($xml->modules->$moduleCode->name)) {
						$moduleName = $xml->modules->$moduleCode->name;
					}
				}
			}
		}
	}
		
	$error = '';
	switch ($err_code) {
		case ION_CORRUPT_FILE:
		case ION_LICENSE_NOT_FOUND:
		case ION_LICENSE_CORRUPT:
			$error = 'License File Not Found or Not Valid. Please login to your account on <a href="https://store.plumrocket.com/">https://store.plumrocket.com/</a> to download license.';
			break;
		case ION_NO_PERMISSIONS:
		case ION_LICENSE_SERVER_INVALID:
			$error = 'License is Not Valid for This Domain. Please go to <a href="https://store.plumrocket.com/">https://store.plumrocket.com/</a> to purchase additional license.';
			break;
		case ION_EXPIRED_FILE:
		case ION_LICENSE_EXPIRED:
			$error = 'License Has Expired. Please login to your account on <a href="https://store.plumrocket.com/">https://store.plumrocket.com/</a> to download permanent license.';
			break;
			
		case ION_CLOCK_SKEW:
			$error = 'An encoded file is used on a system where the clock is set more than 24 hours before the file was encoded.';
			break;
		case ION_LICENSE_PROPERTY_INVALID:
			$error = 'A property marked as ‘enforced’ in the license file was not matched by a property contained in the encoded file.';
			break;
		case ION_LICENSE_HEADER_INVALID:
			$error = 'The header block of the license file has been altered. Please login to your account on <a href="https://store.plumrocket.com/">https://store.plumrocket.com/</a> to download license.';
			break;
		case ION_UNAUTH_INCLUDING_FILE:
			$error = 'The encoded file has been included by a file which is either non-encoded or has incorrect properties';
			break;
		case ION_UNAUTH_INCLUDED_FILE:
			$error = 'The encoded file has included a file which is either nonencoded or has incorrect properties';
			break;
		case ION_UNAUTH_APPEND_PREPEND_FILE:
			$error = 'The php.ini has either the --auto-append-file or --auto-prepend-file setting enabled';
			break;
			
		case PLUMROCKET_COMMUNITY_EDITION_INVALID:
			$error = 'Your license is not valid on Magento Enterprise Edition. Please purchase Magento Enterprise Edition license for this extension on <a href="https://store.plumrocket.com/">https://store.plumrocket.com/</a>';
			break;
	}

	ob_clean();
	
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style>
html,body {height:100%; margin:0px; padding:0px; background: #fff;}
.body-hld {min-width:320px; max-width:1024px; font-size:12px; font-family:Arial, Helvetica, sans-serif; text-align:center; margin:0 auto; padding-top:5%;}
.log {text-align:left; padding:0 5%;}
.log>span {color:#9e0000; font-size:16px; font-weight:700;}
.log>div {padding:8px; border:1px solid #D9D900; background:#FEF5A7; margin-top:5px; min-width:320px; max-width:960px; margin:0 auto;}
img {border:none;}
div.warn {
	width:            168px;
	height:           141px;
	background-image: url('data:image/gif;base64,R0lGODlhqACNAPcAANnZ2d7d3drZ2djX19/e3tnY2ODf39ra2v39/eHg4P7+/uLh4ePi4vn5+dvb2/T09OTj4+zs7OLi4uXj4/z8/OTi4uXk5PHx8fv7++bk5Pr6+uPh4dzc3Pf3993d3fX19fb29t7e3vj4+PLy8vPz8+De3vDw8Ofl5eHf3+bl5eLg4O/v79bV1enp6fLx8e3t7fPy8vDv7+7u7tfW1uPj49TT0+3s7PHw8O7t7eDg4PTz8+Tk5Orp6ezr6+vq6ufm5tPT09XV1eno6Ovr69TU1Obm5tHQ0N/f3+jo6MXExOrq6ujn59bW1u/u7rm4uMfHx+Xl5dLR0fX09NXU1M/Pz9HR0cPCwvf29rm5ueHh4bq6uvj398zMzMbGxsrKyr+/v9rY2Ofn58rJyenn59PS0sbFxdLS0szLy+7s7M7Ozru6utfV1d3b2/b19dDPz9vZ2ba1tcvKysvLy7y7u8PDw8LBwc/Ozs3MzNjW1rq5ucjIyOjm5sHAwLy8vMC/v7i3t93c3Nva2tzb29jY2NfX1////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1wTU06T3JpZ2luYWxEb2N1bWVudElEPSJ4bXAuZGlkOkQwMzc3NzlFNjVCQ0UyMTE4RjBBQjdCMTY4NEZCMTEyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjc4NjI1RTAxQkM4NjExRTJCOUIwOTg3ODRBMjA0MDA3IiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjc4NjI1RTAwQkM4NjExRTJCOUIwOTg3ODRBMjA0MDA3IiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKFdpbmRvd3MpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RDEzNzc3OUU2NUJDRTIxMThGMEFCN0IxNjg0RkIxMTIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RDAzNzc3OUU2NUJDRTIxMThGMEFCN0IxNjg0RkIxMTIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4B//79/Pv6+fj39vX08/Lx8O/u7ezr6uno5+bl5OPi4eDf3t3c29rZ2NfW1dTT0tHQz87NzMvKycjHxsXEw8LBwL++vby7urm4t7a1tLOysbCvrq2sq6qpqKempaSjoqGgn56dnJuamZiXlpWUk5KRkI+OjYyLiomIh4aFhIOCgYB/fn18e3p5eHd2dXRzcnFwb25tbGtqaWhnZmVkY2JhYF9eXVxbWllYV1ZVVFNSUVBPTk1MS0pJSEdGRURDQkFAPz49PDs6OTg3NjU0MzIxMC8uLSwrKikoJyYlJCMiISAfHh0cGxoZGBcWFRQTEhEQDw4NDAsKCQgHBgUEAwIBAAAh+QQAAAAAACwAAAAAqACNAAAI/wALCRxIsKDBgwgTKlzIsKHDhxAjSpxIsaLFixgzatzIsaPHjyBDihxJsqTJkyhTqlzJsqXLlzBjflRAs6YCmThzGlSAgAIFDEB9Irips+hLmj8bdADBtEMDDBRoGp2akicFDR0+NEBQCEGDDx00UBhKtSxJBRS+glAwgscPHiMUgNAa1azdmWk/fChk44RfvzYK6W1Q965hjDw1gCChoO/fvzYUkAChgezhyxMRYOgwQoOLx6BdaBjRAQNXzKgdom3wgEQH0LA7kHhAmGjq2wg1gzBBAQ1s0GgomABhGrfxgmhFkPjw4DfsBx9IiCh8HHfiDyYUCEnBvbv37kIUmP/4UNl2ddTJSY/4zt47DM7TzZ8/nPjBhULt83MvdOFB+fmYadbBBRrEkMGBCCao4IExaHBBaacBeNdqrV2x4IULXjFbbRLepRtvPWAo4oE9CEdchB1OldxyOozoog7RxZeiigho8MEFCozh4ohjKHABeZbNmJMCm5HmggVIJqnkkkwi6QJnpckn5FE12ldIk1hmaQF//gU55UsCEtiElmQq2YSDEH4JE4WTlekmkovRRp2aKiUGwgUU+DDBnnz26eeff/pAwQWUeUnnSSt+AAOgjDbqJwwxznloSdfh+IOjmDK6h49ASjlpSETCd0OmpP75JGkYePqpRzyxdh8EsMb/KuustNY6ayEj0Gboqh6FqQEOtgYrLK04oFkcryCtNtsHwzbbLHNyqoqsRXZegAAPzmZrKw8IEPrftBulp2gF5JZr7rnopqtuBZBKJym4FVWqQAbr1mtvuhlw+i281G52gQg33CvwwDeI8GCq/FLrlZUMNOzwwxBHLPHEEHO5lbQJLxQmBjZQ7PHHH9uAwcEoZqxaWiTMBvLKLD/cGgkcmqxajXciIETLOIMsRLeFYizzQOmBMELORHs8gtAy/qzQddlNsMHTUEct9dRUV231BOJ1qnRCof4bg9Vghy221DEYHOXWB7X6wAhbLOD223DHLffcdNcN9xa5Xox2QQKa/4BBD3YHDkEMDQiEQBs4BF53DxiYkObeAin7gA6K1w1B4QaRUPncOrQWM9p2moDAEpvPbUJCQpQO9xIIDLev0kFfoHrcBmiQ0AUJzO42oSMkDXuN2CnAgO5vB6DQAwQQz0DWr5vcdQNNqCD99NRXb70KYCgEAxvXd199E2Yj/LPapCVg/vnop6+++QMo5IIA68evPme6+rzqhxj4IP/+6M/gPvz8k58PGve4jEkOBgEMoP8S8r4Eyg8GnnsXskKHgBQ4cH8LREgDL6i+FLSuZwkL2g04KL8MHmSDJETfDZAmwU9VqhALSOH6TGgQFMowd/zRGriehwMU+PCHQAyiEP+DuAb3gWGISEwiDhpwMPsJiUJHM4AUp0jFKlrRiixQyAUKcMUuetEAQovWtD5EgRZ88YxY1CIX0YjGFpjoWKta0QNcwMY6GiCLt1ujHb3ogtb47lAUtMAe0YhHhGxxkF60wAeb96X0dCAGiDwjCx6QkB/oMZJVjAF8WpiiF+YAk16cwekQAgEBgNKKOcghI2cUpgbYoASwjKUsZ0nLWsISDzdIiAXeYMte+rIENmBiAafEpg8Q4JjITKYyl8lMZA7gPgiBQiCaSc1qHvMDeePkfMi4BGt6s5kDKEJCFjDNb5qTAEt4Y8k6lKgLnPOdBCgADRIiiHLC05o/cpcTU8P/NAXQ4J7mFMAO6CkIgFqTBszb1XkcuQKDfjMQUEgIIArqUGquYJP7vMwLK+rNQCxAohTlKDNVqVDj+GoIIUipSlfK0pa6tKUHcEBCqOCAl9r0piwdgrHWaRw2PSAAQA2qUIdK1KIWNRAyRYgRBGHUpjqVqFLYkDYDhIHdUOAHT81qVgPBgYTUABBaDStRf6DO+STqBmJN61ADUUiD1ECtcL1BpDJKFXktAK5wDYQbEKKDt+JVrAvQV0npQwERkEYGf1WrIKKAEBNMIbFilQFGe1ql+0BWrTVoLAsuG1YurdIwvlKCB0ZL2tKa9rSoTS1pgYCBg1yACaqNrWxRq4Sd/96GTToAhG53y9ve+va3wOUtEFZwkBawILjITS5wdSBVusokdBRIgXKnS11A1GCUBZHAcavLXd+mwESfrWthlxOD7prXtyzALkEYMIDzujcGc9Uo8HBkAPfalwUvOAgECmBf8+ZAsM51SahI84L+upcFEjjIDvhrYO6+AEriswv5CtHg886AAQcRAIMrTF1c1c9D/tJACzhA4hKb+MQoTrGKU0yIeRrkAINYsYxnPOMW2NYsbIKBIHbM4x77+MdADnKQBwCBgxygAEJOspKXDIPmlqVaFIDAkqdMZR8PoAAHCQSSq8zlJENgUCBU0Xg/sIIumznJAxDAQYyw5TO7ef/HK4ivUeRFgDfbeccFOMBB7CCAO7uZAACec5FEEAE/27kAgVhzIAx95ggY9mw6Udt9GP3mAtzhIGSg9JktNliWbAwJDgi1qEdN6lKb+tSoDjUA0mCQDpgh1bCOtayRMLJhrgllJBiBrHfN61IfoAoreIGwhU2DV/f62LIeQco+RyXFWIsBgYi2tKdN7Wpb+9rYnnYU+OCEbjshD1YgQ7bHTe5yB4IBPAsvSsQlA3O7+93UnoIYkkDvJJThDDOAt77LLQNs/pEl/QTEvgc+7gKwoAYIr8EUZoBogjt82gFIaIA70jUR9ODhGM+4xsfdg/BNPFwLI9TGR07yjd/pw57/9hcGwnCAlrv85TCPucxnTvOae+AINc+5znc+8zDUGo5VwbUJeE70os/8CNYaiAhaYPSmG90Ey57qTGgmuiw4/eo7lwBPP+ABrHtd5llYZKfxYlgQ4EAAaE+72tfO9ra7/e1tDwAFEjICuNv97nhfOw5Y+HGK9FMQeQ+84Nseg4UYYPCID7wgJI4of4nAB4mPPN7bt5AhFEDymG+7DzxuEvJ9IPOgZ3sCGDKCGYT+9NhE+Uj6hoEiAOD1sI+97GdP+9rbnvaD8ABDLhCE2/v+98CXfREICPRkoewBKwi+8pdfe0IAgSFQAALzp898E0Sw7w2hYA6oz/3gM0EOI1hI/xCk3/3y1z4HYsf+QoL2AvO7n/aESAMAFDKCLhDh/fh//Qv4nqz5FkLDBRCAAjiABFiABniACIiARlAHA3UQGuAFccACCTiBFFiBBqhmP6Ju8eIvDcADFviBIDiBLCAGcwAAtkMQEfAEVmAEVxaCLuiCPCBMERYuabE2UvCCOOiCAxAFZaAGcEAFsOcHf/AFZyCBOXiEFCgF2aR+BoE/OzAIUBiFUjiFVFiFVniFWBiFTFAFT/AFWoAFWKAFfUAHXEAEhJCFaJiGakiFO9A4J8IRciQDaziHdIiGhEAEVOAFT9AFesAFVRAEZ1iHgjiIMuBHUjcziiE6ITCIjMiIhP8QBERABEHABI1YiXMYAumXEUETAZbYiZ74iaA4CBHAfxfxQgAQiqiYiqp4hfOXgWPHEOnRAC2wirRYi6nYAg3QO4eoEP43AL74i8AYjMI4jMRYjMZ4jMiYjMq4jMtIUtQyXiDQAsw4jdRYjdZ4jdjoiy2wGP8GEV2DAQSQjeI4juRYjsZIAD/Xd0QCAiugAABACPAYj/I4j/RYj/Z4j/iYj/q4j/zYj/0IAAqwAsShjjYiAwowCP6YkAq5kAzZkA4Jj4OgAP2mAepYVSugATnwkBq5kRzZkfiYAw0gkDMYEaFiAhewAh6Zkiq5kgm5AhfgOCPpjeM1BA0QASx5kzj/iZMR0ABDoE/xAjwroARggQQ4l5NGeZQJeQBHgARZoQRxpoHrV1gPIAMt0AK9w1OQ4xAIYFhVWYjdSJKaIQIPYAIR0AJFsAM0QAMSsJZs2ZZu+ZZwGZdyOZd0WZd2eZd4KQFpuQNF0AIRYH0iYBpMyBMY0ACLcZLCFgGKuZiM2ZiO+ZiQGZmSOZmUWZmWeZmLKWwuORlP8YozcxVKMRcPMJqkWZqmeZqomZqquZqs2Zqu+ZqwaZofAAJOIRae+RA00RNAoQEa0AC++ZvAGZzCOZzEWZzGeZzImZzKuZy/yZtBMRRMiBA1gQDUWZ3WeZ3YmZ3auZ3c2Z3e+Z3gGZ7ZC1kTWVme5nmeGRMQADs=');
margin:0 auto;
}
</style>
</head>
<body>
	<div class="body-hld">
    	<div class="warn"></div><br /><br /><br />
        <div style="font-size: 16px;"><?php echo '<strong>'. $moduleName . ' v' . $version . ': '.'</strong>' . $error; ?></div>
	</div>
</body>
</html>
    
	<?php
	exit;
}

/*
 * <br /><br />
<div class="log">
 <div><?php echo $currentFile; ?></div>
</div>
*/