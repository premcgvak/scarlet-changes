<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

include_once('Mage/Catalog/controllers/Product/CompareController.php');

class Plumrocket_Privatesales_Catalog_Product_CompareController extends Mage_Catalog_Product_CompareController
{
    public function preDispatch()
    {
        parent::preDispatch();
		
		if (Mage::getSingleton('privatesales/splashpage')->getEnabledPage() && !Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }
}
