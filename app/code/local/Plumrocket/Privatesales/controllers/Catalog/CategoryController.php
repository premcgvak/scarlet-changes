<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

include_once('Mage/Catalog/controllers/CategoryController.php');

class Plumrocket_Privatesales_Catalog_CategoryController extends Mage_Catalog_CategoryController
{   
    public function viewAction()
    {
    	$category = $this->_initCatagory();
    	
    	// Category may be false.
    	if (Mage::getSingleton('privatesales/homepage_page')->getEnabledPage()) {
    		
    		// 404 if this catalog cannot showing
    		if (!$category) {
	    		if (!$this->getResponse()->isRedirect()) {
			    	$this->_forward('noRoute');
			    	return;
			    }
    		}
    		
	    	// redirect to 404 if this is weekly category
	    	if (Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_GROUP_OF_BOUTIQUES == $category->getPrivatesaleType()) {
			    if (!$this->getResponse()->isRedirect()) {
		            $this->_forward('noRoute');
		        }
		        return false;
	    	// forward if this is Menu item
	    	} elseif (Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_MENU_ITEM == $category->getPrivatesaleType()) {
	    		$this->_forward('homepage');
	    		return;
	    	}
	    	
	    	/*
	    	 * We will leave standart action for product list.
	    	 * Product list - it is page of boutiqe:
	    	 * Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_BOUTIQUE
	    	 */
	    	if ($category->getData('privatesale_is_link_to_product')) {
	    		// Redirect to product if this catalog is link to product
	    		header('Location: ' . $category->getUrl());
				exit;
	    	}
    	}
    	
    	/*
	     * We have protected this function from second executing
	     */
    	if (Mage::registry('current_category')) {
    		Mage::unregister('current_category');
    	}
		if (Mage::registry('current_entity_key')) {
    		Mage::unregister('current_entity_key');
    	}
    	return parent::viewAction();
    }
    
    public function homepageAction()
    {	
    	$this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }
}
