<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Adminhtml_EmailtemplateController extends Plumrocket_Privatesales_Controller_Adminhtml_Action
{

	protected $_modelLable	= 'Newsletter Template';
	protected $_modelKey 	= 'privatesales/emailtemplate';


	public function getCategoriesAction()
	{

		$_request 	= $this->getRequest();
		$storeId 	= $_request->getParam('store_id');
		$date		= $_request->getParam('date');

		$_result = array();
		if ($storeId && $date){
			$_categories	= $this->getModel()->loadCategoriesByCriteria($date, $storeId);
			$_result		= $this->getModel()->categoriesToOptions($_categories);
		}

	    $this->getResponse()->setBody(json_encode(array(
	    	'count'			=> count($_result),
	    	'categories' 	=> $_result,
	    )));
	}
}