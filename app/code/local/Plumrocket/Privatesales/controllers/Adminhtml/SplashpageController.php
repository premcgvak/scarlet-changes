<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Adminhtml_SplashpageController extends Mage_Adminhtml_Controller_Action
{

	public function init()
    {
		Mage::register('privatesales_custom_store_id', (int) $this->getRequest()->getParam('store'));
	}
	
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_PageController
     */
    protected function _initAction()
    {
        $this->loadLayout()
        	->_setActiveMenu('Plumrocket/privatesales');

        return $this;
    }

    /**
     * Edit CMS page
     */
    public function indexAction()
    {
    	$this->init();
		
        $this->_title($this->__('Splash Page'));
		// 5. Build edit form
        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('privatesales')->__('Plumrocket'), Mage::helper('privatesales')->__('Splash Page'));

        // Register model to use later in blocks
		$module = Mage::getModel('privatesales/splashpage');
		$module->setData('store', Mage::registry('privatesales_custom_store_id'));
        Mage::register('splash_page', $module);

        $this->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
    	$this->init();
		
        // check if data sent
        if (($data = $this->getRequest()->getPost()) && $this->_isAllowed()) {
            $data = $this->_filterPostData($data);

            //init model and set data
            $model = Mage::getModel('privatesales/splashpage');
            $model->setPostData($data);

            Mage::dispatchEvent('privatesales_splashpage_prepare_save', array('page' => $model, 'request' => $this->getRequest()));

            //validating
            if (!$this->_validatePostData($data)) {
                $this->_redirect('*/*/index', array('_current' => true));
                return;
            }

            // try to save it
            try {
                // save the data
                $model->save();

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('privatesales')->__('The page has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/index', array('_current'=>true));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->addException($e,
                    Mage::helper('cms')->__('An error occurred while saving the page.' . $e->getMessage()));
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/index', array());
            return;
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/save');
                break;
            case 'delete':
                return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales');
                break;
        }
    }

    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data)
    {
		return $data;
    }

    /**
     * Validate post data
     *
     * @param array $data
     * @return bool     Return FALSE if someone item is invalid
     */
    protected function _validatePostData($data)
    {
		return true;
    }
}