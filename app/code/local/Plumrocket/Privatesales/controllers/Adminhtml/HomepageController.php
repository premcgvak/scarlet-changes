<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Adminhtml_HomepageController extends Mage_Adminhtml_Controller_Action
{

	public function init()
    {
		Mage::register('privatesales_custom_store_id', (int) $this->getRequest()->getParam('store'));
	}
	
    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_PageController
     */
    protected function _initAction()
    {
        $this->loadLayout()
        	->_setActiveMenu('Plumrocket/privatesales');
			
        return $this;
    }

    /**
     * Edit CMS page
     */
    public function indexAction()
    {
        $this->_title($this->__('Home Page'));
		$this->init();

        // Register model to use later in blocks
		$module = Mage::getModel('privatesales/homepage_draft');
        
        $map = Mage::helper('privatesales/map')->getAdminCurrentMap(
        	'draft', 
        	$this->getRequest()->getParam('mid'),
        	$this->getRequest()->getParam('bid')
        );  
        $module->setColCount( $map->getColCount() );

		$module->setData('store', Mage::registry('privatesales_custom_store_id'));
        Mage::register('homepage', $module);
		
        $this->_initAction()
            ->_addBreadcrumb(Mage::helper('privatesales')->__('Plumrocket'), Mage::helper('privatesales')->__('Home Page'));

        $this->renderLayout();
    }

    /**
     * Save action
     */
    public function saveAction()
    {
    	$this->init();
		
        // check if data sent
        if (($data = $this->getRequest()->getPost()) && $this->_isAllowed()) {
			if ($this->save($data)){
				$this->success('The page has been saved.', true, $data);
				return;
			} else {
				$this->_redirect('*/*/index', array('_current' => true));
				return false;
			}
        }
        $this->_redirect('*/*/index');
    }
	
	private function save($data)
	{
		$data = $this->_filterPostData($data);
		
		//validating
		if (!$this->_validatePostData($data)) {
			return false;
		}

		// try to save it
		try {
			//init model and set data
			$model = Mage::getModel('privatesales/homepage_draft');
			$model->setPostData($data);
			$model->save();
			
			$from = Mage::helper('privatesales/map')->getAdminCurrentMap(
				'draft', 
				$this->getRequest()->getParam('mid'),
				$this->getRequest()->getParam('bid')
			);
			
			// if store not set already, then will be returned new empty element.
			$map = Mage::helper('privatesales/map')->getAdminCurrentMap(
				'draft', 
				$this->getRequest()->getParam('mid'),
				$this->getRequest()->getParam('bid'),
				true
			);
			
			if ($from->getColCount() != $data['col_count']) {
				$map->setData('post_data', array('top' => array()));
			} else {
				$map->setData('post_data', $data['map']);
			}
			$map->setData('store', $data['store']);
			$map->setData('col_count', $data['col_count']);
			$map->setData('enabled_ending_soon', $data['enabled_ending_soon']);
			$map->setData('ending_count_days', $data['ending_count_days']);
			$map->setData('enabled_upcoming_soon', $data['enabled_upcoming_soon']);
			$map->save();
	
			Mage::dispatchEvent('privatesales_homepage_prepare_save', array('page' => $model, 'request' => $this->getRequest()));

			return true;
		} catch (Mage_Core_Exception $e) {
			$this->_getSession()->addError($e->getMessage());
		}
		catch (Exception $e) {
			$this->_getSession()->addException($e,
				Mage::helper('cms')->__('An error occurred while saving the page.' . $e->getMessage()));
		}
		return false;
	}
	
	/**
     * Publish action
     */
    public function publishAction()
    {
    	$this->init();
		
        // check if data sent
        if (($data = $this->getRequest()->getPost()) && $this->_isAllowed()) {
			if ($this->save($data)){
				// try to save it
				try {
					$data = $this->_filterPostData($data);
					
					// init model and set data
					$model = Mage::getModel('privatesales/homepage_page');
					$model->setPostData($data);
					$model->save();
					
					$from = Mage::helper('privatesales/map')->getAdminCurrentMap('draft', 
						$this->getRequest()->getParam('mid'),
						$this->getRequest()->getParam('bid'));
					$to = Mage::helper('privatesales/map')->getAdminCurrentMap('public', 
						$this->getRequest()->getParam('mid'), 
						$this->getRequest()->getParam('bid'), true);

					$to->setData('map', $from->getData('map'));
					$to->setData('store', $data['store']);
					$to->setData('col_count', $from->getData('col_count'));
					$to->setData('enabled_ending_soon', $from->getData('enabled_ending_soon'));
					$to->setData('ending_count_days', $from->getData('ending_count_days'));
					$to->setData('enabled_upcoming_soon', $from->getData('enabled_upcoming_soon'));
					$to->save();
					
					$this->success('The page has been saved and published.', true, $data);
					return;
				} catch (Mage_Core_Exception $e) {
					$this->_getSession()->addError($e->getMessage());
				}
				catch (Exception $e) {
					$this->_getSession()->addException($e,
						Mage::helper('cms')->__('An error occurred while publishing the page.' . $e->getMessage()));
				}
			} else {
				$this->_redirect('*/*/index', array('_current' => true));
				return false;
			}
        }
        $this->_redirect('*/*/index');
    }
	
	
	/**
     * Publish action
     */
    public function cancelAction()
    {
    	$this->init();
		
		// try to save it
		try {
			$from = Mage::helper('privatesales/map')->getAdminCurrentMap('public', 
				$this->getRequest()->getParam('mid'),
				$this->getRequest()->getParam('bid'));
			$to = Mage::helper('privatesales/map')->getAdminCurrentMap('draft', 
				$this->getRequest()->getParam('mid'), 
				$this->getRequest()->getParam('bid'), true);

			$to->setData('map', $from->getData('map'));
			$to->setData('store', $data['store']);
			$to->setData('col_count', $from->getData('col_count'));
			$to->setData('enabled_ending_soon', $from->getData('enabled_ending_soon'));
			$to->setData('ending_count_days', $from->getData('ending_count_days'));
			$to->setData('enabled_upcoming_soon', $from->getData('enabled_upcoming_soon'));
			$to->save();
			
			$this->success('The page has been restored from public page.', false);
			return;
		} catch (Mage_Core_Exception $e) {
			$this->_getSession()->addError($e->getMessage());
		}
		catch (Exception $e) {
			$this->_getSession()->addException($e,
				Mage::helper('cms')->__('An error occurred while canceling the page.' . $e->getMessage()));
		}
    }
	
	private function success($message, $setFormData = true, $data = '') 
	{
		// display success message
		Mage::getSingleton('adminhtml/session')->addSuccess(
			Mage::helper('privatesales')->__($message));
		// clear previously saved data from session
		Mage::getSingleton('adminhtml/session')->setFormData(false);
		
		if ($setFormData) {
			$this->_getSession()->setFormData($data);
		}
		// check if 'Save and Continue'
		if ($this->getRequest()->getParam('back')) {
			$this->_redirect('*/*/index', array('_current'=>true));
			return;
		}
		$this->_redirect('*/*/index', array());
		return;
	}

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        switch ($this->getRequest()->getActionName()) {
            case 'new':
            case 'save':
                return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/save');
                break;
            case 'delete':
                return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/delete');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/save');
                break;
        }
    }

    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data)
    {
		if (!isset($data['map'])) {
			$data['map'] = array('top' => array());
		}
		return $data;
    }

    /**
     * Validate post data
     *
     * @param array $data
     * @return bool     Return FALSE if someone item is invalid
     */
    protected function _validatePostData($data)
    {
		return true;
    }
	
	public function previewAction()
    {
    	$this->init();
		
		// category id
		$cid = (int)$this->getRequest()->getParam('cid');
		// product id
		$pid = (int)$this->getRequest()->getParam('pid');
		// see draft
		$draft = (int)$this->getRequest()->getParam('draft');
		// code
		$code = md5(time() . time() . 'any salt');
		
		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		
		// clear 
		$connection->query(sprintf("DELETE FROM `%s` WHERE active_to < '%s';",
			Mage::getSingleton('core/resource')->getTableName('privatesales_preview_access'),
			strftime('%F %T', Mage::getModel('core/date')->timestamp(time()))
		));
		
		// insert new
		$connection->query(sprintf("INSERT INTO %s (`code`, `active_to`) VALUES ('%s', '%s');",
			Mage::getSingleton('core/resource')->getTableName('privatesales_preview_access'),
			$code,
			strftime('%F %T', Mage::getModel('core/date')->timestamp(time() + 60))
		));
		
		$this->_redirect('privatesales/homepage/offset/offset/0/cid/' . $cid . '/pid/' . $pid . '/draft/' . $draft . '/ssd/'. $code . '/');
    }
}