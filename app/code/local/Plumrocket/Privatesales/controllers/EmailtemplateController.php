<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_EmailtemplateController extends Mage_Core_Controller_Front_Action
{

	protected $_modelKey 	= 'privatesales/emailtemplate';
	
	
	protected function _initAction($template)
	{
		$_request = $this->getRequest();
		if ($_request->getParam('id') && $_request->getParam('secret')){
			$_model = Mage::getModel($this->_modelKey)->load($_request->getParam('id'));
			if ($_model->getSecret() == $_request->getParam('secret')){
				Mage::register('current_model', $_model);
				
				$this->getResponse()->setBody(
					$this->getLayout()->createBlock('privatesales/emailtemplate')->setTemplate($template)->toHtml()
				);
				return $this;
			}
		}

		
		$this->_forward('no-route');
	}


	public function generateAction()
	{
		if (Mage::getSingleton('privatesales/homepage_page')->getEnabledPage()) {
			$this->_initAction('privatesales/emailtemplate/generate.phtml');
		}
	}


	public function previewAction()
	{
		if (Mage::getSingleton('privatesales/homepage_page')->getEnabledPage()) {
			$this->_initAction('privatesales/emailtemplate/preview.phtml');
		}
	}
}