<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_HomepageController extends Mage_Core_Controller_Front_Action
{
	
    public function indexAction()
    {
		if (!(Mage::getSingleton('privatesales/homepage_page')->getEnabledPage())) {
			$this->_forward('index', 'index', 'cms');
			return;
		}
		
		$this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }
    
    public function offsetAction()
    {
    	if (! Mage::getSingleton('privatesales/homepage_page')->getEnabledPage()) {
    		echo 'SUCCESS';
    	}
    	
    	$replaceFrom = '';
    	$replaceTo = '';

    	if (Mage::getStoreConfig('web/url/use_store')) {
    		$activeStoreCode = Mage::app()->getStore()->getCode();

    		if ($activeStoreCode == Mage_Core_Model_Store::ADMIN_CODE) {
    			$replaceFrom = '/' . Mage_Core_Model_Store::ADMIN_CODE;
    			$replaceTo = '/' . Mage_Core_Model_Store::DEFAULT_CODE;
    		}
    	}

		// Verify if the user is logged in to the backend		
		$offset = $this->getRequest()->getParam('offset');
		$ssd = $this->getRequest()->getParam('ssd');
		// @TODO
		$store = (int)$this->getRequest()->getParam('store');

		$isAjax = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']))
			&& (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
		
		if ((int)Mage::getStoreConfig('privatesales/all/future')) {
	    	if ($ssd) {
				$data = Mage::getSingleton('core/resource')->getConnection('core_read')
					->fetchAll(sprintf("SELECT `code` FROM %s WHERE `code` = %s AND `active_to` >= '%s'",
						Mage::getSingleton('core/resource')->getTableName('privatesales_preview_access'),
						Mage::getSingleton('core/resource')->getConnection('default_write')->quote($ssd),
						strftime('%F %T', Mage::getModel('core/date')->timestamp(time()))
					));
				
				if (! $data) {
					$offset = 'no';
				}
	    	}
			
			if ($offset || $offset == '0') {
	
				if ($offset == 'date') {
					$time = strtotime(Mage::helper('privatesales/list')->_formatDate(
						$this->getRequest()->getParam('date'), 'MMM d\, yy')
					) - time();
					
					if ($time > 0) {
						$offset = ($time / (3600 * 24)) + 1;
					} else {
						$offset = 0;
					}
				}
	
				Mage::getSingleton('core/session')->setData('debug_offset_time_on', (int)($offset !== 'no'));
				Mage::getSingleton('core/session')->setData('debug_offset_time_on_days', (int)$offset);
				if (! is_null($this->getRequest()->getParam('draft'))) {
					Mage::getSingleton('core/session')->setData('debug_offset_time_is_draft', (int)$this->getRequest()->getParam('draft'));
				}
	
				// check ajax
				if (! $isAjax) {
					
					// redirect to catalog
					if ($catId = (int)$this->getRequest()->getParam('cid')) {
						$cat = Mage::getModel('catalog/category')->load($catId);
						header('Location: ' . $this->_formatUrl($cat->getUrl(), $replaceFrom, $replaceTo));
						exit;
					}
					
					// redirect to the product
					if ($prodId = (int)$this->getRequest()->getParam('pid')) {
						$_product = Mage::getModel('catalog/product')->load($prodId);
						header('Location: ' . $this->_formatUrl(Mage::getUrl($_product->getUrlPath()), $replaceFrom, $replaceTo));
						exit;
					}
					
					header('Location: ' . $this->_formatUrl(Mage::getUrl('/'), $replaceFrom, $replaceTo));
				}
			}
		}
		if (! $isAjax) {
			header('Location: ' . $this->_formatUrl(Mage::getUrl('/'), $replaceFrom, $replaceTo));
		}
		echo 'SUCCESS';
	}

	private function _formatUrl($url, $replaceFrom, $replaceTo)
	{
		if ($replaceFrom) {
			$url = str_replace($replaceFrom, $replaceTo, $url);
		}
		return $url;
	}
}
