<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

include_once('Mage/Checkout/controllers/CartController.php');

class Plumrocket_Privatesales_Checkout_CartController extends Mage_Checkout_CartController
{
	public function preDispatch()
    {
        parent::preDispatch();
		
		if (Mage::getSingleton('privatesales/splashpage')->getEnabledPage() && !Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }
	
	public function indexAction()
	{
		parent::indexAction();
		
		if (!Mage::helper('privatesales')->moduleEnabled()) {
			return;
		}

		$_quote = Mage::getSingleton('checkout/type_onepage')->getQuote();
		if ((int)Mage::getStoreConfig('privatesales/shipping/set_default')
			&& Mage::getStoreConfig('privatesales/shipping/code_default')
			&& $_quote->getId()
			&& !$_quote->getShippingAddress()->getCountryId()
		) {
			$customerSession = Mage::getSingleton("customer/session");
			
			if ($customerSession->isLoggedIn()) {
				$customerAddress = $customerSession->getCustomer()->getDefaultShippingAddress();
				if ($customerAddress && $customerAddress->getId()){
					$customerCountry = $customerAddress->getCountryId();
					$shipping = $_quote
						->getShippingAddress()
						->setCountryId($customerCountry)
						->setRegionId( $customerAddress->getRegionId() )
						->setShippingMethod( Mage::getStoreConfig('privatesales/shipping/code_default') )
						->save();
				} else {
					$shipping = $_quote
						->getShippingAddress()
						->setCountryId( Mage::getStoreConfig('privatesales/shipping/country_default') )
						->setShippingMethod( Mage::getStoreConfig('privatesales/shipping/code_default') )
						->save();
				}
				Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('checkout/cart/'));
			}
			
		}
	}
}
