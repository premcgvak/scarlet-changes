<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

include_once('Mage/Customer/controllers/AccountController.php');

class Plumrocket_Privatesales_Customer_AccountController extends Mage_Customer_AccountController
{
	protected $_splashPage = false;

	public function preDispatch()
    {
    	$this->_splashPage = Mage::getSingleton('privatesales/splashpage');

        // a brute-force protection here would be nice
        Mage_Core_Controller_Front_Action::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = $this->getRequest()->getActionName();
        $openActions = array(
            'create',
            'login',
            'logoutsuccess',
            'forgotpassword',
            'forgotpasswordpost',
            'changeforgotten',
            'resetpassword',
            'resetpasswordpost',
            'confirm',
            'confirmation',
			// added this action
			'response'
        );
        $pattern = '/^(' . implode('|', $openActions) . ')/i';

        if (!preg_match($pattern, $action)) {
            if (!$this->_getSession()->authenticate($this)) {
                $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }
	
	public function postDispatch()
    {
        Mage_Core_Controller_Front_Action::postDispatch();
		
		if ($this->_splashPage->getEnabledPage()) {
			// get action
			$action = $this->getRequest()->getActionName();
		
			// set actions list, that redirect to ajax response page if request is ajax
			$openActions = array(
				'forgotpasswordpost',
				'loginpost',
				'createpost'
			);
			$pattern = '/^(' . implode('|', $openActions) . ')/i';
			
			// check ajax
			if ((preg_match($pattern, $action))
				&& (!empty($_SERVER['HTTP_X_REQUESTED_WITH']))
				&& (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'))
			{
				$this->_redirect('*/*/response');
				return;
			}
		}
		
		$this->_getSession()->unsNoReferer(false);
    }
	
	public function loginAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
		
		// rollback template's options
		if ($this->_splashPage->getEnabledPage()) {
			$this->_forward('login', 'splashpage', 'privatesales');
			return;
		}
		
		$this->getResponse()->setHeader('Login-Required', 'true');
        $this->loadLayout();
		
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    public function loginPostAction()
    {
		$session = $this->_getSession();
		
		if ($this->_splashPage->getEnabledPage()) {
			if ($this->_splashPage->getEnabledLaunchingSoon()) {
				$session->addError( Mage::helper('privatesales')->__('Login is disabled') );
				$this->_redirect('*/*/login');
				return;
			}
		}
		
		parent::loginPostAction();
    }
	
	public function logoutAction()
    {
		if ($this->_splashPage->getEnabledPage()) {
	        $this->_getSession()->logout()
	            ->setBeforeAuthUrl(Mage::getUrl());

	        $this->_redirect('*/*/login');
	    } else {
	    	parent::logoutAction();
	    }
    }


    public function createAction()
    {
		$session = $this->_getSession();
		
		if ($this->_splashPage->getEnabledPage()) {		
			if (!$this->_splashPage->getEnabledRegistration()) {
				$session->addError( Mage::helper('privatesales')->__('Registration is disabled') );
				$this->_redirect('*/*/login');
				return;
			}
			
			if ($this->_splashPage->getIncludeRegistration() == Plumrocket_Privatesales_Model_Splashpage::INCLUDE_STATUS_AJAX) {
				$this->_redirect('*/*/login');
				return;
			}
		}
		
		parent::createAction();
    }

    public function createPostAction()
    {
		$session = $this->_getSession();
			
		if ($this->_splashPage->getEnabledPage()) {
			if (!$this->_splashPage->getEnabledRegistration()) {
				$session->addError( Mage::helper('privatesales')->__('Registration is disabled') );
				$this->_redirect('*/*/login');
				return;
			}
		}
		
		parent::createPostAction();
		
		// if enabled launching soon
		if ($this->_splashPage->getEnabledPage() && $this->_splashPage->getEnabledLaunchingSoon()) {
			$session->logout();
		}
    }
	
	public function forgotpasswordAction()
    {
		if ($this->_splashPage->getEnabledPage()) {	
			if (($this->_splashPage->getIncludeForgot() == Plumrocket_Privatesales_Model_Splashpage::INCLUDE_STATUS_AJAX)
				|| ($this->_splashPage->getEnabledLaunchingSoon())
			) {
				$this->_redirect('*/*/login');
				return;
			}
		}
		
		parent::forgotpasswordAction();
    }
	
	public function forgotpasswordpostAction()
    {
		if ($this->_splashPage->getEnabledPage()) {
			if ($this->_splashPage->getEnabledLaunchingSoon()) {
				$this->_redirect('*/*/login');
				return;
			}
		}
		
		parent::forgotpasswordpostAction();
	}
	
	public function responseAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
	}
}
