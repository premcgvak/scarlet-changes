<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Helper_List extends Mage_Core_Helper_Abstract
{
	protected $_defaultResultForEncodedMethods = array(
		'getAllBoutiques'	=> array(),
		'getSizes'			=> array(),
		'getActiveBanner'	=> false,
		'_formatDate'		=> '0000-00-00',
		'getTime'			=> 0,
		'getMinLimitQty'	=> 0,
		'getMenuItems'		=> array(),
		'getTree'			=> array(),
		'getEndingTree'		=> array(),
		'getSocialLinkFromRelative' => '',
		// deprecated
		'getTimeZoneOffset'	=> 0,
	);

	protected $_encodedHelper = 'privatesales/listEncoded';

	public function __call($name, $arguments)
	{
		if (Mage::helper('privatesales')->moduleEnabled() || Mage::app()->getStore()->isAdmin()) {
			$encodedHelper = Mage::helper($this->_encodedHelper);
			if (method_exists($encodedHelper, $name)) {
				return call_user_func_array(array($encodedHelper, $name), $arguments);
			}
		}

		return (array_key_exists($name, $this->_defaultResultForEncodedMethods))
			? $this->_defaultResultForEncodedMethods[$name]
			: false;
	}
}