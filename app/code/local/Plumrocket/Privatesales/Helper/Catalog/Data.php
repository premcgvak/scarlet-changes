<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Helper_Catalog_Data extends Mage_Catalog_Helper_Data
{
   
    public function getBreadcrumbPath()
    {
        if (! Mage::helper('privatesales')->moduleEnabled()) {
            return parent::getBreadcrumbPath();
        }

        if (!$this->_categoryPath) {
            $path = array();
            if ($category = Mage::registry('current_category')) {
                $pathInStore = $category->getPathInStore();
                $pathIds = array_reverse(explode(',', $pathInStore));

                //$categories = $category->getParentCategories();
                $cats = Mage::getModel('catalog/category')->getCollection()
					->addFieldToFilter('entity_id', array('in' => $pathIds))
					->addFieldToFilter('is_active', 1)
					->addAttributeToSelect('privatesale_type')
					->addAttributeToSelect('name')
					->addAttributeToSelect('url_key');
					
				foreach($cats as $cat) {
					$categories[$cat->getId()] = $cat;
				}
                
                // add category path breadcrumb
                foreach ($pathIds as $categoryId) {
                    if (isset($categories[$categoryId]) && $categories[$categoryId]->getName() && $categories[$categoryId]->getPrivatesaleType() != Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_GROUP_OF_BOUTIQUES) {
                        $path['category'.$categoryId] = array(
                            'label' => $categories[$categoryId]->getName(),
                            'link' => $this->_isCategoryLink($categoryId) ? $categories[$categoryId]->getUrl() : ''
                        );
                    }
                }
            }

            if ($this->getProduct()) {
                $path['product'] = array('label'=>$this->getProduct()->getName());
            }

            $this->_categoryPath = $path;
        }
        return $this->_categoryPath;
    }
    
}
