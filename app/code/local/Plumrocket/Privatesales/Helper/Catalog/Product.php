<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Helper_Catalog_Product extends Mage_Catalog_Helper_Product
{
	
    public function initProduct($productId, $controller, $params = null)
    {
    	if (! Mage::helper('privatesales')->moduleEnabled()) {
    		return parent::initProduct($productId, $controller, $params);
    	}
    	
    	Mage::getSingleton('catalog/session')->setLastVisitedCategoryId(0);
		
		$product = parent::initProduct($productId, $controller, $params);
		$category = Mage::registry('current_category');
		
		// if address of product not contain catalog id
		if (!$category && $product && ($product->getId() > 0)) {
			$category = $this->initCurrentCategoryForProduct($product, $controller, $params);
		}
		
		if (!$category || !$category->getIsActive(false)) {
			return false;
		}
		return $product;
    }
	
	public function initCurrentCategoryForProduct($product, $controller, $params, $mustCheckTime = false)
	{
		$category = $this->_getParentCategory($product, $mustCheckTime);
		if ($category) {
			$params->setCategoryId($category->getId());
			
			Mage::unregister('current_category');
    		Mage::unregister('current_product');
    		Mage::unregister('product');
			
			$product = parent::initProduct($product->getId(), $controller, $params);
        }
		return $category;
	}
	
	protected function _getParentCategory($_product, $mustCheckTime = false)
	{
		$catsIds = $_product->getCategoryIds($_product);
		$_category = false;
		// current of iterator time
		$currTime = 0;
		
		$_cats = Mage::helper('privatesales/list')->getAllBoutiques(true);
		foreach ($_cats as $cat) {
			if (in_array($cat->getId(), $catsIds)) {
				if ($cat->getIsActive($mustCheckTime)) {
					$time = Mage::helper('privatesales/list')->getTime($cat->getPrivatesaleDateEnd());
					// if previous catalogs were without param date_end OR this catalog ending earlier than previous
					if ($currTime == 0 || ($time < $currTime)) {
						$currTime = $time;
						$_category = $cat;
					}
				} elseif ($_category === false) {
					$_category = $cat;
				}
			}
		}
		return $_category;
	}
}
