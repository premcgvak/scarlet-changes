<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Helper_MapEncoded extends Mage_Core_Helper_Abstract
{
	public function getMap($_map, $cats)
	{
		$mustBeByType = array('top' => 0, 'middle' => 0, 'bottom' => 0);
		foreach ($mustBeByType as $sec => $tmp) {
			$count = 0;
			if (!isset($_map[$sec])) {
				continue;
			}
			foreach ($_map[ $sec ] as $key => $row) {
				if ($key == '_count_' || !$row || !is_array($row)) {
					continue;
				}
				foreach ($row as $item) {
					if ($item['type'] == 1) {
						$count++;
					}
				}
			}
			$mustBeByType[$sec] = $count;
		}
		
		$beType = array('top' => 0, 'middle' => 0, 'bottom' => 0);
		$count = count($cats);
		
		if ($count > $mustBeByType['top']) {
			$beType['top'] = $mustBeByType['top'];
			// for middle and bottom
			$count = $count - $mustBeByType['top'];
			
			if ($count > $mustBeByType['bottom']) {
				$beType['bottom'] = $mustBeByType['bottom'];
				$count = $count - $mustBeByType['bottom'];
				
				if ($count > 0) {
					// for bottom
					if ($mustBeByType['middle'] !== 0) {
						$ost = $count % $mustBeByType['middle'];
						if ($ost > 0) {
							$ost = $mustBeByType['middle'] - $ost;
						}
					} else {
						$ost = 0;
					}
					// we have to decrement bottom for increment middle (to full).
					if ($ost > 0) {
						// descrement :bottom
						if ($ost <= $beType['bottom']) {
							$beType['middle'] = $count + $ost;
							$beType['bottom'] = $beType['bottom'] - $ost;
						// clear :bottom
						} else {
							$beType['middle'] = $count + $beType['bottom'];
							$beType['bottom'] = 0;
						}
					} else {
						$beType['middle'] = ($mustBeByType['middle'] !== 0)? $count : 0;
					}
				} 
			} else {
				$beType['bottom'] = $count;
			}
		} else {
			$beType['top'] = $count;
		}
		
		$map = array();
		// mul middle to count
		foreach ($beType as $type => $items) {
			$map[$type] = array();
			if ($count = $beType[$type]) {
				
				$result = array();
				$rid = 0;
				
				while ($count > 0) {
					foreach ($_map[$type] as $key => $row) {
						if ($key == '_count_' || !$row || !is_array($row) && $count < 1) {
							continue;
						}
						$rid++;
						foreach ($row as $item) {
							if ($item['type'] == 1) {
								if ($count == 0) {
									break;
								}
								$count--;
							}
							$result[$rid][] = $item;
						}
					}
				}
				$map[$type] = $result;
			}
		}
		
		return $map;
	}
	
	public function getCurrentMap($block = 'main', $type = 'public')
	{
		if (Mage::getSingleton('core/session')->getData('debug_offset_time_is_draft')) {
			$type = 'draft';
		}
		
		$map = false;
		if ($cat = Mage::registry('current_category')) {
			$address = $this->filterMapId( $cat->getId() ) . '/' . $block . '/' . $type;
			$map = $this->getMapByAddress($address);
		} else if (Mage::getModel('privatesales/homepage_page')->getDefaultHomepage() > 0) {
			$address = $this->filterMapId( Mage::getModel('privatesales/homepage_page')->getDefaultHomepage() ) . '/' . $block . '/' . $type;
			$map = $this->getMapByAddress($address);
		}
		
		if (!$map) {
			$address = '*/' . $block . '/' . $type;
			$map = $this->getMapByAddress($address);
		}
		
		if (!$map && ($block != 'main')) {
			$address = '*/main/' . $type;
			$map = $this->getMapByAddress($address);
		}
		
		if (!$map) {
			// create new
			$map = Mage::getModel('privatesales/map');
			$map->setData('map_name', $address);
		}
		
		return $map;
	}
	
	public function getAdminCurrentMap($type = 'public', $mid = '*', $bid = 'main', $returnNone = false)
	{
		$mid = $this->filterMapId($mid);
		$bid = $this->filterBlockId($bid);
		$map = false;
		
		$address = $mid . '/' . $bid . '/' . $type;
		$map = $this->getMapByAddress($address);
		
		if (!$returnNone && !$map) {
			if ($mid != '*') {
				$address = '*/' . $bid . '/' . $type;
				$map = $this->getMapByAddress($address);
			}
			
			if (!$map && ($bid != 'main')) {
				$address = '*/main/' . $type;
				$map = $this->getMapByAddress($address);
			}
		}
		
		if (!$map) {
			// create new
			$map = Mage::getModel('privatesales/map');
			$map->setData('map_name', $address);
		}
		
		return $map;
	}
	
	public function getMapByAddress($address)
	{
		$stores[] = '0';
		if ($store_id = Mage::app()->getStore()->getStoreId()) {
			$stores[] = $store_id;
		}
		if ($store_id = (int)Mage::registry('privatesales_custom_store_id')) {
			$stores[] = $store_id;
		}

		$result = Mage::getModel('privatesales/map')
			->getCollection()
			->addFieldToFilter('map_name', $address)
			->addFieldToFilter('store_id', array('in' => $stores))
			->setOrder('store_id', 'DESC')
			->getFirstItem();

		return ($result->getId())? $result: false;
	}
	
	public function filterMapId($mid) 
	{
		$mid = preg_replace('/[^\w]*/', '', $mid);
		if (!$mid) {
			$mid = '*';
		}
		return $mid;
	}
	
	public function filterBlockId($bid)
	{
		$bid = preg_replace('/[^\w]*/', '', $bid);
		if (!in_array($bid, array('main', 'end'))) {
			$bid = 'main';
		}
		return $bid;
	}
}