<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Helper_Data extends Plumrocket_Privatesales_Helper_Main
{
	private $_activeHomepage = false;

	public function moduleEnabled($store = null)
	{
		return (bool)Mage::getStoreConfig('privatesales/general/enable', $store);
	}

	public function disableExtension()
	{
		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		$connection->delete('core_config_data', array($connection->quoteInto('path IN (?)', array('privatesales/general/enable', 'privatesales/all/future', 'privatesales/product_list/minimum_count_for_alert', 'privatesales/boutique_settings/end_action'))));
		$config = Mage::getConfig();
		$config->reinit();
		Mage::app()->reinitStores();
	}
	
	public function getActiveHomepage()
	{
		if ($this->_activeHomepage === false) {
			// Filter by homepage
			// This registry key assigned only catalog page. And this fucntion called only homepage and Menu item
			$activeCatalog = Mage::registry('current_category');
			$this->_activeHomepage = 0;
			
			if ($activeCatalog) {
				$this->_activeHomepage = $activeCatalog->getId();
				if ($activeCatalog->getData('privatesale_type') != Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_MENU_ITEM) {
					$menuItems = Mage::helper('privatesales/list')->getMenuItems();
					
					foreach ($menuItems as $menuItem) {
						$this->_activeHomepage = $menuItem->getId();
						if ($activeCatalog->isParentCatalogById($this->_activeHomepage)) {
							break;
						}
					}
				}
			}
			
			if ($this->_activeHomepage === 0) {
				if (Mage::getSingleton('customer/session')->getCustomer()->getGender() == 1) {
					$this->_activeHomepage = Mage::getModel('privatesales/homepage_page')->getDefaultHomepageForMan();
				} elseif (Mage::getSingleton('customer/session')->getCustomer()->getGender() == 2) {
					$this->_activeHomepage = Mage::getModel('privatesales/homepage_page')->getDefaultHomepageForWoman();
				}
			}
			if ($this->_activeHomepage == 0) {
				$this->_activeHomepage = Mage::getModel('privatesales/homepage_page')->getDefaultHomepage();
			}
		}
		return $this->_activeHomepage;
	}
}