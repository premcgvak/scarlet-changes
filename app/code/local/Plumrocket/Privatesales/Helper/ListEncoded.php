<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Helper_ListEncoded extends Plumrocket_Privatesales_Helper_Main
{
	private $usedBanners = array(); 
	private $menus = array();
	private $catSource = array();
	private $sizes = array();
	private $banners = array();
	
	private $loadedInactive = false;

	private function getCatsSource($includeInactive = false, $storeId = null)
	{
		if (!$this->catSource || ($includeInactive !== $this->loadedInactive)) {
			if (is_null($storeId)){
				$storeId = Mage::app()->getStore()->getStoreId();
			}
		
			$this->catSource = Mage::getModel('catalog/category')
				->getCollection()
				->setStoreId($storeId)
				->addFieldToFilter('privatesale_type', Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_BOUTIQUE)
				->addAttributeToSelect('*')
				->setOrder('position', 'ASC');
				
			if (! $includeInactive) {
				$this->catSource->addFieldToFilter('is_active', 1);
			}
			$this->catSource->load();
			
			$this->loadedInactive = $includeInactive;
		}
		return $this->catSource;
	}
	
	public function getAllBoutiques($includeInactive = false, $storeId = null)
	{
		if (is_null($storeId)){
			$storeId = Mage::app()->getStore()->getStoreId();
		}
		$rootId = Mage::app()->getStore($storeId)->getRootCategoryId();
		$_cats = $this->getCatsSource($includeInactive, $storeId);
		$cats = array();
		
		foreach ($_cats as $cat) {
			if ($cat->isParentCatalogById($rootId)) {
				$cats[] = $cat;
			}
		}
			
		return $cats;
	}
	
	public function getSizes()
	{
		if (!$this->sizes) {
			$_sizes = Mage::getModel('privatesales/size')
				->getCollection()
				->load();
				
			foreach ($_sizes as $size) {
				$this->sizes[ $size->getType() ][ $size->getName() ] = $size;
			}
		}
		return $this->sizes;
	}
	
	function getActiveBanner($item, $type)
	{
		$items = $item['banners'];
		if (!$this->banners) {
			$this->banners = Mage::getModel('privatesales/homepage_parent')->getBanners();
		}
		
		// set of id of active banners into block 
		$activeIds = array();
		$time = Mage::getModel('core/date')->timestamp(time());
		foreach ($items as $item) {
			$from = $this->getTime( $this->_formatDate($item['date_from']) );
			$to = $this->getTime( $this->_formatDate($item['date_to']) );
			
			if (($from == 0 || $from < $time) && ($to == 0 || $to > $time)) {
				$activeIds[] = $item['id'];
			}
		}
		
		$activeId = false;
		// select id from active ids
		if ($type == 'middle') {
			foreach ($activeIds as $id) {
				if (!in_array($id, $this->usedBanners)) {
					$activeId = $id;
					$this->usedBanners[] = $id;
					break;
				}
			}
		} else {
			$activeId = ($activeIds)? $activeIds[0]: false;
		}
		
		if ($activeId !== false) {
			foreach ($this->banners as $b) {
				if ($b->getId() == $activeId) {
					return $b;
				}
			}
		}
		return false;
	}
	
	public function _formatDate($value, $dateFormat = '')
	{
		if (!$dateFormat) {
			$dateFormat = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
		}
		if (!empty($value)) {
			/*
			$filterInput    = new Zend_Filter_LocalizedToNormalized(array(
				'date_format' => $dateFormat
			));
			$filterInternal = new Zend_Filter_NormalizedToLocalized(array(
				'date_format' => Varien_Date::DATE_INTERNAL_FORMAT
			));
			$value = $filterInternal->filter($filterInput->filter($value));
			*/
			$value = new Zend_Date($value, $dateFormat, 'en');
			$value = $value->get(Varien_Date::DATE_INTERNAL_FORMAT);	
			$value = ($value == '1999-11-30')? '0000-00-00' : $value;
		} else {
			$value = '0000-00-00';
		}
		return $value;
	}
	
	public function getTime($value, $allowOffset = true, $offset = null)
	{
		if (is_null($offset)){
			$offset = (int)Mage::getSingleton('core/session')->getData('debug_offset_time_on_days') * 24 * 3600;
		
			if (!(int)Mage::getStoreConfig('privatesales/all/future')
				|| !(int)Mage::getSingleton('core/session')->getData('debug_offset_time_on')
			) {
				$allowOffset = false;
			}
		}
		
		if (!empty($value)) {
			$time = strtotime($value);
			if ($allowOffset) {
				$time = $time - $offset;
			}
			return ($time > 0)? $time: 0;
		}
		return 0;
	}
	
	public function getMinLimitQty()
	{
		return Mage::getModel('privatesales/homepage_page')->getMinLimitQty();
	}
	
	public function getMenuItems($zone = 'client')
	{
		if (!$this->menus) {
			$storeId = Mage::app()->getStore()->getStoreId();
			$rootId = Mage::app($storeId)->getStore()->getRootCategoryId();
			
			$_cats = Mage::getModel('catalog/category')
				->getCollection()
				->setStoreId($storeId)
				->addFieldToFilter('is_active', 1)
				->addFieldToFilter('privatesale_type', Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_MENU_ITEM)
				->addAttributeToSelect('*')
				->setOrder('position', 'ASC')
				->load();
				
			$this->menus = array();
			foreach ($_cats as $cat) {
				if ($cat->isParentCatalogById($rootId) || $zone == 'admin') {
					$this->menus[] = $cat;
				}
			}
		}
		return $this->menus;
	}
	
	public function getTree()
	{
		// reserved for "All"
		$result = array();
		$menus = $this->getMenuItems();
		$cats = $this->getCatsSource();
		
		foreach ($menus as $menu) {
			$mid = $menu->getId();
			$result[$mid] = array();
			
			foreach ($cats as $cat) {
				if ($cat->getIsActive(true) && $cat->isParentCatalogById($mid)) {
					$result[$mid][] = $cat;
				}
			}
		}
		
		return $result;
	}
	
	public function getEndingTree()
	{
		// reserved for "All"
		$result = array();
		$menus = $this->getMenuItems();
		$cats = $this->getCatsSource();
					
		$time = Mage::getModel('core/date')->timestamp(time());
		$map = Mage::helper('privatesales/map')->getMapByAddress('*/main/public');
		$period = $map->getEndingCountDays() * 3600 * 24;
			
		foreach ($menus as $menu) {
			$mid = $menu->getId();
			$result[$mid] = array();
			
			foreach ($cats as $cat) {
				if ($cat->getIsActive(true) && $cat->isParentCatalogById($mid)) {
					$timeEnd = $this->getTime($cat->getPrivatesaleDateEnd());
					if ($timeEnd != 0 && (($timeEnd - $time) < $period)) {
						$result[$mid][] = $cat;
					}
				}
			}
		}
		
		return $result;
	}
	
	public function getSocialLinkFromRelative($origUrl)
	{
		if ((Mage::registry('url_access_by_join_free') !== true)
			&& Mage::getStoreConfig('urlmanager/general/enable')
			&& Mage::getStoreConfig('urlmanager/open/enable')
		) {
			$joinFree = Mage::getStoreConfig('urlmanager/open/link');
			while ($joinFree[0] == '/') {
				$joinFree = substr($joinFree, 1);
			}
			
			$origUrl = str_replace(Mage::getUrl(), Mage::getBaseUrl() . $joinFree . '/', $origUrl);
		}
		return 'addthis:url="' . $origUrl . '"';
	}
	
	/* =========== DEPRECATED ============ */
	function getTimeZoneOffset() { return Mage::helper('privatesales/deprecated')->Helper_List_getTimeZoneOffset($this); }
}