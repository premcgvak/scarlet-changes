<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('privatesales_emailtemplates')}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `store_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `categories_ids` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `period_text` varchar(255) NOT NULL,
  `template` text NOT NULL,
  `template_date_format` char(255) NOT NULL,
  `list_layout` int(11) NOT NULL,
  `list_template` text NOT NULL,
  `list_template_date_format` char(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`)
) ENGINE=MyISAM;
");


$setup = Mage::getModel('eav/entity_setup', 'core_setup');
$entityTypeId = $setup->getEntityTypeId('catalog_category');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getAttributeGroupId($entityTypeId, $attributeSetId, 'General Information');

$setup->addAttribute('catalog_category', 'email_image', array(
	'type'              => 'varchar',
	'backend'           => 'catalog/category_attribute_backend_image',
	'label'             => 'Email Image',
	'input'             => 'image',
	'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
	'visible'           => 1,
	'required'          => 0,
	'user_defined'      => 0,
	'default'           => '',
	'position'          => 6,
));

$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'email_image', '6');
$setup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, 'email_image', '6');

$installer->endSetup();
