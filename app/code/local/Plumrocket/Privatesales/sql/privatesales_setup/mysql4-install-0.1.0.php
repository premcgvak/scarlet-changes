<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('privatesales_banners')}` (
  `banner_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `href` varchar(256) NOT NULL DEFAULT '',
  `name` varchar(256) DEFAULT NULL,
  `exclude` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `{$this->getTable('privatesales_images')}` (
  `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `active_from` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active_to` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `exclude` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `{$this->getTable('privatesales_maps')}` (
  `map_id` int(11) NOT NULL AUTO_INCREMENT,
  `map_name` varchar(256) NOT NULL,
  `map` text NOT NULL,
  `col_count` int(11) NOT NULL,
  `enabled_ending_soon` int(1) NOT NULL DEFAULT '0',
  `ending_count_days` int(4) NOT NULL DEFAULT '0',
  `enabled_upcoming_soon` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`map_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `{$this->getTable('privatesales_maps')}` (`map_id`, `map_name`, `map`, `col_count`, `enabled_ending_soon`, `ending_count_days`, `enabled_upcoming_soon`) VALUES
(1, '*/main/draft', '".'<?xml version="1.0"?>'."\n<data><top/><middle><row><item><colspan>4</colspan><rowspan>1</rowspan><type>1</type></item></row></middle><bottom/></data>\n', 4, 1, 10, 1),
(2, '*/main/public', '".'<?xml version="1.0"?>'."\n<data><top/><middle><row><item><colspan>4</colspan><rowspan>1</rowspan><type>1</type></item></row></middle><bottom/></data>\n', 4, 1, 10, 1);

CREATE TABLE IF NOT EXISTS `{$this->getTable('privatesales_pages')}` (
  `page_id` int(11) NOT NULL,
  `xml` text NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `{$this->getTable('privatesales_pages')}` (`page_id`, `xml`) VALUES
(1, '".'<?xml version="1.0"?>'."\n<data><form_key></form_key><enabled_page>1</enabled_page><enabled_registration>1</enabled_registration><enabled_launching_soon>0</enabled_launching_soon><include_registration>1</include_registration><include_forgot>1</include_forgot><meta_title>Meta Title</meta_title><meta_keywords>Meta Keywords</meta_keywords><meta_description>Meta Description</meta_description><become_text>Become a member text</become_text><launching_text>Launching Soon Registration Confirmation Text</launching_text></data>\n'),
(2, '".'<?xml version="1.0"?>'."\n<data><form_key></form_key><enabled_page>1</enabled_page><meta_title>Meta Title</meta_title><meta_keywords>Meta Keywords</meta_keywords><meta_description>Meta Description</meta_description></data>\n'),
(3, '".'<?xml version="1.0"?>'."\n<data><form_key></form_key><enabled_page>1</enabled_page><meta_title>Meta Title</meta_title><meta_keywords>Meta Keywords</meta_keywords><meta_description>Meta Description</meta_description></data>\n');

CREATE TABLE IF NOT EXISTS `{$this->getTable('privatesales_sizes')}` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('banner','image','catalog') NOT NULL DEFAULT 'banner',
  `name` varchar(256) NOT NULL,
  `height` int(11) NOT NULL DEFAULT '0',
  `width` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

/*
CREATE TABLE IF NOT EXISTS `urlmanager_rules` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_path` varchar(512) NOT NULL,
  `target_path` varchar(512) DEFAULT NULL,
  `access` enum('none','guest','open','customer','denied') NOT NULL DEFAULT 'none',
  `pattern` varchar(512) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


INSERT INTO `urlmanager_rules` (`request_path`, `target_path`, `access`, `pattern`, `priority`) VALUES
('/profile\(\$all\)', '/customer/account$1', 'none', '', 0),
('\(\$open\)/', '/cms/index/index/', 'open', '', 0);*/

Mage::getModel('cms/block')
    ->addData(array(
    	 'title'		=> 'Terms of Services - Splash page',
    	 'identifier'	=> 'terms_splashpage',
    	 'content'		=> 'Terms of Services',
    	 'is_active'	=> true
    ))
    ->save();
    

Mage::getModel('cms/block')
    ->addData(array(
    	 'title'		=> 'Bottom menu on splashpage',
    	 'identifier'	=> 'menu_splashpage',
    	 'content'		=> "<ul class=\"socials\">\r\n<li> <a class=\"fb\" title=\"Join us on Facebook\" href=\"https://www.facebook.com/\" target=\"_blank\">JOIN us on</a> </li>\r\n<li> <a class=\"tw\" title=\"Follow us on Twitter\" href=\"http://twitter.com/\" target=\"_blank\">Follow us on</a> </li>\r\n</ul>\r\n<ul class=\"footnav\"><a title=\"Terms &amp; Conditions\" href=\"{{store direct_url=\"terms/\"}}\">Terms &amp; Conditions</a> </li>\r\n<li> <a title=\"About Us\" href=\"{{store direct_url=\"about/\"}}\">About Us</a> </li>\r\n<li> <a title=\"Contact Us\" href=\"{{store direct_url=\"contacts/\"}}\">Contact Us</a> </li>",
    	 'is_active'	=> true
    ))
    ->save();
    
Mage::getModel('cms/block')
    ->addData(array(
    	 'title'		=> 'bottom_banner_long',
    	 'identifier'	=> 'bottom_banner_long',
    	 'content'		=> "<p><a title=\"Private Sale Solution - Request a free quote\" href=\"http://www.plumrocket.com/contact/index/?from=demo-private-sale\"> <img src=\"http://membership-site.plumrocket.net/skin/frontend/memberonly/default/images/banners/ready-banner2.jpg\" alt=\"Request a free quote\" /> </a></p>",
    	 'is_active'	=> true 
    ))
    ->save();

Mage::getModel('cms/block')
    ->addData(array(
    	 'title'		=> 'bottom_banner',
    	 'identifier'	=> 'bottom_banner',
    	 'content'		=> "<p><a href=\"#\"> <img src=\"http://membership-site.plumrocket.net/skin/frontend/memberonly/default/images/banners/banner-invite-btm.jpg\" alt=\"\" /> </a> <a style=\"text-decoration: none;\"> <img src=\"http://www.plumrocket.com/chat/image.php?id=04&amp;type=inlay\" border=\"0\" alt=\"Plumrocket Live Help\" width=\"297\" height=\"126\" /> </a> <a> <img usemap=\"#Image-Maps_4201201131004297\" src=\"http://membership-site.plumrocket.net/skin/frontend/memberonly/default/images/banners/banner-socials.jpg\" border=\"0\" alt=\"Always be in touch\" /> </a></p>\r\n<p>\r\n<map id=\"_Image-Maps_4201201131004297\" name=\"Image-Maps_4201201131004297\">\r\n<area title=\"Follow us on Twitter\" shape=\"rect\" coords=\"200,62,250,112\" href=\"https://twitter.com/#!/plumrocket\" alt=\"Follow us on Twitter\" target=\"_blank\" />\r\n<area title=\"Follow us on Facebook\" shape=\"rect\" coords=\"150,62,200,112\" href=\"https://www.facebook.com/pages/Plumrocket/160838373976863\" alt=\"Follow us on Facebook\" target=\"_blank\" /> \r\n</map>\r\n</p>",
    	 'is_active'	=> true
    ))
    ->save();
	
Mage::getModel('cms/block')
    ->addData(array(
    	 'title'		=> 'Invite text',
    	 'identifier'	=> 'invite_text',
    	 'content'		=> "<p>Hi!</p><p>I wanted to share with you this new wine private sale site I discovered - {{config path=\"web/unsecure/base_url\"}}<br /><br />It is a free Member only Private Sale site devoted to wine. Every day, members get an email invite to shop a hand-picked selection of high quality wines at the lowest possible prices...</p>",
    	 'is_active'	=> true
    ))
    ->save();
	
Mage::getModel('cms/block')
    ->addData(array(
    	 'title'		=> 'This boutique expired',
    	 'identifier'	=> 'expired_boutique',
    	 'content'		=> "<p>This boutique expired.</p>",
    	 'is_active'	=> true
    ))
    ->save();
	
Mage::getModel('cms/block')
    ->addData(array(
    	 'title'		=> 'This boutique expired popup',
    	 'identifier'	=> 'expired_boutique_popup',
    	 'content'		=> "<p>This Boutique has expired, please go back to the list of shops.</p>",
    	 'is_active'	=> true
    ))
    ->save();

$installer->run("
	INSERT INTO `{$this->getTable('cms_block_store')}` (`block_id`, `store_id`)
	SELECT block_id, 0 FROM `{$this->getTable('cms_block')}` ORDER BY `block_id` DESC LIMIT 7
");

/*
$installer->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'privatesale_date_start', array(
        'type'              => 'datetime',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Start of Sales',
        'input'             => 'date',
        'class'             => '',
        'source'            => '',
        'global'            => '0',
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'default'           => '',
        'searchable'        => true,
        'filterable'        => true,
        'comparable'        => true,
        'visible_on_front'  => true,
        'used_in_product_listing' => true,
        'unique'            => false,
        'wysiwyg_enabled'   => true,
        'apply_to'          => '',
        'is_configurable'   => true
    ));
 
$installer->addAttributeToSet(Mage_Catalog_Model_Category::ENTITY, '3', '3', 'privatesale_date_start','6');
$installer->addAttributeToGroup(Mage_Catalog_Model_Category::ENTITY, '3', '3', 'privatesale_date_start','6');
*/

$setup = Mage::getModel('eav/entity_setup', 'core_setup');

$entityTypeId = $setup->getEntityTypeId('catalog_category'); //Mage_Catalog_Model_Category::ENTITY;
//$attributeSetId = '3';
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);

$setup->addAttributeGroup($entityTypeId, $attributeSetId, 'Private Sales Settings', 15);
$attributeGroupId = $setup->getAttributeGroupId($entityTypeId, $attributeSetId, 'Private Sales Settings');

// DATE START
$setup->addAttribute('catalog_category', 'privatesale_date_start',  array(
    'type'     => 'datetime',
    'label'    => 'Start of Sales',
    'input'    => 'date',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
));
 
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_date_start', '10');
$setup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_date_start','10');

// DATE END
$setup->addAttribute('catalog_category', 'privatesale_date_end',  array(
    'type'     => 'datetime',
    'label'    => 'End of Sales',
    'input'    => 'date',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_date_end', '20');
$setup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_date_end','20');

// STORE FRONT
$setup->addAttribute('catalog_category', 'privatesale_is_storefront',  array(
    'type'     => 'int',
    'label'    => 'Storefront',
    'input'    => 'select',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
	'source'            => 'eav/entity_attribute_source_boolean',
	'note'				=> 'If selected - sales never expire on this homepage'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_is_storefront', '30');
$setup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_is_storefront','30');

// LINK TO PRODUCT
$setup->addAttribute('catalog_category', 'privatesale_is_link_to_product',  array(
    'type'     => 'int',
    'label'    => 'Single Product Boutique',
    'input'    => 'select',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
	'source'            => 'privatesales/attribute_source_products',
	'note'				=> 'If product selected, boutique will be pointed to a product page instead of product list'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_is_link_to_product', '40');
$setup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_is_link_to_product','40');

// BUQTIQUE CATEGORY
$setup->addAttribute('catalog_category', 'privatesale_boutique_category',  array(
    'type'     => 'text',
    'label'    => 'Boutique Home Pages',
    'input'    => 'multiselect',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => false,
	'backend'			=> 'privatesales/attribute_backend_categories',
	'source'            => 'privatesales/attribute_source_categories',
	'note'				=> 'Same boutique can be displayed on one or more Home Pages at the same time',
	'default'           => '0',
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_boutique_category', '50');
$setup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_boutique_category','50');

// TYPE
$setup->addAttribute('catalog_category', 'privatesale_type',  array(
    'type'     => 'int',
    'label'    => 'Category type',
    'input'    => 'select',
    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => true,
    'user_defined'      => false,
	'source'            => 'privatesales/attribute_source_types',
	'default'           => '1',
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_type', '5');
$setup->addAttributeToSet($entityTypeId, $attributeSetId, $attributeGroupId, 'privatesale_type','5');

//this will set data of your custom attribute for root category
Mage::getModel('catalog/category')
    ->load(1)
    ->setImportedCatId(0)
    ->setInitialSetupFlag(true)
    ->setData('privatesale_type', 3)
    ->save();
 
//this will set data of your custom attribute for default category
Mage::getModel('catalog/category')
    ->load(2)
    ->setImportedCatId(0)
    ->setInitialSetupFlag(true)
    ->setData('privatesale_type', 3)
    ->save();
    
// AGREE (CUSTOMER)
$setup->addAttribute('customer', 'agree', array(
	'type'		=> 'int',
	'input'		=> 'select',
	'label'		=> 'Agree to the terms of service',
	'global'	=> 1,
	'visible'	=> 1,
	'required'	=> 1,
	'default'	=> '0',
	'source'	=> 'privatesales/entity_agree',
	'user_defined'		=> 1,
	'visible_on_front'	=> 1,
));
if (version_compare(Mage::getVersion(), '1.6.0', '<='))
{
      $customer = Mage::getModel('customer/customer');
      $attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
      $setup->addAttributeToSet('customer', $attrSetId, 'General', 'agree');
}
if (version_compare(Mage::getVersion(), '1.4.2', '>='))
{
    Mage::getSingleton('eav/config')
    	->getAttribute('customer', 'agree')
    	->setData('used_in_forms', array('customer_account_create'))
    	->save();
}

$installer->endSetup();