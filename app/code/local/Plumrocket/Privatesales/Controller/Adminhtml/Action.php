<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Controller_Adminhtml_Action extends Mage_Adminhtml_Controller_Action
{
	protected $_modelLable	= null;
	protected $_modelKey	= null;
	protected $_model		= null;
	
	public function getModel()
	{
		if (is_null($this->model)){
			$this->model = Mage::getModel($this->_modelKey);
		}
		return $this->model;
	}
	
	protected function _initAction()
	{
		Mage::register('current_model', $this->getModel());
		return $this;
	}
	
	protected function _getSession()
	{
		return Mage::getSingleton('adminhtml/session');
	}
	
	public function indexAction() 
	{
		$this->_title($this->__('Manage '.$this->_modelLable.'s'));
		$this->_initAction();
		$this->loadLayout();
		$this->renderLayout();
	}
	
	public function newAction()
	{
		$this->_forward('edit');
	}
		
	public function editAction()
	{	
		$_request	= $this->getRequest();	
		$_model		= $this->getModel();
		
			
		if ($id = $_request->getParam('id')){
			$_model->load($id);
			$this->_title($this->__('Edit '.$this->_modelLable));
		} else {
			$this->_title($this->__('Add New '.$this->_modelLable));
		}
		
		if (!$id || $_model->getId()) {
			
			$this->_initAction();
			$this->loadLayout();
			$this->renderLayout();
		} else {
			$this->_getSession()->addError($this->__('Item does not exist.'));
			$this->_redirect('*/*/');
		}
	}
		
	public function saveAction()
	{
		$_request	= $this->getRequest();	
		$_model		= $this->getModel();
		$data		= $_request->getParams();

		try { 
			$data['updated_at'] = Mage::getModel('core/date')->date();
			if (!empty($data['id'])){
				$_model->load($data['id']);	
			} else {
				unset($data['id']);
				$data['created_at'] = $data['updated_at'];
			}
			
			$this->_beforeModelSave($data);
			
			$_model
				->setData($data)
				->save()
				->setFormData(false);
				
			$this->_afterModelSave();

			$this->_getSession()->addSuccess($this->__($this->_modelLable.' was successfully saved'));

			if ($this->getRequest()->getParam('back')) {
				$this->_redirect('*/*/edit', array('id' => $_model->getId()));
				return;
			}

		} catch (Exception $e) {
			$this->_getSession()->addError($e->getMessage());
			$_model->setFormData($_request->getPost());
			$this->_redirect('*/*/edit', array('id' => $_request->getParam('id')));
			return;
		}

		$this->_redirect('*/*/');
	}
	
	protected function _beforeModelSave(&$data)
	{
		return $this;
	}
	
	protected function _afterModelSave()
	{
		return $this;
	}


	public function deleteAction()
	{
		$_request	= $this->getRequest();	
		$_model		= $this->getModel();
		
		if( $ids = $_request->getParam('id') ) {
			try {
				if (!is_array($ids)){
					$ids = array($ids);
				}
				foreach($ids as $id){
					$_model->setId($id)->delete();
				}
				$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__($this->_modelLable.'(s) was successfully deleted'));
				$this->_redirect('*/*/');
			} 
			catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $_request->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}
		
		
	public function enableAction()
	{
		$this->_changeStatusAction(1);
	}
		
	public function disableAction()
	{
		$this->_changeStatusAction(0);
	}
		
	private function _changeStatusAction($status)
	{
		$_request	= $this->getRequest();	
		$_model		= $this->getModel();
		
		if( $ids = $_request->getParam('id') ) {
			try {
				if (!is_array($ids)){
					$ids = array($ids);
				}
				foreach($ids as $id){
					$_model->load($id)->setStatus($status)->save();
				}
				$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__($this->_modelLable.'(s) was successfully updated'));
				$this->_redirect('*/*/');
			} 
			catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/');
	}
		
}
