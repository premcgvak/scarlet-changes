<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Map extends Mage_Core_Model_Abstract
{
	
	private $map = array();
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('privatesales/map');
    }
    
    public function getColCount()
    {
    	$count = $this->getData('col_count');
    	if (!$count) {
    		$count = Mage::getStoreConfig('privatesales/homepage/default_col_count');
    	}
    	return $count;
    }
    
	public function getMap()
    {
    	if ($this->map) {
    		return $this->map;
    	}
    	
		$result = array(); 
		$map = simplexml_load_string(
			$this->getData('map')
		);
		if (!$map) {
			$map = array();
		}
		
		$areas = array('top', 'middle', 'bottom');
		foreach ($areas as $area) {
			if (!isset($map->$area)) {
				$map[$area] = array();
			}
		}
		
		foreach ($map as $key => $area) {
			$areaName = (string)$key;
			$result[$areaName] = array();
			
			$i = 1;
			foreach ($area as $row) {
				$minRowSpan = 10000;
				$sumColSpan = 0;
				foreach ($row as $item) {
					$result[$areaName][$i][] = array(
						'colspan' => (int)$item->colspan,
						'rowspan' => (int)$item->rowspan,
						'type' => (int)$item->type,
						'banners' => (((int)$item->type == 2)? $this->loadBanners($item->banners): array())
					);
					if ((int)$item->rowspan < $minRowSpan) {
						$minRowSpan = (int)$item->rowspan;
					}
					$sumColSpan += (int)$item->colspan;
				}
				$i++;
				
				// add empty row(s) if all previous rows has rowspan > 1
				if ($sumColSpan == $this->getColCount()) {
					for ($j = 1; $j < $minRowSpan; $j++) {
						$result[$areaName][$i] = array();
						$i++;
					}
				}
			}
			$result[$areaName]['_count_'] = $i - 1;
		}
		return $result;
    }
    
	private function loadBanners($xml)
	{
		$result = array();
		foreach ($xml->item as $item) {
		
			$time = ((string)$item->date_from == '0000-00-00')? 0: strtotime((string)$item->date_from);
			$dateFrom = ($time)? strftime('%m/%e/%y', $time): '00/00/00';
			
			$time = ((string)$item->date_to == '0000-00-00')? 0: strtotime((string)$item->date_to);
			$dateTo = ($time)? strftime('%m/%e/%y', $time): '00/00/00';
			
			$result[] = array(
				'id' => (int)$item->id,
				'date_from' => $dateFrom,
				'date_to' => $dateTo
			);
		}
		return $result;
	}
}
