<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Emailtemplate extends Mage_Core_Model_Abstract
{
	const HASH_KEY = 'Shn3890Sh8d9D32ad';
	protected $_categoriesCollection = null;


    public function _construct()
    {  
        $this->_init('privatesales/emailtemplate', 'id');
    }

    public function getSecret()
    {
    	return md5(self::HASH_KEY.$this->getId().$this->getStoreId().date('Y-m-d'));
    }

    protected function _beforeSave()
    {
    	$ids = $this->getData('categories_ids');
		if (!empty($ids) && is_array($ids)){
			$ids = implode(',', $ids);
			$this->setCategoriesIds($ids);
		}
		return parent::_beforeSave();
    }

    public function setFormData($data)
	{
		Mage::getSingleton('adminhtml/session')->setEmailtemplateFormData($data);
		return $this;
	}
	
	public function getCategoriesIds()
	{
		$ids = $this->getData('categories_ids');
		if (is_array($ids)){
			return $ids;
		}
		return explode(',', $ids);
		
	}

	public function getFormData()
	{
		$data = Mage::getSingleton('adminhtml/session')->getEmailtemplateFormData();
		if ($data){
			$this->setData($data);
		}

		$data = $this->getData();
		if (empty($data['template_date_format'])){
			$data['template_date_format'] = 'm/d/Y';
		}
		if (empty($data['list_template_date_format'])){
			$data['list_template_date_format'] = 'm/d/Y';
		}

		return $data;
	}

	public function loadCategoriesByCriteria($date = null, $storeId = null)
	{
		if (is_null($storeId)){
			$storeId = $this->getStoreId();
		}

		$offset		= $this->getOffset($date);

		$_categories	= Mage::helper('privatesales/list')->getAllBoutiques(false, $storeId);
		$_result = array();
		foreach ($_categories as $category) {
		   	if ($category->getIsActive(true, $offset)) {
		   		$_result[] = $category;
		   	}
		}

		return $_result;
	}

	public function getOffset($date = null)
	{
		if (is_null($date)){
			$date = $this->getDate();
		}

		$cTime 		=  Mage::getModel('core/date')->timestamp();
		$time 		= strtotime($date);
		$offset		= $time - $cTime;

		return $offset;
	}

	public function getCategoriesCollection()
	{
		if (is_null($this->_categoriesCollection)){
			$this->_categoriesCollection = Mage::getModel('catalog/category')->getCollection()
				->setStoreId($this->getStoreId())
				->addFieldToFilter('entity_id', array('in' => $this->getCategoriesIds()))
				->addAttributeToSelect('*');
		}
		return $this->_categoriesCollection;
	}

	public function categoriesToOptions($categories)
	{
		$_result = array();
		foreach($categories as $category){
			$_result[] = array(
				'value' => $category->getId(),
				'label'	=> $category->getName(),
			);
		}
		return $_result;
	}
}
