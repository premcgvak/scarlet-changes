<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_ObserverEncoded extends Mage_Core_Model_Abstract
{
	public function controllerInitBefore($observer)
	{
		// logout if lauching soon mode is enabled
		if (Mage::getSingleton('privatesales/splashpage')->getEnabledLaunchingSoon()) {
			Mage::getSingleton('customer/session')->logout()
				->setBeforeAuthUrl(Mage::getUrl());
		}

		if (Mage::helper('privatesales')->moduleEnabled()) {
			return $observer;
		}
		
		$front = $observer->getEvent()->getControllerAction();
		$module = $front->getRequest()->getRouteName();
		$controller = $front->getRequest()->getControllerName();
		$action = $front->getRequest()->getActionName();

		if (($module == 'catalog' && ($controller == 'category' || $controller == 'product'))
			|| ($module == 'review' && ($controller == 'customer' || $controller == 'product'))
		) {
			if (Mage::getSingleton('privatesales/splashpage')->getEnabledPage() && !Mage::getSingleton('customer/session')->authenticate($front)) {
				$front->setFlag('', 'no-dispatch', true);
			}
		}

		// Fix save categories' date start and date end.
		if ($module == 'adminhtml' && $controller = 'catalog_category' && $action == 'save') {
			if ($data = $front->getRequest()->getPost()) {
				$dateFormat = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
				if (isset($data['general']['privatesale_date_start']) && $data['general']['privatesale_date_start'] != NULL ) {
					$value = new Zend_Date($data['general']['privatesale_date_start'], $dateFormat, 'en');
					//$data['general']['privatesale_date_start'] = $value->get(Varien_Date::DATE_INTERNAL_FORMAT);
					$data['general']['privatesale_date_start'] = strftime('%F %T', $value->get());
		        }
		        if (isset($data['general']['privatesale_date_end']) && $data['general']['privatesale_date_end'] != NULL) {
					$value = new Zend_Date($data['general']['privatesale_date_end'], $dateFormat, 'en');
					//$data['general']['privatesale_date_end'] = $value->get(Varien_Date::DATE_INTERNAL_FORMAT);
					$data['general']['privatesale_date_end'] = strftime('%F %T', $value->get());
		        }

				$front->getRequest()->setPost($data);
			}
		}
		return $observer;
	}
}
