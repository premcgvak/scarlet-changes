<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Helpers_Parent extends Mage_Core_Model_Abstract
{

	protected function saveImages($images, $modelName)
	{
		if ($images) {
			foreach ($images as $id => $attributesData) {
				$image = Mage::getModel($modelName)->load((int)$id);
				
				$attributesData['exclude'] = (isset($attributesData['exclude']))? 1 : 0;
				$doRemove = (isset($attributesData['remove']))? 1 : 0;
				
				if (!$doRemove) {
					foreach ($attributesData as $attributeCode => $value) {						
						if ($attributeCode == 'active_from' || $attributeCode == 'active_to') {
							$value = Mage::helper('privatesales/list')->_formatDate($value);
						}
						$image->setData($attributeCode, $value);
					}
					$image->save();
				} else {
					$image->delete();
				}
			}
		}
	}
	
	protected function saveFiles($pageId, $imagesPath, $modelName)
	{
		if ($_FILES && isset($_FILES['files']) && $_FILES['files']) {
		
			$max_image_size		= 10 * 1024 * 1024;
			$valid_types 		=  array("gif","jpg", "png", "jpeg");
			
			foreach ($_FILES["files"]['name'] as $id => $_fileName) {
				$tmp_name = $_FILES['files']['tmp_name'][$id];
				$filename = $_FILES['files']['name'][$id];
				$ext = substr($filename,  1 + strrpos($filename, "."));
				
				if (is_uploaded_file($tmp_name)) {
					if (filesize($tmp_name) > $max_image_size) {
						throw new Exception(Mage::helper('privatesales')->__(' Error: File size > 10MB (' . $filename . ').'));
					} elseif (!in_array($ext, $valid_types)) {
						throw new Exception(Mage::helper('privatesales')->__(' Error: Invalid file type (' . $filename . ').'));
					} else {
						if (!file_exists(Mage::getBaseDir('media') . $imagesPath . $filename)) {							
							if (move_uploaded_file($tmp_name, Mage::getBaseDir('media') . $imagesPath . $filename)) {
								$image = Mage::getModel($modelName);
								$image->setName($filename);
								$image->save();
							} else {
								throw new Exception(Mage::helper('privatesales')->__(' Error: moving fie failed (' . $filename . ').'));
							}
						} else {
							throw new Exception(Mage::helper('privatesales')->__(' Error: File already exists (' . $filename . ').'));
						}
					}
				}
			}
		}
	}
}