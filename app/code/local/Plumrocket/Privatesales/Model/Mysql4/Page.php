<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Mysql4_Page extends Mage_Core_Model_Mysql4_Abstract
{
	protected function _construct()
	{
		$this->_init("privatesales/page", "id");
	}
	
	protected function _getLoadSelect($field, $value, $object)
    {
        $field  = $this->_getReadAdapter()->quoteIdentifier(sprintf('%s.%s', $this->getMainTable(), $field));
        $select = $this->_getReadAdapter()->select()
            ->from($this->getMainTable())
            ->where($field . '=?', $value);
			
		if ($field == '`privatesales_pages`.`page_id`') {
			$stores[] = '0';
			// fix: if store set by selector, do not set current store id.
			if ($store_id = (int)Mage::registry('privatesales_custom_store_id')) {
				$stores[] = $store_id;
			} elseif ($store_id = Mage::app()->getStore()->getStoreId()) {
				$stores[] = $store_id;
			}
			
			$select->where('`store_id` IN (' . implode(',', $stores) . ')');
			$select->order('store_id DESC');
		}
		//echo $select;
        return $select;
    }
	
	protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {	
		// load xml options from data base record for the page
		$xml = simplexml_load_string(
			$object->getData('xml')
		);
		
		// set all xml options to data
		if (!$xml) {
			$xml = array();
		}
		foreach ($xml as $key => $val) {
			if (preg_match('/^[0-9]+$/', $val) && $key != 'map') {
				$val = (int)$val;
			}
			$object->setData($key, $val);
		}
		
		$helperName = 'Plumrocket_Privatesales_Model_Helpers_' . ucfirst($object->page_name);
		$class = new $helperName();
		$object = $class->initialObject($object);
		
		return parent::_afterLoad($object);
    }
	
	/**
     * Process page data before saving
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Cms_Model_Resource_Page
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
		// create new xml
		$xml = new SimpleXMLElement("<data></data>");
		
		$helperName = 'Plumrocket_Privatesales_Model_Helpers_' . ucfirst($object->page_name);
		$class = new $helperName();
		$xml = $class->preSave($object, $xml);

		$data = $object->getData('post_data');
		
		// clear data of page
		$store_id = $data['store'];
		if ((int)$store_id != (int)$object->getData('store_id')) {
			$object->setData(array());
		}
		$object->setData('xml', $xml);
		$object->setData('store_id', $store_id);
		$object->setData('page_id', $object->page_id);

        return parent::_beforeSave($object);
    }
}
	 