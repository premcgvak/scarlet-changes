<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Mysql4_Map extends Mage_Core_Model_Mysql4_Abstract
{
	protected function _construct()
	{
		$this->_init("privatesales/map", "map_id");
	}
	
	/**
     * Process page data before saving
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Cms_Model_Resource_Page
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
		$data = $object->getData('post_data');
		if ($data) {
			$xml = new SimpleXMLElement("<data></data>");
			$areas = array('top', 'middle', 'bottom');
			
			foreach ($areas as $areaName) {
			
				$area = $xml->addChild($areaName);
				
				if (isset($data[$areaName]) && $data[$areaName]) {
					foreach ($data[$areaName] as $rowItems) {
						$row = $area->addChild('row');
						foreach ($rowItems as $col) {
							// ignore foots from old banners' images
							if (isset($col['type'])) {
								$item = $row->addChild('item');
								$item->addChild('colspan', $col['colspan']);
								$item->addChild('rowspan', $col['rowspan']);
								$item->addChild('type', $col['type']);
								
								if ((int)$col['type'] == 2) {
									$banners = $item->addChild('banners');
									
									if (isset($col['banners']) && $col['banners'] && is_array($col['banners'])) {
										foreach ($col['banners'] as $key => $val) {
											$banner = $banners->addChild('item');
											
											$banner->addChild('id', $key);
											$banner->addChild('date_from', Mage::helper('privatesales/list')->_formatDate($val['date_from']));
											$banner->addChild('date_to', Mage::helper('privatesales/list')->_formatDate($val['date_to']));
										}
									}
								}
							}
						}
					}
				}
			}
			$object->setData('map', $xml->saveXML());
		}

		// clear data of page
		$store_id = $object->getData('store');
		if ((int)$store_id != (int)$object->getData('store_id')) {
			$object->setMapId(NULL);
		}
		$object->setData('store_id', $store_id);

        return parent::_beforeSave($object);
    }
}
	 