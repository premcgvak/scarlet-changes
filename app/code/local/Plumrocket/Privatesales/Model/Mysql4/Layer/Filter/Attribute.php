<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Mysql4_Layer_Filter_Attribute extends Mage_Catalog_Model_Resource_Layer_Filter_Attribute
{
   
    /**
     * Retrieve array with products counts per attribute option
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @return array
     */
    public function getCount($filter)
    {
        // clone select from collection with filters
        $select = clone $filter->getLayer()->getProductCollection()->getSelect();
        
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        $connection = $this->_getReadAdapter();
        $attribute  = $filter->getAttributeModel();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $filter->getStoreId()),
        );

        $select->join(
            array($tableAlias => $this->getMainTable()),
            join(' AND ', $conditions),
            array(
            	'hash_for_search' => new Zend_Db_Expr("CONCAT ({$tableAlias}.entity_id, \":\", {$tableAlias}.`value`)"),
            	'value'
            )
		);

        $pid_to_val = $connection->fetchPairs($select);
		
		$productsActive = array();
		foreach ($pid_to_val as $pid => $_) {
			$ar = explode(':', $pid);
			$productsActive[ $ar[0] ] = true;
		}
		
        $params = new Varien_Object();
        $controller =  Mage::app()->getFrontController()->getAction();
		
		$prevCat = Mage::registry('current_category');
		if ($prevCat && $prevCat->getId()) {
			$params->setCategoryId( $prevCat->getId() );
		}
		
		foreach ($productsActive as $pid => $_) {
			$item = Mage::getModel('catalog/product')->load($pid);
			Mage::helper('privatesales/catalog_product')->initCurrentCategoryForProduct($item, $controller, $params, true);
		
			$productsActive[$pid] = $item->isSaleable();
			
			Mage::unregister('current_category');
		}
		
		Mage::unregister('current_product');
		Mage::unregister('product');
		Mage::register('current_category', $prevCat);
		
		$result = array();
		foreach ($pid_to_val as $pid => $val) {
			$arr = explode(':', $pid);
			$pid = $arr[0];

			if (!isset($result[$val])) {
				$result[$val] = 0;
			}
			
			if ($productsActive[$pid]) {
				$result[$val]++;
			}
		}
		
		return $result;
    }
}