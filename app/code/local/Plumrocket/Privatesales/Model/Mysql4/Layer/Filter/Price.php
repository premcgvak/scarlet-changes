<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package    Plumrocket_Private_Sales-v2.2.x
@copyright  Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license    http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Mysql4_Layer_Filter_Price extends Mage_Catalog_Model_Resource_Layer_Filter_Price
{

    /**
     * Retrieve array with products counts per price range
     *
     * @param Mage_Catalog_Model_Layer_Filter_Price $filter
     * @param int $range
     * @return array
     */
    public function getCount($filter, $range)
    {
        if (strpos(Mage::getVersion(), '1.7') === 0) {
            $result = $this->getCount_1_7($filter, $range);
        } elseif (strpos(Mage::getVersion(), '1.6') === 0) {
            $result = $this->getCount_1_6($filter, $range);
        } else {
            $result = parent::getCount($filter, $range);
        }
        return $result;
    }

    private function getCount_1_6($filter, $range)
    {
        $select     = $this->_getSelect($filter);
        $connection = $this->_getReadAdapter();
        $response   = $this->_dispatchPreparePriceEvent($filter, $select);
        $table      = $this->_getIndexTableAlias();

        $additional = join('', $response->getAdditionalCalculations());
        $rate       = $filter->getCurrencyRate();

        /**
         * Check and set correct variable values to prevent SQL-injections
         */
        $rate       = floatval($rate);
        $range      = floatval($range);
        if ($range == 0) {
            $range = 1;
        }
        $idExpr  = new Zend_Db_Expr('e.entity_id');
        $rangeExpr  = new Zend_Db_Expr("FLOOR((({$table}.min_price {$additional}) * {$rate}) / {$range}) + 1");

        $select->columns(array(
            'id' => $idExpr,
            'range' => $rangeExpr,
        ));

        $pid_to_range = $connection->fetchPairs($select);
        return $this->_getResultFromPairs($pid_to_range);
    }

    private function getCount_1_7($filter, $range)
    {
        $select = $this->_getSelect($filter);
        $priceExpression = $this->_getFullPriceExpression($filter, $select);

        /**
         * Check and set correct variable values to prevent SQL-injections
         */
        $range = floatval($range);
        if ($range == 0) {
            $range = 1;
        }
        $idExpr  = new Zend_Db_Expr(Mage_Catalog_Model_Resource_Product_Collection::MAIN_TABLE_ALIAS . '.entity_id');
        $rangeExpr = new Zend_Db_Expr("FLOOR(({$priceExpression}) / {$range}) + 1");

        $select->columns(array(
            'id' => $idExpr,
            'range' => $rangeExpr
        ));

        $pid_to_range = $this->_getReadAdapter()->fetchPairs($select);
        return $this->_getResultFromPairs($pid_to_range);
    }

    private function _getResultFromPairs($pid_to_range)
    {
        $productsActive = array();
        foreach ($pid_to_range as $pid => $_) {
            $productsActive[$pid] = true;
        }
        
        $params = new Varien_Object();
        $controller =  Mage::app()->getFrontController()->getAction();
        
        $prevCat = Mage::registry('current_category');
        if ($prevCat && $prevCat->getId()) {
            $params->setCategoryId( $prevCat->getId() );
        }
        
        foreach ($productsActive as $pid => $_) {
            $item = Mage::getModel('catalog/product')->load($pid);
            Mage::helper('privatesales/catalog_product')->initCurrentCategoryForProduct($item, $controller, $params, true);
        
            $productsActive[$pid] = $item->isSaleable();
            
            Mage::unregister('current_category');
        }
        
        Mage::unregister('current_product');
        Mage::unregister('product');
        Mage::register('current_category', $prevCat);
        
        $result = array();
        foreach ($pid_to_range as $pid => $range) {
            if ($productsActive[$pid]) {
                if (! isset($result[$range])) {
                    $result[$range] = 0;
                }

                $result[$range]++;
            }
        }

        return $result;
    }
}
