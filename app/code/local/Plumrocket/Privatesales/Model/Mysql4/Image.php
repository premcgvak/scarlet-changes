<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Mysql4_Image extends Mage_Core_Model_Mysql4_Abstract
{
	protected function _construct()
	{
		$this->_init("privatesales/image", "image_id");
	}
	
	protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
		$fname = Mage::getBaseDir('media') . '/splashpage/' . $object->getData('name');
		if (file_exists($fname)) {
			unlink($fname);
		}
		// Ooops
		if (file_exists($fname)) {
			throw new Exception(Mage::helper('privatesales')->__(' Error: File does not deleted - may be access denied (' . $fname . ').'));
		}
        return parent::_beforeDelete($object);
    }
}
	 