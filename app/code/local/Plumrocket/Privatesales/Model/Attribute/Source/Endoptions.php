<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Attribute_Source_Endoptions
{
	const ERROR = 1;
    const REDIRECT = 2;
	const LEAVE = 3;
    
    public function toOptionArray()
    {
        return array(
            array('value' => self::ERROR, 'label' => Mage::helper('privatesales')->__('Display Page Not Found message (header: 404 error)')),
            //array('value' => self::REDIRECT, 'label' => Mage::helper('privatesales')->__('Redirect visitor to default home page (header: 301 redirect) ')),
			array('value' => self::LEAVE, 'label' => Mage::helper('privatesales')->__('Allow to browse pages but disable "add to cart" button (header: 200 OK)')),
        );
    }
}