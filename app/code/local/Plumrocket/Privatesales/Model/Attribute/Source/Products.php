<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Attribute_Source_Products extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array();
            $currentCategory = Mage::registry('current_category');
            if ($currentCategory && $currentCategory->getId()) {
                $rKey = 'privatesalesProductsAttribute_' . $currentCategory->getId();
                if ($this->_options = Mage::registry($rKey)){
                    return $this->_options;
                }

                $this->_options[] = array(
                    'label' => Mage::helper('privatesales')->__('- Select a product -'),
                    'value' => 0
                );

                $products = $currentCategory->getProductCollection()
                    ->addAttributeToSelect('name')
                    ->addAttributeToSelect('sku')
                    ->addAttributeToFilter('status', 1);

                foreach ($products as $product) {
                    $this->_options[] = array(
                        'label' => $product->getSku() . ' - ' . $product->getData('name'),
                        'value' => $product->getId()
                    );
                }

                Mage::register($rKey, $this->_options);
            }
        }
        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray()
    {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option['value']] = $option['label'];
        }
        return $_options;
    }
}
