<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Override_Catalog_CategoryEncoded extends Mage_Catalog_Model_Category
{
	
	private $storeFrontCalled = false;
	private $isStoreFront = false;
	private $storeFrontId = 0;
	
	private $parentIds = array();
	
	public function getIsActive($callTimeCheck = false, $offset = null)
	{
		$isActive = $this->getData('is_active');
		if (Mage::helper('privatesales')->moduleEnabled()) {
			$callTimeCheck = $callTimeCheck
				|| (Mage::getStoreConfig('privatesales/boutique_settings/end_action') != Plumrocket_Privatesales_Model_Attribute_Source_Endoptions::LEAVE);
			
			if ($isActive
				&& Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_BOUTIQUE == $this->getPrivatesaleType()
				&& $callTimeCheck === true
			) {
				$time = Mage::getModel('core/date')->timestamp(time());
				$from = Mage::helper('privatesales/list')->getTime($this->getPrivatesaleDateStart(), true, $offset);
				$to = Mage::helper('privatesales/list')->getTime($this->getPrivatesaleDateEnd(), true, $offset);
				
				if ((($from == 0) || ($from < $time)) && (($to == 0) || ($to > $time))) {} else {
					$isActive = false;
				}
			}
		}
		
		return $isActive;
	}
	
	public function getPrivatesaleDateStart()
	{
		// support Store Front
		if ($this->isIntoStoreFront()) {
			return '';
		}
		
		$date = $this->getData('privatesale_date_start');
		if (!$date) {
			$parent = $this->getParentCategory();
			$date = $parent->getData('privatesale_date_start');
		}
		return $date;
	}
	
	public function getPrivatesaleDateEnd()
	{
		// support Store Front
		if ($this->isIntoStoreFront()) {
			return '';
		}
		
		$date = $this->getData('privatesale_date_end');
		if (!$date) {
			$parent = $this->getParentCategory();
			$date = $parent->getData('privatesale_date_end');
		}
		return $date;
	}
	
	public function getUrl()
	{
		if (Mage::helper('privatesales')->moduleEnabled()) {
			// rewrite url to product list if we set this catalog as link to product
			$productId = $this->getData('privatesale_is_link_to_product');
			if ($productId
				&& Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_BOUTIQUE == $this->getPrivatesaleType()
			) {
				$productUrl = Mage::getModel('catalog/product')->load($productId)->getUrlPath();
				if ($productUrl[0] == '/') {
					$productUrl = substr($productUrl, 1);
				}
				
				$categoryUrl = parent::getUrl();
				if ($suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix')) {
					$categoryUrl = str_replace($suffix, '', $categoryUrl);
				}
				if ($pos = strpos($categoryUrl, '?')) {
					$categoryUrl = substr($categoryUrl, 0, $pos);
				}
				
				if (substr($categoryUrl, -1, 1) != '/') {
					$categoryUrl .= '/';
				}
				
				return  $categoryUrl . $productUrl;
			}
		}
		return parent::getUrl();
	}
	
	private function _getPrivatesaleBoutiqueCategory()
	{
		if (Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_BOUTIQUE == $this->getPrivatesaleType()) {
			$data = $this->getData('privatesale_boutique_category');
			if (is_array($data)) {
				return $data;
			}
			return ($data)? explode(',', $data): array();
		} else {
			return array();
		}
	}
	
	public function getPrivatesaleType()
	{
		$type = $this->_getData('privatesale_type');
		return (in_array($type, array(
			Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_BOUTIQUE,
			Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_MENU_ITEM,
			Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_GROUP_OF_BOUTIQUES,
		)))? (int)$type: Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_BOUTIQUE;
	}
	
	public function isParentCatalogById($itemId)
	{ 
		if (!$this->parentIds) {
			$this->parentIds = array_merge($this->getParentIds(), $this->_getPrivatesaleBoutiqueCategory());
		}
		return (in_array($itemId, $this->parentIds));
	}
	
	public function isIntoStoreFront()
	{
		if ($this->storeFrontCalled) {
			return $this->isStoreFront;
		} else {
			$menu = Mage::helper('privatesales/list')->getMenuItems();
			
			$res = false;
			foreach ($menu as $item) {
				if ($item->getData('privatesale_is_storefront') && $this->isParentCatalogById($item->getId())) {
					$res = true;
					$id = $item->getId();
					
					$this->storeFrontId = $id;
					// just storefront id
					$this->parentIds = array_merge($this->getParentIds(), array($id));
					break;
				}
			}
			$this->isStoreFront = $res;
			$this->storeFrontCalled = true;
			return $res;
		}
	}
	
	public function getImageBaseUrl()
	{
		return Mage::getBaseDir('media'). DS . 'catalog' . DS . 'category' . DS . $this->getImage();
	}
	
	public function getImageRootUrl()
	{
		return 'catalog' . DS . 'category' . DS . $this->getImage();
	}

	public function getThumbnailUrl()
	{
		if ($this->getThumbnail()) {
			return Mage::getBaseUrl('media').'catalog' . DS . 'category' . DS . $this->getThumbnail();
		} else {
			return '';
		}
	}
}