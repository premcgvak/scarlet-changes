<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

if (Mage::helper('privatesales')->moduleEnabled() || Mage::app()->getStore()->isAdmin()) {
	class Plumrocket_Privatesales_Model_Override_Catalog_Category extends Plumrocket_Privatesales_Model_Override_Catalog_CategoryEncoded {}
} else {
	class Plumrocket_Privatesales_Model_Override_Catalog_Category extends Mage_Catalog_Model_Category {}
}