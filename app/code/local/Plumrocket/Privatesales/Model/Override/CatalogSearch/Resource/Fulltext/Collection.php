<?php

/*
Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Override_CatalogSearch_Resource_Fulltext_Collection extends Mage_CatalogSearch_Model_Resource_Fulltext_Collection
{
    
    private $offsetSize = 0;
    private $fixedSize = false;
	
    public function removeItemByKey($key)
    {
		parent::removeItemByKey($key);
        if (Mage::helper('privatesales')->moduleEnabled()) {
		  $this->offsetSize++;
        }
	}
	
	public function setFixedSize($int)
	{
		$this->fixedSize = $int;
	}
	
	public function getSize()
    {
        if (! Mage::helper('privatesales')->moduleEnabled()) {
            return parent::getSize();
        }

		if ($this->fixedSize !== false) {
			return $this->fixedSize;
		}
        if (is_null($this->_totalRecords)) {
            $sql = $this->getSelectCountSql();
            $this->_totalRecords = $this->getConnection()->fetchOne($sql, $this->_bindParams) - $this->offsetSize;
        }
        return intval($this->_totalRecords);
    }
	
	public function getProductCountSelect()
    {
        if (! Mage::helper('privatesales')->moduleEnabled()) {
            return parent::getProductCountSelect();
        }

        if ($this->_productCountSelect === null) {
            $this->_productCountSelect = clone $this->getSelect();
            $this->_productCountSelect->reset(Zend_Db_Select::COLUMNS)
                ->reset(Zend_Db_Select::GROUP)
                ->reset(Zend_Db_Select::ORDER)
                ->distinct(false)
                ->join(array('count_table' => $this->getTable('catalog/category_product_index')),
                    'count_table.product_id = e.entity_id',
                    array(
                    	'hash_for_search' => new Zend_Db_Expr('CONCAT(count_table.product_id, ":", count_table.category_id)'),
                        'count_table.category_id'
                    )
                )
                ->where('count_table.store_id = ?', $this->getStoreId());
                //->group('count_table.category_id');
        }

        return $this->_productCountSelect;
    }
	
	/**
     * Adding product count to categories collection
     *
     * @param Mage_Eav_Model_Entity_Collection_Abstract $categoryCollection
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function addCountToCategories($categoryCollection)
    {
        if (Mage::helper('privatesales')->moduleEnabled()) {
            return parent::addCountToCategories();
        }

        $isAnchor    = array();
        $isNotAnchor = array();
        foreach ($categoryCollection as $category) {
            if ($category->getIsAnchor()) {
                $isAnchor[]    = $category->getId();
            } else {
                $isNotAnchor[] = $category->getId();
            }
        }
        $productCounts = array();
        if ($isAnchor || $isNotAnchor) {
            $select = $this->getProductCountSelect();

            Mage::dispatchEvent(
                'catalog_product_collection_before_add_count_to_categories',
                array('collection' => $this)
            );

            if ($isAnchor) {
                $anchorStmt = clone $select;
                $anchorStmt->limit(); //reset limits
                $anchorStmt->where('count_table.category_id IN (?)', $isAnchor);
                $productCounts += $this->getConnection()->fetchPairs($anchorStmt);
                $anchorStmt = null;
            }
            if ($isNotAnchor) {
                $notAnchorStmt = clone $select;
                $notAnchorStmt->limit(); //reset limits
                $notAnchorStmt->where('count_table.category_id IN (?)', $isNotAnchor);
                $notAnchorStmt->where('count_table.is_parent = 1');
                $productCounts += $this->getConnection()->fetchPairs($notAnchorStmt);
                $notAnchorStmt = null;
            }
            $select = null;
            $this->unsProductCountSelect();
        }

		$productCounts = $this->_getResultFromPairs($productCounts);

        foreach ($categoryCollection as $category) {
            $_count = 0;
            if (isset($productCounts[$category->getId()])) {
                $_count = $productCounts[$category->getId()];
            }
            $category->setProductCount($_count);
        }

        return $this;
    }

	private function _getResultFromPairs($productCounts)
    {
        $params = new Varien_Object();
        $controller =  Mage::app()->getFrontController()->getAction();
        
        $prevCat = Mage::registry('current_category');
        if ($prevCat && $prevCat->getId()) {
            $params->setCategoryId( $prevCat->getId() );
        }
		
		$pid_to_cats = array();
		foreach ($productCounts as $hash => $cat_id) {
			$ar = explode(':', $hash);
			$pid_to_cats[ (int)$ar[0] ][] = $cat_id;
		}
		
		$catsCount = array();
        
        foreach ($pid_to_cats as $pid => $cats) {
            $item = Mage::getModel('catalog/product')->load($pid);
            Mage::helper('privatesales/catalog_product')->initCurrentCategoryForProduct($item, $controller, $params, true);
        
            if ($item->isSaleable()) {			
				foreach ($cats as $cat_id) {
					if (! isset($catsCount[$cat_id])) {
						$catsCount[$cat_id] = 0;
					}
					$catsCount[$cat_id]++;
				}
			}
            
            Mage::unregister('current_category');
        }
        
        Mage::unregister('current_product');
        Mage::unregister('product');
        Mage::register('current_category', $prevCat);

        return $catsCount;
    }
}
