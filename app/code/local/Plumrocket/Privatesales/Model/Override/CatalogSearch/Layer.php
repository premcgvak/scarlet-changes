<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Override_CatalogSearch_Layer extends Mage_CatalogSearch_Model_Layer
{

    /**
     * Prepare product collection
     *
     * @param Mage_Catalog_Model_Resource_Eav_Resource_Product_Collection $collection
     * @return Mage_Catalog_Model_Layer
     */
    public function prepareProductCollection($collection)
    {
		parent::prepareProductCollection($collection);

		if (Mage::helper('privatesales')->moduleEnabled()) {		
			$colls = clone $collection;
			
			$params = new Varien_Object();
	        $controller =  Mage::app()->getFrontController()->getAction();
			
			$prevCat = Mage::registry('current_category');
			if ($prevCat && $prevCat->getId()) {
				$params->setCategoryId( $prevCat->getId() );
			}
				
			foreach ($colls->getItems() as $key => $item) {
				Mage::helper('privatesales/catalog_product')->initCurrentCategoryForProduct($item, $controller, $params, true);
				
				if (!$item->isSaleable()) {
					$colls->removeItemByKey($key);
				}
				
				Mage::unregister('current_category');
			}
			
			Mage::unregister('current_product');
			Mage::unregister('product');
			Mage::register('current_category', $prevCat);
			// For fix size value.
			$colls->getSize();
			
			$collection->setFixedSize( $colls->getSize() );
		}

        return $this;
    }
}
