<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Page extends Mage_Core_Model_Abstract
{
    /**
     * Page's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
	static private $sizes;
	
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
    	if (Mage::getSingleton('plumbase/observer')->customer() == Mage::getSingleton('plumbase/product')->currentCustomer()) {
			$this->_init('privatesales/page');
			return $this->load($this->page_id, 'page_id');
		}
    }

    public function getEnabledPage()
	{
		return Mage::helper('privatesales')->moduleEnabled() && parent::getEnabledPage();
	}

    public function getAvailableStatuses()
    {
        $statuses = new Varien_Object(array(
            self::STATUS_ENABLED => Mage::helper('privatesales')->__('Enabled'),
            self::STATUS_DISABLED => Mage::helper('privatesales')->__('Disabled'),
        ));

        return $statuses->getData();
    }
	
	public function copyFrom($obj)
	{
		$store_id = $obj->getStoreId();
		$this->setData(array());
		$this->setData('store_id', $store_id);
		$this->setData('xml', $obj->getXml());
		$this->save();
	}
	
	// add cache for items if it is missing
	static public function foreachForImageHash($type, $items)
	{
		if (!self::$sizes[$type]) {
			$_sizes = Mage::getModel('privatesales/size')
				->getCollection()
				->addFieldToFilter('type', $type)
				->load();
				
			foreach ($_sizes as $size) {
				self::$sizes[$type][ $size->getName() ] = true;
			}
		}
		
		foreach ($items as $item) {
			if (!isset(self::$sizes[$type][ $item->getImage() ])
				&& file_exists($item->getImageBaseUrl())
				&& ! is_dir($item->getImageBaseUrl())
			) {
				$image = Mage::getModel('privatesales/size');
				$image->setName($item->getImage());
				$image->setType($type);
				
				$info = getimagesize($item->getImageBaseUrl());
				$image->setWidth($info[0]);
				$image->setHeight($info[1]);
				
				$image->save();
			}
		}
	}
}
