<?php
 
/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Homepage_Parent extends Plumrocket_Privatesales_Model_Page
{
	public $page_name = 'homepage';
	
	const TYPE_BOUTIQUE = 1;
	const TYPE_MENU_ITEM = 2;
	const TYPE_GROUP_OF_BOUTIQUES = 3;
	
	public function getAllBanners()
	{
		$items = Mage::getModel('privatesales/banner')
			->getCollection();
			
		// add cache
		$this->foreachForImageHash('banner', $items);
		return $items;
	}
	
	public function getBanners()
	{
		$items = Mage::getModel('privatesales/banner')
			->getCollection()
			->addFieldToFilter('exclude', 0)
			->load();
			
		// add cache
		$this->foreachForImageHash('banner', $items);
		return $items;
	}
}
