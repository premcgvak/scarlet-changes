<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Model_Splashpage extends Plumrocket_Privatesales_Model_Page
{
	// Note: Require field!!
	public $page_id = 1; 
	public $page_name = 'splashpage'; 

	/**
	* Page's Statuses
	*/
	const INCLUDE_STATUS_STATIC = 0;
	const INCLUDE_STATUS_BOTH = 1;
	const INCLUDE_STATUS_AJAX = 2;

	public function getIncludeStatus()
	{
		$statuses = new Varien_Object(array(
			self::INCLUDE_STATUS_STATIC => Mage::helper('privatesales')->__('Only static page'),
			self::INCLUDE_STATUS_BOTH => Mage::helper('privatesales')->__('Both options'),
			self::INCLUDE_STATUS_AJAX => Mage::helper('privatesales')->__('Only AJAX'),
		));

		return $statuses->getData();
	}
	
	public function getAllImages()
	{
		$items = Mage::getModel('privatesales/image')->getCollection();
			
		// add cache
		$this->foreachForImageHash('image', $items);
		return $items;
	}

	public function getEnabledLaunchingSoon()
	{
		return parent::getData('enabled_launching_soon')
			&& (Mage::getSingleton('core/session')->getData('debug_offset_time_on') !== 1);
	}
	
	public function getRandomImage()
	{
		$time = Mage::getModel('core/date')->timestamp(time());
		
		$currIndex = (int)Mage::getSingleton('core/session')->getActiveSplashIndex();
		$currIds = (array)Mage::getSingleton('core/session')->getActiveSplashIds();
		
		$colls = Mage::getModel('privatesales/image')
			->getCollection()
			->addFieldToFilter('exclude', 0)
			->setOrder('sort_order', 'ASC');
			
		$first = NULL;
		$active = NULL;
		foreach ($colls as $coll) {
			$timeStart = Mage::helper('privatesales/list')->getTime($coll->getActiveFrom());
			$timeEnd = Mage::helper('privatesales/list')->getTime($coll->getActiveTo());
			
			if (($timeStart == 0 || $timeStart < $time)
				&& ($timeEnd == 0 || $timeEnd > $time))
			{
				if ($coll->getSortOrder() >= $currIndex && !in_array($coll->getId(), $currIds)) {
					$active = $coll;
					break;
				} elseif (!$first) {
					$first = $coll;
				}
			}
		}
		if (!$active) {
			$active = $first;
			$currIds = array();
		}
			
		if ($active) {
			$currIds[] = $active->getId();
			Mage::getSingleton('core/session')->setActiveSplashIndex(
				$active->getSortOrder()
			);
			
			Mage::getSingleton('core/session')->setActiveSplashIds(
				$currIds
			);
		}
			
		$name = ($active)? $active->getName(): '';
		
		// add cache
		if ($active) {
			$this->foreachForImageHash('image', array($active));
		}
		return $name;
	}
}
