<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Catalog_Catalogsearch_List extends Mage_Catalog_Block_Product_List
{

    protected function _getProductCollection()
    {
        if (!Mage::helper('privatesales')->moduleEnabled()) {
            return parent::_getProductCollection();
        }
        
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                    ->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();
			
			// -------

			// Fix collections order and direction. Else this colection will be loaded and order
			// and direction will be not apply to the collection
	        $toolbar = $this->getToolbarBlock();
	
	        // use sortable parameters
	        if ($orders = $this->getAvailableOrders()) {
	            $toolbar->setAvailableOrders($orders);
	        }
	        if ($sort = $this->getSortBy()) {
	            $toolbar->setDefaultOrder($sort);
	        }
	        if ($dir = $this->getDefaultDirection()) {
	            $toolbar->setDefaultDirection($dir);
	        }
	        if ($modes = $this->getModes()) {
	            $toolbar->setModes($modes);
	        }
	
	        // set collection to toolbar and apply sort
	        $toolbar->setCollection($this->_productCollection);
			
			// Main part
            $controller =  Mage::app()->getFrontController()->getAction();

            if ($controller->getRequest()->getModuleName() == 'catalogsearch') {
                $params = new Varien_Object();

                $prevCat = Mage::registry('current_category');
                if ($prevCat && $prevCat->getId()) {
                    $params->setCategoryId( $prevCat->getId() );
                }
                    
                foreach ($this->_productCollection->getItems() as $key => $item) {
                    Mage::helper('privatesales/catalog_product')->initCurrentCategoryForProduct($item, $controller, $params, true);
                    
                    if (!$item->isSaleable()) {
                        $this->_productCollection->removeItemByKey($key);
                    }
                    
                    Mage::unregister('current_category');
                }
                
                Mage::unregister('current_product');
                Mage::unregister('product');
                Mage::register('current_category', $prevCat);
            }
            // -----------------

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }

        return $this->_productCollection;
    }
}
