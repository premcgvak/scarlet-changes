<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Catalog_Category_View extends Mage_Catalog_Block_Category_View
{
	public function getLeftTime()
	{
		$cat = $this->getCurrentCategory();
		$endTime = Mage::helper('privatesales/list')->getTime($cat->getPrivatesaleDateEnd());
		$leftTime = $endTime - Mage::getModel('core/date')->timestamp( time() );
		
		if ($leftTime > 0) {
			return $leftTime;
		} else {
			return ($endTime > 0)? 'expired': 0;
		}
	}

	public function getCurrentCategory()
	{
		$cat = parent::getCurrentCategory();
		if (!$cat) {
			$this->setData('current_category', Mage::getModel('catalog/category'));
		}
		return $this->getData('current_category');
	}
}