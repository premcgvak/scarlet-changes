<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Catalog_Product_View extends Plumrocket_Privatesales_Block_Catalog_Category_View
{

	protected function _toHtml()
    {
        if (! Mage::helper('privatesales')->moduleEnabled()) {
            return '';
        }
        return parent::_toHtml();
    }
    
	public function getBack()
	{
		$_category  = $this->getCurrentCategory();
		
		if ($_category->getData('privatesale_is_link_to_product')
			&& Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_BOUTIQUE == $_category->getPrivatesaleType()
		) {
			return '/';
		}
		
		return $_category->getUrl();
	}
	
	public function getBackCaption()
	{
		$_category  = $this->getCurrentCategory();
		return $_category->getName();
	}
	
	public function getProduct()
    {
        if (!Mage::registry('product') && $this->getProductId()) {
            $product = Mage::getModel('catalog/product')->load($this->getProductId());
            Mage::register('product', $product);
        }
        return Mage::registry('product');
    }
}