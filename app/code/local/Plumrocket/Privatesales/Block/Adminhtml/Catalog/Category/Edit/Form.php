<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Catalog_Category_Edit_Form extends Mage_Adminhtml_Block_Catalog_Category_Edit_Form
{    
    protected function _prepareLayout()
    {
        if (Mage::helper('privatesales')->moduleEnabled()) {
    		$category = $this->getCategory();
            $categoryId = (int) $category->getId(); // 0 when we create category, otherwise some value for editing category
    		
    		 // Preview Mode button
            if (!$category->isReadonly()
    			&& Plumrocket_Privatesales_Model_Homepage_Parent::TYPE_GROUP_OF_BOUTIQUES != $category->getPrivatesaleType()
    			&& (int)Mage::getStoreConfig('privatesales/all/future')
    		) {
                $resetPath = $categoryId ? 'privatesales/adminhtml_homepage/preview/cid/' . $categoryId . '/' : 'privatesales/adminhtml_homepage/preview/';
    			$this->addAdditionalButton('preview_button', array(
    				'label'     => Mage::helper('catalog')->__('Boutique Preview'),
    				'class'		=> 'preview',
    				'onclick' => "window.open('". $this->getUrl($resetPath, array('_current' => false)) ."', 'preview', 'location=yes,toolbar=yes,menubar=yes,directories=yes,status=yes,resizable=yes,scrollbars=yes,height=undefined,width=undefined', false);"
    			));
            }
        }

        return parent::_prepareLayout();
    }
	
	public function getPreviewButtonHtml()
    {
        if ($this->hasStoreRootCategory()) {
            return $this->getChildHtml('preview_button');
        }
        return '';
    }
}
