<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Catalog_Product_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{    
    protected function _prepareLayout()
    {
        if (Mage::helper('privatesales')->moduleEnabled()) {
    		if (!$this->getProduct()->isReadonly()
    			&& (int)Mage::getStoreConfig('privatesales/all/future')
    		) {
    			$resetPath = 'privatesales/adminhtml_homepage/preview/pid/' . $this->getProduct()->getId() . '/';
                $this->setChild('preview_button',
                    $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setData(array(
                            'label'     => Mage::helper('catalog')->__('Product Preview '),
    						'class'		=> 'preview',
    						'onclick' => "window.open('". $this->getUrl($resetPath, array('_current' => false)) ."', 'preview', 'location=yes,toolbar=yes,menubar=yes,directories=yes,status=yes,resizable=yes,scrollbars=yes,height=undefined,width=undefined', false);"
                        ))
                );
            }
        }

        return parent::_prepareLayout();
    }
	
	public function getPreviewButtonHtml()
    {
        return $this->getChildHtml('preview_button');
    }
}
