<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Catalog_Category_Tab_Attributes extends Mage_Adminhtml_Block_Catalog_Category_Tab_Attributes
{    

	private $_attributes = array(
		1 => array(
			'privatesale_is_storefront',
		),
		2 => array(
			'privatesale_date_start', 
			'privatesale_date_end', 
			'privatesale_is_link_to_product', 
			'privatesale_boutique_category'
		),
		3 => array(
			'privatesale_is_storefront', 
			'privatesale_is_link_to_product', 
			'privatesale_boutique_category'
		)
	);
	
    /**
     * Prepare form before rendering HTML
     *
     * @return Mage_Adminhtml_Block_Catalog_Category_Tab_Attributes
     */
    protected function _prepareForm() 
	{
		parent::_prepareForm();
		if (Mage::helper('privatesales')->moduleEnabled()) {
			$form = $this->getForm();
		
			// set onChange for field with types
			if ($element = $form->getElement('privatesale_type')) {
				$element->setData('onchange', 'onPrivatesaleTypeChanged(this)');
			}
			
			// disable elements related to active type
			$type = $this->getCategory()->getPrivatesaleType();
			$type = in_array($type, array(1, 2, 3))? $type: 1;
			
			foreach ($this->_attributes[$type] as $attribute) {
				if ($element = $form->getElement($attribute)) {
					$element->setDisabled(true);
				}
			}
			
			// Change date fields of Privatesales module to datetime format
			$dateTimeFormatIso = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
	        
			if ($element = $form->getElement('privatesale_date_start')) {
				$element->setFormat($dateTimeFormatIso);
				$element->setInputFormat(Varien_Date::DATETIME_INTERNAL_FORMAT);
				$element->setNoSpan(true);
				$element->setTime(true);
			}
			
			if ($element = $form->getElement('privatesale_date_end')) {
				$element->setFormat($dateTimeFormatIso);
				$element->setInputFormat(Varien_Date::DATETIME_INTERNAL_FORMAT);
				$element->setNoSpan(true);
				$element->setTime(true);
			}
			
			$this->setForm($form);
			return;
		}
    }
}
