<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Splashpage_Tab_Meta
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Mage_Cms_Model_Page */
        $model = Mage::registry('splash_page');

        /*
         * Checking if user have permissions to save information
         */
        $isElementDisabled = !(bool)$this->_isAllowedAction('save');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('splash_');
		
		$fieldset = $form->addFieldset('meta_fieldset', array('legend' => Mage::helper('privatesales')->__('Meta Information'), 'class' => 'fieldset-wide'));
		
		$fieldset->addField('meta_title', 'text', array(
            'name' => 'meta_title',
            'label' => Mage::helper('privatesales')->__('Meta Title'),
            'title' => Mage::helper('privatesales')->__('Meta Title'),
            'disabled'  => $isElementDisabled
        ));

        $fieldset->addField('meta_keywords', 'textarea', array(
            'name' => 'meta_keywords',
            'label' => Mage::helper('privatesales')->__('Meta Keywords'),
            'title' => Mage::helper('privatesales')->__('Meta Keywords'),
            'disabled'  => $isElementDisabled
        ));

        $fieldset->addField('meta_description', 'textarea', array(
            'name' => 'meta_description',
            'label' => Mage::helper('privatesales')->__('Meta Description'),
            'title' => Mage::helper('privatesales')->__('Meta Description'),
            'disabled'  => $isElementDisabled
        ));
        
		Mage::dispatchEvent('privatesale_splashpage_tab_meta_prepare_form', array('form' => $form));
		
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('privatesales')->__('Meta Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('privatesales')->__('Meta Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/' . $action);
    }
}
