<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Splashpage_Tab_General
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Mage_Cms_Model_Page */
        $model = Mage::registry('splash_page');
        
        /*
         * Checking if user have permissions to save information
         */
        $isElementDisabled = !(bool)$this->_isAllowedAction('save');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('splash_');
		
        $fieldset = $form->addFieldset('general_fieldset', array('legend' => Mage::helper('privatesales')->__('General'), 'class' => 'fieldset-wide'));
		
		$fieldset->addField('store', 'hidden', array(
			'name'      => 'store'
        ));
		
		$fieldset->addField('enabled_page', 'select', array(
			'name'      => 'enabled_page',
            'label'     => Mage::helper('privatesales')->__('Enable Splash Page'),
            'title'     => Mage::helper('privatesales')->__('Enable Splash Page'),
            'required'  => true,
            'options'   => $model->getAvailableStatuses(),
			'note'      => Mage::helper('privatesales')->__('If splash page is disabled, clients will be landing directly on website home page without login'),
            'disabled'  => $isElementDisabled,
			//'values'	=> Mage::getResourceModel('system/config_source_yesno')->load()->toOptionArray()
        ));
		
		$fieldset->addField('become_text', 'textarea', array(
            'name' => 'become_text',
            'label' => Mage::helper('privatesales')->__('Become a member text'),
            'title' => Mage::helper('privatesales')->__('Become a member text'),
            'disabled'  => $isElementDisabled
        ));
		
        Mage::dispatchEvent('privatesales_splashpage_tab_general_prepare_form', array('form' => $form));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('privatesales')->__('General');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('privatesales')->__('General');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/' . $action);
    }
}
