<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package  Plumrocket_Private_Sales-v2.2.x
@copyright  Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license  http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Emailtemplate extends Mage_Adminhtml_Block_Widget_Grid_Container 
{
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_emailtemplate';
        $this->_blockGroup = 'privatesales';
        $this->_headerText = $this->__('Newsletter Email Templates');

        
    }

    
    protected function _prepareLayout()
   	{
       $this->setChild( 'grid',
           $this->getLayout()->createBlock( $this->_blockGroup.'/' . $this->_controller . '_grid',
           $this->_controller . '.grid')->setSaveParametersInSession(true) );
       return parent::_prepareLayout();
   	}
}
