<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package  Plumrocket_Private_Sales-v2.2.x
@copyright  Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license  http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Emailtemplate_Grid extends Mage_Adminhtml_Block_Widget_Grid 
{

	public function __construct()
    {
        $this->setId('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);


		
       	parent::__construct();
    }

	
	protected function _prepareCollection()
    {
    	$collection = Mage::getModel('privatesales/emailtemplate')->getCollection();
		$this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {

		$this->addColumn('id', array(
            'header'    => $this->__('ID'),
            'align'     =>'left',
            'index'     => 'id',
            'type' => 'text',  
        ));

        $this->addColumn('name', array(
            'header'    => $this->__('Name'),
            'align'     =>'left',
            'index'     => 'name',
            'type' => 'text',  
        ));
		
		if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('cms')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }       
        
        $this->addColumn('date', array(
            'header'    => $this->__('Date'),
            'align'     =>'left',
            'index'     => 'date',
            'type' => 'datetime',
        ));
		
		$this->addColumn('created_at', array(
            'header'    => $this->__('Created'),
            'align'     =>'left',
            'index'     => 'created_at',
            'type' => 'datetime',
        ));
	
		$this->addColumn('updated_at', array(
            'header'    => $this->__('Updated'),
            'align'     =>'left',
            'index'     => 'updated_at',
            'type' => 'datetime',
        ));
		
        return parent::_prepareColumns();
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

		
		$this->getMassactionBlock()->addItem('delete', array(
             'label'	=> $this->__('Delete'),
             'url'  	=> $this->getUrl('*/*/delete', array('_current'=>true)),
			 'confirm'  => $this->__('Are you sure?'),
        ));
        
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
