<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package  Plumrocket_Private_Sales-v2.2.x
@copyright  Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license  http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Emailtemplate_Edit_Tabs_General
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        $model		= Mage::registry('current_model'); 
        $request	= $this->getRequest();

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('emailemplate_');
		
        $fieldset = $form->addFieldset('general_fieldset', array('legend' => $this->__('General'), 'class' => 'fieldset-wide'));
		
		$fieldset->addField('id', 'hidden', array(
			'name'		=> 'id',
		));
		
		$fieldset->addField('name', 'text', array(
			'name'		=> 'name',
			'required'	=> true,
			'label'		=> $this->__('Name'),
		));

		if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'select', array(
                'name'      => 'store_id',
                'label'     => $this->__('Store View'),
                'title'     => $this->__('Store View'),
                'required'  => true,
                'onchange' => "updateEmailCategories();",
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, false),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'store_id',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }
		
		

        //$dateFormat = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
		//$locale			= Mage::app()->getLocale();
		//$dateStrFormat	= $locale->getDateTimeFormat(Mage_яCore_Model_Locale::FORMAT_TYPE_SHORT);
        $dateStrFormat = 'M/d/yy h:mm a';
    
		//$value = $locale->date($model->getData('date'))->toString($dateStrFormat);
        $value = $model->getData('date');
		$fieldset->addField('date', 'date', array(
			'name'		=> 'date',
			'label'		=> $this->__('Date'),
			'image' 	=> $this->getSkinUrl('images/grid-cal.gif'),
			'format' 	=> $dateStrFormat,
			'no_span'   => true,
			'time' 		=> true,
			'required'	=> true,
            'onchange'  => "updateEmailCategories();",
            'note'      => 'Please set Newsletter sending date and time.',
		))->setValue($value, $dateStrFormat);


		$_categories = $model->loadCategoriesByCriteria();
		$field = $fieldset->addField('categories_ids', 'multiselect', array(
            'name'      => 'categories_ids',
            'label'     => $this->__('Boutiques'),
            'title'     => $this->__('Boutiques'),
            'required'  => true,
            'values'	=> $model->categoriesToOptions($_categories),
            'after_element_html' => '
            	<!--<button title="Update Categories" type="button" class="scalable " onclick="updateEmailCategories();" style=""><span><span><span>Update Categories</span></span></span></button>-->
            	<script type="text/javascript">
            		var updateCategoryUrl = "'.$this->getUrl('privatesales/adminhtml_emailtemplate/getCategories').'";
            	</script>
            ',
            'note'      => 'Selected boutiques will be included in newsletter template.',
        ));

        $fieldset = $form->addFieldset('template_fieldset', array('legend' => $this->__('Newsletter'), 'class' => 'fieldset-wide'));

		$wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(
            array('tab_id' => $this->getTabId())
        );

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => $this->__('Title'),
            'note'      => 'Newsletter title.',
        ));

        $fieldset->addField('period_text', 'text', array(
            'name'      => 'period_text',
            'label'     => $this->__('Period'),
            'note'      => 'Active boutiques date range.',
        ));

        $fieldset->addField('template', 'editor', array(
        	'label'     => $this->__('Full Email Template'),
            'title'     => $this->__('Full Email Template'),
            'name'      => 'template',
            'style'     => 'height:36em;',
            'required'  => true,
            //'config'    => $wysiwygConfig,
            'after_element_html' => '
                <button title="Load Default Template" type="button" class="scalable " onclick="loadDefaultTemplate();" style=""><span><span><span>Load Default Template</span></span></span></button>
                <textarea id="default_template" style="display:none;">'.($this->getLayout()->createBlock("adminhtml/template")->setTemplate('privatesales/emailtemplate/default/template.phtml')->toHtml()).'</textarea>
                <br/>
                Use variables:<br/>
                <b>{{title}}</b> to insert title into email template;<br/>
                <b>{{period}}</b> to insert period into email template;<br/>
                <b>{{boutiques_list}}</b> to insert list of boutiques into email template;<br/>
                <b>{{start_date}}</b> to insert earlier boutiques\' start date into email template;<br/>
                <b>{{end_date}}</b> to insert later boutiques\' start date into email template.<br/>
                <br/>
            ',
        ));

        $fieldset->addField('template_date_format', 'text', array(
            'name'      => 'template_date_format',
            'label'     => $this->__('Date Format'),
            'note' => 'Default value is "m/d/Y" (e.g. '.date('m/d/Y').'). Learn more about <a href="http://php.net/manual/en/function.date.php#refsect1-function.date-parameters" target="_blank" >date formats</a>.',
        ));

        $fieldset = $form->addFieldset('list_fieldset', array('legend' => $this->__('Row of Boutiques List'), 'class' => 'fieldset-wide'));
		
        $field = $fieldset->addField('list_layout', 'select', array(
            'name'      => 'list_layout',
            'label'     => $this->__('Layout'),
            'title'     => $this->__('Layout'),
            'required'  => true,
            'onchange' => "updateEmailCategories();",
            'values'    => array(
                1 => $this->__('One Boutique In Row'),
                2 => $this->__('Two Boutiques In Row'),
                3 => $this->__('Three Boutiques In Row'),
            ),
        ));

        $fieldset->addField('list_template', 'editor', array(
            'label'     => $this->__('Row Template'),
            'title'     => $this->__('Row Template'),
            'name'      => 'list_template',
            'style'     => 'height:36em;',
            'required'  => true,
            //'config'    => $wysiwygConfig,
            'after_element_html' => '
                <button title="Load Default Template" type="button" class="scalable " onclick="loadDefaultListTemplate();" style=""><span><span><span>Load Default Template</span></span></span></button>
                <textarea id="default_list_emplate_1" style="display:none;">'.($this->getLayout()->createBlock("adminhtml/template")->setTemplate('privatesales/emailtemplate/default/list_template_1.phtml')->toHtml()).'</textarea>
                <textarea id="default_list_emplate_2" style="display:none;">'.($this->getLayout()->createBlock("adminhtml/template")->setTemplate('privatesales/emailtemplate/default/list_template_2.phtml')->toHtml()).'</textarea>
                <textarea id="default_list_emplate_3" style="display:none;">'.($this->getLayout()->createBlock("adminhtml/template")->setTemplate('privatesales/emailtemplate/default/list_template_3.phtml')->toHtml()).'</textarea>
                <br/>
                Use variables:<br/>
                <b>{{boutique.name}}</b> to insert boutique name into row template of boutiques list;<br/>
                <b>{{boutique.short_name}}</b> to insert  boutique short name into row template of boutiques list;<br/>
                <b>{{boutique.page_title}}</b> to insert boutique page title into row template of boutiques list;<br/>
                <b>{{boutique.short_page_title}}</b> to insert  boutique short page title into row template of boutiques list;<br/>
                <b>{{boutique.url}}</b> to insert  boutique url into row template of boutiques list;<br/>
                <b>{{boutique.image}}</b> to insert  boutique image into row template of boutiques list;<br/>
                <b>{{boutique.start_date}}</b> to insert  boutique start date into row template of boutiques list;<br/>
                <b>{{boutique.end_date}}</b> to insert  boutique end date into row template of boutiques list.<br/>
                You can also use value "<b>boutique2</b>" and "<b>boutique3</b>" instead of "<b>boutique</b>" if you use "Two Boutiques In Row" or "Three Boutiques In Row" layout.<br/>
                Please use tags "<b>'.htmlspecialchars('<!-- boutique 2 -->').'</b>" and "<b>'.htmlspecialchars('<!-- boutique 3 -->').'</b>" to determine each boutique position.
            ',
        ));

        $fieldset->addField('list_template_date_format', 'text', array(
            'name'      => 'list_template_date_format',
            'label'     => $this->__('Date Format'),
            'note' => 'Default value is "m/d/Y" (e.g. '.date('m/d/Y').'). Learn more about <a href="http://php.net/manual/en/function.date.php#refsect1-function.date-parameters" target="_blank" >date formats</a>.',
        ));
		
		
		

		$form->setValues($model->getFormData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('General Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('General Settings');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

}
