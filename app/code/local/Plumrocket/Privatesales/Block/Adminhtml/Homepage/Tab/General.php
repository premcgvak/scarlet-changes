<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Homepage_Tab_General
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Mage_Cms_Model_Page */
        $model = Mage::registry('homepage');
        
        /*
         * Checking if user have permissions to save information
         */
        $isElementDisabled = !(bool)$this->_isAllowedAction('save');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('homepage_');
		
        $fieldset = $form->addFieldset('general_fieldset', array('legend' => Mage::helper('privatesales')->__('General Settings')));
		
		$fieldset->addField('store', 'hidden', array(
			'name'      => 'store'
        ));
        
        $fieldset->addField('enabled_page', 'select', array(
			'name'      => 'enabled_page',
            'label'     => Mage::helper('privatesales')->__('Enable Home Page'),
            'title'     => Mage::helper('privatesales')->__('Enable Home Page'),
            'required'  => true,
            'options'   => $model->getAvailableStatuses(),
            'disabled'  => $isElementDisabled,
			//'values'	=> Mage::getResourceModel('system/config_source_yesno')->load()->toOptionArray()
        ));
		
		$fieldset2 = $form->addFieldset('homepages_fieldset', array('legend' => Mage::helper('privatesales')->__('Default Homepage')));
		
		$fieldset2->addField('default_homepage', 'select', array(
			'name'      => 'default_homepage',
            'label'     => Mage::helper('privatesales')->__('Default Home Page'),
            'title'     => Mage::helper('privatesales')->__('Default Home Page'),
            'required'  => true,
            'options'   => Mage::getSingleton('privatesales/attribute_source_categories')->getOptionArray(),
            'disabled'  => $isElementDisabled,
			//'values'	=> Mage::getResourceModel('system/config_source_yesno')->load()->toOptionArray()
        ));
		
		$fieldset2->addField('default_homepage_for_man', 'select', array(
			'name'      => 'default_homepage_for_man',
            'label'     => Mage::helper('privatesales')->__('Default Home Page For Men'),
            'title'     => Mage::helper('privatesales')->__('Default Home Page For Men'),
            'required'  => true,
            'options'   => Mage::getSingleton('privatesales/attribute_source_categories')->getOptionArray(),
            'disabled'  => $isElementDisabled,
			//'values'	=> Mage::getResourceModel('system/config_source_yesno')->load()->toOptionArray()
        ));
		
		$fieldset2->addField('default_homepage_for_woman', 'select', array(
			'name'      => 'default_homepage_for_woman',
            'label'     => Mage::helper('privatesales')->__('Default Home Page For Women'),
            'title'     => Mage::helper('privatesales')->__('Default Home Page For Women'),
            'required'  => true,
            'options'   => Mage::getSingleton('privatesales/attribute_source_categories')->getOptionArray(),
            'disabled'  => $isElementDisabled,
			//'values'	=> Mage::getResourceModel('system/config_source_yesno')->load()->toOptionArray()
        ));
        
        Mage::dispatchEvent('privatesales_homepage_tab_general_prepare_form', array('form' => $form));

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }	

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('privatesales')->__('General Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('privatesales')->__('General Settings');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/' . $action);
    }
}
