<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Adminhtml_Homepage_Tab_Map
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {
        /* @var $model Mage_Cms_Model_Page */
        $model = Mage::registry('homepage');
		$isElementDisabled = !(bool)$this->_isAllowedAction('save');

        return parent::_prepareForm();
    }
	
	public function getBanners()
	{
		$model = Mage::registry('homepage');
		return $model->getBanners();
	}
	
	public function getMap()
	{
		//$model = Mage::registry('homepage');
		return Mage::helper('privatesales/map')->getAdminCurrentMap('draft', 
			$this->getRequest()->getParam('mid'),
			$this->getRequest()->getParam('bid'));
	}
	
	public function getMenu()
	{
		return Mage::helper('privatesales/list')->getMenuItems('admin');
	}
	
	public function getActivePage()
	{
		return Mage::helper('privatesales/map')->filterMapId( $this->getRequest()->getParam('mid') ); 
	} 
	
	public function getActiveBlock()
	{
		return Mage::helper('privatesales/map')->filterMapId( $this->getRequest()->getParam('bid') ); 
	} 

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('privatesales')->__('Home Page Layout');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('privatesales')->__('Home Page Layout');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('plumrocket/privatesales/' . $action);
    }
}
