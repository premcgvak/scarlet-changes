<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package  Plumrocket_Private_Sales-v2.2.x
@copyright  Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license  http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Emailtemplate extends Mage_Core_Block_Template
{
	const DEFAULT_DATE_FORMAT = 'm/d/Y';

	protected $_listHtml 	= null;
	protected $_startDate 	= null;
	protected $_endDate 	= null;

	public function getModel()
	{
		return Mage::registry('current_model');
	}


	public function getListColumnsCount()
	{
		return $this->getModel()->getListLayout();
	}

	public function getListStartDate()
	{
		if (is_null($this->_startDate)){
			foreach($this->getModel()->getCategoriesCollection() as $_category){
				$date = $_category->getPrivatesaleDateEnd();
				if (is_null($this->_startDate) || ($date && $date < $this->_startDate) ){
					$this->_startDate = $date;
				}
			}
			if ($this->_startDate){
				$_dateFormat = ($this->getModel()->getTemplateDateFormat()) ? $this->getModel()->getTemplateDateFormat() : self::DEFAULT_DATE_FORMAT;
				$this->_startDate = date($_dateFormat, strtotime($this->_startDate));
			}
		}
		return $this->_startDate;
	}

	public function getListEndDate()
	{
		if (is_null($this->_endDate)){
			foreach($this->getModel()->getCategoriesCollection() as $_category){
				$date = $_category->getPrivatesaleDateEnd();
				if (is_null($this->_endDate) || ($date && $date > $this->_endDate) ){
					$this->_endDate = $date;
				}
			}
			if ($this->_endDate){
				$_dateFormat = ($this->getModel()->getTemplateDateFormat()) ? $this->getModel()->getTemplateDateFormat() : self::DEFAULT_DATE_FORMAT;
				$this->_endDate = date($_dateFormat, strtotime($this->_endDate));
			}
		}
		return $this->_endDate;
	}

	public function getTemplateHtml()
	{
		$_model 	= $this->getModel();
		$template 	= $_model->getTemplate();
		$search 	= array(
			'{{boutiques_list}}', 
			'{{start_date}}', 
			'{{end_date}}', 
			'{{title}}', 
			'{{period}}'
		);
		
		$replace 	= array(
			$this->getListHtml(), 
			htmlspecialchars($this->getListStartDate()), 
			htmlspecialchars($this->getListEndDate()), 
			$_model->getTitle() ? htmlspecialchars($_model->getTitle()) : '*|MC:SUBJECT|*', 
			$_model->getPeriodText() ? htmlspecialchars($_model->getPeriodText()) : htmlspecialchars($this->getListStartDate()).' - '.htmlspecialchars($this->getListEndDate()),
		);
		$template 	= str_replace($search, $replace, $template);

		return Mage::helper('cms')->getPageTemplateProcessor()->filter($template);
	}

	public function getListHtml()
	{	
		if (is_null($this->_listHtml)){
			$_columns 		= (int) $this->getListColumnsCount();
			$_dateFormat   	= ($this->getModel()->getListTemplateDateFormat()) ? $this->getModel()->getListTemplateDateFormat() : self::DEFAULT_DATE_FORMAT;

			$_c = $this->getModel()->getCategoriesCollection();
			$_categories = array();
			foreach($_c as $_category){
				$_categories[] = $_category;
			}

			$this->_listHtml = '';
			for($i = 0; $i < count($_categories); $i += $_columns){
				$_listTemplate 	= $this->getModel()->getListTemplate();
				for($j = 0; $j < $_columns; $j++){
					if (isset($_categories[$i + $j])){
						$_category = $_categories[$i + $j];

						$_category->setStoreId($this->getModel()->getStoreId()); $_category->getUrl(); //fix

						//find short name
						$shortedName 	= $_category->getName();
						$shortPageTitle = $_category->getMetaTitle();
						switch($_columns){
							case 2 : $maxShortedNameLenght = 35; break;
							case 3 : $maxShortedNameLenght = 25; break;
							default : $maxShortedNameLenght = 65; break;
						}
						if (mb_strlen($shortedName) > $maxShortedNameLenght){
							$shortedName = mb_substr($shortedName, 0, $maxShortedNameLenght).'...';
						}
						if (mb_strlen($shortPageTitle) > $maxShortedNameLenght){
							$shortPageTitle = mb_substr($shortPageTitle, 0, $maxShortedNameLenght).'...';
						}

						//find start date
						if ($startDate = $_category->getPrivatesaleDateStart()){
							$startDate = date($_dateFormat, strtotime($startDate));
						}

						//find end date
						if ($endDate = $_category->getPrivatesaleDateEnd()){
							$endDate = date($_dateFormat, strtotime($endDate));
						}

						$data = array(
							'url' 			=> $_category->getUrl(), //Mage::getModel('core/url')->setStore( $this->getModel()->getStoreId())->getDirectUrl($_category->getRequestPath()), //htmlspecialchars($_category->getUrl()),
							'name'			=> htmlspecialchars($_category->getName()),
							'short_name'  	=> htmlspecialchars($shortedName),
							'page_title'	=> $_category->getMetaTitle() ? htmlspecialchars($_category->getMetaTitle()) : htmlspecialchars($_category->getName()),
							'short_page_title'	=> $shortPageTitle ?  htmlspecialchars($shortPageTitle) : htmlspecialchars($shortedName),
							'image' 		=> htmlspecialchars(($_category->getEmailImage()) ? Mage::getBaseUrl('media').'catalog/category/'.$_category->getEmailImage() : $_category->getImageUrl()),
							'start_date' 	=> htmlspecialchars($startDate),
							'end_date' 		=> htmlspecialchars($endDate),
						);

						$number = ($j) ? ($j + 1) : '';
						foreach($data as $key => $value){
							$_listTemplate = str_replace('{{boutique'.$number.'.'.$key.'}}', htmlspecialchars($value), $_listTemplate);
						}

						$_listTemplate = str_replace('{{n}}', $i, $_listTemplate);

					} else {
						$tag = '<!-- boutique '.($j + 1).' -->';
						$_listTemplate = preg_replace('/'.$tag.'(.*)'.$tag.'/Uis', '', $_listTemplate);

					}
				}
				$this->_listHtml .= $_listTemplate;
			}
		}
		return $this->_listHtml;
	}

}
