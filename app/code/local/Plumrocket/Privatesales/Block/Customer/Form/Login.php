<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Customer_Form_Login extends Mage_Core_Block_Template
{
	private $splashPage;
	private $_username = -1;

    protected function _prepareLayout()
    {
		$this->splashPage = Mage::getSingleton('privatesales/splashpage');
		
		if (Mage::getSingleton('privatesales/splashpage')->getEnabledPage()) {
			$head = $this->getLayout()->getBlock('head');
			$head->setKeywords( $this->splashPage->getData('meta_keywords') );
			$head->setDescription( $this->splashPage->getData('meta_description') );
		}
		
        $res =  parent::_prepareLayout();
    }
	
	/**
     * Retrieve form posting url
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->helper('customer')->getLoginPostUrl();
    }

    /**
     * Retrieve create new account url
     *
     * @return string
     */
    public function getCreateAccountUrl()
    {
        $url = $this->getData('create_account_url');
        if (is_null($url)) {
            $url = $this->helper('customer')->getRegisterUrl();
        }
        return $url;
    }

    /**
     * Retrieve password forgotten url
     *
     * @return string
     */
    public function getForgotPasswordUrl()
    {
        return $this->helper('customer')->getForgotPasswordUrl();
    }

    /**
     * Retrieve username for form field
     *
     * @return string
     */
    public function getUsername()
    {
        if (-1 === $this->_username) {
            $this->_username = Mage::getSingleton('customer/session')->getUsername(true);
        }
        return $this->_username;
    }
	
	// --------------------
	/*
	 * isXXX - enabled in System
	 * isAjaxXXX - include this block to splash page
	 * isStaticXXX - enabled static page
	*/
	
	public function isEnabledPage()
	{
		return $this->splashPage->getEnabledPage();
	}
	
	public function getBecomeText()
	{
		return $this->splashPage->getBecomeText();
	}
	
	public function isEnabledLaunchingSoon()
	{
		return $this->splashPage->getEnabledLaunchingSoon();
	}
	
	public function getLaunchingText()
	{
		return $this->splashPage->getLaunchingText();
	}
	
	// Logins
	public function isUserLogin()
	{
		return (!$this->splashPage->getEnabledPage())
			|| (!$this->splashPage->getEnabledLaunchingSoon());
	}
	
	// Registration
	public function isUserRegistration()
	{
		return (!$this->splashPage->getEnabledPage())
			|| ($this->splashPage->getEnabledRegistration());
	}
	
	public function isAjaxUserRegistration()
	{
		return ($this->isUserRegistration())
			&& ($this->splashPage->getEnabledPage())
			&& ($this->splashPage->getIncludeRegistration() != Plumrocket_Privatesales_Model_Splashpage::INCLUDE_STATUS_STATIC);
	}
	
	public function isStaticUserRegistration()
	{
		return ($this->isUserRegistration())
			&& (($this->splashPage->getIncludeRegistration() != Plumrocket_Privatesales_Model_Splashpage::INCLUDE_STATUS_AJAX)
				|| (!$this->splashPage->getEnabledPage())
			);
	}
	
	// Forgot
	public function isForgotPassword()
	{
		return (!$this->splashPage->getEnabledPage())
			|| (!$this->splashPage->getEnabledLaunchingSoon());
	}
	
	public function isAjaxForgotPassword()
	{
		return ($this->isForgotPassword())
			&& ($this->splashPage->getEnabledPage())
			&& ($this->splashPage->getIncludeForgot() != Plumrocket_Privatesales_Model_Splashpage::INCLUDE_STATUS_STATIC);
	}
	
	public function isStaticForgotPassword()
	{
		return ($this->isForgotPassword())
			&& (($this->splashPage->getIncludeForgot() != Plumrocket_Privatesales_Model_Splashpage::INCLUDE_STATUS_AJAX)
				|| (!$this->splashPage->getEnabledPage())
			);
	}
	
	public function getRandomImage()
	{
		return $this->splashPage->getRandomImage();
	}
}

