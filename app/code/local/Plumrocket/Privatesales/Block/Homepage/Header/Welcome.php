<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Homepage_Header_Welcome extends Mage_Page_Block_Html_Header
{
    protected function _toHtml()
    {
        if (! Mage::helper('privatesales')->moduleEnabled()) {
            return '';
        }
        return parent::_toHtml();
    }

	public function addWelcomeLink()
	{
		$parentBlock = $this->getParentBlock();
        if ($parentBlock) {
			$text = $this->getWelcome();
            $parentBlock->addLink($text, null, $text, false, array(), 5, null, 'class="welcome-text"');
        }
        return $this;
	}
	
	public function getWelcome()
    {
        if (empty($this->_data['welcome'])) {
            if (Mage::isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->_data['welcome'] = $this->__('Welcome, %s!', $this->escapeHtml(Mage::getSingleton('customer/session')->getCustomer()->getName()));
            } else {
                $this->_data['welcome'] = Mage::getStoreConfig('design/header/welcome');
            }
        }

        return $this->_data['welcome'];
    }
}