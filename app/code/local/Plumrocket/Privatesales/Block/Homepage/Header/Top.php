<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Homepage_Header_Top extends Mage_Page_Block_Html_Header
{
	protected function _toHtml()
	{
		if (! Mage::helper('privatesales')->moduleEnabled()) {
			return '';
		}
		return parent::_toHtml();
	}

	public function getDate()
	{
		$offset = (int)Mage::getSingleton('core/session')->getData('debug_offset_time_on_days');
		$time = Mage::getModel('core/date')->timestamp(time()) + $offset * 3600 * 24;
		$date = ($time)? strftime('%b %e, %Y', $time): 'Yan 00, 0000';
		return $date;
	}
	
	public function isActive()
	{
		return (int)Mage::getStoreConfig('privatesales/all/future')
			&& (int)Mage::getSingleton('core/session')->getData('debug_offset_time_on');
	}

	public function getOffsetUrl()
	{
		$url = Mage::getUrl('privatesales/homepage/offset');
		if (! Mage::app()->getStore()->isCurrentlySecure()) {
	        $url = str_replace('https://', 'http://', $url);
	    } else {
	    	$url = str_replace('http://', 'https://', $url);
	    }
		return $url;
	}
}