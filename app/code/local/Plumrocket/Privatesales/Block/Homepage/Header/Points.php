<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

if (class_exists(Rewardpoints_Block_Dashboard)) {
	class Plumrocket_Privatesales_Block_Homepage_Header_Points extends Rewardpoints_Block_Dashboard
	{

		protected function _toHtml()
	    {
	        if (! Mage::helper('privatesales')->moduleEnabled()) {
	            return '';
	        }
	        return parent::_toHtml();
	    }
		
		public function addPointsLink($path, $label)
		{
			$parentBlock = $this->getParentBlock();
			if ($parentBlock) {
				$formattedAmount = Mage::helper('core')->currency($this->getPointsCurrent(), true, false);
				$countText = sprintf('<span class="color">%s</span>', $formattedAmount);
				$label = sprintf($label, $countText);
				$text = sprintf('<a href="%s">%s</a>', $path, $label);
				
				$parentBlock->addLink($text, null, $label, false, array(), 50, null, 'class="points-text"');
			}
			return $this;
		}
	}
} else {
	class Plumrocket_Privatesales_Block_Homepage_Header_Points extends Mage_Core_Block_Template
	{
		protected function _toHtml()
	    {
	        if (! Mage::helper('privatesales')->moduleEnabled()) {
	            return '';
	        }
	        return parent::_toHtml();
	    }
    
		public function addPointsLink($path, $label)
		{
			return $this;
		}
	}
}
