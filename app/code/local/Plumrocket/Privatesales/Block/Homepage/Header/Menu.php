<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Homepage_Header_Menu extends Mage_Core_Block_Template
{
	protected function _toHtml()
	{
		if (! Mage::helper('privatesales')->moduleEnabled()) {
			return '';
		}
		return parent::_toHtml();
	}

	public function renderCategoriesMenuHtml($ending = false)
    {
    	$mainTree = Mage::helper('privatesales/list')->getTree();
		$endingTree = ($ending)? Mage::helper('privatesales/list')->getEndingTree() : array();
    	$menuItems = Mage::helper('privatesales/list')->getMenuItems();
		
		$result = array();
		foreach ($menuItems as $menu) {
			$mainItems = (isset($mainTree[ $menu->getId() ]))? $mainTree[ $menu->getId() ]: array();
			$endingItems = (isset($endingTree[ $menu->getId() ]))? $endingTree[ $menu->getId() ]: array();
			
			$endingIds = array();
			foreach ($endingItems as $item) {
				$endingIds[] = $item->getId();
			}
			
			foreach ($mainItems as $key => $item) {
				if (in_array($item->getId(), $endingIds)) {
					unset($mainItems[$key]);
				}
			}
			
			$result[ $menu->getId() ] = array(
				'item' => $menu,
				'children' => $mainItems,
				'ending' => $endingItems
			);
		}
		
		return $result;
    }
	
	public function getCurrentMenuItem()
	{
		return Mage::registry('current_category');
	}
}
