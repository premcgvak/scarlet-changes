<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Homepage_Endingsoon extends Plumrocket_Privatesales_Block_Homepage_Index
{
	protected $cats = array();
	
	public function getBoutiques()
	{
		if (!$this->cats && Mage::helper('privatesales')->moduleEnabled()) {
			$itemId = Mage::helper('privatesales')->getActiveHomepage();
			$time = Mage::getModel('core/date')->timestamp(time());
			$map = Mage::helper('privatesales/map')->getCurrentMap();
			$period = $map->getEndingCountDays() * 3600 * 24;
			
			$cats = array();
			$_cats = Mage::helper('privatesales/list')->getAllBoutiques();
			
			if ($map->getEnabledEndingSoon()) {
		    	foreach ($_cats as $cat) {
		    		if ($cat->getIsActive(true)) {
		    			
	    				$timeEnd = Mage::helper('privatesales/list')->getTime($cat->getPrivatesaleDateEnd());
	    				if ($timeEnd == 0 || (($timeEnd - $time) >= $period)) {
	    					continue;
	    				} 
		
		    			if ($itemId && $cat->isParentCatalogById($itemId)) {
		    				$cats[] = $cat;
		    			} elseif (!$itemId) {
		    				$cats[] = $cat;
		    			}
		    		}
		    	}
			}

			$this->cats = $this->sortByPositions($cats);
	    	
	    	Plumrocket_Privatesales_Model_Page::foreachForImageHash('catalog', $this->cats);
		}
    	return $this->cats;
	}
	
	public function getMap()
	{
		$map = Mage::helper('privatesales/map')->getCurrentMap('end');
		
    	return Mage::helper('privatesales/map')->getMap(
    		$map->getMap(), 
    		$this->getBoutiques()
    	);
	}

	public function getColCount()
	{
		$map = Mage::helper('privatesales/map')->getCurrentMap('end');
		return $map->getColCount();
	}
}