<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Homepage_Index extends Mage_Core_Block_Template
{
	protected $cats = array();
	protected $page;
	
	public function __construct()
	{
		parent::__construct();
		$this->page = Mage::getModel('privatesales/homepage_page');
	}

	protected function _toHtml()
	{
		if (Mage::helper('privatesales')->moduleEnabled()) {
			return parent::_toHtml();
		} else {
			return '';
		}
	}
	
	protected function _prepareLayout()
    {
    	if (Mage::helper('privatesales')->moduleEnabled()) {
			$this->homePage = Mage::getSingleton('privatesales/homepage_page');
			
			$activeCatalog = Mage::registry('current_category');
			$obj = ($activeCatalog)? $activeCatalog: $this->homePage;
			
	        $head = $this->getLayout()->getBlock('head');
			$head->setTitle( $obj->getData('meta_title') );
			$head->setHeaderTitle( $obj->getData('meta_title') );
			$head->setKeywords( $obj->getData('meta_keywords') );
			$head->setDescription( $obj->getData('meta_description') );
		}
		
        return parent::_prepareLayout();
    }
	
	public function getBoutiques()
	{
		if (!$this->cats && Mage::helper('privatesales')->moduleEnabled()) {
			$itemId = Mage::helper('privatesales')->getActiveHomepage();
			$time = Mage::getModel('core/date')->timestamp(time());
			$map = Mage::helper('privatesales/map')->getCurrentMap();
			$period = $map->getEndingCountDays() * 3600 * 24;
			
			$cats = array();
			$_cats = Mage::helper('privatesales/list')->getAllBoutiques();
			
	    	foreach ($_cats as $cat) {
	    		if ($cat->getIsActive(true)) {
	    			if ($map->getEnabledEndingSoon()) {
	    				$timeEnd = Mage::helper('privatesales/list')->getTime($cat->getPrivatesaleDateEnd());
	    				if ($timeEnd != 0 && (($timeEnd - $time) < $period)) {
	    					continue;
	    				}
	    			}
		
	    			if ($itemId && $cat->isParentCatalogById($itemId)) {
	    				$cats[] = $cat;
	    			} elseif (!$itemId) {
	    				$cats[] = $cat;
	    			}
	    		}
	    	}

	    	$this->cats = $this->sortByPositions($cats);
	    	
	    	Plumrocket_Privatesales_Model_Page::foreachForImageHash('catalog', $this->cats);
		}
    	return $this->cats;
	}

	public function sortByPositions($cats)
	{
		// get all homepages from current store root category
		$homepages = Mage::helper('privatesales/list')->getMenuItems('admin');
		$idToSort = array();
		$i = 0;
		foreach ($homepages as $homepage) {
			$i++;
			$idToSort[ $homepage->getId() ] = array(
				'sort_id' => $i,
				'curr_pos' => 0,
			);
		}
		unset($menus);
		
		$sortedGroups = array();
		foreach ($cats as $cat) {
			$_pids = $cat->getParentIds();

			foreach ($_pids as $_pid) {
				if (array_key_exists($_pid, $idToSort)) {
					$iter = ++$idToSort[$_pid]['curr_pos'];
					if (! array_key_exists($iter, $sortedGroups)) {
						$sortedGroups[ $iter ] = array();
					}
					$sortedGroups[ $iter ][ $idToSort[$_pid]['sort_id'] ] = $cat;
					break;
				}
			}
		}

		$result = array();
		foreach ($sortedGroups as $i => $items) {
			ksort($items);
			foreach ($items as $p => $item) {
				$result[] = $item;
			}
		}
		return $result;
	}
	
	public function getMap()
	{
		$map = Mage::helper('privatesales/map')->getCurrentMap();
		
    	return Mage::helper('privatesales/map')->getMap(
    		$map->getMap(), 
    		$this->getBoutiques()
    	);
	}
	
	public function getBannersById()
	{
		$_banners = $this->page->getBanners();
		$banners = array();
		foreach ($_banners as $banner) {
			$banners[ $banner->getId() ] = $banner;
		}
		return $banners;
	}

	public function getColCount()
	{
		$map = Mage::helper('privatesales/map')->getCurrentMap();
		return $map->getColCount();
	}
	
	public function getSizes()
	{
		// already use cache into helper
		return Mage::helper('privatesales/list')->getSizes();
	}
	
	public function renderArray($data, $text)
	{
		foreach ($data as $key => $val) {
			if (is_string($val) || is_int($val) || $val == '') {
				$text = str_replace('%' . $key . '%', $val, $text);
			}
		}
		return $text;
	}
	
	public function renderBoutique($element, $text)
	{
		$data = $element->getData();
		$data['url'] = $element->getUrl();
		$data['image_url'] = $element->getImageUrl();
		$data['privatesale_date_start'] = Mage::helper('privatesales/list')->getTime($element->getPrivatesaleDateStart());
		
		$endTime = Mage::helper('privatesales/list')->getTime($element->getPrivatesaleDateEnd());
		$data['privatesale_date_end'] = $endTime;
		
		$leftTime = $endTime - Mage::getModel('core/date')->timestamp(time());
		if ($leftTime > 0) {
			$data['privatesale_date_end_left'] = $leftTime;
		} else {
			$data['privatesale_date_end_left'] = ($endTime > 0)? $this->__('This boutique expired.'):  0;
		}
		
		$sizes = $this->getSizes();
		$data['image_height'] = 0;
		if (isset($sizes['catalog'][ $element->getImage() ])) {
			$data['image_height'] = $sizes['catalog'][ $element->getImage() ]->getHeight();
		}
		
		return $this->renderArray($data, $text);
	}
	
	public function renderBanner($element, $text)
	{
		$data = $element->getData();
		$data['image_url'] = $element->getImageUrl();
		
		$sizes = $this->getSizes();
		$data['image_height'] = 0;
		if (isset($sizes['banner'][ $element->getImage() ])) {
			$data['image_height'] = $sizes['banner'][ $element->getImage() ]->getHeight();
		}
		
		return $this->renderArray($data, $text);
	}
}