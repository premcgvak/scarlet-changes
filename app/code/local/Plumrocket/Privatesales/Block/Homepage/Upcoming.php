<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Privatesales_Block_Homepage_Upcoming extends Mage_Core_Block_Template
{
	private $cats;
	
	public function __construct()
	{
		parent::__construct();
	}

	protected function _toHtml()
	{
		if (Mage::helper('privatesales')->moduleEnabled()) {
			return parent::_toHtml();
		} else {
			return '';
		}
	}
	
	public function getBoutiques()
	{
		if (!$this->cats) {
			$itemId = Mage::helper('privatesales')->getActiveHomepage();
			
			$this->cats = array();
			$cats = Mage::helper('privatesales/list')->getAllBoutiques();
				
			$time = Mage::getModel('core/date')->timestamp(time());
			$forCache = array();
			foreach ($cats as $cat) {
				$from = Mage::helper('privatesales/list')->getTime($cat->getPrivatesaleDateStart());
	    		if ($from > $time) {
	    			
	    			if ($itemId && $cat->isParentCatalogById($itemId)) {
	    				$fromReal = Mage::helper('privatesales/list')->getTime($cat->getPrivatesaleDateStart(), false);
	    				$this->cats[ date('Y-m-d', $fromReal) ][] = $cat;
	    				$forCache[] = $cat;
	    			} elseif (!$itemId) {
	    				$fromReal = Mage::helper('privatesales/list')->getTime($cat->getPrivatesaleDateStart(), false);
		    			$this->cats[ date('Y-m-d', $fromReal) ][] = $cat;
		    			$forCache[] = $cat;
	    			}
	    		}
	    	}
			if ($this->cats) {
				ksort($this->cats);
			}
	    	
	    	Plumrocket_Privatesales_Model_Page::foreachForImageHash('catalog', $forCache);
	    	unset($forCache);
		}
		return $this->cats;
	}
	
	public function isEnabledBlock()
	{
		$map = Mage::helper('privatesales/map')->getCurrentMap();
		return $map->getEnabledUpcomingSoon();
	}
}