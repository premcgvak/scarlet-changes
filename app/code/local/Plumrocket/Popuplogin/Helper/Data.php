<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Helper_Data extends Plumrocket_Popuplogin_Helper_Main
{
	public function moduleEnabled($store = null)
	{
		$enable = (bool)Mage::getStoreConfig('popuplogin/general/enable', $store);
		
		if ($enable) {
			// If the module is enabled then check access by stores id.
			$enable = false;
			$currentStoreId = Mage::app()->getStore()->getStoreId();
			
			$stores = explode(',', (string)Mage::getStoreConfig('popuplogin/general/visibility', $store));
			
			foreach ($stores as $id) {
				$storeId = (int)$id;
				if (($storeId == 0) || ($storeId == $currentStoreId)) {
					$enable = true;
					break;
				}
			}
		}
		return $enable;
	}

	public function disableExtension()
	{
		$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
		$connection->delete('core_config_data', array($connection->quoteInto('path IN (?)', array('popuplogin/general/enable', 'popuplogin/general/visibility', 'popuplogin/general/disable_on', 'popuplogin/register/success_page', 'popuplogin/register/welcome', 'popuplogin/login/success_page', 'popuplogin/design/animation', 'popuplogin/tracking/registration',))));
		$config = Mage::getConfig();
		$config->reinit();
		Mage::app()->reinitStores();
	}
}
	 
