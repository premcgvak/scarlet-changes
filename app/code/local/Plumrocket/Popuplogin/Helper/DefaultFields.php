<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Helper_DefaultFields extends Mage_Core_Helper_Abstract
{
	private $_data = null;

	public function getData()
	{
		if (is_null($this->_data)) {
			$fields = array(
				'firstname' 		=> 'First Name',
				'middlename' 		=> 'Middle Name',
				'lastname' 			=> 'Last Name',
				'suffix' 			=> 'Suffix',
				'email' 			=> 'Email',
				'confirm_email' 	=> 'Confirm Email',
				'password' 			=> 'Password',
				'confirm_password' 	=> 'Confirm Password',
				'dob' 				=> 'Date of Birth',
				'gender' 			=> 'Gender',
				'taxvat' 			=> 'Tax/VAT Number',
			);

			$result = array();
			$i = 10;
			foreach ($fields as $key => $label) {
				$result[$key] = array(
					'name'			=> $key,
					'orig_label' 	=> $label,
					'label' 		=> $label,
					'enable'		=> 0,
					'sort_order'	=> $i,
				);
				$i += 10;
			}
			$this->_data = $result;
		}
		return $this->_data;
	}
}
	 
