<?php
$installer = $this;
$installer->startSetup();

$tblName = $this->getTable('core_config_data');
$syncPaths = array(
	'popuplogin/settings/mode'				=> 'popuplogin/general/mode',
	'popuplogin/settings/login'				=> 'popuplogin/login/show',
	'popuplogin/settings/register'			=> 'popuplogin/register/show',
	'popuplogin/settings/forgotpassword'	=> 'popuplogin/forgotpassword/show',
	'popuplogin/settings/close'				=> 'popuplogin/general/close',
	'popuplogin/settings/welcome'			=> 'popuplogin/register/welcome',
);

foreach ($syncPaths as $from => $to) {
	$installer->run("
		UPDATE $tblName t1
			JOIN $tblName t2 ON t2.path = '$from'
		SET t1.`value` = t2.`value`
		WHERE t1.path = '$to'
	");	
}

$installer->endSetup();