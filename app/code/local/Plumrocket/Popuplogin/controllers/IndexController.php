<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package    Plumrocket_Popup_Login-v1.3.x
@copyright  Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license    http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_IndexController extends Mage_Core_Controller_Front_Action
{
    private $_formFields = array();

	public function registerAction()
    {
        if (! Mage::helper('popuplogin')->moduleEnabled()) {
            return false;
        }
        
        $session = Mage::getSingleton('customer/session');
        $value = Mage::getStoreConfig('popuplogin/register/form_fields');
        $this->_formFields = Mage::getSingleton('popuplogin/backend_formFields')->parseValue($value);
        
        try {
            $inputData = $this->getRequest()->getParams();
            $customer = $this->_initCustomer($inputData);

            if (!$this->_validateCustomer($customer)) {
                return false;
            }
            $customer->save();

            Mage::dispatchEvent('customer_register_success',
                array('account_controller' => $this, 'customer' => $customer)
            );

            if ($customer->isConfirmationRequired()) {
                $customer->sendNewAccountEmail(
                    'confirmation',
                    $session->getBeforeAuthUrl(),
                    Mage::app()->getStore()->getId()
                );
                $session->addSuccess(Mage::helper('popuplogin')->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));          
            } else {
                $customer->sendNewAccountEmail(
                    'registered',
                    '',
                    Mage::app()->getStore()->getId()
                );
                $session->setCustomerAsLoggedIn($customer);
                $session->renewSession();
            }
        }
        catch (Mage_Core_Exception $e) {
            $session->addError(Mage::helper('popuplogin')->__($e->getMessage()));
        } catch (Exception $e) {
            $session->addError(Mage::helper('popuplogin')->__($e->getMessage()));
        }        
    }

    protected function _initCustomer($data)
    {
        $customer = Mage::getModel('customer/customer')->setId(null);
        $customer->getGroupId();
        $customer->setData($data);

        if (!$this->_show('password')) {
            $customer->setPassword( $customer->generatePassword() );
        }
        return $customer;
    }

    protected function _show($name)
    {
        return (array_key_exists($name, $this->_formFields) && $this->_formFields[$name]['enable']);
    }

    protected function _validateCustomer($customer)
    {
        $session = Mage::getSingleton('customer/session');
        $success = true;

        if ($this->_show('firstname')) {
            if (!Zend_Validate::is( trim($customer->getFirstname()) , 'NotEmpty')) {
                $session->addError(Mage::helper('customer')->__('The first name cannot be empty.'));
                $success = false;
            }
        }

        if ($this->_show('lastname')) {
            if (!Zend_Validate::is( trim($customer->getLastname()) , 'NotEmpty')) {
                $session->addError(Mage::helper('customer')->__('The last name cannot be empty.'));
                $success = false;
            }
        }

        if (!Zend_Validate::is($customer->getEmail(), 'EmailAddress')) {
            $session->addError(Mage::helper('customer')->__('Invalid email address "%s".', $customer->getEmail()));
            $success = false;
        }

        if ($this->_show('confirm_email')) {
            if ($customer->getEmail() != $customer->getConfirmEmail()) {
                $session->addError(Mage::helper('customer')->__('Please make sure your emails match.'));
                $success = false;
            }
        }

        $password = $customer->getPassword();
        if (!$customer->getId() && !Zend_Validate::is($password , 'NotEmpty')) {
            $session->addError(Mage::helper('customer')->__('The password cannot be empty.'));
            $success = false;
        }
        if (strlen($password) && !Zend_Validate::is($password, 'StringLength', array(6))) {
            $session->addError(Mage::helper('customer')->__('The minimum password length is %s', 6));
            $success = false;
        }
        if ($this->_show('confirm_password')) {
            $confirmation = $customer->getConfirmation();
            if ($password != $confirmation) {
                $session->addError(Mage::helper('customer')->__('Please make sure your passwords match.'));
                $success = false;
            }
        }

        $entityType = Mage::getSingleton('eav/config')->getEntityType('customer');

        if ($this->_show('dob')) {
            $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'dob');
            if ($attribute->getIsRequired() && '' == trim($customer->getDob())) {
                $session->addError(Mage::helper('customer')->__('The Date of Birth is required.'));
                $success = false;
            }
        }

        if ($this->_show('taxvat')) {
            $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'taxvat');
            if ($attribute->getIsRequired() && '' == trim($customer->getTaxvat())) {
                $session->addError(Mage::helper('customer')->__('The TAX/VAT number is required.'));
                $success = false;
            }
        }

        if ($this->_show('gender')) {
            $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'gender');
            if ($attribute->getIsRequired() && '' == trim($customer->getGender())) {
                $session->addError(Mage::helper('customer')->__('Gender is required.'));
                $success = false;
            }
        }
        return $success;
    }
}