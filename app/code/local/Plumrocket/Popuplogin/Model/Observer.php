<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Model_Observer
{
	public function customerPostDispach(Varien_Event_Observer $observer)
    {
    	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
			&& (strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
			&& Mage::helper('popuplogin')->moduleEnabled()
		) {
	    	$front = $observer->getEvent()->getControllerAction();
	    	$front->loadLayout();
			
			$block = $front->getLayout()->getMessagesBlock();
			foreach (array('customer/session', 'catalog/session') as $storageName) {
	            $storage = Mage::getSingleton($storageName);
	            if ($storage) {
	                $block->addMessages($storage->getMessages(true));
	                $block->setEscapeMessageFlag($storage->getEscapeMessages(true));
	                $block->addStorageType($storageName);
	            }
			}
			
			$front->getResponse()
                ->clearHeader('Location')
                ->clearRawHeader('Location')
                ->setHttpResponseCode(200)
                ->setBody($block->getGroupedHtml());
		} else {
			return $observer;
		}
	}
	
	public function customerRegisterSuccess(Varien_Event_Observer $observer)
    {
    	if (Mage::helper('popuplogin')->moduleEnabled()) {
    		Mage::getSingleton('core/session')->setShowPopupAfterRegistrationCode(true);
    	}
		return $observer;
	}

	// -- Adminhtml ---
	public function adminSystemConfigChangedSection($observer)
	{
		$data = $observer->getEvent()->getData();

		if ($data['section'] == 'popuplogin') {
			$groups = Mage::app()->getRequest()->getParam('groups');
			$defaultForm  		= $groups['general']['fields']['default_form']['value'];
			$loginEnabled 		= $groups['login']['fields']['show']['value'];
			$registerEnabled 	= $groups['register']['fields']['show']['value'];

			if ($defaultForm == 'login') {
				if (! $loginEnabled) {
					Mage::getSingleton('adminhtml/session')->addWarning('Login Form option cannot be chosen. To choose it please enable "Show Login Form" option.<br />Otherwise Registration Form will be displayed by default.'); 
				}
			} else {
				if (! $registerEnabled) {
					Mage::getSingleton('adminhtml/session')->addWarning('Registration Form option cannot be chosen. To choose it please enable "Show Registration Form"option.<br />Otherwise Login Form will be displayed by default.'); 
				}
			}
    	}
    	return $observer;
	}
}
