<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package    Plumrocket_Popup_Login-v1.3.x
@copyright  Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license    http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Model_Values_Pages
{
    const STAY_ON_PAGE  = '__stay__';
    const CUSTOM_URL    = '__custom__';
    const ACCOUNT_PAGE  = '__account__';
    const LOGIN_PAGE    = '__login__';

    public function toOptionArray()
    {
        $pages = array(
            array('value' => self::STAY_ON_PAGE,    'label' => Mage::helper('popuplogin')->__('Stay on current page')),
            array('value' => self::CUSTOM_URL,      'label' => Mage::helper('popuplogin')->__('Redirect to Custom URL')),
            array('value' => '__none__',            'label' => Mage::helper('popuplogin')->__('----')),
            array('value' => self::ACCOUNT_PAGE,    'label' => Mage::helper('popuplogin')->__('Customer -> Account Dashboard')),
            array('value' => self::LOGIN_PAGE,      'label' => Mage::helper('popuplogin')->__('Login Page')),
        );

        $items = Mage::getModel('cms/page')->getCollection();
        foreach ($items as $item) {
            $pages[] = array(
                'value' => $item->getIdentifier(),
                'label' => $item->getTitle()
            );
        }
        return $pages;
    }
}