<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Model_Values_Animations
{
    public function toOptionArray()
    {
        return array(
        	array('value' => '',				'label'	=> 'None'),
            array('value' => 'fadeIn',			'label' => Mage::helper('popuplogin')->__('Fade In')),
            array('value' => 'fadeInDown',		'label' => Mage::helper('popuplogin')->__('Fade In (Down)')),
            array('value' => 'fadeInLeft',      'label' => Mage::helper('popuplogin')->__('Fade In (Left)')),
            array('value' => 'fadeInRight',     'label' => Mage::helper('popuplogin')->__('Fade In (Right)')),
            array('value' => 'fadeInUp',        'label' => Mage::helper('popuplogin')->__('Fade In (Up)')),
            array('value' => 'fadeInDownBig',	'label' => Mage::helper('popuplogin')->__('Slide In (Down) - Default')),
            array('value' => 'fadeInLeftBig',	'label' => Mage::helper('popuplogin')->__('Slide In (Left)')),
            array('value' => 'fadeInRightBig',	'label' => Mage::helper('popuplogin')->__('Slide In (Right)')),
            array('value' => 'fadeInUpBig',		'label' => Mage::helper('popuplogin')->__('Slide In (Up)')),
            array('value' => 'zoomIn',			'label' => Mage::helper('popuplogin')->__('Zoom In')),
            array('value' => 'flip3d_hor',      'label' => Mage::helper('popuplogin')->__('3D Flip (horizontal)')),
        );
    }
}