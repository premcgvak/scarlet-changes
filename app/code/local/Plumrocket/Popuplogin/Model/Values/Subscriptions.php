<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Model_Values_Subscriptions
{
    const SHOW_CHECKED = 1;
	const SHOW_UNCHECKED = 2;
	const HIDE_AND_SUBSCRIBE = 3;
	const HIDE = 4;
    
    public function toOptionArray()
    {
        return array(
            array('value' => self::SHOW_CHECKED, 		'label' => Mage::helper('popuplogin')->__('Show Newsletter Checkbox Checked')),
            array('value' => self::SHOW_UNCHECKED, 		'label' => Mage::helper('popuplogin')->__('Show Newsletter Checkbox Unchecked')),
            array('value' => self::HIDE_AND_SUBSCRIBE, 	'label' => Mage::helper('popuplogin')->__('Do not Show Newsletter Checkbox and Auto-Subscribe all Users')),
            array('value' => self::HIDE, 				'label' => Mage::helper('popuplogin')->__('Do not Subscribe to Newsletter')),
        );
    }
}