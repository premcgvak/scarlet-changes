<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Model_Values_Forms
{
    const LOGIN = 'login';
	const REGISTER = 'register';
    
    public function toOptionArray()
    {
        return array(
            array('value' => self::LOGIN,    'label' => Mage::helper('popuplogin')->__('Login Form')),
            array('value' => self::REGISTER, 'label' => Mage::helper('popuplogin')->__('Registration Form')),
        );
    }
}