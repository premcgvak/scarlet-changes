<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Model_Values_Themes
{   
	const LIGHT_SILVER = 'simple-form-logo';
    const _DEFAULT = 'prpop-default';
    const GLAMOUR_GRAY = 'glamour-grey';
    
    public function toOptionArray()
    {
        return array(
            array('value' => self::LIGHT_SILVER, 'label' => Mage::helper('popuplogin')->__('Light Silver - Default')),
            array('value' => self::_DEFAULT, 'label' => Mage::helper('popuplogin')->__('Modern Blue')),
            array('value' => self::GLAMOUR_GRAY, 'label' => Mage::helper('popuplogin')->__('Glamour Gray')),
        );
    }
}