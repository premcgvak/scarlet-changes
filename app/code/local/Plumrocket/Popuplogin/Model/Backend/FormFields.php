<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Model_Backend_FormFields extends Mage_Core_Model_Config_Data
{
    public function parseValue($value)
    {
        $result = Mage::helper('popuplogin/defaultFields')->getData();
        $values = json_decode($value);
        if ($values) {
            foreach ($values as $name => $value) {
                if (is_array($value) 
                    && array_key_exists($name, $result)
                ) {
                    $result[$name]['label'] = (isset($value[0]))? (string)$value[0]: $result[$name]['label'];
                    $result[$name]['enable'] = (isset($value[1]))? (int)$value[1]: $result[$name]['enable'];
                    $result[$name]['sort_order'] = (isset($value[2]))? (int)$value[2]: $result[$name]['sort_order'];
                }
            }
        }

        uasort($result, array($this, '_sortFields'));
        return $result;
    }

    protected function _sortFields($a, $b)
    {
        if (!isset($a['sort_order'])) {
            $a['sort_order'] = 0;
        }
        if (!isset($b['sort_order'])) {
            $b['sort_order'] = 0;
        }
        if ($a['sort_order'] == $b['sort_order']) {
            return 0;
        }
        return ($a['sort_order'] < $b['sort_order']) ? -1 : 1;
    }

    protected function _afterLoad()
    {
        $value = $this->parseValue($this->getValue());
		$this->setValue($value);
		parent::_afterLoad();
    }
 
    protected function _beforeSave()
    {
    	$toSave = array();
    	$values = $this->getValue();
    	$result = Mage::helper('popuplogin/defaultFields')->getData();
    	
    	foreach ($values as $name => $value) {
    		if (array_key_exists($name, $result)) {
    			$toSave[$name] = array(
    				(isset($value['label'])? (string)$value['label']: ''),
    				(int)isset($value['enable']),
                    (int)$value['sort_order']
    			);
    		}
    	}
        if (array_key_exists('email', $toSave)) {
            $toSave['email'][1] = 1;
        }
        // enable password if not and confirmation already enabled
        if (array_key_exists('confirm_password', $toSave)
            && $toSave['confirm_password'][1]
            && !(
                array_key_exists('password', $toSave)
                && $toSave['password'][1]
            )
        ) {
            $toSave['password'][1] = 1;
        }
    	$this->setValue(json_encode($toSave));
        parent::_beforeSave();
    }
}