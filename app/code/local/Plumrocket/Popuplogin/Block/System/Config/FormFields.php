<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package    Plumrocket_Popup_Login-v1.3.x
@copyright  Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license    http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Block_System_Config_FormFields extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		return $this->getLayout()->createBlock('popuplogin/system_config_formFields_inputTable')
			->setContainerFieldId($element->getName())
			->setRowKey('name')
			->addColumn('orig_label', array(
				'header'    => Mage::helper('popuplogin')->__('Field'),
				'index'     => 'orig_label',
				'type'      => 'label',
				'width'     => '36%',
			))
			->addColumn('label', array(
				'header'    => Mage::helper('popuplogin')->__('Displayed Name'),
				'index'     => 'label',
				'type'      => 'input',
				'width'     => '28%',
			))
			->addColumn('sort_order', array(
				'header'    => Mage::helper('popuplogin')->__('Sort Order'),
				'index'     => 'sort_order',
				'type'      => 'input',
				'width'     => '28%',
			))
			->addColumn('enable', array(
				'header'    => Mage::helper('popuplogin')->__('Enable'),
				'index'     => 'enable',
				'type'      => 'checkbox',
				'value'     => 1,
				'width'     => '8%',
			))
			->setArray($element->getValue())
			->toHtml();
	}

	public function render(Varien_Data_Form_Element_Abstract $element)
    {
    	$html = parent::render($element);
    	$html = str_replace('<td class="value', '<td colspan="2" class="value', $html);
    	// delete last td
    	$html = str_replace('<td class=""></td>', '', $html);
        
    	return $html;
    }
}