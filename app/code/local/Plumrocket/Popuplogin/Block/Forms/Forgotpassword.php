<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Block_Forms_Forgotpassword extends Mage_Customer_Block_Account_Forgotpassword
{
	protected function _toHtml()
	{
		if (Mage::helper('popuplogin')->moduleEnabled()
			&&(bool)Mage::getStoreConfig('popuplogin/forgotpassword/show')
		) {} else {
			$this->setTemplate('popuplogin/empty.phtml');
		}
		return parent::_toHtml();
	}
	
	public function getPostActionUrl()
	{
		$url = Mage::getUrl('customer/account/forgotpasswordpost');
		if (! Mage::app()->getStore()->isCurrentlySecure()) {
	        $url = str_replace('https://', 'http://', $url);
	    }
		return $url;
	}

	public function showAfterLoad()
	{
		// if not showed register and login forms then should display it form
		return ! ((bool)Mage::getStoreConfig('popuplogin/register/show')
			|| (bool)Mage::getStoreConfig('popuplogin/login/show')
		);
	}

	public function placeholder($text)
	{
		return Mage::getStoreConfig('popuplogin/design/theme') == Plumrocket_Popuplogin_Model_Values_Themes::LIGHT_SILVER? 
			' placeholder="' . $this->__($text) . '" ': '';
	}
}

