<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Block_Forms_Login extends Mage_Customer_Block_Form_Login
{
	private $userMode = NULL;
	
	protected function _prepareLayout()
    {
        return Mage_Core_Block_Template::_prepareLayout();
    }
	
	protected function _toHtml()
	{
		if (Mage::helper('popuplogin')->moduleEnabled() 
			&& (bool)Mage::getStoreConfig('popuplogin/login/show')
		) {} else {
			$this->setTemplate('popuplogin/empty.phtml');
		}
		return parent::_toHtml();
	}
	
	public function showLogin()
	{
		return (bool)Mage::getStoreConfig('popuplogin/login/show');
	}
	
	public function showRegister()
	{
		return (bool)Mage::getStoreConfig('popuplogin/register/show');
	}
	
	public function showForgotpassword()
	{
		return (bool)Mage::getStoreConfig('popuplogin/forgotpassword/show');
	}
	
	public function showRememberMe()
	{
		return (bool)(($node = Mage::getConfig()->getNode('modules/Clockworkgeek_Rememberme'))
			&& ((string)$node->active === 'true'));
	}
	
	public function getWelcomeText()
	{
		$_cmsHelper = Mage::helper('cms');
		$_process = $_cmsHelper->getBlockTemplateProcessor();
		return $_process->filter( Mage::getStoreConfig('popuplogin/register/welcome') );
	}
	
	public function getPostActionUrl()
	{
		$url = parent::getPostActionUrl();
		if (! Mage::app()->getStore()->isCurrentlySecure()) {
	        $url = str_replace('https://', 'http://', $url);
	    }
		return $url;
	}

	public function showAfterLoad()
	{
		return Mage::getStoreConfig('popuplogin/general/default_form') == Plumrocket_Popuplogin_Model_Values_Forms::LOGIN
			|| ((bool)Mage::getStoreConfig('popuplogin/register/show') === false);
	}

	public function placeholder($text)
	{
		return Mage::getStoreConfig('popuplogin/design/theme') == Plumrocket_Popuplogin_Model_Values_Themes::LIGHT_SILVER? 
			' placeholder="' . $this->__($text) . '" ': '';
	}
}

