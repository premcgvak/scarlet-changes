<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Block_Forms_Register extends Mage_Customer_Block_Form_Register
{
	protected $_formFields = array();

	protected function _prepareLayout()
    {
    	return Mage_Directory_Block_Data::_prepareLayout();
    }
	
	public function showAgree()
	{
		$attribute = Mage::getModel('eav/config')->getAttribute('customer', 'agree');
		return ($attribute && $attribute->getId());
	}
	
	protected function _toHtml()
	{
		if (Mage::helper('popuplogin')->moduleEnabled() 
			&& (bool)Mage::getStoreConfig('popuplogin/register/show')
		) {
			$value = Mage::getStoreConfig('popuplogin/register/form_fields');
    		$this->_formFields = Mage::getSingleton('popuplogin/backend_formFields')->parseValue($value);
		
			$mode = Mage::getStoreConfig('popuplogin/register/subscribe');
			if (! $this->getFormData()->hasIsSubscribed()
				&& (
					($mode == Plumrocket_Popuplogin_Model_Values_Subscriptions::SHOW_CHECKED)
					|| ($mode == Plumrocket_Popuplogin_Model_Values_Subscriptions::HIDE_AND_SUBSCRIBE)
			)) {
				$this->getFormData()->setIsSubscribed(1);
			}
		} else {
			$this->setTemplate('popuplogin/empty.phtml');
		}
		return parent::_toHtml();
	}

	public function createWidget($name) 
	{
		switch ($name) {
			case 'taxvat':
				$blockName = 'customer/widget_taxvat';
				break;
			case 'dob':
				$blockName = 'customer/widget_dob';
				break;
			case 'gender':
				$blockName = 'customer/widget_gender';
				break;
			default:
				$blockName = 'core/template';
				break;
		}
		$block = $this->getLayout()
			->createBlock($blockName)
			->setTemplate('popuplogin/forms/register/' . $name . '.phtml');
		
		$label = (array_key_exists($name, $this->_formFields))? $this->_formFields[$name]['label']: '';
		$block->setLabel($label);
		if (Mage::getStoreConfig('popuplogin/design/theme') == Plumrocket_Popuplogin_Model_Values_Themes::LIGHT_SILVER) {
			$block->setPlaceholder($label);
		}
		return $block;
	}

	public function showAfterLoad()
	{
		return Mage::getStoreConfig('popuplogin/general/default_form') == Plumrocket_Popuplogin_Model_Values_Forms::REGISTER
			|| ((bool)Mage::getStoreConfig('popuplogin/login/show') === false);
	}
	
	public function getPostActionUrl()
	{
		//$url = parent::getPostActionUrl();
		$url = $this->getUrl('popuplogin/index/register');
		if (! Mage::app()->getStore()->isCurrentlySecure()) {
	        $url = str_replace('https://', 'http://', $url);
	    }
		return $url;
	}

	public function isNewsletterEnabled()
	{
		$mode = Mage::getStoreConfig('popuplogin/register/subscribe');
		return ($mode == Plumrocket_Popuplogin_Model_Values_Subscriptions::SHOW_CHECKED)
			|| ($mode == Plumrocket_Popuplogin_Model_Values_Subscriptions::SHOW_UNCHECKED);
	}

	public function getChildHtml($name = '', $useCache = true, $sorted = false)
	{
		if ($name === '') {
			$out = '';
			$i = 0;
			foreach ($this->_formFields as $name => $item) {
				if ((bool)$item['enable']) {
					$_block = $this->createWidget($name);
					if ($_block) {
						$out .= $_block->toHtml();
						if (++$i == 2) {
							$out .= '<li class="row-special"></li>';
							$i = 0;
						}
					}
				}
			}
			return $out;
		}
		return parent::getChildHtml($name, $useCache, $sorted);
	}
}

