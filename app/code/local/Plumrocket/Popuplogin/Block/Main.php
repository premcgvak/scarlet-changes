<?php

/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

class Plumrocket_Popuplogin_Block_Main extends Mage_Core_Block_Template
{
	private $userMode = NULL;
	
	protected function _toHtml()
	{
		if (Mage::getSingleton('plumbase/observer')->customer() != Mage::getSingleton('plumbase/product')->currentCustomer()
			|| ! Mage::helper('popuplogin')->moduleEnabled()
			|| $this->helper('customer')->isLoggedIn()
			|| ! $this->_checkLocation()
		) {
			$this->setTemplate('popuplogin/empty.phtml');
		}
		return parent::_toHtml();
	}

	protected function _checkLocation()
	{
		$currentUrl = Mage::helper('core/url')->getCurrentUrl();
		$currentUrl = $this->_getRelativePath($currentUrl);

		$urls = explode("\n", Mage::getStoreConfig('popuplogin/general/disable_on'));
		foreach ($urls as $url) {
			if ($this->_getRelativePath($url) == $currentUrl) {
				return false;
			}
		}
		return true;
	}

	protected function _endSlash($path)
	{
		$_len = strlen($path);
		if ($_len > 0 && $path[$_len - 1] != '/') {
			$path .= '/';
		}
		return $path;
	}

	protected function _getRelativePath($path)
	{
		$mainUrl = $this->getUrl();
		$mainUrl = $this->_endSlash($mainUrl);
		$path = $this->_endSlash($path);
		$path = str_replace($mainUrl, '', $path);
		if (strlen($path) == 0 || $path[0] != '/') {
			$path = '/' . $path;
		}
		return $path;
	}
	
	public function setMode($mode)
	{
		$this->userMode = (int)$mode;
	}
	
	public function mode()
	{
		$mode = (int)Mage::getStoreConfig('popuplogin/general/mode');
		if (! is_null($this->userMode)) {
			$mode = $this->userMode;
		}
		return $mode;
	}
	
	public function showEventTracking()
	{
		return (int)Mage::getStoreConfig('popuplogin/tracking/google');
	}

	public function getTheme()
	{
		return Mage::getStoreConfig('popuplogin/design/theme');
	}

	public function getAnimation()
	{
		return Mage::getStoreConfig('popuplogin/design/animation');
	}

	public function getSuccessUrl($form = 'login')
	{
		$successPage = Mage::getStoreConfig('popuplogin/' . $form . '/success_page');
		$customPage = Mage::getStoreConfig('popuplogin/' . $form . '/custom_page');
		$page = '';
		switch ($successPage) {
			case '':
			case Plumrocket_Popuplogin_Model_Values_Pages::STAY_ON_PAGE:
				$page = '';
				break;
			case Plumrocket_Popuplogin_Model_Values_Pages::CUSTOM_URL:
				$page = $customPage;
				break;
			case Plumrocket_Popuplogin_Model_Values_Pages::ACCOUNT_PAGE:
				$page = Mage::getUrl('customer/account');
				break;
			case Plumrocket_Popuplogin_Model_Values_Pages::LOGIN_PAGE:
				$page = Mage::getUrl('customer/account/login');
				break;
			default:
				$page = Mage::getUrl($successPage);
				break;
		}
		return $page;
	}

	// -- For children: call like $this->getParentBlock()->method()
	// ..
	public function showCloseOn()
	{
		return (bool)Mage::getStoreConfig('popuplogin/general/close');
	}

	public function showLogin()
	{
		return (bool)Mage::getStoreConfig('popuplogin/login/show');
	}

	public function showCloseOnLogin()
	{
		return $this->showCloseOn()
			&& $this->showLogin();
	}
	
	public function showCloseOnRegister()
	{
		return $this->showCloseOn()
			&& !$this->showLogin();
	}

	public function getLoaderImageSrc()
	{
		$fName = $this->getTheme();

		switch ($fName) {
			case 'glamour-grey':
				$fName_str = 'ajax-loader-h-dark.gif';
				break;
			case 'prpop-default':
				$fName_str = 'ajax-loader-h.gif';
				break;
			case 'simple-form-logo':
			default:
				$fName_str = 'ajax-loader-h-sf.gif';
		}
		return $this->getSkinUrl('images/plumrocket/popuplogin/' . $fName_str);
	}

	public function getLogoImageSrc()
	{
		$path = Mage::getStoreConfig('popuplogin/design/logo');
		if (!$path) {
			return $this->getSkinUrl('images/plumrocket/popuplogin/pop-top-logo.png');
		}
		return Mage::getBaseUrl('media') . 'popuplogin/' . $path;
	}

	public function isConfirmationRequired()
	{
		return Mage::getSingleton('customer/customer')->isConfirmationRequired();
	}
}

