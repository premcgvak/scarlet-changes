<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

try {
    
    $installer->run("ALTER TABLE `".$this->getTable('ch_canadapost_quote_param')."` DROP FOREIGN KEY ch_canadapost_quote_param_ibfk_1 ");
    
    $installer->run("ALTER TABLE `".$this->getTable('ch_canadapost_quote_param')."` ADD `magento_order_id` INT( 11 ) NULL ;");
    
    $installer->run("ALTER TABLE `".$this->getTable('ch_canadapost_quote_param')."` 
                        ADD CONSTRAINT `ch_canadapost_quote_param_ibfk_1` 
                        FOREIGN KEY ( `magento_quote_id` ) 
                        REFERENCES `".$this->getTable('sales_flat_quote')."` 
                        (`entity_id`) ON DELETE SET NULL ON UPDATE SET NULL ;");
    
} catch (Exception $e) {

}



$installer->endSetup();
