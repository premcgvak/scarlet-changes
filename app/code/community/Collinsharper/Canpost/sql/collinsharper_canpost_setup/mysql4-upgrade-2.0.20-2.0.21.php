<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

try {
    $installer->run("ALTER TABLE `".$this->getTable('ch_canadapost_shipment')."` ADD COLUMN last_update datetime");
} catch (Exception $e) {
    // Check if we're just trying to re-run scripts and the column already exists.
    if (strpos($e->getMessage(), "SQLSTATE[42S21]: Column already exists") === false) {
        throw $e;
    }
}

try {
    $installer->run("ALTER TABLE `".$this->getTable('ch_canadapost_shipment')."` ADD COLUMN update_message  varchar(255)");
} catch (Exception $e) {

    if (strpos($e->getMessage(), "SQLSTATE[42S21]: Column already exists") === false) {
        throw $e;
    }
}

try {
    $installer->run("ALTER TABLE `".$this->getTable('ch_canadapost_shipment')."` ADD COLUMN update_errors int(11) ");
} catch (Exception $e) {

    if (strpos($e->getMessage(), "SQLSTATE[42S21]: Column already exists") === false) {
        throw $e;
    }
}



$installer->endSetup();
