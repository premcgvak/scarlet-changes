<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$attribute = new Mage_Eav_Model_Entity_Attribute();

$attribute->loadByCode('catalog_product', 'signature');

if($attribute->getAttributeId() == '') {

    $installer->addAttribute('catalog_product', 'signature', array(
            'group' => 'General',
            'type' => 'int',
            'backend' => '',
            'frontend' => '',
            'label' => 'Require Signature on Shipping',
            'note' => 'Require Signature on Shipping',
            'input' => 'boolean',
            'class' => '',
            'source' => 'eav/entity_attribute_source_table',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'default' => '0',
            'visible_on_front' => false,
            'unique' => false,
            'is_configurable' => false,
            'used_for_promo_rules' => true
        ));

}

$attribute->loadByCode('catalog_product', 'proof_of_age');

if($attribute->getAttributeId() == '') {

    $installer->addAttribute('catalog_product', 'proof_of_age', array(
            'group' => 'General',
            'type' => 'varchar',
            'backend' => '',
            'frontend' => '',
            'label' => 'Canada Post Proof of Age Required',
            'note' => 'Canada Post Proof of Age Required',
            'input' => 'select',
            'class' => '',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'default' => '0',
            'visible_on_front' => false,
            'unique' => false,
            'is_configurable' => false,
            'used_for_promo_rules' => true,
            'option' => array(
                'value' => array(
                    'none' => array(Mage::helper('adminhtml')->__('None')),
                    'PA18' => array('18+'),
                    'PA19' => array('19+'),
                    'by_province' => array('18+ or 19+ by province'),
                )
            ),
            'default' => array('none'),
    ));

}

$installer->run("DROP TABLE IF EXISTS `ch_canadapost_manifest`");

$installer->run("CREATE TABLE `ch_canadapost_manifest` (
  `id` int(11) NOT NULL auto_increment,
  `group_id` varchar(255) NOT NULL default '',
  `url` varchar(255) NOT NULL default '',
  `media_type` varchar(100) NOT NULL default '',
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL,
  `status` VARCHAR(100) NOT NULL default '',
  PRIMARY KEY  (`id`))
  ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->run("DROP TABLE IF EXISTS `ch_canadapost_office`");

$installer->run("CREATE TABLE `ch_canadapost_office` (
  `id` int(11) NOT NULL auto_increment,
  `city` varchar(255) NOT NULL default '',
  `postal_code` varchar(6) NOT NULL default '',
  `province` varchar(2) NOT NULL default '',
  `address` varchar(255) NOT NULL default '',
  `location` varchar(255) NOT NULL default '',
  `link` varchar(255) NOT NULL default '',
  `media_type` varchar(255) NOT NULL default '',
  `cp_office_id` varchar(20) NOT NULL default '',
  `cp_office_name` varchar(100) NOT NULL default '',
  `bilingual` TINYINT(1) NOT NULL default 0,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `status` VARCHAR(100) NOT NULL default '',
  PRIMARY KEY  (`id`))
  ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->run("DROP TABLE IF EXISTS `ch_canadapost_return`");

$installer->run("CREATE TABLE `ch_canadapost_return` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` int(10) NOT NULL,
  `shipment_id` int(10) NULL,
  `status` varchar(20) NOT NULL default '',
  `tracking_pin` varchar(100) NOT NULL default '',
  `link` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->run("DROP TABLE IF EXISTS `ch_canadapost_manifest_link`");

$installer->run("CREATE TABLE `ch_canadapost_manifest_link` (
  `id` int(11) NOT NULL auto_increment,
  `manifest_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  FOREIGN KEY (manifest_id) REFERENCES ch_canadapost_manifest(id) ON DELETE CASCADE) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

//$installer->run("ALTER TABLE `sales_flat_shipment` ADD COLUMN manifest_id int(11) NULL;");
//$installer->run("ALTER TABLE `sales_flat_quote` ADD COLUMN `signature` tinyint(1) NOT NULL default 0;");
//$installer->run("ALTER TABLE `sales_flat_quote` ADD COLUMN `coverage` tinyint(1) NOT NULL default 0;");
//$installer->run("ALTER TABLE `sales_flat_quote` ADD COLUMN cp_office_id int(11) NULL;");
//$installer->run("ALTER TABLE `sales_flat_quote` ADD COLUMN `card_for_pickup` tinyint(1) NOT NULL default 0;");
//$installer->run("ALTER TABLE `sales_flat_quote` ADD COLUMN `do_not_safe_drop` tinyint(1) NOT NULL default 0;");
//$installer->run("ALTER TABLE `sales_flat_quote` ADD COLUMN `leave_at_door` tinyint(1) NOT NULL default 0;");

$installer->run("DROP TABLE IF EXISTS `ch_canadapost_quote_param`");

$installer->run("CREATE TABLE `ch_canadapost_quote_param` (
  `id` int(11) NOT NULL auto_increment,
  `magento_quote_id` int(10) unsigned NULL,
  `signature` tinyint(1) NOT NULL default 0,
  `coverage` tinyint(1) NOT NULL default 0,
  `cp_office_id` int(11) NULL,
  `card_for_pickup` tinyint(1) NOT NULL default 0,
  `do_not_safe_drop` tinyint(1) NOT NULL default 0,
  `leave_at_door` tinyint(1) NOT NULL default 0,
  PRIMARY KEY  (`id`),
  FOREIGN KEY (`magento_quote_id`) REFERENCES  sales_flat_quote(entity_id) ON DELETE CASCADE,
  FOREIGN KEY (cp_office_id) REFERENCES   ch_canadapost_office(id)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

$installer->endSetup();