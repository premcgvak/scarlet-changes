<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$attribute = new Mage_Eav_Model_Entity_Attribute();

$attribute->loadByCode('catalog_product', 'proof_of_age');

if($attribute->getAttributeId() == '') {

    $installer->addAttribute('catalog_product', 'proof_of_age', array(
            'group' => 'General',
            'type' => 'varchar',
            'backend' => '',
            'frontend' => '',
            'label' => 'Canada Post Proof of Age Required',
            'note' => 'Canada Post Proof of Age Required',
            'input' => 'select',
            'class' => '',
            'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'default' => '0',
            'visible_on_front' => false,
            'unique' => false,
            'is_configurable' => false,
            'used_for_promo_rules' => true,
            'option' => array(
                'value' => array(
                    'none' => array(Mage::helper('adminhtml')->__('None')),
                    'PA18' => array('18+'),
                    'PA19' => array('19+'),
                    'by_province' => array('18+ or 19+ by province'),
                )
            ),
            'default' => array('none'),
    ));

}

$installer->endSetup();