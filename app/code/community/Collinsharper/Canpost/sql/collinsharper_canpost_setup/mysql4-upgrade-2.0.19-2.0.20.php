<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

try {
    $installer->run("ALTER TABLE `".$this->getTable('sales_flat_shipment')."` ADD COLUMN shipment_cost  decimal(12,4) COMMENT 'Merchant Shipment Cost'");
} catch (Exception $e) {
    // Check if we're just trying to re-run scripts and the column already exists.
    if (strpos($e->getMessage(), "SQLSTATE[42S21]: Column already exists") === false) {
        throw $e;
    }
}


$installer->endSetup();
