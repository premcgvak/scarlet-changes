<?php

/**
 *
 * @category    Collinsharper
 * @package     Collinsharper_Canpost
 * @author      Maxim Nulman
 */
class Collinsharper_Canpost_Adminhtml_SignupController extends Mage_Adminhtml_Controller_Action {

    protected function _getSession() {

        return Mage::getSingleton('adminhtml/session');
    }

    public function updateAction() {

        $this->loadLayout();

        try {

            $block = $this->getLayout()->createBlock(
                    'Collinsharper_Canpost_Block_Adminhtml_Signup_Form', 
                    'signup_form', 
                    array('template' => 'canpost/signup/form.phtml')
            );

            $this->getLayout()->getBlock('content')->append($block);
            
        } catch (exception $e) {

            Mage::helper('chcanpost2module')->log($e->getMessage());
        }

        $this->renderLayout();
        
    }

    
    public function returnAction() {

	$storeCode = $this->getRequest()->getParam('store', '');

        $websiteCode = $this->getRequest()->getParam('website', '');

        if (!empty($storeCode)) {

                $scope_id = Mage::getModel('core/store')->load($storeCode)->getId();

                $scope_name = 'stores';

        } else if (!empty($websiteCode)) {

                $scope_id = Mage::getModel('core/website')->load($websiteCode)->getId();

                $scope_name = 'websites';

        } else {

                $scope_id = 0;

                $scope_name = 'default';

        }


        $token = (string) $this->getRequest()->getParam('token-id');
        
        $status = (string) $this->getRequest()->getParam('registration-status');

        if ($status == 'SUCCESS' && $token == Mage::getSingleton('core/session')->getCanadapostRegistrationToken()) {

            $customerdata = Mage::Helper('chcanpost2module/rest_registration')->getRegistrationData($token);

            $cfg = new Mage_Core_Model_Config();
            
            $cfg->saveConfig('carriers/chcanpost2module/api_customer_number', (string) $customerdata->{'customer-number'}, $scope_name, $scope_id);
            
            $cfg->saveConfig('carriers/chcanpost2module/contract', (string) $customerdata->{'contract-number'}, $scope_name, $scope_id);
            
            $cfg->saveConfig('carriers/chcanpost2module/api_login', (string) $customerdata->{'merchant-username'}, $scope_name, $scope_id);
            
            $cfg->saveConfig('carriers/chcanpost2module/api_password', (string) $customerdata->{'merchant-password'}, $scope_name, $scope_id);
            
            $cfg->saveConfig('carriers/chcanpost2module/has_default_credit_card', (int)($customerdata->{'has-default-credit-card'} == 'true'), $scope_name, $scope_id);

            $this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Your Canada Post Account Information has been updated.'));
            
        } else {
            
            $this->_getSession()->addError(Mage::helper('adminhtml')->__('Your Canada Post Account signup was canceled or did not finish. Please contact them by phone to finish setup.'));
            
        }

        $store = $this->getRequest()->getParam('store', '');

        $website = $this->getRequest()->getParam('website', '');

        if (!empty($store)) {

                $urlPar['store'] = $store;

        }

        if (!empty($website)) {

                $urlPar['website'] = $website;

        }
 
        $url = Mage::getSingleton('adminhtml/url')->getUrl("adminhtml/system_config/edit/section/carriers/", $urlPar);

        $this->_redirectUrl($url);
        
        return;
        
    }

}
