<?php
/** 
 *
 * @category    Collinsharper
 * @package     Collinsharper_Canpost
 * @author      Maxim Nulman
 */
class Collinsharper_Canpost_GmapController extends Mage_Core_Controller_Front_Action
{
    
    
    public function indexAction()
    {
        
        $this->loadLayout();  
        
        $this->renderLayout();
        
    }
    
    
    public function officeAction()
    {
        
        $this->loadLayout();        

        $this->renderLayout();
        
    }
    
    
}
