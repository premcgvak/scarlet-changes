<?php
class IWD_ColorSwatch_Model_Mysql4_Swatch extends Mage_Core_Model_Mysql4_Abstract{

	protected function _construct()
	{
		$this->_init('colorswatch/swatch', 'entity_id');
	}

	protected function _prepareDataForSave(Mage_Core_Model_Abstract $object) 
	{
		$currentTime = now();
		if ((! $object->getId () || $object->isObjectNew ()) && ! $object->getCreatedAt ()) {
			$object->setCreatedAt ( $currentTime );
		}
		$data = parent::_prepareDataForSave ( $object );
		return $data;
	}
	
}
