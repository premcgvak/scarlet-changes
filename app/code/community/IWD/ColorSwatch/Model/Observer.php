<?php
class IWD_ColorSwatch_Model_Observer{
	
	public function checkRequiredModules($observer){
		$cache = Mage::app()->getCache();
		
		if (Mage::getSingleton('admin/session')->isLoggedIn()) {
			if (!Mage::getConfig()->getModuleConfig('IWD_All')->is('active', 'true')){
				if ($cache->load("iwd_colorswatch")===false){
					$message = 'Important: Please setup IWD_ALL in order to finish <strong>IWD Color Swatch</strong> installation.<br />
						Please download <a href="http://iwdextensions.com/media/modules/iwd_all.tgz" target="_blank">IWD_ALL</a> and setup it via Magento Connect.<br />
						Please refer to installation <a href="https://docs.google.com/document/d/1zF1lQtD22otdhTHb8xyBpJM2QdFMQxBfWJLGenkIOQo/edit" target="_blank">guide</a>';
				
					Mage::getSingleton('adminhtml/session')->addNotice($message);
					$cache->save('true', 'iwd_colorswatch', array("iwd_colorswatch"), $lifeTime=5);
				}
			}
		}
	}
	
	
	
}