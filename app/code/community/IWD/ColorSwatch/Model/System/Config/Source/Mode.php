<?php

class IWD_ColorSwatch_Model_System_Config_Source_Mode{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Images')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('Color Picker')),        	
        );
    }

}
