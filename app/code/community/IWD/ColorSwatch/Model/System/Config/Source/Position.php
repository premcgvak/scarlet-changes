<?php

class IWD_ColorSwatch_Model_System_Config_Source_Position{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Left')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('Right')),
        	array('value' => 3, 'label'=>Mage::helper('adminhtml')->__('Bottom')),
        	array('value' => 4, 'label'=>Mage::helper('adminhtml')->__('Top')),
        );
    }

}
