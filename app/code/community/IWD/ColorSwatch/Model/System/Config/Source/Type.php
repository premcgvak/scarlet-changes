<?php

class IWD_ColorSwatch_Model_System_Config_Source_Type{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('All')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('Only Available')),        	
        );
    }

}
