<?php
class IWD_ColorSwatch_Helper_Images extends Mage_Core_Helper_Abstract{
	
	public function prepareMediaProduct($params){
		
		$superAttributesData = $params['super_attribute'];
		$product = $params['product'];
		
		$_product = Mage::getModel('catalog/product')->load((int)$product);
		
		if ($_product->getId()){
			
		
			
			$supperAttributes = array_keys($superAttributesData);
			
			$data = array();
			
			//prepare attribute
			foreach($supperAttributes as $attributeId){
				if (!empty($superAttributesData[$attributeId])){
					$selectedAttributeCode = $_product->getTypeInstance(true)->getAttributeById($attributeId, $_product)->getAttributeCode();
					$data[$attributeId]['code'] = $selectedAttributeCode;;
					$data[$attributeId]['value'] = $superAttributesData[$attributeId];
					$data[$attributeId]['switch'] = $this->checkIsInUse($attributeId);
				}
			}
			
		
			//check product
			$_configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($_product);
			$_productCollection = $_configurable->getUsedProductCollection()->addAttributeToSelect('*');
			
			foreach($_productCollection as $_product){
				$idsProducts[] = $_product->getId(); 
			}
			
			$_collection = Mage::getModel('catalog/product')->getCollection();
			
			foreach($data as $item){
				$_collection->addAttributeToFilter($item['code'], array('eq'=>$item['value']));
			}
			$_collection->addFieldToFilter('entity_id', array('in'=>$idsProducts));
			
			if ($_collection->getSize()==0){
				return false;
			}
			
			$_product = $_collection->getFirstItem();
			
			//reload product object
			$_product = Mage::getModel('catalog/product')->load($_product->getId());
			Mage::register('colorswatch_current_product', $_product);
			Mage::register('product', $_product);
			
			
			
			return $_product;		
		}
		
	}
	
	
	
	private function getAllProductAttributes(){
		$collection = Mage::getResourceModel('catalog/product_attribute_collection');
		$collection->addFieldToFilter('is_global',array('eq'=>1));
		$collection->addFieldToFilter('is_configurable',array('eq'=>1));
		$collection->addFieldToFilter('frontend_input',array('eq'=>'select'));
		return $collection;
	}
	
	private function checkIsInUse($attributeId){
		$collection = Mage::getModel('colorswatch/swatch')->getCollection();
		$collection->addFieldToFilter('attribute_id', array('eq'=>$attributeId));
		if ($collection->getSize()>0){
			return true;
		}		
		return false;
	}
	
	
	
	
}