<?php
class IWD_ColorSwatch_Helper_Switch extends Mage_Core_Helper_Abstract{

	const PLACEHOLDER_OPTION = 'colorswatch/adminhtml-placeholder.jpg';
	
	/**
	 * List of all colorswatch items 
	 * @var Varien_Data_Collection
	 */
	private $_collection; 
	
	/**
	 * List of attributes that used from colorswatch
	 * @var array()
	 */
	private $_useAttributes;
	
	/**
	 * List of attributes from curent product
	 * @var array()
	 */
	private $_productAttributes =array();
	
	/**
	 * List of all attributes that used in configurable products
	 * @var array()
	 */
	private $_attributesProductCollection; 
	
	
	
	
	public function __construct(){
		$this->prepareCollection();
	} 
	
	
	private function prepareCollection(){
		$collection = Mage::getModel('colorswatch/swatch')->getCollection();
		
		$this->_collection = $collection;
		$attributes = array();
		foreach ($this->_collection as $item){
			$attributes[] = $item->getAttributeId();
		}
		
		$attributes = array_unique($attributes);
		
		$this->_useAttributes = $attributes;
		
		/** get attribute collection **/
		$collection = Mage::getResourceModel('catalog/product_attribute_collection');
		$collection->addFieldToFilter('is_global',array('eq'=>1));
		$collection->addFieldToFilter('is_configurable',array('eq'=>1));
		$collection->addFieldToFilter('frontend_input',array('eq'=>'select'));
		
		$this->_attributesProductCollection = $collection;
		
	}
	
	public function isSwitchedAttribute($attributeId){
		foreach ($this->_useAttributes as $useAttributeId){
			if ($attributeId==$useAttributeId){
				
				if ($this->isAllowAttribute($attributeId)){				
					$this->_productAttributes[] = $useAttributeId;
					return true;
				}
			}
		}
		
		return false;
	}
	
	private function isAllowAttribute($attributeId){
		$item = Mage::getModel('colorswatch/attribute')->getCollection()->addFieldToFilter('attribute_id', array('eq'=>$attributeId))->getFirstItem();
		
		$status = $item->getStatus();
		return $status ? true : false;
	}
	
	
	public function getJsonModuleConfig(){
		$config = array();
		$config['enabled'] = (int) Mage::helper('colorswatch')->isEnable();
		$config['mode'] = (int) Mage::getStoreConfig('colorswatch/default/mode');
		$config['baseUrl'] = Mage::getBaseUrl('link', true);
		$config['imageUrl'] = $this->_getUrl('colorswatch/json/pull', array('_secure'=>true));
		
		$config['width'] = Mage::getStoreConfig('colorswatch/default/width');
		$config['height'] = Mage::getStoreConfig('colorswatch/default/width');
		$config['preselect'] = Mage::getStoreConfig('colorswatch/default/preselect');
		return Mage::helper('core')->jsonEncode($config);
	}
	
	public function getJsonConfig(Mage_Catalog_Model_Product $_product){
		//$_product = Mage::getModel('catalog/product')->load($product->getId());//RELOAD PRODUCT
		$_configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($_product);
		
		//$usedProductAttributes = $_configurable->getUsedProductAttributeIds($_product);
		$_productCollection = $_configurable->getUsedProductCollection()->addAttributeToSelect('*');
		
		
		$config = array();
		
		foreach($this->_productAttributes as $attributeId){
			
			$code = $this->getAttributeCode($attributeId);
			
			$options = array();
			
			foreach($_productCollection as $_productSimple){
				$options[] = $_productSimple->getData($code); 
			}
			
			$options = array_unique($options);
			
			$config[$attributeId]['options'] = $options;
			$config[$attributeId]['attribute_id'] = $attributeId;
			$config[$attributeId]['code'] = $code;
			$config[$attributeId]['additional'] = (int)$this->_getAdditionalStatus($attributeId);
			
		}
		
		$config = $this->pushColorSwatchData($config);
		
		
		return Mage::helper('core')->jsonEncode($config);
		//print_r($usedProductAttributes);
	} 
	
	
	public function _getAdditionalStatus($attributeId){
		
		$item = Mage::getModel('colorswatch/attribute')->getCollection()->addFieldToFilter('attribute_id', array('eq'=>$attributeId))->getFirstItem();
	
		$additional = $item->getAdditional();
		return $additional ? true : false;
		
	}
	
	public function checkIsAllow($attributeId){
		$item = Mage::getModel('colorswatch/attribute')->getCollection()->addFieldToFilter('attribute_id', array('eq'=>$attributeId))->getFirstItem();
	
		$status = $item->getStatus();
		return $status ? true : false;
	}
	
	public function getAttributeCode ($attributeId){
		
		foreach ($this->_attributesProductCollection as $_attribute){
			if ($attributeId == $_attribute->getAttributeId()){
				return $_attribute->getAttributeCode();
			}
		}

	}
	
	private function getOptionLabel($optionsId){
		$storeId = Mage::app()->getStore()->getId();
		$resource = Mage::getSingleton('core/resource');
		$connection = $resource->getConnection('core_read');
		
		$table = $resource->getTableName('eav/attribute_option_value');
		
		$query = "SELECT * FROM {$table} WHERE option_id = '{$optionsId}' AND store_id = '{$storeId}' LIMIT 1";
		
		$rowArray = $connection->fetchRow($query);
		
		if (!isset($rowArray['value'])){
			
			$query = "SELECT * FROM {$table} WHERE option_id = '{$optionsId}' LIMIT 1";
			
			$rowArray = $connection->fetchRow($query);
			
			return htmlspecialchars(($rowArray['value']), ENT_QUOTES);
		}else{
			return htmlspecialchars(($rowArray['value']), ENT_QUOTES);
		}
	
		return '';
		
	}
	
	private function pushColorSwatchData($config){

		
		
		$mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA, true);
		
		foreach($config as $attributeId=>$data){
			
			foreach($data['options'] as $index=>$option){
				
				foreach($this->_collection as $entryItem){
					
					if ($entryItem->getData('attribute_id')==$attributeId && $entryItem->getData('option_id')==$option){
						$config[$attributeId]['options'][$index] = array();
						$config[$attributeId]['options'][$index]['id'] = $option;
						$config[$attributeId]['options'][$index]['label'] = $this->getOptionLabel($option);
						
						//NORMAL
						$cachePath = Mage::helper('colorswatch/image')->init('image', $entryItem->getData('image'))->resize(Mage::getStoreConfig('colorswatch/default/width'), Mage::getStoreConfig('colorswatch/default/height'));						
						$config[$attributeId]['options'][$index]['image'] = $cachePath->__toString();
						$config[$attributeId]['options'][$index]['hex_image'] = $entryItem->getData('hex_image');
						
						//ACTIVE
						$cachePath = Mage::helper('colorswatch/image')->init('active', $entryItem->getData('active'))->resize(Mage::getStoreConfig('colorswatch/default/width'), Mage::getStoreConfig('colorswatch/default/height'));
						$config[$attributeId]['options'][$index]['active'] = $cachePath->__toString();
						$config[$attributeId]['options'][$index]['hex_active'] = $entryItem->getData('hex_active');
						
						//HOVER
						$cachePath = Mage::helper('colorswatch/image')->init('hover', $entryItem->getData('hover'))->resize(Mage::getStoreConfig('colorswatch/default/width'), Mage::getStoreConfig('colorswatch/default/height'));
						$config[$attributeId]['options'][$index]['hover'] = $cachePath->__toString();
						$config[$attributeId]['options'][$index]['hex_hover'] = $entryItem->getData('hex_hover');
						
						//DISABLED
						$cachePath = Mage::helper('colorswatch/image')->init('disabled', $entryItem->getData('disabled'))->resize(Mage::getStoreConfig('colorswatch/default/width'), Mage::getStoreConfig('colorswatch/default/height'));
						$config[$attributeId]['options'][$index]['disabled'] = $cachePath->__toString();
						$config[$attributeId]['options'][$index]['hex_disabled'] = $entryItem->getData('hex_disabled');
					}
						
				}
				
			}
				
		}
		
		
		//check if exists not converted options
		
		foreach($config as $attributeId=>$data){
				
			foreach($data['options'] as $index=>$option){
		
				foreach($this->_collection as $entryItem){
						
					if (!is_array($config[$attributeId]['options'][$index])){
						$config[$attributeId]['options'][$index] = array();
						$config[$attributeId]['options'][$index]['id'] = $option;
						
						//SET PLACEHOLDER
						$config[$attributeId]['options'][$index]['image'] = $mediaUrl . self::PLACEHOLDER_OPTION;
						$config[$attributeId]['options'][$index]['hex_image'] = 'ffffff';
						
						
						$config[$attributeId]['options'][$index]['active'] = $mediaUrl . self::PLACEHOLDER_OPTION;
						$config[$attributeId]['options'][$index]['hex_active'] = 'ffffff';
						
						
						$config[$attributeId]['options'][$index]['hover'] = $mediaUrl . self::PLACEHOLDER_OPTION;
						$config[$attributeId]['options'][$index]['hex_hover'] = 'ffffff';
						
						
						$config[$attributeId]['options'][$index]['disabled'] = $mediaUrl . self::PLACEHOLDER_OPTION;
						$config[$attributeId]['options'][$index]['hex_disabled'] = 'ffffff';
						
						
						
					}
					
		
				}
		
			}
		
		}
		
	
		return $config;
	}
	
}
