<?php
class IWD_ColorSwatch_Helper_Zoom extends Mage_Core_Helper_Abstract{
	
	const XML_PATH_STATUS = 'colorswatch/zoom/status';
	
	const XML_PATH_WIDTH_IMAGE = 'colorswatch/zoom/width';
	
	const XML_PATH_HEIGHT_IMAGE = 'colorswatch/zoom/height';
	
	const XML_PATH_WIDTH_ZOOM = 'colorswatch/zoom/width_zoom';
	
	const XML_PATH_HEIGHT_ZOOM = 'colorswatch/zoom/height_zoom';
	
	const XML_PATH_ZOOM_RATIO = 'colorswatch/zoom/ratio';
	
	const XML_PATH_THUMBNAILS_POSITION = 'colorswatch/zoom/thumbnail_position';
	
	const XML_PATH_THUMBNAILS_WIDTH = 'colorswatch/zoom/thumbnail_width';
	
	const XML_PATH_THUMBNAILS_HEIGHT = 'colorswatch/zoom/thumbnail_height';
	
	const XML_PATH_THUMBNAILS_SLIDER = 'colorswatch/zoom/thumbnail_slider';
	
	
	public function getTemplateConfig(){
		$config = new Varien_Object();
		
		//image size
		$config->setImageWidth(Mage::getStoreConfig(self::XML_PATH_WIDTH_IMAGE));		
		$config->setImageHeight(Mage::getStoreConfig(self::XML_PATH_HEIGHT_IMAGE));
		
		//thumnails position
		$config->setThumbnailsPosition(Mage::getStoreConfig(self::XML_PATH_THUMBNAILS_POSITION));
		
		//zoombox
		$config->setZoomBoxWidth(Mage::getStoreConfig(self::XML_PATH_WIDTH_ZOOM));
		$config->setZoomBoxHeight(Mage::getStoreConfig(self::XML_PATH_HEIGHT_ZOOM));
		
		//zoom ratio
		$config->setZoomRatio(Mage::getStoreConfig(self::XML_PATH_ZOOM_RATIO));
		
		
		//thumbnails sizes
		$config->setThumbnailWidth(Mage::getStoreConfig(self::XML_PATH_THUMBNAILS_WIDTH));
		$config->setThumbnailHeight(Mage::getStoreConfig(self::XML_PATH_THUMBNAILS_HEIGHT));
		
		$config->setThumbnailSlider(Mage::getStoreConfig(self::XML_PATH_THUMBNAILS_SLIDER));
		
		return $config;
	}
	
	
	public function checkPositionMainImage($config){
		$thumbnailPosition = $config->getThumbnailsPosition();
		
		switch($thumbnailPosition){
			case '1': //left
				$classImageWrapper = 'right';
			break;
			case '2': //right
				$classImageWrapper = 'left';
			break;
			case '3': //bottom
				$classImageWrapper = 'top';
			break;
			case '4': //top
				$classImageWrapper = 'bottom';
			break;
				
		}
		return $classImageWrapper;
	}
	
	
	
	public function checkPositionThumbnails($config){
		$thumbnailPosition = $config->getThumbnailsPosition();
	
		switch($thumbnailPosition){
			case '1': //left
				$classImageWrapper = 'left';
				break;
			case '2': //right
				$classImageWrapper = 'right';
				break;
			case '3': //bottom
				$classImageWrapper = 'bottom';
				break;
			case '4': //top
				$classImageWrapper = 'top';
				break;
	
		}
		return $classImageWrapper;
	}
	
	
	public function getJsonConfig(){
		$config = $this->getTemplateConfig();	
	
		return $config->toJson();
	}
}