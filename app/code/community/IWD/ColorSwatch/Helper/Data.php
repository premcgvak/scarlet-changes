<?php
class IWD_ColorSwatch_Helper_Data extends Mage_Core_Helper_Abstract{
	
	protected $_version = 'CE';

	const XML_PATH_MODE = 'colorswatch/default/mode';
	
	public function saveImages($params){
		$allowAttribute = isset($params['allow_attribute']) ? $params['allow_attribute'] : array();
                $attribute_list = isset($params['attribute_list']) ? $params['attribute_list'] : array();
                
		$this->_setAllowAttribute($allowAttribute, $attribute_list);
		Mage::getModel('colorswatch/image')->clearCache();
		
		$fields = isset($params['image'])? $params['image']:false;
		if (!$fields) return;
		
		foreach($fields as $attributeId=>$options){
		
			foreach($options as $optionId=>$images){
				
				$filename = (string) $images['image'];
				$filenameActive = (string) $images['active'];
				$filenameHover = (string) $images['hover'];
				$filenameDisabled = (string) $images['disabled'];
				if (empty($filename)){
					continue;
				}
				
				$id = isset($images['id'])? (int)$images['id'] : false;
				
				if ($id){
					$model = Mage::getModel('colorswatch/swatch')->load($id);
				}else{
					$model = Mage::getModel('colorswatch/swatch');
					$model->setData('attribute_id', (int)$attributeId);
					$model->setData('option_id', (int)$optionId);
				}
				
				if (Mage::helper('colorswatch')->isColorPickerMode()){
					//if color picker mode
					$model->setData('hex_image', $filename); 
					$model->setData('hex_active', $filenameActive);
					$model->setData('hex_hover', $filenameHover);
					$model->setData('hex_disabled', $filenameDisabled);
				}else{
					//if file mode
					$model->setData('image', $filename);
					$model->setData('active', $filenameActive);
					$model->setData('hover', $filenameHover);
					$model->setData('disabled', $filenameDisabled);
				}
				
				
				
				try{
					$model->save();
					
					if (!Mage::helper('colorswatch')->isColorPickerMode()){
						if (!empty($images['image'])){
							$this->moveTmpFile($images['image']);
						}
						
						if (!empty($images['active'])){
							$this->moveTmpFile($images['active']);
						}
						
						if (!empty($images['hover'])){
							$this->moveTmpFile($images['hover']);
						}
						
						if (!empty($images['disabled'])){
							$this->moveTmpFile($images['disabled']);
						}
					}
	
				}catch(Exception $e){
					Mage::logException($e);
				}
			}
			
		}
	}
	
	
	private function _setAllowAttribute($data, $attribute_list = array()){
		$collection = Mage::getModel('colorswatch/attribute')->getCollection();
		foreach($collection as $item){
			try{
                            //We only deleteattributes from this page
                            if(in_array($item->getAttributeId(), $attribute_list)){
                                $item->delete();
                            }
			}catch(Exception $e){
				Mage::logException($e);
			}
		}
		
		foreach ($data as $id=>$options){
			$model = Mage::getModel('colorswatch/attribute');
			$model->setData('attribute_id', (int) $id);
			$model->setData('status', isset($options['status'])? $options['status']:0);
			$model->setData('additional', isset($options['additional'])? $options['additional']:0);
			
			try{
				$model->save();
			}catch(Exception $e){
				Mage::logException($e);
			}
		}
		
	}
	
	private function moveTmpFile($filename){
		$tempFolder = Mage::getSingleton('catalog/product_media_config')->getBaseTmpMediaPath();
		
		$filenamePath = $tempFolder . DS . $filename;
		
		$destination  = Mage::getBaseDir('media') . DS . 'colorswatch' . DS .  $filename;
	
		if (file_exists($filenamePath)){
			rename($filenamePath, $destination);
			
		}
	}
	
	private function removeImageFile($filename){
		$filename  = Mage::getBaseDir('media') . DS . 'colorswatch' . DS .  $filename;
		@unlink($filename);
	}
	
	public function removeImages($params){
		
		
		$fields = isset($params['remove']) ? $params['remove'] : false;
		
		if ($fields===false) {
			return;
		}
		
		foreach($fields as $id=>$options){
		
			$item = $this->loadItem((int)$id);
			if (isset($options['image'])){
				$image = $item->getData('image');
				$item->setData('image', '');
				$this->removeImageFile($image);
			}	
			
			try{
				$item->save();
			}catch(Exception $e){
				Mage::logException($e);
			}
			
		}
	}
	
	private function loadItem($id){
		return Mage::getModel('colorswatch/swatch')->load($id);
	}
	
	public function isAvailableVersion(){
	
		$mage  = new Mage();
		if (!is_callable(array($mage, 'getEdition'))){
			$edition = 'Community';
		}else{
			$edition = Mage::getEdition();
		}
		unset($mage);
			
		if ($edition=='Enterprise' && $this->_version=='CE'){
			return false;
		}
		return true;
	
	}
	
	
	public function isColorPickerMode(){
		$status = Mage::getStoreConfig(self::XML_PATH_MODE);
		if ($status==2){
			return 1;
		}
		return 0;
	}
	
	public function isEnable(){
		$status = Mage::getStoreConfig('colorswatch/default/status');
		if ($this->isAvailableVersion()){
			return $status;
		}
		return false;
	}
}