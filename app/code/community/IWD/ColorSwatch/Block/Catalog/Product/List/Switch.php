<?php
class IWD_ColorSwatch_Block_Catalog_Product_List_Switch extends Mage_Core_Block_Template{
	
	const XML_PATH_ATTRIBUTE_CODE ='colorswatch/list/attribute';
	const XML_PATH_ENABLED_LIST = 'colorswatch/list/status';
	
	public $_attributeId;
	
	public function prepareCollection(){
		
		if (!Mage::helper('colorswatch')->isEnable()){
			return false;
		}
		
		
		$attributeCode = Mage::getStoreConfig(self::XML_PATH_ATTRIBUTE_CODE);
		
		$listAccess = Mage::getStoreConfig(self::XML_PATH_ENABLED_LIST);
		if (!$listAccess){
			return false;
		}
		
		if (empty($attributeCode)){
			return false;	
		}
			
		$_product = $this->getData('product');
		
		if ($_product->getTypeId()!='configurable'){
			return false;
		}


		$configurableAttributeCollection = $_product->getTypeInstance(true)->getUsedProductAttributes($_product);

		foreach ($configurableAttributeCollection as $attribute){
			$attributeCodes[] =  $attribute->getAttributeCode();
		}
		if (!in_array($attributeCode, $attributeCodes)){
			return false;
		}else{
			foreach ($configurableAttributeCollection as $attribute){
				if ($attributeCode  ==  $attribute->getAttributeCode()){
					$attributeId = $attribute->getAttributeId();
					if (!Mage::helper('colorswatch/switch')->checkIsAllow($attributeId)){
						return false;
					}
				}
				
			}
		}
		
		$this->_attributeId = $attributeId;
		
		$_configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($_product);
		
		$_productCollection = $_configurable->getUsedProductCollection()->addAttributeToSelect(array($attributeCode));

		if ($_productCollection->getSize()==0){
			return false;
		}

		foreach($_productCollection as $product){
			$options[] = $product->getData($attributeCode);
		}
				
		$options = array_unique($options);
		
		$collectionAllOption = $this->getCollectionOption($attributeCode, $options);
		
		return $collectionAllOption;
	}
	
	
	
	private function getCollectionOption($code, $options){
		$code = trim($code);
		$code = strtolower($code);
		$collection = Mage::getResourceModel('catalog/product_attribute_collection');
		$collection->addFieldToFilter('attribute_code',array('eq'=>$code));
		
		$attribute = $collection->getFirstItem();
	
		
		$_collection = Mage::getModel('colorswatch/swatch')->getCollection()
									->addFieldToFilter('attribute_id', array('eq'=>$attribute->getAttributeId()))
									->addFieldToFilter('option_id', array('in'=>$options));
		return $_collection;
	}
	
	
	protected function getSwatchUrl($type, $item){
		
		if (Mage::helper('colorswatch')->isColorPickerMode()){
			$image = $item->getData('hex_' . $type);
			if (empty($image)){
				return 'ffffff';
			}
			return $image;
		}else{
			$image = $item->getData($type);
		}
		
		$_imageHelper = Mage::helper('colorswatch/image');
		$cachePath = $_imageHelper->init('image', $image)->resize(Mage::getStoreConfig('colorswatch/list/width'), Mage::getStoreConfig('colorswatch/list/height'));
		
		//$url = Mage::getBaseUrl('media', true) . 'colorswatch/' . $image;
		return $cachePath->__toString();
	}
	
	
	protected function getConfig(){
		$config = new Varien_Object();
		
		$config->setData('width', Mage::getStoreConfig('colorswatch/list/width'));
		$config->setData('height', Mage::getStoreConfig('colorswatch/list/height'));
		
		return $config;
	}
	
	
	protected function getImage($item){
		$_product = $this->getData('product');
		
		$attributesInfo = array(
				$item->getAttributeId() => $item->getOptionId()
		);
		
		$product = $_product->getTypeInstance(true)->getProductByAttributes($attributesInfo, $_product);
		$product = Mage::getModel('catalog/product')->load($product->getId());
		
		if ($product && $product->getId()){
			 
			return  $this->helper('catalog/image')->init($product, Mage::getStoreConfig('colorswatch/list/type'))
				->resize(Mage::getStoreConfig('colorswatch/list/image_width_'.$this->getData('type')), Mage::getStoreConfig('colorswatch/list/image_height_'.$this->getData('type')));
		}
		
	}
}