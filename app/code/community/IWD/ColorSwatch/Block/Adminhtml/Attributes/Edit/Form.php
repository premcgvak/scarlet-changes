<?php
class IWD_ColorSwatch_Block_Adminhtml_Attributes_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('colorswatch_form');
        $this->setTitle(Mage::helper('colorswatch')->__('Block Information'));
    }

    protected function _prepareForm()
    {
        
        return parent::_prepareForm();
    }
}
