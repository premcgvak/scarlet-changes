<?php

class IWD_ColorSwatch_Block_Adminhtml_Attributes_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Add and update buttons
     *
     * @return void
     */
    public function __construct()
    {
        $this->_objectId   = 'colorswatch_id';        
        $this->_blockGroup = 'colorswatch';
        $this->_controller = 'adminhtml_attributes';
        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('colorswatch')->__('Save Options'));
        $this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('save');
        $this->addButton('save_and_edit_button', array(
            'label'   => Mage::helper('tag')->__('Save and Continue Edit'),
            'onclick' => "saveAndContinueEdit('" . $this->getSaveAndContinueUrl() . "')",
            'class'   => 'save'
        ), 1);
    }

  

    /**
     * Retrieve Header text
     *
     * @return string
     */
    public function getHeaderText()
    {        
        return Mage::helper('colorswatch')->__('Color Swatch Attributes');
    }

 
    

    /**
     * Check whether it is single store mode
     *
     * @return bool
     */
    public function isSingleStoreMode()
    {
        return Mage::app()->isSingleStoreMode();
    }

    /**
     * Retrieve Tag Save URL
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('*/*/save', array('_current'=>true));
    }

    /**
     * Retrieve Tag SaveAndContinue URL
     *
     * @return string
     */
    public function getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array('_current' => true, 'ret' => 'edit', 'continue' => $this->getRequest()->getParam('ret', 'index')));
    }

    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/' . $this->getRequest()->getParam('ret', 'index'));
    }
}
