<?php
class IWD_ColorSwatch_Block_Adminhtml_Attributes_List extends Mage_Core_Block_Template{
	
	const PLACEHOLDER_OPTION = 'colorswatch/adminhtml-placeholder.jpg';
	
	 
	private $_collection;
	
	public function _construct(){
		$this->getCollectionSaved();
		parent::_construct();
	}
	
        protected function _prepareLayout()
        {
            parent::_prepareLayout();
            
            //we retrive the collection
            $collection = $this->getListAttributes(Mage::app()->getRequest()->getParam('p'));
            
            //We store the collection to access it in _beforeToHtml
            $this->setCollection($collection);
                        
            //$limit = ($this->getLimit())?$this->getLimit():10;
            
            $pager = $this->getLayout()->createBlock('page/html_pager', 'pager');
            $pager->setAvailableLimit(array(10 => 10));
            $pager->setCollection($collection);
            $this->setChild('pager', $pager);

            return $this;
        }

        
        public function getPagerHtml()
        {
            return $this->getChildHtml('pager');
        }
        
        
	public function getListAttributes($page = 0){
		
		//$collection =  Mage::getModel('catalog/eav_attribute');
		//$collection->addFieldToFilter('is_global', array('eq'=>'1'));
		$collection = Mage::getResourceModel('catalog/product_attribute_collection');
                        //->setPageSize(5)->setCurPage($page);
		$collection->addFieldToFilter('is_global',array('eq'=>1));
		$collection->addFieldToFilter('is_configurable',array('eq'=>1));
		$collection->addFieldToFilter('frontend_input',array('eq'=>'select'));
                $collection->setOrder('frontend_label', 'ASC');
		
		//->getItems();
		
		return $collection;
		
	}
	
	
	public function getUploadUrl(){
		
		$url = Mage::helper("adminhtml")->getUrl('colorswatch/json/upload');
		return $url;

	}
	
	public function getFlashObject(){
		$url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS, true) . 'iwd/colorswatch/adminhtml/uploadify.swf';
		return $url;
	}
	
	private function getCollectionSaved(){
		$_collection = Mage::getModel('colorswatch/swatch')->getCollection();
	
		$this->_collection = $_collection;
	}
	
	
	public function getValueImage($type, $attribute, $option, $hex=false){
		
		foreach($this->_collection as $item){
			if ($item['attribute_id']==$attribute && $item['option_id']==$option){
				if ($hex){
					return $item['hex_'.$type];
				}else{
					return $item[$type];
				}
			}
		}
	}
	
	
	public function getUrlImage($type, $attribute, $option){
		
		$placeholder = Mage::getBaseUrl('media', true) . self::PLACEHOLDER_OPTION;

		foreach($this->_collection as $item){
			if ($item['attribute_id']==$attribute && $item['option_id']==$option){
				if (!empty($item[$type])){
					$url = Mage::getBaseUrl('media', true) . 'colorswatch/' . $item[$type];
				}else{
					$url = $placeholder;
				}
				return $url;
			}
		}

		return $placeholder;
	}
	
	
	public function getEntryId($attribute, $option){
		
		foreach($this->_collection as $item){
			if ($item['attribute_id']==$attribute && $item['option_id']==$option){
				return $item->getId();
			}
		}
		
	}
	
	public function getBackground($attribute, $option, $type){
		$color = false;
		foreach($this->_collection as $item){
			if ($item['attribute_id']==$attribute && $item['option_id']==$option){
				$color =  $item->getData('hex_'.$type);
			}
		}
		
		if ($color){
			return $color;
		}
	}
	
	
	public function checkIsAllow($attributeId){
		$item = Mage::getModel('colorswatch/attribute')->getCollection()->addFieldToFilter('attribute_id', array('eq'=>$attributeId))->getFirstItem();
		
		$status = $item->getStatus();
		return $status ? true : false;
	}
	
	public function checkIsAllowAdditional($attributeId){
		$item = Mage::getModel('colorswatch/attribute')->getCollection()->addFieldToFilter('attribute_id', array('eq'=>$attributeId))->getFirstItem();
	
		$additional = $item->getAdditional();
		return $additional ? true : false;
	}
	
}

