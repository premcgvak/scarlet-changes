<?php
class IWD_ColorSwatch_Adminhtml_AttributesController extends Mage_Adminhtml_Controller_Action{
	
	public function indexAction(){
		
		$this->loadLayout();
		$this->renderLayout();
		
	}
	
	
	public function saveAction(){
		$params = $this->getRequest()->getParams();
		Mage::helper('colorswatch')->saveImages($params);
		Mage::helper('colorswatch')->removeImages($params);
		
		return $this->_redirect('*/*/index', array());
	}
	
	
	
}