<?php
class IWD_ColorSwatch_JsonController extends Mage_Core_Controller_Front_Action{
	
	
	public function uploadAction(){
		
		$result = array();
		
		try {
				
			$uploader = new Varien_File_Uploader('colowswatchImages');
			$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
			$uploader->setAllowRenameFiles(true);
			$uploader->setFilesDispersion(false);
		
			$result = $uploader->save(
					Mage::getSingleton('catalog/product_media_config')->getBaseTmpMediaPath()
			);
		
			$result['url'] = Mage::getSingleton('catalog/product_media_config')->getTmpMediaUrl($result['file']);
			$result['file'] = $result['file'];
			
			$result['id'] =  $this->getRequest()->getParam('idRow', false); 
				
		}catch(Exception $e){
				
			$result = array('error'=>$e->getMessage(), 'errorcode'=>$e->getCode());
		}
		
		$this->getResponse()->setBody(Zend_Json::encode($result));
	}
	
	
	
	public function pullAction(){
		
		$response = array();
		
		if (!$this->getRequest()->getParam('super_attribute', false)){
			$this->getResponse()->setBody(Zend_Json::encode($response));
			return;
		}
		
		$params = $this->getRequest()->getParams();
		
		$_helper = Mage::helper('colorswatch/images');
		$status = $_helper->prepareMediaProduct($params);
		
		if (!$status){
			$this->getResponse()->setBody(Zend_Json::encode($response));
			return;
		}
		
		
		$this->loadLayout(false);
		$output = $this->getLayout()->getOutput();
		
		$result['media'] = $output;	
		$this->getResponse()->setBody( Mage::helper ( 'core' )->jsonEncode ( $result ));
		
	}
}
