<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("

				CREATE TABLE {$this->getTable('iwd_colorswatch')} (
				  `entity_id` int(11) NOT NULL AUTO_INCREMENT,
				  `attribute_id` int(11) NOT NULL,
				  `option_id` int(11) NOT NULL,
				  `image` text,
				  `hex_image` varchar(6) DEFAULT NULL,
				  `active` text,
				  `hex_active` varchar(6) DEFAULT NULL,
				  `hover` text,
				  `hex_hover` varchar(6) DEFAULT NULL,
				  `disabled` text,
				  `hex_disabled` varchar(6) DEFAULT NULL,
				  PRIMARY KEY (`entity_id`)
				) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

");



$installer->run("
		
		CREATE TABLE {$this->getTable('iwd_colorswatch_attribute')} (
		  `entity_id` int(11) NOT NULL AUTO_INCREMENT,
		  `attribute_id` int(11) DEFAULT NULL,
		  `status` tinyint(4) DEFAULT NULL,
		  `additional` tinyint(4) DEFAULT NULL,
		  PRIMARY KEY (`entity_id`)
		) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

");

$installer->endSetup();