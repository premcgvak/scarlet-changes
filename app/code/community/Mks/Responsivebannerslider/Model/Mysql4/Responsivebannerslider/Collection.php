<?php
    class Mks_Responsivebannerslider_Model_Mysql4_Responsivebannerslider_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
    {

		public function _construct(){
			$this->_init("responsivebannerslider/responsivebannerslider");
		}

		public function addStoreFilter($store)
	    {
	        if ($store instanceof Mage_Core_Model_Store) {
	            $store = array($store->getId());
	        }

	        $this->getSelect()->join(
	            array('store_table' => $this->getTable('banners_store')),
	            'main_table.id = store_table.banners_id',
	            array()
	        )
	        ->where('store_table.store_id in (?)', array(0, $store));

	        return $this;
	    }

    }
	 