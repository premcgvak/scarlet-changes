<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Product Scheduler \\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_ProductScheduler           \\\\\\\
 ///////    * @author     ExtensionHut <info@extensionhut.com>            ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2015 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */

$installer = $this;
$installer->startSetup();

$time = array(
	0 => 0,
	1 => 1,
	2 => 2,
	3 => 3,
	4 => 4,
	5 => 5,
	6 => 6,
	7 => 7,
	8 => 8,
	9 => 9,
	10 => 10,
	11 => 11,
	12 => 12,
	13 => 13,
	14 => 14,
	15 => 15,
	16 => 16,
	17 => 17,
	18 => 18,
	19 => 19,
	20 => 20,
	21 => 21,
	22 => 22,
	23 => 23,
);


$model=Mage::getModel('eav/entity_setup','core_setup');

$data=array(
	'type'=>'datetime',
	'input'=>'date',
	'label'=>'Start Date',
	'global'=>Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'required'=>'0',
	'is_comparable'=>'0',
	'is_searchable'=>'0',
	'is_unique'=>'1',
	'is_configurable'=>'1',
	'user_defined'=>'1',
	'sort_order' => 100,
);
$model->addAttribute('catalog_product','eh_schedule_start_date',$data);

$installer->addAttribute('catalog_product', "eh_schedule_start_time", array(
    'type'       => 'int',
    'input'      => 'select',
    'label'      => 'Start Time (Hour)',
    'user_defined'=>'1',
    'sort_order' => 200,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend'    => 'eav/entity_attribute_backend_array',
    'option'     => array (
        'values' => $time
    ),

));

$data['label']='End Date';
$data['sort_order']='300';
$model->addAttribute('catalog_product','eh_schedule_end_date',$data);

$installer->addAttribute('catalog_product', "eh_schedule_end_time", array(
    'type'       => 'int',
    'input'      => 'select',
    'label'      => 'End Time (Hour)',
    'user_defined'=>'1',
    'sort_order' => 400,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend'    => 'eav/entity_attribute_backend_array',
    'option'     => array (
        'values' => $time
    ),

));

$installer->addAttribute('catalog_product', "eh_schedule_status", array(
    'type'       => 'int',
    'input'      => 'select',
    'label'      => 'Status',
    'user_defined'=>'1',
    'sort_order' => 500,
    'required'   => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'backend'    => 'eav/entity_attribute_backend_array',
    'option'     => array (
        'values' => array(
            0 => 'Disable',
            1 => 'Enable',
        )
    ),

));

$scheduleStartDateAttributeId=$model->getAttribute('catalog_product','eh_schedule_start_date');
$scheduleStartTimeAttributeId=$model->getAttribute('catalog_product','eh_schedule_start_time');
$scheduleEndDateAttributeId=$model->getAttribute('catalog_product','eh_schedule_end_date');
$scheduleEndTimeAttributeId=$model->getAttribute('catalog_product','eh_schedule_end_time');
$scheduleProductStatusAttributeId=$model->getAttribute('catalog_product','eh_schedule_status');

$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();
$sets = Mage::getModel('eav/entity_attribute_set')->getResourceCollection()->addFilter('entity_type_id', $entityTypeId);

foreach ($sets as $set){
    $modelGroup = Mage::getModel('eav/entity_attribute_group');
    $modelGroup->setAttributeGroupName('Product Schedule')->setAttributeSetId($set->getId())->setSortOrder(150);
    $modelGroup->save();
}
		
$allAttributeSetIds=$model->getAllAttributeSetIds('catalog_product');
foreach ($allAttributeSetIds as $attributeSetId) {
	try{
		$attributeGroupId=$model->getAttributeGroup('catalog_product',$attributeSetId,'Product Schedule');
	}
	catch(Exception $e){
		$attributeGroupId=$model->getDefaultAttributeGroupId('catalog/product',$attributeSetId);
	}
	
	$model->addAttributeToSet('catalog_product',$attributeSetId,$attributeGroupId['attribute_group_id'],$scheduleStartDateAttributeId['attribute_id']);
	$model->addAttributeToSet('catalog_product',$attributeSetId,$attributeGroupId['attribute_group_id'],$scheduleStartTimeAttributeId['attribute_id']);
    $model->addAttributeToSet('catalog_product',$attributeSetId,$attributeGroupId['attribute_group_id'],$scheduleEndDateAttributeId['attribute_id']);
    $model->addAttributeToSet('catalog_product',$attributeSetId,$attributeGroupId['attribute_group_id'],$scheduleEndTimeAttributeId['attribute_id']);
    $model->addAttributeToSet('catalog_product',$attributeSetId,$attributeGroupId['attribute_group_id'],$scheduleProductStatusAttributeId['attribute_id']);
}


$installer->endSetup();
