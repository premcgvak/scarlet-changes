<?php
/*////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\  Product Scheduler \\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ NOTICE OF LICENSE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                                                                   ///////
 \\\\\\\ This source file is subject to the Open Software License (OSL 3.0)\\\\\\\
 ///////   that is bundled with this package in the file LICENSE.txt.      ///////
 \\\\\\\   It is also available through the world-wide-web at this URL:    \\\\\\\
 ///////          http://opensource.org/licenses/osl-3.0.php               ///////
 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
 ///////                      * @category   EH                            ///////
 \\\\\\\                      * @package    EH_ProductScheduler           \\\\\\\
 ///////    * @author     ExtensionHut <info@extensionhut.com>            ///////
 \\\\\\\                                                                   \\\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 \\\\\\* @copyright  Copyright 2015 © www.extensionhut.com All right reserved\\\\\
 /////////////////////////////////////////////////////////////////////////////////
 */

class EH_ProductScheduler_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SCHEDULER_ACTIVE	= 'productscheduler/general/active';
    const SCHEDULER_EMAIL_ACTIVE	= 'productscheduler/emailnotifications/active';
    const SCHEDULER_EMAIL_DAYS_BEFORE	= 'productscheduler/emailnotifications/days_before';
    const SCHEDULER_EMAIL_ADDRESS	= 'productscheduler/emailnotifications/email_address';

    const LOG_FILE = 'product_scheduler.log';


    protected $today;

    protected $current_hour;

    public function log($m)
    {
        Mage::log($m, null, self::LOG_FILE);

    }

    public function isModuleActive() {
        return Mage::getStoreConfig(self::SCHEDULER_ACTIVE);
    }

    public function isReportEmailActive() {
        return Mage::getStoreConfig(self::SCHEDULER_EMAIL_ACTIVE);
    }

    public function getProductsForNotifications() {
        $days_before = Mage::getStoreConfig(self::SCHEDULER_EMAIL_DAYS_BEFORE);
        $product_list = array();
        if(!$days_before || $days_before == 1) {
            $today = Mage::getModel('core/date')->date('Y-m-d 00:00:00', strtotime('+1 day'));
            $yesterday = Mage::getModel('core/date')->date('Y-m-d 00:00:00');
        } elseif($days_before > 1) {
            $today = Mage::getModel('core/date')->date('Y-m-d 00:00:00', strtotime('+'.$days_before.' day'));
            $yesterday = Mage::getModel('core/date')->date('Y-m-d 00:00:00', strtotime('+'.($days_before - 1).' day'));
        }
        if(isset($today) && isset($yesterday)) {
            $collection = Mage::getModel('catalog/product')->getCollection();
            $collection->addAttributeToSelect(array('name', 'eh_schedule_start_date', 'eh_schedule_end_date', 'eh_schedule_status'));
            $collection->addFieldToFilter(array(
                array('attribute'=>'eh_schedule_start_date','eq'=>$today),
                array('attribute'=>'eh_schedule_end_date','eq'=>$yesterday)
            ));
            foreach ($collection as $product) {
                if($product->getEhScheduleStatus()){
                    $productId = $product->getId();
                    $EH_scheduleStatusText = $product->getAttributeText('eh_schedule_status');
                    $EH_schedulestartStatus = 1;$EH_scheduleendStatus = 2;
                    if($EH_scheduleStatusText=='Disable'){ $EH_schedulestartStatus = 2; $EH_scheduleendStatus = 1; }

                    if($product->getEhScheduleStartDate() == $today) {
                        if($EH_schedulestartStatus == 1) {
                            $product_list['enabled'][] = $product;
                        } else {
                            $product_list['disabled'][] = $product;
                        }
                    } elseif($product->getEhScheduleEndDate() == $yesterday) {
                        if($EH_scheduleendStatus == 1) {
                            $product_list['enabled'][] = $product;
                        } else {
                            $product_list['disabled'][] = $product;
                        }
                    }
                }
            }
        }
        return $product_list;
    }

    public function sendEmailNotifications() {
        if($this->isModuleActive() && $this->isReportEmailActive()) {
            $report_data = $this->getProductsForNotifications();
            if(!empty($report_data['enabled']) || !empty($report_data['disabled'])) {
                $html = array();
                $html['enabled'] = '';
                $html['disabled'] = '';
                if(isset($report_data['enabled'])) {
                    foreach($report_data['enabled'] as $_product) {
                        $html['enabled'] .= '<tr><td>'.$_product->getId().'</td><td>'.$_product->getName().'</td><td>'.$_product->getSku().'</td></tr>';
                    }
                }
                if(isset($report_data['disabled'])) {
                    foreach($report_data['disabled'] as $_product) {
                        $html['disabled'] .= '<tr><td>'.$_product->getId().'</td><td>'.$_product->getName().'</td><td>'.$_product->getSku().'</td></tr>';
                    }
                }
                $emailTemplate = Mage::getModel('core/email_template')->loadDefault('productscheduler_email_template');
                $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
                $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
                $days_before = Mage::getStoreConfig(self::SCHEDULER_EMAIL_DAYS_BEFORE);
                if($days_before == "" || $days_before == 0) {
                    $days_before = 1;
                } else {
                    $days_before = $days_before;
                }
                $emailTemplateVariables = array();
                $emailTemplateVariables['report'] = '<p style="font-size:12px; line-height:16px; margin:0 0 8px 0;">Following are the products which are going to enable on '.Mage::getModel("core/date")->date("F jS, Y", strtotime("+".$days_before." day")).'</p>';
                if($html['enabled'] != "") {
                    $emailTemplateVariables['report'] .= '<table border="1" cellspacing="0" cellpadding="5" width="650">
											<tr>
												<th>Product ID</th>
												<th>Product Name</th>
												<th>SKU</th>
											</tr>
											'.$html['enabled'].'
										</table>';
                }
                if($html['disabled'] != "") {
                    $emailTemplateVariables['report'] .= '<p style="font-size:12px; line-height:16px; margin:20px 0 8px 0;">Following are the products which are going to disable on '.Mage::getModel("core/date")->date("F jS, Y", strtotime("+".$days_before." day")).'</p>
										<table border="1" cellspacing="0" cellpadding="5" width="650">
											<tr>
												<th>Product ID</th>
												<th>Product Name</th>
												<th>SKU</th>
											</tr>
											'.$html['disabled'].'
										</table>';
                }
                $recipients = explode(',',Mage::getStoreConfig(self::SCHEDULER_EMAIL_ADDRESS));
                $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
                foreach($recipients as $recipient) {
                    $mail = Mage::getModel('core/email')
                        ->setToName($senderName)
                        ->setToEmail($recipient)
                        ->setBody($processedTemplate)
                        ->setSubject('Product Scheduler Report')
                        ->setFromEmail($senderEmail)
                        ->setFromName($senderName)
                        ->setType('html');
                    $mail->send();
                }
                return true;
            } else {
                return false;
            }
        }
    }

    public function changeProductStatus() {
        if($this->isModuleActive()) {
            $collection = Mage::getModel('catalog/product')->getCollection();
            $this->today = Mage::getModel('core/date')->date('Y-m-d 00:00:00');
            $this->current_hour = (int)Mage::getModel('core/date')->date('G');


            $collection->addAttributeToSelect(array('eh_schedule_start_date', 'eh_schedule_start_time', 'eh_schedule_end_date', 'eh_schedule_end_time', 'eh_schedule_status'), 'inner');
            $collection->addFieldToFilter(array(
                array('attribute'=>'eh_schedule_start_date','eq'=>$this->today),
                array('attribute'=>'eh_schedule_end_date','eq'=>$this->today)
            ));

            Mage::getSingleton('core/resource_iterator')->walk($collection->getSelect(),array(array($this, '_updateProductStatus')));
        }
    }

    public function _updateProductStatus($args){
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product');

        $product->setData($args['row']);
        $start_time =  $product->getAttributeText('eh_schedule_start_time') ? $product->getAttributeText('eh_schedule_start_time') : 0;
        $end_time = $product->getAttributeText('eh_schedule_end_time') ? $product->getAttributeText('eh_schedule_end_time') : 0;

        if($start_time == $this->current_hour || $this->current_hour == $end_time){
            $EH_scheduleStatusText = $product->getAttributeText('eh_schedule_status');
            $productId = $product->getId();

            if($this->current_hour == $end_time && $this->today == $args['row']['eh_schedule_end_date']){
                $EH_scheduleStatusText = ($EH_scheduleStatusText=='Enable')?'Disable':'Enable';
            }

            if($EH_scheduleStatusText == 'Enable') {
                $this->log($this->today.' : '.$this->current_hour.' => Change status to ENABLE for product with id : '.$productId);
                Mage::getModel('catalog/product_status')->updateProductStatus($productId, 0, Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
            } elseif($EH_scheduleStatusText == 'Disable') {
                $this->log($this->today.' : '.$this->current_hour.' => Change status to DISABLE for product with id : '.$productId);
                Mage::getModel('catalog/product_status')->updateProductStatus($productId, 0, Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
            }
        }
    }
}
