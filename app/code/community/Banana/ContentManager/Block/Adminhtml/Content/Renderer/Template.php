<?php

class Banana_ContentManager_Block_Adminhtml_Content_Renderer_Template extends Mage_Adminhtml_Block_Abstract
{
    public function setRendererTemplate($renderer)
    {
        $this->setTemplate('banana/contentmanager/grid/'.$renderer.'.phtml');
    }
    
    public function createButton($label, $url, $classes = "")
    {
        return '<button title="'.$label.'" type="button" class="scalable '.$classes.'" onclick="setLocation(\''.$url.'\')" style=""><span><span><span>'.$label.'</span></span></span></button>';
    }
    
    public function isAllowed($storeId)
    {
        $contentTypeId  = (int) $this->getRequest()->getParam('ct_id');
        $contentTypeModel = Mage::getModel('contentmanager/contenttype')->load($contentTypeId);
      
        return Mage::getSingleton('admin/session')->isAllowed('contentmanager/content_'.$contentTypeModel->getIdentifier().'_view_0') || Mage::getSingleton('admin/session')->isAllowed('contentmanager/content_'.$contentTypeModel->getIdentifier().'_view_'.$storeId) || Mage::getSingleton('admin/session')->isAllowed('contentmanager/content_everything');
    }    
    
    public function getContentModel($storeId, $contentId)
    {
        $helper = $this->helper('contentmanager');
        if(!isset($helper->loadedContents[$storeId.'_'.$contentId]))
        {
            $helper->loadedContents[$storeId.'_'.$contentId] = Mage::getModel('contentmanager/content')->setStoreId($storeId)->load($contentId);
        }
        return $helper->loadedContents[$storeId.'_'.$contentId];
    }
}

