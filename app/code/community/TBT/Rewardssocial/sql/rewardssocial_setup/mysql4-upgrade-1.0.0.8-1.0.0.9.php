<?php

$installer = $this;

$installer->startSetup();

$this->dropForeignKey($this->getTable('rewardssocial/purchase_share'), "FK_REWARDSSOCIAL_PURCHASE_SHARE_CUSTOMER");

$this->addForeignKey(
    'FK_REWARDSSOCIAL_PURCHASE_SHARE_CUSTOMER',
    $this->getTable('rewardssocial/purchase_share'),
    'customer_id',
    $this->getTable('customer/entity'),
    'entity_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE, 
    Varien_Db_Ddl_Table::ACTION_CASCADE
);

$installer->endSetup();
