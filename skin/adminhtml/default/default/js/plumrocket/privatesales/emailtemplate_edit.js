;

var defaultEmailtemplateData = {};
jQuery(document).ready(function(){
	defaultEmailtemplateData = jQuery('#edit_form').serialize();
});

jQuery.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    jQuery.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function updateEmailCategories()
{
	jQuery.ajax({
		url		: updateCategoryUrl,
		data	: jQuery('#edit_form').serialize(),
		type	: 'POST',
		dataType: 'json',
		success : function(data){
			var categories = data.categories;

			var options = '';
			//if (data.count){
				for(var i in categories)
				{
					var id 		= categories[i]['value'];
					var name 	= categories[i]['label'];
					if (!id) continue;

					options += '<option  value="'+id+'">'+name+'</option>';
				}
			//}
			//alert(options);
			jQuery('#emailemplate_categories_ids').html(options);
		},
		error : function(){
			alert('Unexpected error please try again later.');
		}
	});
}

function loadDefaultTemplate()
{
	if (confirm('Are you sure you want to load default template? Current template will be lost.')) {
   		jQuery('#emailemplate_template').val(jQuery('#default_template').val());
	}
}

function loadDefaultListTemplate()
{
	if (confirm('Are you sure you want to load default template? Current template will be lost.')) {
		var number = jQuery('#emailemplate_list_layout').val();
		if (!number) number = 1;
   		jQuery('#emailemplate_list_template').val(jQuery('#default_list_emplate_'+number).val());
	}
}


;