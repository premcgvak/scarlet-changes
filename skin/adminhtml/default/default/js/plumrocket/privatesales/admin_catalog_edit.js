/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

function onPrivatesaleTypeChanged(element) {
    var type = element.value;
	var toDisable = {
		1 : ['privatesale_is_storefront'],
		2 : ['privatesale_date_start', 'privatesale_date_end', 'privatesale_is_link_to_product', 'privatesale_boutique_category]['],
		3 : ['privatesale_is_storefront', 'privatesale_is_link_to_product', 'privatesale_boutique_category][']
	};
	
	// enable all elements
	var name2id = {};
	element.up(2).select('input', 'select', 'textarea').each(function(el){
        if (element.id != el.id) {
            el.disabled = false;
			el.value = '';
			name2id[ el.name ] = el.id;
        }
    });
	element.up(2).select('img').each(function(el){
		el.show();
    });
	
	for (i = 0, len = toDisable[type].length; i < len; i++) {
		var name = 'general[' + toDisable[type][i] + ']';
		var id = name2id[name];
		
		$$('#' + id)[0].disabled = true;
		
		if (toDisable[type][i] == 'privatesale_date_start' || toDisable[type][i] == 'privatesale_date_end') {
			$$('#' + id + '_trig')[0].hide();
		}
	}
}