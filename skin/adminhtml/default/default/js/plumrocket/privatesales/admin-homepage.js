/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

function HomepageMap() {
	var items = {};
	var activeItem = {};
	var colCount = 0;
	
	var banners = {};
	var pathToBanner = '/';
	var buttonPath = '/';
	
	// **
	this.init = function(data, _colCount, _banners, _path, _buttonPath) {
		colCount = _colCount;
		banners = _banners;
		pathToBanner = _path;
		buttonPath = _buttonPath;
		
		items = {};
		for (var areaName in data) {
			items[areaName] = {};
			
			// array of offset for rows below these with rowspan > 1
			var offset = {};
			
			for (var rowId in data[areaName]) {
				if (rowId != '_count_') {
					items[areaName][rowId] = {};
					startIn = 1;
					for (var i = 0, len = data[areaName][rowId].length; i < len; i++) {
						// find free cell
						while (offset[startIn] && offset[startIn].rowspan > 0) {
							startIn += offset[startIn].colspan;
						}
					
						items[areaName][rowId][startIn] = data[areaName][rowId][i];
						
						// add offset if rowspan > 1
						if (items[areaName][rowId][startIn].rowspan > 1) {
							offset[startIn] = {
								colspan: items[areaName][rowId][startIn].colspan,
								rowspan: items[areaName][rowId][startIn].rowspan // rospan will be decrement immediately after For function
							};
						}
						
						startIn += data[areaName][rowId][i].colspan;
					}
					
					for (var i in offset) {
						offset[i].rowspan -= 1;
						if (offset[i].rowspan == 0) {
							delete offset[i];
						}
					}
					
					_appendRow(items[areaName][rowId], areaName, rowId, null);
				}else {
					items[areaName]['_count_'] = data[areaName]['_count_'];
				}
			}
		}
	}
	
	// **
	this.addRow = function(areaName)
	{
		var ind = items[areaName]['_count_'];
		ind++;
		
		items[areaName][ind] = {};
		for (var i = 1; i <= colCount; i++) {
			items[areaName][ind][i] = {
				colspan: 1,
				rowspan: 1,
				type: 1,
				banners: []
			};
		}
		_appendRow(items[areaName][ind], areaName, ind, null);
		
		items[areaName]['_count_'] = ind;
	}
	
	// **
	this.setItemAs = function(type)
	{
		items[activeItem.areaName][activeItem.rowId][activeItem.tdId].type = type;
		
		var div = jQuery('#td_' + activeItem.idPrefix);
		_setType(type, activeItem.areaName, activeItem.rowId, activeItem.tdId);
		
		jQuery('.qtip').css('display', 'none');
		if (type == 2) {
			jQuery.fly(div.parent('td'), {
				source: '#banner-menu-holder',
			});
			
			setTimeout(function(){
				jQuery('#banner-menu-holder').css('display', 'block');
				_hideBanners();
			}, 100);
		}
	}
	
	// **
	this.mergeTo = function(type)
	{
		switch (type) {
			case 'right':
				var row = items[activeItem.areaName][activeItem.rowId];
				var currId = activeItem.tdId;
				var nextId = getNextItem(currId, row);
				
				if (nextId > 0 && (row[nextId].rowspan == row[currId].rowspan) && (nextId == currId + row[currId].colspan)) {
					var colspan = row[nextId].colspan + row[currId].colspan;
					
					items[activeItem.areaName][activeItem.rowId][currId].colspan = colspan;
					delete items[activeItem.areaName][activeItem.rowId][nextId];
					
					jQuery('#td_' + activeItem.areaName + '_' + activeItem.rowId + '_' + nextId).parent('td').remove();
					jQuery('#td_' + activeItem.idPrefix).parent('td').attr('colspan', colspan);
					//set value
					jQuery('#map_' + activeItem.idPrefix + '_colspan').val(colspan);
				}
				break;
			case 'down':				
				var nextRow = activeItem.rowId;
				for (var i = 0; i < activeItem.rowspan; i++) { 
					nextRow = getNextItem(nextRow, items[activeItem.areaName]);
				}
				
				if (nextRow) {
					if (items[activeItem.areaName][nextRow][activeItem.tdId] && activeItem.colspan == items[activeItem.areaName][nextRow][activeItem.tdId].colspan) {
						var rowspan = items[activeItem.areaName][nextRow][activeItem.tdId].rowspan + activeItem.rowspan;
						
						items[activeItem.areaName][activeItem.rowId][activeItem.tdId].rowspan = rowspan;
						delete items[activeItem.areaName][nextRow][activeItem.tdId];
						
						jQuery('#td_' + activeItem.areaName + '_' + nextRow + '_' + activeItem.tdId).parent('td').remove();
						jQuery('#td_' + activeItem.idPrefix).parent('td').attr('rowspan', rowspan);
						// set value
						jQuery('#map_' + activeItem.idPrefix + '_rowspan').val(rowspan);
					}
				}
				
				break;
		}
	}
	
	this.breakUnion = function ()
	{
		var currItem = items[activeItem.areaName][activeItem.rowId][activeItem.tdId];
		//var log = '';
		
		if (currItem.colspan != 1 || currItem.rowspan != 1) {
			jQuery('.action_panel_source').appendTo('#store_for_menu');
			jQuery('#td_' + activeItem.idPrefix).parent('td').remove();
			delete items[activeItem.areaName][activeItem.rowId][activeItem.tdId];
			
			for (var r = activeItem.rowId; r < (parseInt(activeItem.rowId) + parseInt(currItem.rowspan)); r++) {
				for (var c = activeItem.tdId; c < (parseInt(activeItem.tdId) + parseInt(currItem.colspan)); c++) {
					//log += "Add cell ( " + r + ", " + c + ") \n";
					items[activeItem.areaName][r][c] = {
						colspan: 1,
						rowspan: 1,
						type: 1,
						banners: []
					};
				}
				var tr = jQuery('#row_' + activeItem.areaName + '_' + r + ' td').remove();
				var tr = jQuery('#row_' + activeItem.areaName + '_' + r).empty();
				_appendRow(items[activeItem.areaName][r], activeItem.areaName, r, tr);
			}
		}
		//alert(log);
	}
	
	this.setBanner = function(id)
	{
		if (jQuery('#set_banner_' + id).is(':checked') && (id in banners)) {
			_addBanner(banners[id], true, activeItem);
			
			_hideBanners();
			_showBanners();
		}
	}
	
	// ================ private section ========================= //
	
	// **
	function _appendRow(data, areaName, rowId, rowInstance)
	{
		if (rowInstance == null) {
			var tr = jQuery('<tr></tr>')
				.attr('id', 'row_' + areaName + '_' + rowId)
				.insertBefore('#add_row_' + areaName)
		} else {
			tr = jQuery(rowInstance);
		}
		
		var td = jQuery('<td></td>')
			.appendTo(tr);
			
		// Delete row
		var _div = jQuery('<div></div>')
			.css('height', '64px')
			.appendTo(td);
		
		jQuery('<a></a>')
			.text('Delete row')
			.css('cursor', 'pointer')
			.bind('click', {areaName: areaName, rowId: rowId}, _deleteRow)
			.appendTo(_div);
			
		var sortedIds = [];
		for (var i in data) {
			sortedIds.push(i);
		}
		sortedIds.sort(function(a,b){return parseInt(a) - parseInt(b)});

		//for (var i in data) {
		for (iter = 0; iter < sortedIds.length; iter++) {
			var i = sortedIds[iter];
			// TD
			var td = jQuery('<td></td>')
				.addClass('item')
				.css({'text-align': 'center'})
				.bind('click', {areaName: areaName, rowId: rowId, tdId: parseInt(i)}, _showActionPanel)
				.appendTo(tr);
			
			// Input :colspan
			jQuery('<input />')
				.attr({
					'type': 'hidden', 
					'id': 'map_' + areaName + '_' + rowId + '_' + i + '_colspan',
					'name': 'map[' + areaName + '][' + rowId + '][' + i + '][colspan]'
				})
				.val(data[i].colspan)
				.appendTo(td);
				
			// Input :rowspan
			jQuery('<input />')
				.attr({
					'type': 'hidden', 
					'id': 'map_' + areaName + '_' + rowId + '_' + i + '_rowspan',
					'name': 'map[' + areaName + '][' + rowId + '][' + i + '][rowspan]'
				})
				.val(data[i].rowspan)
				.appendTo(td);
				
			// Input :colspan
			jQuery('<input />')
				.attr({
					'type': 'hidden', 
					'id': 'map_' + areaName + '_' + rowId + '_' + i + '_type',
					'name': 'map[' + areaName + '][' + rowId + '][' + i + '][type]'
				})
				.val(data[i].type)
				.appendTo(td);
				
			// Inputs :banners
			// First: not add hidden elements for banners if we refresh row after break an union of blocks
			if (rowInstance == null && data[i].banners && data[i].banners.length > 0) {
				for (z = 0, len = data[i].banners.length; z < len; z++) {
					if (data[i].banners[z] && banners[ data[i].banners[z].id ]) {
						data[i].banners[z].href = banners[ data[i].banners[z].id ].href;
						_addBanner(data[i].banners[z], false, {areaName: areaName, rowId: rowId, tdId: i});
					}
				}
			}
				
			// DIV
			var div = jQuery('<div></div>')
				.css({'position': 'relative'})
				.attr('id', 'td_' + areaName + '_' + rowId + '_' + i)
				.appendTo(td);
				
			// :Type
			_setType(data[i].type, areaName, rowId, i);
			
			// :Colspan
			if (data[i].colspan > 1) {
				jQuery(td).attr('colspan', data[i].colspan);
			}
			
			// :Rowspan
			if (data[i].rowspan > 1) {
				jQuery(td).attr('rowspan', data[i].rowspan);
			}
		}
	}
	
	// **
	function _setType(type, areaName, rowId, tdId)
	{
		var types = {1: 'Boutique', 2: 'Banner', 0: 'Empty space'};
		
		if (!(type in types)) {
			type = 1;
		}
		
		jQuery('.action_panel_source').appendTo('#store_for_menu');
		var div = jQuery('#td_' + areaName + '_' + rowId + '_' + tdId);
		
		jQuery(div).text(types[type]);
		jQuery(div)
			.parent('td')
			.attr('class', '')
			.addClass(types[type].toLowerCase());
			
		// set value
		jQuery('#map_' + areaName + '_' + rowId + '_' + tdId + '_type').val(type);
	}
	
	// **
	function _deleteRow(data)
	{
		var areaName = data.data.areaName;
		var rowId = data.data.rowId;
		
		var _colspan = 0;
		var rowspan = 0;
		var diffRowSpan = false;
		
		jQuery('.action_panel_source').appendTo('#store_for_menu');
		
		for (var i in items[areaName][rowId]) {
			_colspan += items[areaName][rowId][i].colspan;
			
			if (rowspan == 0) {
				rowspan = items[areaName][rowId][i].rowspan;
			} else if (rowspan != items[areaName][rowId][i].rowspan) {
				diffRowSpan = true;
			}
		}
		
		if (_colspan == colCount && diffRowSpan === false) {
			for (var i = 0; i < rowspan; i++) {
				var nextRow = getNextItem(rowId, items[areaName]);
				delete items[areaName][rowId];
				
				jQuery('#row_' + areaName + '_' + rowId).remove();
				rowId = nextRow;
			}
		}
	}
	
	// **
	function _showActionPanel(data)
	{
		activeItem = data.data;
		var currItem = items[activeItem.areaName][activeItem.rowId][activeItem.tdId];
		// set data of active item
		for (var i in currItem) {
			activeItem[i] = currItem[i];
		}
		activeItem.idPrefix = activeItem.areaName + '_' + activeItem.rowId + '_' + activeItem.tdId;
		activeItem.namePrefix = '[' + activeItem.areaName + '][' + activeItem.rowId + '][' + activeItem.tdId + ']';
		
		var parent = '#td_' + activeItem.idPrefix;
		jQuery('.action_panel').remove();
		
		jQuery('.action_panel_source').appendTo(parent);
			
		// correct position
		var _wd = (jQuery(parent).width() -  jQuery('.action_panel_source').width()) / 2;
		jQuery('.action_panel_source').css('left', _wd);
		
		// ---- Show banners
		_hideBanners();
		if (activeItem.type == 2) {
			_showBanners();
		}
	}
	
	function _showBanners()
	{
		//jQuery('#banners_panel').css('display', 'block');
		jQuery('#ready_banners > div').css('display', 'block');
		
		jQuery('.active_banner_' + activeItem.idPrefix).each(function(){
			jQuery(this).css('display', 'block');

			var id = parseInt(
				jQuery(this).attr('id').replace('active_banner_' + activeItem.idPrefix + '_', '')
			);
			
			// disable in ready list
			jQuery('#ready_banner_' + id).css('display', 'none');
			jQuery('#ready_banner_' + id + ' input[type="checkbox"]').removeAttr('checked');
		});
	}
	
	function _addBanner(banner, visible, data)
	{
		if (!(banner.id in banners)) {
			return false;
		}
		var _clone = jQuery('.action_banner')
			.clone()
			.attr({'class': '', 'id': 'active_banner_' + data.areaName + '_' + data.rowId + '_' + data.tdId + '_' + banner.id})
			.addClass('active_banner_' + data.areaName + '_' + data.rowId + '_' + data.tdId)
			.addClass('active_banner')
			.addClass('item')
			.appendTo('#active_banners .list_banners')
			.css('display', (visible)? 'block': 'none');
			
		jQuery(_clone).find('.banner-image').attr('src', pathToBanner + banners[ banner.id ].name);
		jQuery(_clone).find('input[type="checkbox"]')
			.attr('id', 'rem_banner_' + data.rowId + '_' + data.tdId + '_' + banner.id)
			.bind('click', {id: banner.id}, _removeBanner)
			.addClass('remove_banner');
		jQuery(_clone).find('label').attr('for', 'rem_banner_' + data.rowId + '_' + data.tdId + '_' + banner.id);
		jQuery(_clone).find('a').text('banner url').attr({'href': banner.href, 'title': banner.href});
		
		// :date from
		var _it = jQuery(_clone).find('.date_from')
			.attr({
				'name': 'map[' + data.areaName + '][' + data.rowId + '][' + data.tdId + '][banners][' + banner.id + '][date_from]',
				'id': 'map_' + data.areaName + '_' + data.rowId + '_' + data.tdId + '_banners_' + banner.id + '_date_from'
			})
			.val((visible)? '00/00/00': banner.date_from);
			
		_it.datepicker({
			dateFormat: "m/dd/y",
			showOn: "both",
			buttonImage: buttonPath,
			buttonImageOnly: true,
			buttonText: 'Select Date'
		});
			
		// :date to
		var _it = jQuery(_clone).find('.date_to')
			.attr({
				'name': 'map[' + data.areaName + '][' + data.rowId + '][' + data.tdId + '][banners][' + banner.id + '][date_to]',
				'id': 'map_' + data.areaName + '_' + data.rowId + '_' + data.tdId + '_banners_' + banner.id + '_date_to'
			}).val((visible)? '00/00/00': banner.date_to);
			
		_it.datepicker({
			dateFormat: "m/dd/y",
			showOn: "both",
			buttonImage: buttonPath,
			buttonImageOnly: true,
			buttonText: 'Select Date'
		});
	}
	
	function _removeBanner(data)
	{
		var id = data.data.id;
		
		jQuery('#active_banner_' + activeItem.idPrefix + '_' + id).remove();
		jQuery('#ready_banner_' + id).css('display', 'block');
	}
	
	function _hideBanners()
	{
		//jQuery('#banners_panel').css('display', 'none');
		
		jQuery('#active_banners .active_banner').css('display', 'none');
		jQuery('#ready_banners > div').css('display', 'block');
		jQuery('#ready_banners input[type="checkbox"]').removeAttr('checked');
	}
	
	// **
	function getNextItem(key, data)
	{
		var found = false;
		for (var j in data) {
			if (j == '_count_') {
				continue;
			}
			if (found == true) {
				return parseInt(j);
			}
			if (j == key) {
				found = true;
			}
		}
		return 0;
	}
	
	// ** 
	function getPrevItem(key, data)
	{
		var prev = false;
		for (var j in data) {
			if (j == '_count_') {
				continue;
			}
			if (j == key) {
				return parseInt(prev);
			}
			prev = j;
		}
		return false;
	}
}

function reRenderTree(event, switcher)
{
	// re-render tree by store switcher
    if (event) {
        var obj = event.target;
        var newStoreId = obj.value * 1;

        if (switcher.useConfirm) {
            if (!confirm("Please confirm site switching. All data that hasn\'t been saved will be lost.")) {
                return false;
            }
        }
        
        url = location.href.toString();
        url = url.replace(/\/store\/[0-9]+/, '')
        url = url + 'store/' + newStoreId + '/';
        
        location.href = url;
    }
}

Ext.onReady(function() {
	
	if ((typeof varienStoreSwitcher)!='undefined') {
        varienStoreSwitcher.storeSelectorClickCallback = reRenderTree;
    }
});
