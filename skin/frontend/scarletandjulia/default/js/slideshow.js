/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     default_default
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

$j(document).ready(function () {

    // ==============================================
    // UI Pattern - Slideshow
    // ==============================================

    /*$j('.slideshow-container .slideshow')
        .cycle({
            slides: '> li',
            pager: '.slideshow-pager',
            pagerTemplate: '<span class="pager-box"></span>',
            speed: 600,
            pauseOnHover: true,
            swipe: true,
            prev: '.slideshow-prev',
            next: '.slideshow-next',
            fx: 'scrollHorz'
        });*/

    // ==============================================
    // Carrousel Produits
    // ==============================================
    function heightProductsLi() {
        $j('.p-container .category-products .products-grid').each(function() {
            var liMaxHeight = 0;

            $j(this).find('li').each(function(e, index) {
                if($j(this).height() > 0) {
                    $j(this).css('height', '');
                }

                if(liMaxHeight < $j(this).outerHeight()) {
                    liMaxHeight = $j(this).outerHeight();
                }
            });

            $j(this).find('li').height(liMaxHeight);

            $j(this).css('height', 'inherit');
            $j(this).height(liMaxHeight);
        });
    }

    var compteur = 1;
    $j('.p-container .category-products .products-grid').each(function() {

        $j(this).closest('.p-container').find('.cycle-prev').attr('id', 'prev-' + compteur);
        $j(this).closest('.p-container').find('.cycle-next').attr('id', 'next-' + compteur);

        $j(this).cycle({
            slides: '> li',
            speed: 300,
            timeout: 0,
            swipe: true,
            prev: '#prev-' + compteur,
            next: '#next-' + compteur,
            fx: 'carousel',
            carouselFluid: true,
            allowWrap: true
        });

        compteur++;
    });

    /*enquire.register('screen and (min-width: 0)', function() {
        setTimeout(heightProductsLi, 1000);
    }).register('screen and (min-width: 320px) and (max-width: ' + (bp.medium - 1) + 'px)', function() {
        heightProductsLi();
    }).register('screen and (min-width: ' + (bp.medium) + 'px) and (max-width: ' + (bp.large - 1) + 'px)', function() {
        heightProductsLi();
    }).register('screen and (min-width: ' + (bp.large) + 'px)', function() {
        heightProductsLi();
    });*/
});
