;$j = jQuery;
window.hasOwnProperty = function(obj){
	return (this[obj]) ? true : false;
}
if (!window.hasOwnProperty('IWD')) {
	IWD = {};
};

IWD.ColorSwatch = {
		configAttributes:null,
		config:null,
		returnOriginTimeout:null,
		
		//GLOBAL INIT METHOD
		init: function(){
			IWD.ColorSwatch.config = $j.parseJSON(IWD.ColorSwatch.config);
			if (IWD.ColorSwatch.config.enabled !=1){				
				return;
			}
			
			IWD.ColorSwatch.configAttributes = $j.parseJSON(IWD.ColorSwatch.configAttributes);	
			$j('.switch-attribute select').each(function(){
				if ($j(this).is(':disabled')){
					IWD.ColorSwatch.buildFullAttribute($j(this));
				}else{
					IWD.ColorSwatch.buildByOptions($j(this));
				}
			});
			
			IWD.ColorSwatch.initChange();
			IWD.ColorSwatch.initSwitch();
			IWD.ColorSwatch.preSelect();
		}, 
		
		preSelect: function(){
			if (IWD.ColorSwatch.config.preselect==1){
				$j('.colorswatch-list li:first').not('.disabled').click();
			}
		},
		
		//INIT CHANGE SELECT
		initChange: function(){
			$j('.iwd-colorswatch select').change(function(){
				var $select = $j(this);
				IWD.ColorSwatch.reBuildAttribute($select);
				
			});
		},
		
		
		reBuildAttribute: function(){
			//remove all list
			$j('.colorswatch-list').remove();
			
			$j('.switch-attribute select').each(function(){
				var $select = $j(this)
				if ($select.is(':disabled')){
					IWD.ColorSwatch.buildFullAttribute($select);
				}else{
					IWD.ColorSwatch.buildByOptions($select);
				}
				IWD.ColorSwatch.applySelectedOption($select);
			});
			
		},
		
		applySelectedOption: function($select){
			var $ulContainer = $select.next(),
				selectedOption= $select.val(),
				text = $select.find(':selected').text(),
				id = $select.attr('id');
				if (selectedOption!=''){
					$j('#label-'+id).html(': '+text);
				}else{
					$j('#label-'+id).empty();
				}
			
			$ulContainer.find('li').each(function(){
				if ($j(this).data('id')==selectedOption){
					$j(this).addClass('active-switch');
					
				}
			});
		},
		
		
		//BULD FULL LIST OPTIONS
		buildFullAttribute: function(select){
			select.addClass('hidden');
			var parent = select.parent(); 
			$atributeId= select.data('id');
			
			var options = IWD.ColorSwatch.configAttributes[$atributeId]['options'];
			
			var additional =  IWD.ColorSwatch.configAttributes[$atributeId]['additional'];
			
			var ul  = $j('<ul />').addClass('colorswatch-list'); 
			if (additional==0){
				ul.addClass('without-additional')
			}
			$j.each(options, function(index, option){
				
				
				var li  = $j('<li />')
								.addClass('colorswatch-item')
								.addClass('disabled')
								.width(IWD.ColorSwatch.config.width)
								.height(IWD.ColorSwatch.config.height)
								.prop('title', option.label)
								.data('id', option.id);

                if(option.label == select.children("option:selected").html()) {
                    li.addClass('active');
                }

				if (IWD.ColorSwatch.config.mode==1){
					$j('<img />').addClass('colorswatch-item-img').attr('src', option.image).addClass('normal').appendTo(li);
					if (additional==1){
						$j('<img />').addClass('colorswatch-item-img').attr('src', option.active).addClass('active').appendTo(li);
						$j('<img />').addClass('colorswatch-item-img').attr('src', option.hover).addClass('hover').appendTo(li);
						$j('<img />').addClass('colorswatch-item-img').attr('src', option.disabled).addClass('disabled').appendTo(li);
					}
					
				}else{
					$j('<span />').addClass('colorswatch-item-img').attr('style', 'background-color:#' + option.hex_image).addClass('normal').width(IWD.ColorSwatch.config.width).height(IWD.ColorSwatch.config.height).appendTo(li);
					if (additional==1){
						$j('<span />').addClass('colorswatch-item-img').attr('style', 'background-color:#' + option.hex_active).addClass('active').width(IWD.ColorSwatch.config.width).height(IWD.ColorSwatch.config.height).appendTo(li);
						$j('<span />').addClass('colorswatch-item-img').attr('style', 'background-color:#' + option.hex_hover).addClass('hover').width(IWD.ColorSwatch.config.width).height(IWD.ColorSwatch.config.height).appendTo(li);
						$j('<span />').addClass('colorswatch-item-img').attr('style', 'background-color:#' + option.hex_disabled).addClass('disabled').width(IWD.ColorSwatch.config.width).height(IWD.ColorSwatch.config.height).appendTo(li);
					}
				}
				li.appendTo(ul);
			
			});

            ul.appendTo('.sj-colorswatch-carrousel');
		}, 
		
		//BULD LIST OPTION FROM SELECT
		buildByOptions: function(select){
			select.addClass('hidden');
			var parent = select.parent(); 
			$atributeId= select.data('id');
			
			var options = IWD.ColorSwatch.configAttributes[$atributeId]['options'];
			var additional =  IWD.ColorSwatch.configAttributes[$atributeId]['additional'];
			
			
			var ul  = $j('<ul />').addClass('colorswatch-list');
			if (additional==0){
				ul.addClass('without-additional')
			}
			
			//get list options from select	
			var selectOptions  = [];
			
			select.find('option').each(function(index){
				var value  = $j(this).val();
				if (value != ''){
					selectOptions[index] = value;
				}
			});
			
			$j.each(options, function(index, option){				
				
				var found = jQuery.inArray( option.id, selectOptions );
				
				if (found != -1){
					
					var li  = $j('<li />')
										.addClass('colorswatch-item')
										.data('id', option.id)
										.width(IWD.ColorSwatch.config.width)
										.height(IWD.ColorSwatch.config.height)	
										.prop('title', option.label)
										.data('select', select.attr('id'));

                    if(option.label == select.children("option:selected").html()) {
                        li.addClass('active');
                    }
					
					if (IWD.ColorSwatch.config.mode==1){
						$j('<img />').addClass('colorswatch-item-img').attr('src', option.image).addClass('normal').appendTo(li);
						if (additional==1){
							$j('<img />').addClass('colorswatch-item-img').attr('src', option.active).addClass('active').appendTo(li);
							$j('<img />').addClass('colorswatch-item-img').attr('src', option.hover).addClass('hover').appendTo(li);
							$j('<img />').addClass('colorswatch-item-img').attr('src', option.disabled).addClass('disabled').appendTo(li);
						}
					}else{
						$j('<span />').addClass('colorswatch-item-img').attr('style', 'background-color:#' + option.hex_image).addClass('normal').width(IWD.ColorSwatch.config.width).height(IWD.ColorSwatch.config.height).appendTo(li);
						if (additional==1){
							$j('<span />').addClass('colorswatch-item-img').attr('style', 'background-color:#' + option.hex_active).addClass('active').width(IWD.ColorSwatch.config.width).height(IWD.ColorSwatch.config.height).appendTo(li);
							$j('<span />').addClass('colorswatch-item-img').attr('style', 'background-color:#' + option.hex_hover).addClass('hover').width(IWD.ColorSwatch.config.width).height(IWD.ColorSwatch.config.height).appendTo(li);
							$j('<span />').addClass('colorswatch-item-img').attr('style', 'background-color:#' + option.hex_disabled).addClass('disabled').width(IWD.ColorSwatch.config.width).height(IWD.ColorSwatch.config.height).appendTo(li);
						}
					}
					li.appendTo(ul);
				}
			
			});

            ul.appendTo('.sj-colorswatch-carrousel');
		},
		
		//init event to click at colorswatch item(<li>)
		initSwitch: function(){
			$j(document).on('click','.colorswatch-item', function(){
				
				if ($j(this).hasClass('disabled')){
					return;
				}

				$j('.colorswatch-item').removeClass('active-switch');
				
				$j(this).addClass('active-switch');
				
				var selectId = $j(this).data('select'),
				value = $j(this).data('id');

                //console.log('selectId = ' + selectId + ' / value = ' + value);
				
				$j('#'+selectId).val(value);

					/*var id = $j(this).attr('id');
					$(id).simulate("change");*/
				try{
					spConfig.configureElement(document.getElementById(selectId));				
					spConfig.reloadPrice();
				}catch(e){
					console.debug(e);
				}
				IWD.ColorSwatch.reBuildAttribute();
				
				IWD.ColorSwatch.pullImage();
			});

            $j(document).on('change','.super-attribute-select', function(){
                var optionSelected = $j('.super-attribute-select option:selected');

                if ($j(optionSelected).hasClass('disabled')){
                    return;
                }

                $j('.super-attribute-select option').removeClass('active-switch');

                $j(optionSelected).addClass('active-switch');

                var selectId = $j(optionSelected).data('select'),
                    value = $j(optionSelected).data('id');

                $j('#'+selectId).val(value);

                /*var id = $j(this).attr('id');
                 $(id).simulate("change");*/
                try{
                    spConfig.configureElement(document.getElementById(selectId));
                    spConfig.reloadPrice();
                }catch(e){
                    console.debug(e);
                }
                IWD.ColorSwatch.reBuildAttribute();

              	IWD.ColorSwatch.pullImage();
            });
		}, 
		
		//pull image from product for selected attribute
		pullImage: function(){
			var form = $j('#product_addtocart_form').serialize(); 
			//$j('.colorswatch-images-zoom .ajax-loader').fadeIn();
            $j('.ajax-loader').fadeIn();
			$j.post(IWD.ColorSwatch.config.imageUrl, form, function(response){
				//$j('.colorswatch-images-zoom .ajax-loader').hide();
                $j('.ajax-loader').hide();
				if (typeof(response.media)!="undefined"){
					$j('.colorswatch-images-zoom').removeClass('colorswatch-images-zoom').html(response.media);					
				}
                IWD.ColorSwatch.initCarrousel();
			},'json');
		},
		
		//show image grom simple product by attribute option
		renderImageCategoryPage: function(){
			$j('.colorswatch-list-product-list li').hover(function(){				
				var imageLink = $j(this).data('image'), 
				parent = $j(this).closest('li.item'),
				img = parent.find('.product-image img:first');
				
				if (img.data('origin') == undefined){
					var origin = img.attr('src');
					img.attr('src', imageLink).data('origin', origin);;
				}else{
					img.attr('src', imageLink);
				}

				
			});
			
			$j('.colorswatch-list-product-list').hover(function(){
				
			}, function(){
				var parent = $j(this).closest('li.item'),
				img = parent.find('.product-image img:first'),			
				url = img.data('origin'); 
				
				img.attr('src',url);
			});

		},

        initCarrousel: function(){
            var checkIfColorElementsExists = ($j('.sj-colorswatch-carrousel .colorswatch-list').length != 0);

            if (checkIfColorElementsExists === true) {
                var prevButton = $j('.sj-colorswatch-container .prev');
                var nextButton = $j('.sj-colorswatch-container .next');
                var carrouselAllLi = $j('.sj-colorswatch-carrousel .colorswatch-list li');
                var activeLiIndex;

                carrouselAllLi.each(function(index) {
                    if($j(this).hasClass('active')) {
                        activeLiIndex = index;
                    }
                });

                $j('.sj-colorswatch-container .sj-colorswatch-carrousel .colorswatch-list').slick({
                    speed: 100,
                    slide: 'li',
                    infinite: false,
                    initialSlide: activeLiIndex,
                    prevArrow: prevButton,
                    nextArrow: nextButton,
                    variableWidth: true
                });
            }
        }
		
};

$j(document).ready(function(){
	IWD.ColorSwatch.renderImageCategoryPage();
});
