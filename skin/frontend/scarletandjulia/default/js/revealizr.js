// Avoid PrototypeJS conflicts, assign jQuery to $j instead of $
var $j = jQuery.noConflict();

// revealizr = Modernizr + Detectizr + Revealizr


/* Modernizr 2.8.3 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-fontface-backgroundsize-borderimage-borderradius-boxshadow-flexbox-flexboxlegacy-hsla-multiplebgs-opacity-rgba-textshadow-cssanimations-csscolumns-generatedcontent-cssgradients-cssreflections-csstransforms-csstransforms3d-csstransitions-applicationcache-canvas-canvastext-draganddrop-hashchange-history-audio-video-indexeddb-input-inputtypes-localstorage-postmessage-sessionstorage-websockets-websqldatabase-webworkers-geolocation-inlinesvg-smil-svg-svgclippaths-touch-webgl-printshiv-mq-cssclasses-teststyles-testprop-testallprops-hasevent-prefixes-domprefixes-load
 */
;window.Modernizr=function(a,b,c){function D(a){j.cssText=a}function E(a,b){return D(n.join(a+";")+(b||""))}function F(a,b){return typeof a===b}function G(a,b){return!!~(""+a).indexOf(b)}function H(a,b){for(var d in a){var e=a[d];if(!G(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function I(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:F(f,"function")?f.bind(d||b):f}return!1}function J(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+p.join(d+" ")+d).split(" ");return F(b,"string")||F(b,"undefined")?H(e,b):(e=(a+" "+q.join(d+" ")+d).split(" "),I(e,b,c))}function K(){e.input=function(c){for(var d=0,e=c.length;d<e;d++)u[c[d]]=c[d]in k;return u.list&&(u.list=!!b.createElement("datalist")&&!!a.HTMLDataListElement),u}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),e.inputtypes=function(a){for(var d=0,e,f,h,i=a.length;d<i;d++)k.setAttribute("type",f=a[d]),e=k.type!=="text",e&&(k.value=l,k.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(f)&&k.style.WebkitAppearance!==c?(g.appendChild(k),h=b.defaultView,e=h.getComputedStyle&&h.getComputedStyle(k,null).WebkitAppearance!=="textfield"&&k.offsetHeight!==0,g.removeChild(k)):/^(search|tel)$/.test(f)||(/^(url|email)$/.test(f)?e=k.checkValidity&&k.checkValidity()===!1:e=k.value!=l)),t[a[d]]=!!e;return t}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var d="2.8.3",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k=b.createElement("input"),l=":)",m={}.toString,n=" -webkit- -moz- -o- -ms- ".split(" "),o="Webkit Moz O ms",p=o.split(" "),q=o.toLowerCase().split(" "),r={svg:"http://www.w3.org/2000/svg"},s={},t={},u={},v=[],w=v.slice,x,y=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},z=function(b){var c=a.matchMedia||a.msMatchMedia;if(c)return c(b)&&c(b).matches||!1;var d;return y("@media "+b+" { #"+h+" { position: absolute; } }",function(b){d=(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle)["position"]=="absolute"}),d},A=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=F(e[d],"function"),F(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),B={}.hasOwnProperty,C;!F(B,"undefined")&&!F(B.call,"undefined")?C=function(a,b){return B.call(a,b)}:C=function(a,b){return b in a&&F(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=w.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(w.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(w.call(arguments)))};return e}),s.flexbox=function(){return J("flexWrap")},s.flexboxlegacy=function(){return J("boxDirection")},s.canvas=function(){var a=b.createElement("canvas");return!!a.getContext&&!!a.getContext("2d")},s.canvastext=function(){return!!e.canvas&&!!F(b.createElement("canvas").getContext("2d").fillText,"function")},s.webgl=function(){return!!a.WebGLRenderingContext},s.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:y(["@media (",n.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c},s.geolocation=function(){return"geolocation"in navigator},s.postmessage=function(){return!!a.postMessage},s.websqldatabase=function(){return!!a.openDatabase},s.indexedDB=function(){return!!J("indexedDB",a)},s.hashchange=function(){return A("hashchange",a)&&(b.documentMode===c||b.documentMode>7)},s.history=function(){return!!a.history&&!!history.pushState},s.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a},s.websockets=function(){return"WebSocket"in a||"MozWebSocket"in a},s.rgba=function(){return D("background-color:rgba(150,255,150,.5)"),G(j.backgroundColor,"rgba")},s.hsla=function(){return D("background-color:hsla(120,40%,100%,.5)"),G(j.backgroundColor,"rgba")||G(j.backgroundColor,"hsla")},s.multiplebgs=function(){return D("background:url(https://),url(https://),red url(https://)"),/(url\s*\(.*?){3}/.test(j.background)},s.backgroundsize=function(){return J("backgroundSize")},s.borderimage=function(){return J("borderImage")},s.borderradius=function(){return J("borderRadius")},s.boxshadow=function(){return J("boxShadow")},s.textshadow=function(){return b.createElement("div").style.textShadow===""},s.opacity=function(){return E("opacity:.55"),/^0.55$/.test(j.opacity)},s.cssanimations=function(){return J("animationName")},s.csscolumns=function(){return J("columnCount")},s.cssgradients=function(){var a="background-image:",b="gradient(linear,left top,right bottom,from(#9f9),to(white));",c="linear-gradient(left top,#9f9, white);";return D((a+"-webkit- ".split(" ").join(b+a)+n.join(c+a)).slice(0,-a.length)),G(j.backgroundImage,"gradient")},s.cssreflections=function(){return J("boxReflect")},s.csstransforms=function(){return!!J("transform")},s.csstransforms3d=function(){var a=!!J("perspective");return a&&"webkitPerspective"in g.style&&y("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(b,c){a=b.offsetLeft===9&&b.offsetHeight===3}),a},s.csstransitions=function(){return J("transition")},s.fontface=function(){var a;return y('@font-face {font-family:"font";src:url("https://")}',function(c,d){var e=b.getElementById("smodernizr"),f=e.sheet||e.styleSheet,g=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"";a=/src/i.test(g)&&g.indexOf(d.split(" ")[0])===0}),a},s.generatedcontent=function(){var a;return y(["#",h,"{font:0/0 a}#",h,':after{content:"',l,'";visibility:hidden;font:3px/1 a}'].join(""),function(b){a=b.offsetHeight>=3}),a},s.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c},s.audio=function(){var a=b.createElement("audio"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,"")}catch(d){}return c},s.localstorage=function(){try{return localStorage.setItem(h,h),localStorage.removeItem(h),!0}catch(a){return!1}},s.sessionstorage=function(){try{return sessionStorage.setItem(h,h),sessionStorage.removeItem(h),!0}catch(a){return!1}},s.webworkers=function(){return!!a.Worker},s.applicationcache=function(){return!!a.applicationCache},s.svg=function(){return!!b.createElementNS&&!!b.createElementNS(r.svg,"svg").createSVGRect},s.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==r.svg},s.smil=function(){return!!b.createElementNS&&/SVGAnimate/.test(m.call(b.createElementNS(r.svg,"animate")))},s.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(m.call(b.createElementNS(r.svg,"clipPath")))};for(var L in s)C(s,L)&&(x=L.toLowerCase(),e[x]=s[L](),v.push((e[x]?"":"no-")+x));return e.input||K(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)C(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},D(""),i=k=null,e._version=d,e._prefixes=n,e._domPrefixes=q,e._cssomPrefixes=p,e.mq=z,e.hasEvent=A,e.testProp=function(a){return H([a])},e.testAllProps=J,e.testStyles=y,g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+v.join(" "):""),e}(this,this.document),function(a,b){function l(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function m(){var a=s.elements;return typeof a=="string"?a.split(" "):a}function n(a){var b=j[a[h]];return b||(b={},i++,a[h]=i,j[i]=b),b}function o(a,c,d){c||(c=b);if(k)return c.createElement(a);d||(d=n(c));var g;return d.cache[a]?g=d.cache[a].cloneNode():f.test(a)?g=(d.cache[a]=d.createElem(a)).cloneNode():g=d.createElem(a),g.canHaveChildren&&!e.test(a)&&!g.tagUrn?d.frag.appendChild(g):g}function p(a,c){a||(a=b);if(k)return a.createDocumentFragment();c=c||n(a);var d=c.frag.cloneNode(),e=0,f=m(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function q(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return s.shivMethods?o(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/\w+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(s,b.frag)}function r(a){a||(a=b);var c=n(a);return s.shivCSS&&!g&&!c.hasCSS&&(c.hasCSS=!!l(a,"article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),k||q(a,c),a}function w(a){var b,c=a.getElementsByTagName("*"),d=c.length,e=RegExp("^(?:"+m().join("|")+")$","i"),f=[];while(d--)b=c[d],e.test(b.nodeName)&&f.push(b.applyElement(x(b)));return f}function x(a){var b,c=a.attributes,d=c.length,e=a.ownerDocument.createElement(u+":"+a.nodeName);while(d--)b=c[d],b.specified&&e.setAttribute(b.nodeName,b.nodeValue);return e.style.cssText=a.style.cssText,e}function y(a){var b,c=a.split("{"),d=c.length,e=RegExp("(^|[\\s,>+~])("+m().join("|")+")(?=[[\\s,>+~#.:]|$)","gi"),f="$1"+u+"\\:$2";while(d--)b=c[d]=c[d].split("}"),b[b.length-1]=b[b.length-1].replace(e,f),c[d]=b.join("}");return c.join("{")}function z(a){var b=a.length;while(b--)a[b].removeNode()}function A(a){function g(){clearTimeout(d._removeSheetTimer),b&&b.removeNode(!0),b=null}var b,c,d=n(a),e=a.namespaces,f=a.parentWindow;return!v||a.printShived?a:(typeof e[u]=="undefined"&&e.add(u),f.attachEvent("onbeforeprint",function(){g();var d,e,f,h=a.styleSheets,i=[],j=h.length,k=Array(j);while(j--)k[j]=h[j];while(f=k.pop())if(!f.disabled&&t.test(f.media)){try{d=f.imports,e=d.length}catch(m){e=0}for(j=0;j<e;j++)k.push(d[j]);try{i.push(f.cssText)}catch(m){}}i=y(i.reverse().join("")),c=w(a),b=l(a,i)}),f.attachEvent("onafterprint",function(){z(c),clearTimeout(d._removeSheetTimer),d._removeSheetTimer=setTimeout(g,500)}),a.printShived=!0,a)}var c="3.7.0",d=a.html5||{},e=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,f=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,g,h="_html5shiv",i=0,j={},k;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",g="hidden"in a,k=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){g=!0,k=!0}})();var s={elements:d.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:c,shivCSS:d.shivCSS!==!1,supportsUnknownElements:k,shivMethods:d.shivMethods!==!1,type:"default",shivDocument:r,createElement:o,createDocumentFragment:p};a.html5=s,r(b);var t=/^$|\b(?:all|print)\b/,u="html5shiv",v=!k&&function(){var c=b.documentElement;return typeof b.namespaces!="undefined"&&typeof b.parentWindow!="undefined"&&typeof c.applyElement!="undefined"&&typeof c.removeNode!="undefined"&&typeof a.attachEvent!="undefined"}();s.type+=" print",s.shivPrint=A,A(b)}(this,document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};

/*! Detectizr v2.0.0 | (c) 2012 Baris Aydinoglu | Licensed MIT */
//window.Detectizr=function(a,b,c,d){var e,f,g={},h=a.Modernizr,i=["tv","tablet","mobile","desktop"],j={addAllFeaturesAsClass:!1,detectDevice:!0,detectDeviceModel:!0,detectScreen:!0,detectOS:!0,detectBrowser:!0,detectPlugins:!0},k=[{name:"adobereader",substrs:["Adobe","Acrobat"],progIds:["AcroPDF.PDF","PDF.PDFCtrl.5"]},{name:"flash",substrs:["Shockwave Flash"],progIds:["ShockwaveFlash.ShockwaveFlash.1"]},{name:"wmplayer",substrs:["Windows Media"],progIds:["wmplayer.ocx"]},{name:"silverlight",substrs:["Silverlight"],progIds:["AgControl.AgControl"]},{name:"quicktime",substrs:["QuickTime"],progIds:["QuickTime.QuickTime"]}],l=/[\t\r\n]/g,m=c.documentElement;function n(a,b){var c,d,e;if(arguments.length>2)for(c=1,d=arguments.length;d>c;c+=1)n(a,arguments[c]);else for(e in b)b.hasOwnProperty(e)&&(a[e]=b[e]);return a}function o(a){return g.browser.userAgent.indexOf(a)>-1}function p(a){return a.test(g.browser.userAgent)}function q(a){return a.exec(g.browser.userAgent)}function r(a){return a.replace(/^\s+|\s+$/g,"")}function s(a){return null===a||a===d?"":String(a).replace(/((\s|\-|\.)+[a-z0-9])/g,function(a){return a.toUpperCase().replace(/(\s|\-|\.)/g,"")})}function t(a,b){var c=b||"",d=1===a.nodeType&&(a.className?(" "+a.className+" ").replace(l," "):"");if(d){while(d.indexOf(" "+c+" ")>=0)d=d.replace(" "+c+" "," ");a.className=b?r(d):""}}function u(a,b,c){a&&(a=s(a),b&&(b=s(b),w(a+b,!0),c&&w(a+b+"_"+c,!0)))}function v(){a.clearTimeout(e),e=a.setTimeout(function(){f=g.device.orientation,g.device.orientation=a.innerHeight>a.innerWidth?"portrait":"landscape",w(g.device.orientation,!0),f!==g.device.orientation&&w(f,!1)},10)}function w(a,b){a&&h&&(j.addAllFeaturesAsClass?h.addTest(a,b):(b="function"==typeof b?b():b,b?h.addTest(a,!0):(delete h[a],t(m,a))))}function x(a,b){a.version=b;var c=b.split(".");c.length>0?(c=c.reverse(),a.major=c.pop(),c.length>0?(a.minor=c.pop(),c.length>0?(c=c.reverse(),a.patch=c.join(".")):a.patch="0"):a.minor="0"):a.major="0"}function y(d){var e,f,l,m,r,t,y,z,A=this;if(j=n({},j,d||{}),j.detectDevice){for(g.device={type:"",model:"",orientation:""},m=g.device,p(/googletv|smarttv|internet.tv|netcast|nettv|appletv|boxee|kylo|roku|dlnadoc|ce\-html/)?(m.type=i[0],m.model="smartTv"):p(/xbox|playstation.3|wii/)?(m.type=i[0],m.model="gameConsole"):p(/ip(a|ro)d/)?(m.type=i[1],m.model="ipad"):p(/tablet/)&&!p(/rx-34/)||p(/folio/)?(m.type=i[1],m.model=String(q(/playbook/)||"")):p(/linux/)&&p(/android/)&&!p(/fennec|mobi|htc.magic|htcX06ht|nexus.one|sc-02b|fone.945/)?(m.type=i[1],m.model="android"):p(/kindle/)||p(/mac.os/)&&p(/silk/)?(m.type=i[1],m.model="kindle"):p(/gt-p10|sc-01c|shw-m180s|sgh-t849|sch-i800|shw-m180l|sph-p100|sgh-i987|zt180|htc(.flyer|\_flyer)|sprint.atp51|viewpad7|pandigital(sprnova|nova)|ideos.s7|dell.streak.7|advent.vega|a101it|a70bht|mid7015|next2|nook/)||p(/mb511/)&&p(/rutem/)?(m.type=i[1],m.model="android"):p(/bb10/)?(m.type=i[1],m.model="blackberry"):(m.model=q(/iphone|ipod|android|blackberry|opera mini|opera mobi|skyfire|maemo|windows phone|palm|iemobile|symbian|symbianos|fennec|j2me/),null!==m.model?(m.type=i[2],m.model=String(m.model)):(m.model="",p(/bolt|fennec|iris|maemo|minimo|mobi|mowser|netfront|novarra|prism|rx-34|skyfire|tear|xv6875|xv6975|google.wireless.transcoder/)?m.type=i[2]:p(/opera/)&&p(/windows.nt.5/)&&p(/htc|xda|mini|vario|samsung\-gt\-i8000|samsung\-sgh\-i9/)?m.type=i[2]:p(/windows.(nt|xp|me|9)/)&&!p(/phone/)||p(/win(9|.9|nt)/)||p(/\(windows 8\)/)?m.type=i[3]:p(/macintosh|powerpc/)&&!p(/silk/)?(m.type=i[3],m.model="mac"):p(/linux/)&&p(/x11/)?m.type=i[3]:p(/solaris|sunos|bsd/)?m.type=i[3]:p(/bot|crawler|spider|yahoo|ia_archiver|covario-ids|findlinks|dataparksearch|larbin|mediapartners-google|ng-search|snappy|teoma|jeeves|tineye/)&&!p(/mobile/)?(m.type=i[3],m.model="crawler"):m.type=i[2])),e=0,f=i.length;f>e;e+=1)w(i[e],m.type===i[e]);j.detectDeviceModel&&w(s(m.model),!0)}if(j.detectScreen&&(h&&h.mq&&(w("smallScreen",h.mq("only screen and (max-width: 480px)")),w("verySmallScreen",h.mq("only screen and (max-width: 320px)")),w("veryVerySmallScreen",h.mq("only screen and (max-width: 240px)"))),m.type===i[1]||m.type===i[2]?(a.onresize=function(a){v(a)},v()):(m.orientation="landscape",w(m.orientation,!0))),j.detectOS&&(g.os={},r=g.os,""!==m.model&&("ipad"===m.model||"iphone"===m.model||"ipod"===m.model?(r.name="ios",x(r,(p(/os\s([\d_]+)/)?RegExp.$1:"").replace(/_/g,"."))):"android"===m.model?(r.name="android",x(r,p(/android\s([\d\.]+)/)?RegExp.$1:"")):"blackberry"===m.model?(r.name="blackberry",x(r,p(/version\/([^\s]+)/)?RegExp.$1:"")):"playbook"===m.model&&(r.name="blackberry",x(r,p(/os ([^\s]+)/)?RegExp.$1.replace(";",""):""))),r.name||(o("win")||o("16bit")?(r.name="windows",o("windows nt 6.3")?x(r,"8.1"):o("windows nt 6.2")||p(/\(windows 8\)/)?x(r,"8"):o("windows nt 6.1")?x(r,"7"):o("windows nt 6.0")?x(r,"vista"):o("windows nt 5.2")||o("windows nt 5.1")||o("windows xp")?x(r,"xp"):o("windows nt 5.0")||o("windows 2000")?x(r,"2k"):o("winnt")||o("windows nt")?x(r,"nt"):o("win98")||o("windows 98")?x(r,"98"):(o("win95")||o("windows 95"))&&x(r,"95")):o("mac")||o("darwin")?(r.name="mac os",o("68k")||o("68000")?x(r,"68k"):o("ppc")||o("powerpc")?x(r,"ppc"):o("os x")&&x(r,(p(/os\sx\s([\d_]+)/)?RegExp.$1:"os x").replace(/_/g,"."))):o("webtv")?r.name="webtv":o("x11")||o("inux")?r.name="linux":o("sunos")?r.name="sun":o("irix")?r.name="irix":o("freebsd")?r.name="freebsd":o("bsd")&&(r.name="bsd")),r.name&&(w(r.name,!0),r.major&&(u(r.name,r.major),r.minor&&u(r.name,r.major,r.minor))),r.addressRegisterSize=p(/\sx64|\sx86|\swin64|\swow64|\samd64/)?"64bit":"32bit",w(r.addressRegisterSize,!0)),j.detectBrowser&&(t=g.browser,p(/opera|webtv/)||!p(/msie\s([\d\w\.]+)/)&&!o("trident")?o("firefox")?(t.engine="gecko",t.name="firefox",x(t,p(/firefox\/([\d\w\.]+)/)?RegExp.$1:"")):o("gecko/")?t.engine="gecko":o("opera")?(t.name="opera",t.engine="presto",x(t,p(/version\/([\d\.]+)/)?RegExp.$1:p(/opera(\s|\/)([\d\.]+)/)?RegExp.$2:"")):o("konqueror")?t.name="konqueror":o("chrome")?(t.engine="webkit",t.name="chrome",x(t,p(/chrome\/([\d\.]+)/)?RegExp.$1:"")):o("iron")?(t.engine="webkit",t.name="iron"):o("crios")?(t.name="chrome",t.engine="webkit",x(t,p(/crios\/([\d\.]+)/)?RegExp.$1:"")):o("applewebkit/")?(t.name="safari",t.engine="webkit",x(t,p(/version\/([\d\.]+)/)?RegExp.$1:"")):o("mozilla/")&&(t.engine="gecko"):(t.engine="trident",t.name="ie",!a.addEventListener&&c.documentMode&&7===c.documentMode?x(t,"8.compat"):p(/trident.*rv[ :](\d+)\./)?x(t,RegExp.$1):x(t,p(/trident\/4\.0/)?"8":RegExp.$1)),t.name&&(w(t.name,!0),t.major&&(u(t.name,t.major),t.minor&&u(t.name,t.major,t.minor))),w(t.engine,!0),t.language=b.userLanguage||b.language,w(t.language,!0)),j.detectPlugins){for(t.plugins=[],A.detectPlugin=function(a){var c,d,e,g=b.plugins;for(f=g.length-1;f>=0;f--){for(c=g[f],d=c.name+c.description,e=0,l=a.length;l>=0;l--)-1!==d.indexOf(a[l])&&(e+=1);if(e===a.length)return!0}return!1},A.detectObject=function(a){for(f=a.length-1;f>=0;f--)try{new ActiveXObject(a[f])}catch(b){}return!1},e=k.length-1;e>=0;e--)y=k[e],z=!1,a.ActiveXObject?z=A.detectObject(y.progIds):b.plugins&&(z=A.detectPlugin(y.substrs)),z&&(t.plugins.push(y.name),w(y.name,!0));b.javaEnabled()&&(t.plugins.push("java"),w("java",!0))}}return g.detect=function(a){return y(a)},g.init=function(){g!==d&&(g.browser={userAgent:(b.userAgent||b.vendor||a.opera).toLowerCase()},g.detect())},g.init(),g}(this,this.navigator,this.document);

/*! Detectizr v2.0.0 | (c) 2012 Baris Aydinoglu | Licensed MIT */
window.Detectizr=function(a,b,c,d){var e,f,g={},h=a.Modernizr,i=["tv","tablet","mobile","desktop"],j={addAllFeaturesAsClass:!1,detectDevice:!0,detectDeviceModel:!0,detectScreen:!0,detectOS:!0,detectBrowser:!0,detectPlugins:!0},k=[{name:"adobereader",substrs:["Adobe","Acrobat"],progIds:["AcroPDF.PDF","PDF.PDFCtrl.5"]},{name:"flash",substrs:["Shockwave Flash"],progIds:["ShockwaveFlash.ShockwaveFlash.1"]},{name:"wmplayer",substrs:["Windows Media"],progIds:["wmplayer.ocx"]},{name:"silverlight",substrs:["Silverlight"],progIds:["AgControl.AgControl"]},{name:"quicktime",substrs:["QuickTime"],progIds:["QuickTime.QuickTime"]}],l=/[\t\r\n]/g,m=c.documentElement;function n(a,b){var c,d,e;if(arguments.length>2)for(c=1,d=arguments.length;d>c;c+=1)n(a,arguments[c]);else for(e in b)b.hasOwnProperty(e)&&(a[e]=b[e]);return a}function o(a){return g.browser.userAgent.indexOf(a)>-1}function p(a){return a.test(g.browser.userAgent)}function q(a){return a.exec(g.browser.userAgent)}function r(a){return a.replace(/^\s+|\s+$/g,"")}function s(a){return null===a||a===d?"":String(a).replace(/((\s|\-|\.)+[a-z0-9])/g,function(a){return a.toUpperCase().replace(/(\s|\-|\.)/g,"")})}function t(a,b){var c=b||"",d=1===a.nodeType&&(a.className?(" "+a.className+" ").replace(l," "):"");if(d){while(d.indexOf(" "+c+" ")>=0)d=d.replace(" "+c+" "," ");a.className=b?r(d):""}}function u(a,b,c){a&&(a=s(a),b&&(b=s(b),w(a+b,!0),c&&w(a+b+"_"+c,!0)))}function v(){a.clearTimeout(e),e=a.setTimeout(function(){f=g.device.orientation,g.device.orientation=a.innerHeight>a.innerWidth?"portrait":"landscape",w(g.device.orientation,!0),f!==g.device.orientation&&w(f,!1)},10)}function w(a,b){a&&h&&(j.addAllFeaturesAsClass?h.addTest(a,b):(b="function"==typeof b?b():b,b?h.addTest(a,!0):(delete h[a],t(m,a))))}function x(a,b){a.version=b;var c=b.split(".");c.length>0?(c=c.reverse(),a.major=c.pop(),c.length>0?(a.minor=c.pop(),c.length>0?(c=c.reverse(),a.patch=c.join(".")):a.patch="0"):a.minor="0"):a.major="0"}function y(d){var e,f,l,m,r,t,y,z,A=this;if(j=n({},j,d||{}),j.detectDevice){for(g.device={type:"",model:"",orientation:""},m=g.device,p(/googletv|smarttv|internet.tv|netcast|nettv|appletv|boxee|kylo|roku|dlnadoc|ce\-html/)?(m.type=i[0],m.model="smartTv"):p(/xbox|playstation.3|wii/)?(m.type=i[0],m.model="gameConsole"):p(/ip(a|ro)d/)?(m.type=i[1],m.model="ipad"):p(/tablet/)&&!p(/rx-34/)||p(/folio/)?(m.type=i[1],m.model=String(q(/playbook/)||"")):p(/linux/)&&p(/android/)&&!p(/fennec|mobi|htc.magic|htcX06ht|nexus.one|sc-02b|fone.945/)?(m.type=i[1],m.model="android"):p(/kindle/)||p(/mac.os/)&&p(/silk/)?(m.type=i[1],m.model="kindle"):p(/gt-p10|sc-01c|shw-m180s|sgh-t849|sch-i800|shw-m180l|sph-p100|sgh-i987|zt180|htc(.flyer|\_flyer)|sprint.atp51|viewpad7|pandigital(sprnova|nova)|ideos.s7|dell.streak.7|advent.vega|a101it|a70bht|mid7015|next2|nook/)||p(/mb511/)&&p(/rutem/)?(m.type=i[1],m.model="android"):p(/bb10/)?(m.type=i[1],m.model="blackberry"):(m.model=q(/iphone|ipod|android|blackberry|opera mini|opera mobi|skyfire|maemo|windows phone|palm|iemobile|symbian|symbianos|fennec|j2me/),null!==m.model?(m.type=i[2],m.model=String(m.model)):(m.model="",p(/bolt|fennec|iris|maemo|minimo|mobi|mowser|netfront|novarra|prism|rx-34|skyfire|tear|xv6875|xv6975|google.wireless.transcoder/)?m.type=i[2]:p(/opera/)&&p(/windows.nt.5/)&&p(/htc|xda|mini|vario|samsung\-gt\-i8000|samsung\-sgh\-i9/)?m.type=i[2]:p(/windows.(nt|xp|me|9)/)&&!p(/phone/)||p(/win(9|.9|nt)/)||p(/\(windows 8\)/)?m.type=i[3]:p(/macintosh|powerpc/)&&!p(/silk/)?(m.type=i[3],m.model="mac"):p(/linux/)&&p(/x11/)?m.type=i[3]:p(/solaris|sunos|bsd/)?m.type=i[3]:p(/bot|crawler|spider|yahoo|ia_archiver|covario-ids|findlinks|dataparksearch|larbin|mediapartners-google|ng-search|snappy|teoma|jeeves|tineye/)&&!p(/mobile/)?(m.type=i[3],m.model="crawler"):m.type=i[2])),e=0,f=i.length;f>e;e+=1)w(i[e],m.type===i[e]);j.detectDeviceModel&&w(s(m.model),!0)}if(j.detectScreen&&(h&&h.mq&&(w("smallScreen",h.mq("only screen and (max-width: 480px)")),w("verySmallScreen",h.mq("only screen and (max-width: 320px)")),w("veryVerySmallScreen",h.mq("only screen and (max-width: 240px)"))),m.type===i[1]||m.type===i[2]?(a.onresize=function(a){v(a)},v()):(m.orientation="landscape",w(m.orientation,!0))),j.detectOS&&(g.os={},r=g.os,""!==m.model&&("ipad"===m.model||"iphone"===m.model||"ipod"===m.model?(r.name="ios",x(r,(p(/os\s([\d_]+)/)?RegExp.$1:"").replace(/_/g,"."))):"android"===m.model?(r.name="android",x(r,p(/android\s([\d\.]+)/)?RegExp.$1:"")):"blackberry"===m.model?(r.name="blackberry",x(r,p(/version\/([^\s]+)/)?RegExp.$1:"")):"playbook"===m.model&&(r.name="blackberry",x(r,p(/os ([^\s]+)/)?RegExp.$1.replace(";",""):""))),r.name||(o("win")||o("16bit")?(r.name="windows",o("windows nt 6.3")?x(r,"8.1"):o("windows nt 6.2")||p(/\(windows 8\)/)?x(r,"8"):o("windows nt 6.1")?x(r,"7"):o("windows nt 6.0")?x(r,"vista"):o("windows nt 5.2")||o("windows nt 5.1")||o("windows xp")?x(r,"xp"):o("windows nt 5.0")||o("windows 2000")?x(r,"2k"):o("winnt")||o("windows nt")?x(r,"nt"):o("win98")||o("windows 98")?x(r,"98"):(o("win95")||o("windows 95"))&&x(r,"95")):o("mac")||o("darwin")?(r.name="mac os",o("68k")||o("68000")?x(r,"68k"):o("ppc")||o("powerpc")?x(r,"ppc"):o("os x")&&x(r,(p(/os\sx\s([\d_]+)/)?RegExp.$1:"os x").replace(/_/g,"."))):o("webtv")?r.name="webtv":o("x11")||o("inux")?r.name="linux":o("sunos")?r.name="sun":o("irix")?r.name="irix":o("freebsd")?r.name="freebsd":o("bsd")&&(r.name="bsd")),r.name&&(w(r.name,!0),r.major&&(u(r.name,r.major),r.minor&&u(r.name,r.major,r.minor))),r.addressRegisterSize=p(/\sx64|\sx86|\swin64|\swow64|\samd64/)?"64bit":"32bit",w(r.addressRegisterSize,!0)),j.detectBrowser&&(t=g.browser,p(/opera|webtv/)||!p(/msie\s([\d\w\.]+)/)&&!o("trident")?o("firefox")?(t.engine="gecko",t.name="firefox",x(t,p(/firefox\/([\d\w\.]+)/)?RegExp.$1:"")):o("gecko/")?t.engine="gecko":o("opera")?(t.name="opera",t.engine="presto",x(t,p(/version\/([\d\.]+)/)?RegExp.$1:p(/opera(\s|\/)([\d\.]+)/)?RegExp.$2:"")):o("konqueror")?t.name="konqueror":o("chrome")?(t.engine="webkit",t.name="chrome",x(t,p(/chrome\/([\d\.]+)/)?RegExp.$1:"")):o("iron")?(t.engine="webkit",t.name="iron"):o("crios")?(t.name="chrome",t.engine="webkit",x(t,p(/crios\/([\d\.]+)/)?RegExp.$1:"")):o("applewebkit/")?(t.name="safari",t.engine="webkit",x(t,p(/version\/([\d\.]+)/)?RegExp.$1:"")):o("mozilla/")&&(t.engine="gecko"):(t.engine="trident",t.name="ie",!a.addEventListener&&c.documentMode&&7===c.documentMode?x(t,"8.compat"):p(/trident.*rv[ :](\d+)\./)?x(t,RegExp.$1):x(t,p(/trident\/4\.0/)?"8":RegExp.$1)),t.name&&(w(t.name,!0),t.major&&(u(t.name,t.major),t.minor&&u(t.name,t.major,t.minor))),w(t.engine,!0),t.language=b.userLanguage||b.language,w(t.language,!0)),j.detectPlugins){for(t.plugins=[],A.detectPlugin=function(a){var c,d,e,g=b.plugins;for(f=g.length-1;f>=0;f--){for(c=g[f],d=c.name+c.description,e=0,l=a.length;l>=0;l--)-1!==d.indexOf(a[l])&&(e+=1);if(e===a.length)return!0}return!1},A.detectObject=function(a){for(f=a.length-1;f>=0;f--)try{new ActiveXObject(a[f])}catch(b){}return!1},e=k.length-1;e>=0;e--)y=k[e],z=!1,a.ActiveXObject?z=A.detectObject(y.progIds):b.plugins&&(z=A.detectPlugin(y.substrs)),z&&(t.plugins.push(y.name),w(y.name,!0));b.javaEnabled()&&(t.plugins.push("java"),w("java",!0))}}return g.detect=function(a){return y(a)},g.init=function(){g!==d&&(g.browser={userAgent:(b.userAgent||b.vendor||a.opera).toLowerCase()},g.detect())},g.init(),g}(this,this.navigator,this.document);
//# sourceMappingURL=detectizr.min.map

/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
window.matchMedia=window.matchMedia||(function(e,f){var c,a=e.documentElement,b=a.firstElementChild||a.firstChild,d=e.createElement("body"),g=e.createElement("div");g.id="mq-test-1";g.style.cssText="position:absolute;top:-100em";d.style.background="none";d.appendChild(g);return function(h){g.innerHTML='&shy;<style media="'+h+'"> #mq-test-1 { width: 42px; }</style>';a.insertBefore(d,b);c=g.offsetWidth==42;a.removeChild(d);return{matches:c,media:h}}})(document);



// Activate Detectizr
Detectizr.detect({
	// option for enabling HTML classes of all features (not only the true features) to be added
	addAllFeaturesAsClass: false,
	// option for enabling detection of device
	detectDevice: true,
	// option for enabling detection of device model
	detectDeviceModel: true,
	// option for enabling detection of screen size
	detectScreen: true,
	// option for enabling detection of operating system type and version
	detectOS: true,
	// option for enabling detection of browser type and version
	detectBrowser: true,
	// option for enabling detection of common browser plugins
	detectPlugins: true
});



/*! Revealizr - Detect features v1.0.0 | (c) 2014 J-M Noël - Tink.ca | Licensed MIT */
/*
	Ajout de class possible dans le HTML:
		- desktop-size
		- tablet-size
		- mobile-size
		- server-dev
		- server-int
		- server-qa
		- server-tac
		- egc
		- egc-content
*/



var firstLoad = true,
	tabletDevice = false,
	tabletSize = false
	mobileDevice = false,
	mobileSize = false,
	desktopDevice = false,
	desktopSize = false,
	currentViewportSize = '',
	lastViewportSize = '',
	windowOrientation = '',
	ie8 = false,
	mac = false,
	browserNatifAndroid = false,
	androidBrowser = false,
	blackberry_mobile = false,
	resizeORorientation = '',
	clickORtouchend = '',
	egc = false,
	egcContent = false,
	server = '';

if( $j('html').attr('lang') ) {
	var lang = $j('html').attr('lang'),
		lang_generic = $j('html').attr('lang').split('-'),
		lang_generic = lang_generic[0];
}



function detectFeatures() {

	if ( $j('html').hasClass('blackberry10') && $j('html').hasClass('smallscreen') ) {
		$j('html').addClass('mobile');
		$j('html').removeClass('tablet');
	}

	//On identifie le device utilisé
	if( $j('html').hasClass('desktop') ){
		desktopDevice = true;
		tabletDevice = false;
		mobileDevice = false;
	}else if( $j('html').hasClass('tablet') ) {
		desktopDevice = false;
		tabletDevice = true;
		mobileDevice = false;
	}else if( $j('html').hasClass('mobile') ) {
		desktopDevice = false;
		tabletDevice = false;
		mobileDevice = true;
	}

	//On identifie le breakpoint
	if( Modernizr.mq('only all and (max-width: 719px)') ) {
	    $j('html').removeClass('desktop-size');
	    $j('html').removeClass('tablet-size');
	    $j('html').addClass('mobile-size');
	    mobileSize = true;
	    tabletSize = false;
	    desktopSize = false;
	}else if( Modernizr.mq('only all and (max-width: 1023px)') ) {
		$j('html').removeClass('desktop-size');
		$j('html').addClass('tablet-size');
		$j('html').removeClass('mobile-size');
	    mobileSize = false;
	    tabletSize = true;
	    desktopSize = false;
	}else if( Modernizr.mq('only all and (min-width: 1024px)') ){
		$j('html').addClass('desktop-size');
		$j('html').removeClass('tablet-size');
		$j('html').removeClass('mobile-size');
		mobileSize = false;
	    tabletSize = false;
	    desktopSize = true;
	}else {
		//Si les media queries de modernizr ne sont pas supportés - IE9-
		if( $j(window).width() > 1023 ){
			$j('html').addClass('desktop-size');
			$j('html').removeClass('tablet-size');
			$j('html').removeClass('mobile-size');
			mobileSize = true;
			tabletSize = false;
			desktopSize = false;
		}else if( $j(window).width() > 719 ){
			$j('html').removeClass('desktop-size');
			$j('html').addClass('tablet-size');
			$j('html').removeClass('mobile-size');
			mobileSize = false;
			tabletSize = true;
			desktopSize = false;
		}else if( $j(window).width() <= 719 ){
			$j('html').removeClass('desktop-size');
			$j('html').removeClass('tablet-size');
			$j('html').addClass('mobile-size');
			mobileSize = true;
			tabletSize = false;
			desktopSize = false;
		}
	}

	//on récupère le dernier viewport size si ce n'est pas le load initial
	if( firstLoad == false ){
		lastViewportSize = currentViewportSize;
	}

	//on identifie le viewport courant
	if( mobileSize ){
		viewportSize( 'mobile-size' );
	}else if( tabletSize ){
		viewportSize( 'tablet-size' );
	}else if( desktopSize ){
		viewportSize( 'desktop-size' );
	}

	function viewportSize(newViewportSize){
		currentViewportSize = newViewportSize;
	}

	//Détection de l'orientation du device
	if( $j('html').hasClass('portrait') ){
		windowOrientation = 'portrait';
	}
	if( $j('html').hasClass('landscape') ){
		windowOrientation = 'landscape';
	}

	//class egc
	if ( document.location.href.indexOf('/site-web/') > -1 ) {
		egc = true;
		$j('html').addClass('egc');
	}

	if ( document.location.href.indexOf('mode=content') > -1 ) {
		egcContent = true;
		$j('html').addClass('egc-content');
	}

	//si en DEV
	if ( document.location.href.indexOf('local.') > -1 || document.location.href.indexOf('localhost') > -1 || document.location.href.indexOf('.s2i.com') > -1 ) {
		server = 'dev';
		$j('html').addClass('server-dev');
	}

	//si en INT
	if ( document.location.href.indexOf('.tinkint') > -1 ) {
		server = 'int';
		$j('html').addClass('server-int');
	}

	//si en QA
	if ( document.location.href.indexOf('.tinkqa') > -1 ) {
		server = 'qa';
		$j('html').addClass('server-qa');
	}

	//si en TAC
	if ( document.location.href.indexOf('.tinktac') > -1 ) {
		server = 'tac';
		$j('html').addClass('server-tac');
	}

	//détection de browser / os spécifique
	if ( $j('html').hasClass('mac') ){
		mac = true;
	}

	//détection du browser natif d'android
	function isAndroidBrowser() {
		var objAgent = navigator.userAgent;
		var objfullVersion  = ''+parseFloat(navigator.appVersion);
		// si Android Browser Natif avec agent Chrome (Samsung S4)
		if ((objOffsetVersion=objAgent.indexOf("Chrome")) != -1  && $j('html').hasClass('android')) {
			objfullVersion = objAgent.substring(objOffsetVersion+7, objOffsetVersion+9);
			if (objfullVersion < 30) {
				oldAndroid = true;
				$j('html').addClass('old-android');
				return true;
			}
		}
		// si Android Browser Natif avec agent Safari (Galaxy Tab)
		else if ((objOffsetVersion=objAgent.indexOf("Safari")) != -1  && $j('html').hasClass('android')) {
			objfullVersion = objAgent.substring(objOffsetVersion+7, objOffsetVersion+9);
			oldAndroid = true;
			$j('html').addClass('old-android');
			return true;
		}
		return false;
	}
	isAndroidBrowser();

	if( $j('html').hasClass('blackberry') && $j('html').hasClass('smallScreen') ){
		blackberry_mobile = 'true';
		$j('html').removeClass("tablet").addClass("mobile");
	}	

	if( $j('html').hasClass('ie8') ){
		ie8 = 'true';
	}

	if ( $j('html').hasClass('android') && $j('html').hasClass('chrome28') ){
		androidBrowser = true;
	}

	//Détection pour savoir si on utilise touchend ou click, resize ou orientationchange
	if( (mobileDevice || tabletDevice) && !androidBrowser ){
		resizeORorientation = 'orientationchange';
		clickORtouchend = 'touchend';
	}else {
		resizeORorientation = 'resize';
		clickORtouchend = 'click';
	}

	firstLoad = false;

}


