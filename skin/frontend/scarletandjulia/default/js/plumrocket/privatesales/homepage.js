/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

jQuery(document).ready(function() {
	// Loop through all the div.thatSetsABackgroundWithAnIcon on your page
	jQuery('.shop table tr td div').each(function(){
		// Get their parent td's height
		var tdHeight = jQuery(this).closest('td').height();

		// Set the div's height to their parent td's height
		jQuery(this).height(tdHeight);
	});
	
	// --------- HOVER ON SHOPS -------------------
	jQuery('.shop').hover(
		function(){
			jQuery(this).find('.brandholder').addClass('hover');
				
		},
		function(){
			jQuery(this).find('.brandholder').removeClass('hover');
		}
	);
	
	// ---------  FOR COUNT DOWN -------------------
	
	function _getTime(text)
	{
		var time = new Date().getTime();
		return new Date(time + parseInt(text) * 1000);
	}

	jQuery('.count-down').each(function(){
		var text = jQuery(this).next('.cntdown-source:first').text();
		if (text && /^[0-9]+$/.test(text)) {
			if (text != '0') {
				jQuery(this).countdown({
					until: _getTime(text), 
					format: 'dHM',
					layout: homepage_timer_label,
					labels: homepage_timer_labels_few.split(','),
					// The display texts for the counters if only one
					labels1: homepage_timer_labels_one.split(',')
				});
			}
		} else {
			jQuery(this).text(text);
		}
	});
	
	// ---- Middle Coming soon images ------------------
	jQuery('#upcoming-sales-block ul li').each(function(){
		var liH = jQuery(this).outerHeight();
		var divH = jQuery(this).find('.img-holder').outerHeight();
		
		jQuery(this).find('.img-holder').css('top', (divH - liH) / -2);
	});
	
});