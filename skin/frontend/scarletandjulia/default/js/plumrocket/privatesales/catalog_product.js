/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

jQuery.noConflict();

jQuery(document).ready(function(){

	// ---- Hover effect ------
	jQuery('.list-of-products li').hover(
		function(){
			jQuery(this).find('.view-item-over').css('filter', 'alpha(opacity=50)');
			jQuery(this).find('.view-item-over').animate({ "opacity": "show"},  200);
		},
		function(){
			jQuery(this).find('.view-item-over').animate({ "opacity": "hide" },  200);
		}

	)
	
	// ------ Readmore -----------//
	jQuery('#readmore-button').click(function(){
		jQuery('#readmore-container').toggle();
		jQuery('#readmore-before').hide();
		jQuery('#readmore-container')
			.append('<span class="hide_all" style="display: block;"><a href="#">HIDE BIO</a></span>')
			.find('.hide_all').click(function(){
				jQuery('#readmore-container').toggle();
				jQuery('#readmore-button').show();
				jQuery('#readmore-before').show();
				jQuery(this).remove();
				return false;
			});
		return false;
	});
	
	// ---------  FOR COUNT DOWN -------------------
	
	function _getTime(text)
	{
		var time = new Date().getTime();
		return new Date(time + parseInt(text) * 1000);
	}

	jQuery('.cntdown-pr').each(function(){
		var text = jQuery(this).next('.cntdown-source:first').text();
		if (text && parseInt(text) != 0) {
			jQuery(this).countdown({
				until: _getTime(text), 
				format: 'dHM',
				layout: homepage_timer_label,
				labels: homepage_timer_labels_few.split(','),
				// The display texts for the counters if only one
				labels1: homepage_timer_labels_one.split(','),
				onExpiry: function () {
					jQuery('#expired-popup').css('display', 'block');
					jQuery('.add-to-box').remove();
				}
			});
		}
	});
	
	// ---------  Tabs -------------------
	jQuery('#tabs div.tab-container').hide();
	jQuery('#tabs div.tab-container:first').show();
	jQuery('#tabs ul li:first').addClass('active');
	
	jQuery('#tabs ul li a').click(function(){
		jQuery('#tabs ul li').removeClass('active');
		jQuery(this).parent().addClass('active');
		var currentTab = jQuery(this).attr('href');
		jQuery('#tabs div.tab-container').hide();
		jQuery(currentTab).show();
		return false;
	});
	
});