/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

//jQuery.noConflict();
(function(jQuery){
jQuery(document).ready(function(){
	if ((typeof isEnabledSplashPage !== 'undefined') && isEnabledSplashPage) {
		
		function blockForm(id){
			jQuery('#'+id).data('blocked', true);
		}
		
		function unblockForm(id){
			jQuery('#'+id).data('blocked', false);
		}
			
		function isFormBlocked(id){
			return jQuery('#'+id).data('blocked');
		}
		
		
		// bind 'myForm' and provide a simple callback function 
		jQuery('#forgot-form').ajaxForm({
			beforeSubmit : function() {
					
				if (isFormBlocked('forgot-form'))
					return false;
				blockForm('forgot-form');
					
				// hack for IE
				if ( jQuery.browser.msie ) {
					var validator  = new Validation( document.getElementById('forgot-form') );
					if (validator && validator.validate()) {} else {
						return false;
					}
				}
				
				jQuery('.ajax-loader').css('display', 'block');
			},
			success: function (responseText, statusText, xhr, $form)  { 
				if (statusText == 'success') {
					hideAllBlocks();
					var succ_text = jQuery('#forgot_text').html();
					var email = jQuery('#forgot-form #email_address').val();
					jQuery('#forgot_text').html( succ_text.replace('%email%', email) );
					jQuery('.forgot-success').show();
				} else {
					showErrors(responseText);
				}
			},
			error: function()
			{
				unblockForm('forgot-form');
			}
		});
		
		jQuery('#register-form').ajaxForm({
			beforeSubmit : function() {
				
				if (isFormBlocked('register-form'))
					return false;
				blockForm('register-form');
				
				// hack for IE
				if ( jQuery.browser.msie ) {
					var validator  = new Validation( document.getElementById('register-form') );
					if (validator && validator.validate()) {} else {
						return false;
					}
				}
				
				jQuery('.ajax-loader').css('display', 'block');
			},
			success: function (responseText, statusText, xhr, $form)  {
				if (responseText.indexOf('error-msg', 0) == -1) {
					hideAllBlocks();
					jQuery('#for_messages').hide();
					
					if (redirectAfterRegistration) {
						jQuery('.reg-success').show();
                        // redirect en commentaire pour ne pas que la page se recharge après l'inscription
						//window.location = baseUrlForRedirect;
					} else {
						jQuery('#launching_soon').show();
					}
				} else {
					showErrors(responseText);
					unblockForm('register-form');
				}
			}
		});
		
		jQuery('#login-form').ajaxForm({
			beforeSubmit : function() {
				
				if (isFormBlocked('login-form'))
					return false;
				blockForm('login-form');
				
				// hack for IE
				if ( jQuery.browser.msie ) {
					var validator  = new Validation( document.getElementById('login-form') );
					if (validator && validator.validate()) {} else {
						return false;
					}
				}
							
				jQuery('.ajax-loader').css('display', 'block');
			},
			success: function (responseText, statusText, xhr, $form)  {
				if (statusText == 'success') {
					if (responseText == '') {
						window.location = baseUrlForRedirect; //window.location;
					}else {
						showErrors(responseText);
						unblockForm('login-form');
					}
				}
			}
		});
		
		// --- images load
		jQuery("#bg img").css('visibility', 'visible');
		initPage(function() {
			initBgStreach();
		});
		
		// ---- buttons
		jQuery('.show_registration').click(function(){
			hideAllBlocks();
			jQuery('.account-create').css('display', 'block');
			return false;
		});
		
		jQuery(document).delegate('.show_forgot', 'click', function(){
			hideAllBlocks();
			jQuery('.account-forgot').css('display', 'block');
			return false;
		});
		
		jQuery('.backtologin').click(function(){
			hideAllBlocks();
			jQuery('.account-login').css('display', 'block');
			return false;
		});
		
		jQuery('input[name="gender"]').click(function(){
			jQuery('#gender_fix').val(1);
		});

        jQuery(document).delegate('.hide-message', 'click', function(){
			jQuery('#for_messages').css('display', 'none');
		});
		
		jQuery('.close').click(function(){
			jQuery('#overlay-box').hide(500);
		});
		
		jQuery('.show_terms').click(function(){
			jQuery('#overlay-box').show(500);
		});
		
		// Lauching soon
		jQuery(window).resize(reposition);
		reposition();
		
		jQuery('#launching_soon .close').click(function(){
			jQuery('#launching_soon').hide();
		});
	} 
});
})(jQuery161);

function reposition()
{
	jQuery('#launching_soon').css('top', (jQuery(window).height() - jQuery('#launching_soon').outerHeight()) / 2);
	jQuery('#launching_soon').css('left', (jQuery(window).width() - jQuery('#launching_soon').outerWidth()) / 2);
}

function showErrors(text)
{
	//if (text) {
		jQuery('#for_messages').html(text);
	//}
	jQuery('#for_messages').css('display', 'block');
	jQuery('.ajax-loader').css('display', 'none');
	
	//if (text) {
		jQuery('#pop-up-button').clone()
			.removeAttr('id')
			.css('display', 'inline')
			.appendTo('#for_messages ul li ul:first');
	//}
}

function hideAllBlocks()
{
	jQuery('.ajax-loader').css('display', 'none');
	jQuery('.splash-container > div').css('display', 'none');
	jQuery('#for_messages').css('display', 'none').empty();
}


// domReady
function initPage(fn) {
	var scope = this, calledFlag;
	(function(){
		if (document.addEventListener) {
			document.addEventListener('DOMContentLoaded', function(){
				if(!calledFlag) { calledFlag = true; fn.call(scope); }
			}, false)
		}
		if (!document.readyState || document.readyState.indexOf('in') != -1) {
			setTimeout(arguments.callee, 9);
		} else {
			if(!calledFlag) { calledFlag = true; fn.call(scope); }
		}
	}());
}

// initBgStreach
function initBgStreach() {
	var holder = document.getElementById('bg');
	if(holder) {
		var images = holder.getElementsByTagName('img');
		if(images.length) {
			var rnd = Math.round(Math.random() * (images.length - 1));
			
			for(var i = 0; i < images.length; i++) {
				if(i !== rnd && images.length > 1) {
					images[i].style.display = 'none';
				}
				else {
					BackgroundStretcher.stretchImage(images[i]); // add image to stretch collection
				}
			}
			BackgroundStretcher.setBgHolder(holder); // set holder, which will crop content
		}
	}
}

var alreadyLoaded = false;

// image stretch module
BackgroundStretcher = {
	images: [],
	holders: [],
	viewWidth: 0,
	viewHeight: 0,
	stretchByWindow: false, // or entire page
	init: function(){
		this.addHandlers();
		this.resizeAll();
		return this;
	},
	stretchImage: function(obj) {
		var img = new Image();
		img.onload = this.bind(function(){
			obj.iRatio = img.width / img.height;
			obj.iWidth = img.width;
			obj.iHeight = img.height;
			obj.style.msInterpolationMode = 'bicubic'; // IE7 fix
			this.resizeImage(obj);
		});
		img.src = obj.src;
		this.images.push(obj);
	},
	setBgHolder: function(obj) {
		this.holders.push(obj);
		this.resizeAll();
	},
	resizeImage: function(obj) {
		if(obj.iRatio) {
			var slideWidth = this.viewWidth;
			var slideHeight = slideWidth / obj.iRatio;
			if(slideHeight < this.viewHeight) {
				slideHeight = this.viewHeight;
				slideWidth = slideHeight * obj.iRatio;
			}
			obj.style.width = slideWidth+'px';
			obj.style.height = slideHeight+'px';
			obj.style.top = (this.viewHeight - slideHeight)/2+'px';
			obj.style.left = (this.viewWidth - slideWidth)/2+'px';
		}
	},
	resizeHolder: function(obj) {
		obj.style.width = this.viewWidth+'px';
		obj.style.height = this.viewHeight+'px';
	},
	getWindowWidth: function() {
		return typeof window.innerWidth === 'number' ? window.innerWidth : document.documentElement.clientWidth;
	},
	getWindowHeight: function() {
		return typeof window.innerHeight === 'number' ? window.innerHeight : document.documentElement.clientHeight;
	},
	getPageWidth: function() {
		if(!document.body) return 0;
		return Math.max(
			Math.max(document.body.clientWidth, document.documentElement.clientWidth),
			Math.max(document.body.offsetWidth, document.body.scrollWidth)
		);
	},
	getPageHeight: function() {
		if(!document.body) return 0;
		return Math.max(
			Math.max(document.body.clientHeight, document.documentElement.clientHeight),
			Math.max(document.body.offsetHeight, document.body.scrollHeight)
		);
	},
	resizeAll: function() {
		// crop holder width by window size
		for(var i = 0; i < this.holders.length; i++) {
			this.holders[i].style.width = '100%'; 
		}
		
		// delay required for IE to handle resize
		clearTimeout(this.resizeTimer);
		this.resizeTimer = setTimeout(this.bind(function(){
			// hide background holders
			for(var i = 0; i < this.holders.length; i++) {
				this.holders[i].style.display = 'none';
			}
			
			// calculate real page dimensions with hidden background blocks
			this.viewWidth = this[this.stretchByWindow ? 'getWindowWidth' : 'getPageWidth']();
			this.viewHeight = this[this.stretchByWindow ? 'getWindowHeight' : 'getPageHeight']();
			
			// show and resize all background holders
			for(i = 0; i < this.holders.length; i++) {
				if (alreadyLoaded) {
					this.holders[i].style.display = 'block';
				} else {
					jQuery(this.holders[i]).fadeIn('slow');
				}
				this.resizeHolder(this.holders[i]);
				
				alreadyLoaded = true;
			}
			for(i = 0; i < this.images.length; i++) {
				// @Plumrocket
				this.resizeImage(this.images[i]);
			}
		}),10);
	},
	addHandlers: function() {
		if (window.addEventListener) {
			window.addEventListener('resize', this.bind(this.resizeAll), false);
			window.addEventListener('orientationchange', this.bind(this.resizeAll), false);
		} else if (window.attachEvent) {
			window.attachEvent('onresize', this.bind(this.resizeAll));
		}
	},
	bind: function(fn, scope, args) {
		var newScope = scope || this;
		return function() {
			return fn.apply(newScope, args || arguments);
		}
	} 
}.init();