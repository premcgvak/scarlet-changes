/*jslint browser: true, nomen: true*/
/*global $, jQuery, _, resizeORorientation, detectFeatures*/

// Avoid PrototypeJS conflicts, assign jQuery to $j instead of $
var $j = jQuery.noConflict();

(function ($) {

    // we call detectfeatures to add awesome class to <html> (.server-dev .server-qa .server-int ...)
    detectFeatures();
    $(window).on(resizeORorientation, _.throttle(function () {
        "use strict";
        detectFeatures();
    }, 200));

    /* ***********************************************
     * Exécution lorsque le document est prêt
     * ***********************************************/
    $(document).ready(function () {
        "use strict";

        /* ***********************************************
         * Initialisation du menu principal via jQuery.mmenu
         * ***********************************************/
        initJqueryMmenu();
        /* *********************************************** */

        /* ***********************************************
         * Bannière de texte de livraison gratuite dans le header
         * ***********************************************/
        initializeHeaderFreeShippingText();
        setInterval(changeHeaderFreeShippingText, 4000);
        /* *********************************************** */

        /* ***********************************************
         * Fermeture des overlays du menu "Mon compte" et de la boite de recherche
         * ***********************************************/
        $(document).on('click', function (event) {
            if (!$(event.target).closest('#header-account').length) {
                if ($('header.main-header .account-button').hasClass('timed-active')) {
                    $('header.main-header .account-button').removeClass('skip-active').removeClass('timed-active');
                    $('#header-account').removeClass('skip-active').removeClass('timed-active');
                }
            }
            if (!$(event.target).closest('#header-search').length) {
                if ($('header.main-header .search-button').hasClass('timed-active')) {
                    $('header.main-header .search-button').removeClass('skip-active').removeClass('timed-active');
                    $('#header-search').removeClass('skip-active').removeClass('timed-active');
                }
            }
        });
        /* *********************************************** */

        /* ***********************************************
         * Ajustement de la marge de chaque élément du menu principal
         * ***********************************************/
        $('header.main-header .main-menu .ct-menu').fullWidth('a.level0');
        /* *********************************************** */

        /* ***********************************************
         * Ajustement de la hauteur des LI dans la liste des produits (catalogue)
         * ***********************************************/
        setTimeout(heightProductsLiCatalog, 200);
        /* *********************************************** */

        /* ***********************************************
         * Initialisation du plugin pour les placeholders multiligne
         * ***********************************************/
        $('textarea[placeholder]').placeholder();
        /* *********************************************** */

        /* ***********************************************
         * Initialisation des carrousels sur la page d'accueil
         * ***********************************************/
        initHomeCarousels();
        /* *********************************************** */

        /* ***********************************************
         * Initialisation de la création des colonnes dans la page de liste des marques
         * ***********************************************/
        initBrandsListColumn();
        /* *********************************************** */

        /* ***********************************************
         * Exécution des fonctions "sticky"
         * ***********************************************/
        stickProductImageInDetail();
        stickBrandBackgroundImageCatalog();
        stickBrandBackgroundImageInfoPage();
        /* *********************************************** */

        /* ***********************************************
         * Sur un scroll de la page, exécuter ces fonctions
         * ***********************************************/
        $(window).scroll(function() {
            stickProductImageInDetail();
            stickBrandBackgroundImageCatalog();
            stickBrandBackgroundImageInfoPage();
        });
        /* *********************************************** */

        /* ***********************************************
         * Quand on resize la page, on doit exécuter ces fonctions
         * ***********************************************/
        $(window).on(resizeORorientation, _.throttle(function () {
            $('header.main-header .main-menu .ct-menu').fullWidth('a.level0');
            stickProductImageInDetail();
            stickBrandBackgroundImageCatalog();
            stickBrandBackgroundImageInfoPage();
            heightProductsLiCatalog();
        }, 10));
        /* *********************************************** */

    });
    /* *********************************************** */


    /* ***********************************************
     * Activation du plugin jQuery.mmenu pour avoir le menu mobile et tablette
     * ***********************************************/
    function initJqueryMmenu() {
        $('body:not(.customer-account):not(.section-ventes-privees) #header-nav').mmenu({
            // options
        }, {
            // configuration
            clone: true
        });

        $('body.section-ventes-privees #header-nav').mmenu({
            // options
            slidingSubmenus: false,
            header: {
                add: true,
                content: $('body.section-ventes-privees #main-menu > .cat-title').html()
            }
        }, {
            // configuration
            clone: true
        });

        $('body.customer-account #my-account-nav').mmenu({
            // options
            slidingSubmenus: false,
            header: {
                add: true,
                content: $('body.customer-account #my-account-nav').prev().find('span').html()
            }
        }, {
            // configuration
            clone: true
        });
    }

    // Lorsque mmenu est initalisé, on lance la fonction pour ajuster le Big Menu
    var ajustMainMenu = setInterval(ajustBigMenu, 50);

    // Ajouter les classes "mm-fullsubopen" sur les liens "parents" dans le menu mobile
    var ajustMainMenuMobile = setInterval(ajustMenuMobile, 50);

    /* *********************************************** */


    /* ***********************************************
     * Fonctions d'exécutions pour la bannière de texte dans le header sur la livraison gratuite
     * ***********************************************/
    // Initialisation de la bannière
    function initializeHeaderFreeShippingText() {
        var activeElement = $('header.main-header .livraison-gratuite ul li.active');
        activeElement.css({
            top: 0,
            left: 0,
            'opacity': 1
        });
    }

    // Faire afficher le prochain élément de la liste
    function changeHeaderFreeShippingText() {
        var firstElement = $('header.main-header .livraison-gratuite ul li:first-child');
        var activeElement = $('header.main-header .livraison-gratuite ul li.active');
        var nextElement = activeElement.next();
        var do_nextElement_exist = (typeof nextElement[0] !== 'undefined');
        var newElement = firstElement;

        if (do_nextElement_exist === true) {
            newElement = nextElement;
        }
        else {
            newElement = firstElement;
        }

        // Animation
        activeElement.removeClass('active');
        newElement.addClass('active');
        activeElement.animate({opacity: 0}, 750, function () {
            $(this).css({
                top: -9999,
                left: -9999
            });
        });
        newElement.css({
            top: 0,
            left: 0
        });
        newElement.animate({opacity: 1}, 750);
    }

    /* *********************************************** */


    /* ***********************************************
     * Fonction pour ajuster la marge de chaque élément dans un contenant donné
     * ***********************************************/
    $.fn.extend({

        fullWidth: function (elementToResize) {

            var container = $(this),
                elementToResize = elementToResize;

            container.each(function () {

                var newcontainer = $(this),
                    containerWidth = newcontainer.width(),
                    elementToResize2 = newcontainer.find(elementToResize),
                    elementToResizeWidth = 0;

                elementToResize2.each(function () {
                    elementToResizeWidth += $(this).width();
                });

                var widthLeft = containerWidth - elementToResizeWidth,
                    paddingNew = ( ( widthLeft / (elementToResize2.length - 1) ) / 2) - 0.3;

                elementToResize2.css({
                    'margin-left': paddingNew,
                    'margin-right': paddingNew
                });


            });

            container.delay(0, function () {
                $(this).addClass('loaded');
            });

        }

    });
    /* *********************************************** */


    /* ***********************************************
     * Fonction pour ajuster le big menu après que jQuery.mmenu ait été exécuté
     * ***********************************************/
    function ajustBigMenu() {

        var checkIfMMenuIsInitialized = ($('#mm-header-nav').length != 0);

        if (checkIfMMenuIsInitialized === true) {

            // --------------------------------
            // Si jquery.mmenu est initialisé, on arrête la vérification par intervalle
            // --------------------------------
            clearInterval(ajustMainMenu);
            // --------------------------------


            // --------------------------------
            // Ajustement du menu des colonnes
            // --------------------------------
            var mainNav = $('header.main-header .main-menu li.level0');

            mainNav.each(function () {

                // Déclaration de l'objet qui contiendra le contenu des différentes colonnes
                var objectColonnesHTML = ['', '', '', '', ''];


                // Récupération de chacune des "colonnes"
                var childNav = $(this).children('.children');

                childNav.find('li.level1').each(function () {
                    if ($(this).hasClass('colonne-1')) {
                        objectColonnesHTML[0] += $(this).outerHTML();
                    }
                    if ($(this).hasClass('colonne-2')) {
                        objectColonnesHTML[1] += $(this).outerHTML();
                    }
                    if ($(this).hasClass('colonne-3')) {
                        objectColonnesHTML[2] += $(this).outerHTML();
                    }
                    if ($(this).hasClass('colonne-4')) {
                        objectColonnesHTML[3] += $(this).outerHTML();
                    }
                    if ($(this).hasClass('colonne-5')) {
                        objectColonnesHTML[4] += $(this).outerHTML();
                    }
                });


                // Supression du contenant original et construction de la nouvelle arborescence
                childNav.html('');

                var navIndex;
                var childContentFinal = '<div class="level0">';

                for (navIndex = 0; navIndex < objectColonnesHTML.length; navIndex++) {
                    if (objectColonnesHTML[navIndex] != '') {
                        childContentFinal += '<ul class="colonne colonne-' + (navIndex + 1) + '">' + objectColonnesHTML[navIndex] + '</ul>';
                    }
                }

                childContentFinal += '</div>';


                // Affichage du résultat final
                childNav.html(childContentFinal);
            });
            // --------------------------------

            // --------------------------------
            // Lorsque tout est terminé, on ajoute une classe sur le menu principal pour permettre l'affichage des sous-menus
            // --------------------------------
            $('header.main-header .main-menu').addClass('menu-ready');
            // --------------------------------

        }
    }

    /* *********************************************** */


    /* ***********************************************
     * Fonction pour ajuster le menu mobile après que jQuery.mmenu ait été exécuté
     * ***********************************************/
    function ajustMenuMobile() {
        var checkIfMMenuIsInitialized = ($('#mm-header-nav').length != 0);

        if (checkIfMMenuIsInitialized === true) {

            // --------------------------------
            // Si jquery.mmenu est initialisé, on arrête la vérification par intervalle
            // --------------------------------
            clearInterval(ajustMainMenuMobile);
            // --------------------------------


            // --------------------------------
            // Ajout de la classe "mm-fullsubopen" sur les liens "parents" pour qu'un clic sur le texte donne le même résultat que la flèche
            // --------------------------------
            $("#mm-header-nav").find( ".mm-subopen" ).addClass( "mm-fullsubopen" );
            // --------------------------------
        }
    }

    /* *********************************************** */


    /* ***********************************************
     * Dans la fiche de produit, on transforme l'image(s) du produit pour qu'il soit sticky
     * ***********************************************/
    function stickProductImageInDetail() {

        if(!mobileSize && $('.product-view .product-img-box').length == 1) {

            // Récupération des valeurs pour le header, la position de la zone de détail du produit,
            // la hauteur de la zone de l'image du produit et la hauteur de la zone de détail du produit
            var windowTop = $(window).scrollTop();
            var productDetailInfoTop = $('.product-view .product-shop').offset().top;
            var imageBoxHeight = $('.product-view .product-img-box').outerHeight(true);
            var productViewHeight = $('.product-view').outerHeight(true);


            if (windowTop + 20 >= productDetailInfoTop && imageBoxHeight < productViewHeight) {
                $('.product-view .product-img-box').addClass('sticky');

                if (imageBoxHeight + windowTop < productViewHeight + productDetailInfoTop) {
                    $('.product-view .product-img-box').css('top', windowTop - productDetailInfoTop);
                }
                else {
                    $('.product-view .product-img-box').css('top', productViewHeight - imageBoxHeight);
                }
            }
            else {
                $('.product-view .product-img-box').removeClass('sticky');
                $('.product-view .product-img-box').css('top', '');
            }
        }
    }
    /* *********************************************** */


    /* ***********************************************
     * Dans le catalogue d'une marque, on gère l'image de background pour qu'elle soit fixe
     * ***********************************************/
    function stickBrandBackgroundImageCatalog() {

        if($('body.section-marque.catalog-category-view').length == 1) {

            // Récupération des valeurs pour le header et la position de la zone de contenu
            var windowTop = $(window).scrollTop();
            var mainContainerPosition = $('body.section-marque.catalog-category-view .main-container').offset().top;

            $('body.section-marque.catalog-category-view .main-container').css('background-attachment', 'fixed');

            if (windowTop >= mainContainerPosition) {
                $('body.section-marque.catalog-category-view .main-container').css('background-position', 'center top');
            }
            else {
                $('body.section-marque.catalog-category-view .main-container').css('background-position', 'center ' + (mainContainerPosition - windowTop) + 'px');
            }
        }
    }
    /* *********************************************** */


    /* ***********************************************
     * Dans la page d'information d'une marque, on gère l'image de background pour qu'elle soit fixe
     * ***********************************************/
    function stickBrandBackgroundImageInfoPage() {

        if($('body.contentmanager-contenttype-brand').length == 1) {

            // Récupération des valeurs pour le header et la position de la zone de contenu
            var windowTop = $(window).scrollTop();
            var mainContainerPosition = $('body.contentmanager-contenttype-brand .main-container').offset().top;

            $('body.contentmanager-contenttype-brand .main-container').css('background-attachment', 'fixed');

            if (windowTop >= mainContainerPosition) {
                $('body.contentmanager-contenttype-brand .main-container').css('background-position', 'center top');
            }
            else {
                $('body.contentmanager-contenttype-brand .main-container').css('background-position', 'center ' + (mainContainerPosition - windowTop) + 'px');
            }
        }
    }
    /* *********************************************** */


    /* ***********************************************
     * Catalogue de produits - Ajustement de la hauteur des cellules
     * ***********************************************/
    function heightProductsLiCatalog() {
        if(!mobileSize && ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index'))) {
            $('.category-products .products-grid').each(function () {
                var liMaxHeight = 0;

                $(this).find('li .product-desc').each(function () {
                    if ($(this).height() > 0) {
                        $(this).css('height', '');
                    }

                    if (liMaxHeight < $(this).height()) {
                        liMaxHeight = $(this).height();
                    }
                });

                $(this).find('li .product-desc').height(liMaxHeight);

                $(this).css('height', 'inherit');
            });
        }
        else if(mobileSize) {
            $('.category-products .products-grid li .product-desc').each(function () {
                $(this).css('height', '');
            });
        }
    }
    /* *********************************************** */


    /* ***********************************************
     * Page d'accueil - Carrousels de produits
     * ***********************************************/
    function initHomeCarousels() {
        // Carrousel dans la colonne de droite
        if($('.ct-view-home .right .p-container .products-grid').length == 1) {
            $('.ct-view-home .right .p-container').each(function() {
                var prevButton = $(this).find('.p-car-pager-left');
                var nextButton = $(this).find('.p-car-pager-right');

                $(this).find('.products-grid').slick({
                    speed: 300,
                    slide: 'li',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    prevArrow: prevButton,
                    nextArrow: nextButton
                });
            });
        }

        // Carrousel dans la colonne de droite
        if($('.ct-view-home .bottom-content.p-container .products-grid').length == 1) {
            $('.ct-view-home .bottom-content.p-container').each(function() {
                var prevButton = $(this).find('.p-car-pager-left');
                var nextButton = $(this).find('.p-car-pager-right');

                $(this).find('.products-grid').slick({
                    speed: 600,
                    slide: 'li',
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    prevArrow: prevButton,
                    nextArrow: nextButton,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                speed: 600,
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        },
                        {
                            breakpoint: 720,
                            settings: {
                                speed: 300,
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });
            });
        }
    }
    /* *********************************************** */


    /* ***********************************************
     * Initialisation de la création des colonnes dans la page de liste des marques
     * ***********************************************/
    function initBrandsListColumn() {
        "use strict";


        // Variables
        var mainContainer = $('.catalog-category-view').find('.brands-alpha'),
            mainList = mainContainer.find('> li'),
            brandsList = mainContainer.find('.brands li'),
            nbBrands = brandsList.length,
            nbColumns = 4,
            elementsPerColumn = Math.floor(nbBrands / nbColumns),
            elementsIndexToValidate = [],
            lastLetterInColumn = [],
            brandsByColumn = ['','','',''];


        // Récupération de la liste des index des marque à évaluer pour la séparation par colonne
        for(var i=nbColumns; i >= 1; i--) {
            elementsIndexToValidate[i] = nbBrands - (elementsPerColumn * (nbColumns - i));
            //console.log(elementsIndexToValidate[i]);
        }


        // Vérification des marques par lettres à inclure dans une colonne
        brandsList.each(function(index) {
            if(elementsIndexToValidate.indexOf(index + 1) != -1) {
                lastLetterInColumn.push($(this).parent('.brands').siblings('div').html());
            }
        });


        // Récupération du HTML à mettre dans chacune des colonnes
        var presentColumn = 0;
        mainList.each(function() {
            var letter = $(this).find('div').html();

            brandsByColumn[presentColumn] += '<li>' + $(this).html() + '</li>';

            if(lastLetterInColumn.indexOf(letter) != -1) {
                //console.log(letter);
                presentColumn++;
            }
        });


        // Écriture des colonnes
        var printColumns = '<div class="listContainer1">';
        for(var i=1; i <= nbColumns; i++) {
            printColumns += '<ul class="brands-alpha listCol' + i + '">'+ brandsByColumn[i-1] +'</ul>';
        }
        printColumns += '</div>';

        $('.catalog-category-view').find('.brands-alpha').remove();

        $('.catalog-category-view').find('.brands-listing').append(printColumns);

        //console.log(printColumns);


        var is_Columns_Created = setInterval(function() {

            if($('body.catalog-category-view .brands-listing > .listContainer1 .listCol4').length != 0) {
                clearInterval(is_Columns_Created);

                $('body.catalog-category-view .brands-listing > .listContainer1').prepend('<div class="col1"></div><div class="col2"></div>');
                $('.listCol1,.listCol2').prependTo('body.catalog-category-view .brands-listing > .listContainer1 > .col1');
                $('.listCol3,.listCol4').prependTo('body.catalog-category-view .brands-listing > .listContainer1 > .col2');

                // Ajouter une classe sur le Page Title pour enlever le border-bottom
                $('body.catalog-category-view .page-title').addClass('brands-listing-page');
            }

        }, 50);
    }
    /* *********************************************** */
   })($j);
