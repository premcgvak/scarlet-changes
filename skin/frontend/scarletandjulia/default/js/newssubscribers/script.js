var EsNewsSubscribers = {

    cookieLiveTime: 100,

    displayDelay: 15,

    cookieName: 'es_newssubscriber',

    baseUrl: '',

    setCookieLiveTime: function(value)
    {
        this.cookieLiveTime = value;
    },

    setDisplayDelay: function (value)
    {
        this.displayDelay = value;
    },

    setCookieName: function(value)
    {
        this.cookieName = value;
    },

    setBaseUrl: function(url)
    {
        this.baseUrl = url;
    },

    getBaseUrl: function(url)
    {
        return this.baseUrl;
    },

    createCookie: function() {
        var days = this.cookieLiveTime;
        var value = 1;
        var name = this.cookieName;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = escape(name)+"="+escape(value)+expires+"; path=/";
    },

    readCookie: function(name) {
        var name = this.cookieName;
        var nameEQ = escape(name) + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return unescape(c.substring(nameEQ.length,c.length));
        }
        return null;
    },

    boxClose: function()
    {
        jQuery('#esns_background_layer').fadeOut(500, function() {
            jQuery('#esns_background_layer').removeClass('active');
        });
    },

    boxOpen: function()
    {
        setTimeout(function(){
            jQuery('#esns_background_layer').addClass('active').animate({
                opacity: 1
            }, 500);

            jQuery('#esns_box_layer').css('margin-top', ((jQuery(window).outerHeight(false)-jQuery('#esns_box_layer').outerHeight(false)) /2)+'px');

            EsNewsSubscribers.createCookie();
        }, this.displayDelay * 1000);

    }
};

jQuery(function() {
    //jQuery('#esns_background_layer').hide();
    if (EsNewsSubscribers.readCookie() != 1) {
        EsNewsSubscribers.boxOpen();
    }
    jQuery('#esns_submit').click(function(){
        var email = jQuery('#esns_email').val();
        jQuery.post(EsNewsSubscribers.getBaseUrl()+'newsletter/subscriber/newajax/', {'email':email}, function(resp) {
            if (resp.errorMsg) {

                if(jQuery('#esns_box_layer').hasClass('error-msg') === false) {
                    jQuery('#esns_box_layer').addClass('error-msg');
                }

                jQuery('#esns_box_subscribe_response_error').html(resp.errorMsg);
                jQuery('#esns_box_layer').css('margin-top', ((jQuery(window).outerHeight(false)-jQuery('#esns_box_layer').outerHeight(false)) /2)+'px');

            } else {

                jQuery('#esns_box_subscribe_response_error').html('');
                jQuery('#esns_content_form').hide();
                jQuery('#esns_confirm_box').show();
                jQuery('#esns_box_layer').addClass('step-2');

            }
        });
    });
    jQuery('#esns_box_close').click(function(){
        EsNewsSubscribers.boxClose();
    });

    jQuery('.shop-link').click(function(){
        EsNewsSubscribers.boxClose();
    });
});

jQuery.noConflict();