/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

@package	Plumrocket_Popup_Login-v1.3.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

if (typeof popupLoginSettings === 'undefined') {
	popupLoginSettings = {
		baseUrlForRedirect: '/',
		registerUrlForRedirect: '/',
		popupLoginMode: 3,
		showEventTracking: 0,
		conf_req: 0
	}
}

var popupLoginNewNames = false;
pjQueryFix = function (param) {
	if (popupLoginNewNames) {
		if (typeof param == 'string') {
			param = param.replace(/\./g, '.prpop-');
			param = param.replace(/\#/g, '#prpop-');
		}
	} else if (typeof param == 'string') {
		param = param.replace('.ajax-loader-wrapper', '.ajax-loader');
	}
	return pjQuery_1_9(param);
}

pjQuery_1_9(document).ready(function() {
	popupLoginNewNames = pjQuery_1_9('.prpop-pop-up-form').length > 0;

	if (!popupLoginNewNames && pjQuery_1_9('.pop-up-form').length == 0) {
		return ;
	}

	pjQueryFix('.pop-up-form').find('a, input, label, button, span').data('popup', 'yes');
	
	// Switch beetwen modes
	switch (popupLoginSettings.popupLoginMode) {
		case 1:
			showPopupLogin(false);
			break;
		case 2:
			bindPopupLogin(document);
			break;
		case 3:
			bindPopupLogin('a, button');
			break;
		default: // 4
			bindPopupLogin('.show_popup_login');
			break;
	}
	
	
	function blockForm(id){
		pjQueryFix('#'+id).data('blocked', true);
	}
	
	function unblockForm(id){
		pjQueryFix('#'+id).data('blocked', false);
	}
		
	function isFormBlocked(id){
		return pjQueryFix('#'+id).data('blocked');
	}
	
	var forgotSuccessText = '';
	// bind 'myForm' and provide a simple callback function 
	pjQueryFix('#forgot-form').ajaxForm({
		beforeSubmit : function() {
				
			if (isFormBlocked('forgot-form'))
				return false;
			blockForm('forgot-form');
				
			// hack for IE
			var validator  = new Validation( pjQueryFix('#forgot-form').get(0) );
			if (validator && validator.validate()) {} else {
				unblockForm('forgot-form');
				return false;
			}
			
			popupEventTracking('Forgot Password', 'Send request');
			if (screen.width < 641) {
				pjQueryFix('.account-forgot .ajax-loader-wrapper').css('display', 'block');
			} else {
				pjQueryFix('.account-forgot .ajax-loader-wrapper').css('display', 'inline');
			}
		},
		success: function (responseText, statusText, xhr, $form)  {
			//responseText = responseText.replace(/\s+/g, '');
			unblockForm('forgot-form');
			if (statusText == 'success') {
				popupEventTracking('Forgot Password', 'Success');
				
				hideAllBlocks();
				if (!forgotSuccessText) {
					forgotSuccessText = pjQueryFix('#forgot-text').html();
				}
				var email = pjQueryFix('#forgot-form #email-address').val();
				if (forgotSuccessText) {
					pjQueryFix('#forgot-text').html( forgotSuccessText.replace('%email%', email) );
				}
				pjQueryFix('.forgot-success').show();
			} else {
				popupEventTracking('Forgot Password', 'Error: ' + responseText);
				
				showErrors(responseText);
			}
		},
		error: function()
		{
			unblockForm('forgot-form');
		}
	});
	
	pjQueryFix('#register-form').ajaxForm({
		beforeSubmit : function() {
			
			if (isFormBlocked('register-form'))
				return false;
			blockForm('register-form');
			
			// hack for IE
			var validator  = new Validation( pjQueryFix('#register-form').get(0) );
			if (validator && validator.validate()) {} else {
				unblockForm('register-form');
				return false;
			}
			if (screen.width < 641) {
				pjQueryFix('.account-create .ajax-loader-wrapper').css('display', 'block');
			} else {
				pjQueryFix('.account-create .ajax-loader-wrapper').css('display', 'inline');
			}
			
			popupEventTracking('Create an User', 'Send request');
		},
		success: function (responseText, statusText, xhr, $form)  {
			unblockForm('register-form');
			if (responseText.replace(/\s+/g, '').indexOf('error-msg', 0) == -1) {
				popupEventTracking('Create an User', 'Success');
				
				hideAllBlocks();
				pjQueryFix('#for_messages_popup').hide();

				var $successBlock = pjQueryFix('.reg-success');
				if ($successBlock.length > 0) {
					pjQueryFix('.reg-success').show();
					setTimeout(function(){
						popupRedirect(false);
					}, 3000);
				} else {
					popupRedirect(false);
				}
			} else {
				popupEventTracking('Create an User', 'Error: ' + responseText);
				
				showErrors(responseText);
			}
		},
		error: function()
		{
			unblockForm('register-form');
		}
	});
	
	pjQueryFix('#login-form').ajaxForm({
		beforeSubmit : function() {
			
			if (isFormBlocked('login-form'))
				return false;
			blockForm('login-form');
			
			// hack for IE
			var validator  = new Validation( pjQueryFix('#login-form').get(0) );
			if (validator && validator.validate()) {} else {
				unblockForm('login-form');
				return false;
			}
			if (screen.width < 641) {
				pjQueryFix('.account-login .ajax-loader-wrapper').css('display', 'block');
			} else {
				pjQueryFix('.account-login .ajax-loader-wrapper').css('display', 'inline');
			}
			
			popupEventTracking('Login', 'Send request');
		},
		success: function (responseText, statusText, xhr, $form)  {
			//responseText = responseText.replace(/\s+/g, '');
			unblockForm('login-form');
			if (statusText == 'success') {
				if (responseText == '') {
					popupEventTracking('Login', 'Success');
					popupRedirect(true);
				} else {
					popupEventTracking('Login', 'Error: ' + responseText);
					
					showErrors(responseText);
				}
			}
		},
		error: function()
		{
			unblockForm('login-form');
		}
	});
	
	// ---- buttons
	pjQueryFix('.show_registration').click(function(){
		popupEventTracking('Create an User', 'Show form');
		
		hideAllBlocks();
		pjQueryFix('.account-create').css('display', 'block');
		popupTextsizer();
		return false;
	});
	
	pjQueryFix(document.body).on('click', '.show_forgot,.prpop-show_forgot' ,function(){
		popupEventTracking('Forgot Password', 'Show form');
		
		hideAllBlocks();
		pjQueryFix('.account-forgot').css('display', 'block');
		popupTextsizer();
		return false;
	});
	
	pjQueryFix('.backtologin').click(function(){
		hideAllBlocks();
		pjQueryFix('.account-login').css('display', 'block');
		popupTextsizer();
		return false;
	});
	
	pjQueryFix('input[name="gender"]').click(function(){
		pjQueryFix('#gender_fix').val(1);
	});
	
	pjQueryFix(document.body).on('click', '.hide-message' ,function(){
		pjQueryFix('#for_messages_popup').css('display', 'none');
	});
	
	pjQueryFix('.close').click(function(){
		pjQueryFix('#overlay-box').hide(500);
		pjQueryFix('body, html').removeAttr('style');
		return false;
	});
	
	pjQueryFix('.show_terms,.show-terms').click(function(){
		pjQueryFix('body, html').css({'overflow':'hidden'});
		popupTextsizer();
		pjQueryFix('#overlay-box').show(500);
		pjQueryFix(document).scrollTop(0);
	});

	pjQuery_1_9('#psloginShowBtn').click(function() {
		setTimeout(popupTextsizer, 10);
	});
	
	pjQueryFix(window).resize(popupTextsizer);
	popupTextsizer();
});

function popupRedirect(loginForm)
{
	if ('jsFunction' in popupLoginSettings) {
		pjQueryFix('#addedoverlay').hide();
		pjQueryFix('.pop-up-form').hide(500);
		pjQueryFix('body, html').removeAttr('style');
		
		popupLoginSettings.jsFunction.click();
	} else {
		// support old phtml code.
		if (loginForm || !('registerUrlForRedirect' in popupLoginSettings)) {
			var redirectTo = popupLoginSettings.baseUrlForRedirect;
		} else {
			var redirectTo = popupLoginSettings.registerUrlForRedirect;
		}
		// if null to refresh page
		if (redirectTo) {
			window.location = redirectTo;
		} else {
			if (popupLoginSettings.conf_req == 0) {
				window.location = window.location.href;
			}
		}
	}
	
	popupLoginSettings.disablePopup = true;
}

function popupTextsizer()
{
	// Terms of Service
	var logopicksize = pjQueryFix('#popup_logo_img').outerHeight();
	var wsize =  pjQueryFix(window).height() - (logopicksize + 120);
	pjQueryFix('.pop_up_text_hld').css({'height': wsize + 'px'});

	// Popup Block
	if (/*(screen.width < 641) && */(pjQueryFix('.pop-up-form').is(':visible'))) {
		var wh = pjQueryFix(window).height();
		var ww = pjQueryFix(window).width();

		var $splash_container = pjQueryFix('.splash-container');
		if ($splash_container.length == 0) {
			var $splash_container = pjQueryFix('.pop-up-bg');
		}
		coef = 0.1;
		if (pjQueryFix(window).width() < 480) {
			coef = 0.05;
		}
		var ph = $splash_container.height() + (ww * coef);
		//console.log($splash_container.height(), '+ ', coef, ' * ', ww, '=', ph);

		if (ph > wh) {
			pjQueryFix('body').css('overflow', 'hidden');
			pjQueryFix('.pop-up-form').css('overflow-y', 'scroll');
		} else {
			pjQueryFix('body').removeAttr('style');
			pjQueryFix('.pop-up-form').removeAttr('style').css('display', 'block');
		}
	}
}

function popupEventTracking(action, label)
{
	if (popupLoginSettings.showEventTracking && typeof _gaq !== 'undefined' && _gaq !== false) {
		_gaq.push(['_trackEvent', 'Popup Login', action, label.replace(/(<([^>]+)>)/ig, '')]);
	}
}

function bindPopupLogin(target)
{
	pjQueryFix(target).bind('touchstart', showPopupLogin);
	pjQueryFix(target).click(showPopupLogin);
	pjQueryFix(target).mousedown(showPopupLogin);
	pjQueryFix(target).bind("contextmenu", function(e) {
		return false;
	});
}

function showPopupLogin(event)
{
	if (('disablePopup' in popupLoginSettings) && (popupLoginSettings.disablePopup)) {
		return true;
	}
	
	$target = false;
	if ((typeof event === 'undefined') || (event === false)) {
		// manually or page load
	} else if (('target' in event) && (event.target)) {
		// jquery object
		$target = pjQueryFix(event.target);
	} else {
		// this
		$target = pjQueryFix(event);
	}
	
	// Ignore popup elements
	if ($target && $target.data('popup') == 'yes') {
		return true;
	}
	
	pjQueryFix('#addedoverlay').show(0, function() {
		pjQueryFix('.pop-up-form').show();
	});
	
	if ($target) {
		// click on <a>
		if (popupLoginSettings.popupLoginMode == 3) {
			var href = $target.attr('href');

			if ((typeof href !== 'undefined') && (href !== false) && href) {
				popupLoginSettings.baseUrlForRedirect = href.replace('#', '');
			}
			
			if ($target.get(0).getAttribute('onclick')) {
				popupLoginSettings.jsFunction = $target;
			} else {
				$target.parents().each(function() {
					if (pjQueryFix(this).get(0).getAttribute('onclick')) {
						popupLoginSettings.jsFunction = $target;
					}
				});
			}
		}
		if ((typeof event !== 'undefined') || (event !== false)) {
			event.stopPropagation();
		}
	}
	
	return false;
}

function closePopupLogin()
{
	pjQueryFix('#addedoverlay').hide();
	pjQueryFix('.pop-up-form').hide();
	popupLoginSettings.baseUrlForRedirect = window.location.toString().replace('#', '');
	return false;
}

function showErrors(text)
{
	if (!text) {
		return;
	}
	pjQueryFix('#for_messages_popup').html(text);
	pjQueryFix('#for_messages_popup').css('display', 'block');
	pjQueryFix('.ajax-loader-wrapper').css('display', 'none');
	
	pjQueryFix('#pop-up-button').clone()
		.removeAttr('id')
		.css('display', 'inline')
		.appendTo( pjQueryFix('#for_messages_popup ul li ul:first') );
}

function hideAllBlocks()
{
	pjQueryFix('.ajax-loader-wrapper').css('display', 'none');
	pjQueryFix('.splash-container > div').css('display', 'none');
	pjQueryFix('#for_messages_popup').css('display', 'none').empty();
}