/*

Plumrocket Inc.

NOTICE OF LICENSE

This source file is subject to the End-user License Agreement
that is available through the world-wide-web at this URL:
http://wiki.plumrocket.net/wiki/EULA
If you are unable to obtain it through the world-wide-web, please
send an email to support@plumrocket.com so we can send you a copy immediately.

DISCLAIMER

Do not edit or add to this file

@package	Plumrocket_Private_Sales-v2.2.x
@copyright	Copyright (c) 2013 Plumrocket Inc. (http://www.plumrocket.com)
@license	http://wiki.plumrocket.net/wiki/EULA  End-user License Agreement
 
*/

(function($) {

	$.fly = function (parent, options)
	{ 
		box = $(options.source);
		box.css('position', 'absolute');
		
		// rigth side
		if ((parent.offset().left + parent.outerWidth() + box.outerWidth()) < $(window).width()) {
			var left = parent.offset().left + parent.outerWidth();
		// left side
		} else {
			var left = parent.offset().left - box.outerWidth();
		}
		var _top = parent.offset().top - (box.outerHeight() - parent.outerHeight()) / 2;
		
		var offsetFix = 0;
		// fix for command panel into admin panel of Magento
		if ($('.content-header-floating').css('display') == 'block') {
			offsetFix = $('.content-header-floating').outerHeight();
		}
		
		// move to bottom
		if (_top < ($(window).scrollTop() + offsetFix)) {
			_top = $(window).scrollTop() + offsetFix;
		}
		
		// move to top
		var bottomBorder = $(window).scrollTop() + $(window).height();
		if ((_top + box.outerHeight()) > bottomBorder) {
			_top = bottomBorder - box.outerHeight();
		}
		
		box.css('left', left);
		box.css('top', _top);
		box.css('display', 'block');
		
		/*
		$(window).resize(function() {
			parent.click();
		});
		*/
	}

	$.fn.fly = function (options)
	{
		options = options || {};
		options.source = options.source || '';
		
		$(this.selector).live('click', function() {
			new $.fly($(this), options);
		});
		
		$(document).click(function(ev) {
			// How it works? I do not know. Nothing not know it. It is terrible. It is dirty code. It is incorrect code. But it is works. What a fuck.
			if ($(ev.target).parents().index($(options.source)) == -1 
				&& $(ev.target).hasClass('banner') == false 
				&& ! $(ev.target).hasClass('remove_banner')
				&& ! $(ev.target).hasClass('ui-icon')
				&& $(ev.target).parents().index($('.ui-datepicker')) == -1
			) {
				$(options.source).css('display', 'none');
			}        
		})

		return this;
	};
})( jQuery );
