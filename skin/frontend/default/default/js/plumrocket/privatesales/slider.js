var sliding_help_block_visibility = false;

jQuery(document).ready(function() {
	// ---------  FOR RIGHT SLIDER ---------------
	jQuery("#sliding-help-block").scrollFollow({
		speed: 1000,
		offset: 60
	});

	jQuery("#button-help-sld" ).click(function(){
		if  (sliding_help_block_visibility) {
			jQuery("#option-holder").fadeOut("slow"); 
		} else {
			jQuery("#option-holder").fadeIn("slow");
		}
		sliding_help_block_visibility = !sliding_help_block_visibility;
	});

	jQuery( ".option-holder-close-btn" ).click(function(){
		jQuery("#option-holder").fadeOut("slow");   
		sliding_help_block_visibility = false;
	});
});